;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP RFC BINDINGS (libsapnwrfc)
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.LIBSAPNWRFC")

;;; ---------------------------------------------------------------------------


;;; SAP RFC ERRORS

(defconstant +TB-CONDITION-LEVEL-ERROR-RFC-MISC-FAULURE+                    tb.core:+TB-MSG-LEVEL-ERROR+)
(defconstant +TB-CONDITION-MESSAGE-NR-ERROR-RFC-MISC-FAILURE+               "TGWS101E")
(defconstant +TB-CONDITION-REASON-CODE-ERROR-RFC-MISC-FAILURE+              "00000001")
(defconstant +TB-CONDITION-REASON-TEXT-ERROR-RFC-MISC-FAILURE+              "An unspecified SAP RFC error occured.")

(defconstant +TB-CONDITION-LEVEL-ERROR-RFC-COMMUNICATION-FAULURE+           tb.core:+TB-MSG-LEVEL-ERROR+)
(defconstant +TB-CONDITION-MESSAGE-NR-ERROR-RFC-COMMUNICATION-FAILURE+      "TGWS101E")
(defconstant +TB-CONDITION-REASON-CODE-ERROR-RFC-COMMUNICATION-FAILURE+     "00000002")
(defconstant +TB-CONDITION-REASON-TEXT-ERROR-RFC-COMMUNICATION-FAILURE+     "SAP RFC communication failure between the ThingBone Gateway and SAP occured.")

(defconstant +TB-CONDITION-LEVEL-CRITICAL-RFC-INVALID-PARAMETER+               tb.core:+TB-MSG-LEVEL-CRITICAL+)
(defconstant +TB-CONDITION-MESSAGE-NR-CRITICAL-RFC-INVALID-PARAMETER+          "TGWS101E")
(defconstant +TB-CONDITION-REASON-CODE-CRITICAL-RFC-INVALID-PARAMETER+         "00000003")
(defconstant +TB-CONDITION-REASON-TEXT-CRITICAL-RFC-INVALID-PARAMETER+         "Invalid parameter or invalid number of parameters passed to an SAP RFC function module. Most likely, this is a bug in the ThingBone SAP Gateway module.")

(defconstant +TB-CONDITION-LEVEL-ERROR-RFC-CONVERSION-FAILURE+              tb.core:+TB-MSG-LEVEL-ERROR+)
(defconstant +TB-CONDITION-MESSAGE-NR-ERROR-RFC-CONVERSION-FAILURE+         "TGWS101E")
(defconstant +TB-CONDITION-REASON-CODE-ERROR-RFC-CONVERSION-FAILURE+         "00000004")
(defconstant +TB-CONDITION-REASON-TEXT-ERROR-RFC-CONVERSION-FAILURE+        "A value could not be converted to the required format to be passed to a SAP function module.")


;;; CONDITION DEFINITIONS

(defclass rfc-condition (tb.core:tb-base)
  ())

(define-condition rfc-error (rfc-condition tb.core:tb-error)
  ()
  (:report (lambda (c s)
	     (format s "ThingBone Condition: ~A"
		     (tb.core:message c)))))

(define-condition rfc-critical (rfc-condition tb.core:tb-critical)
  ()
  (:report (lambda (c s)
	     (format s "ThingBone Condition: ~A"
		     (tb.core:message c)))))

(define-condition rfc-misc-failure (rfc-error)
  ()
  (:default-initargs
   :reason-code +TB-CONDITION-REASON-CODE-ERROR-RFC-MISC-FAILURE+
    :reason-text +TB-CONDITION-REASON-TEXT-ERROR-RFC-MISC-FAILURE+
    :message-nr +TB-CONDITION-MESSAGE-NR-ERROR-RFC-MISC-FAILURE+))

(define-condition rfc-communication-failure (rfc-error)
  ()
  (:default-initargs
   :reason-code +TB-CONDITION-REASON-CODE-ERROR-RFC-COMMUNICATION-FAILURE+
    :reason-text +TB-CONDITION-REASON-TEXT-ERROR-RFC-COMMUNICATION-FAILURE+
    :message-nr +TB-CONDITION-MESSAGE-NR-ERROR-RFC-COMMUNICATION-FAILURE+
    ))

(define-condition rfc-invalid-parameter (rfc-critical)
  ((parameter-name  :accessor :parameter-name  :initarg :parameter-name  :initform nil)
   (parameter-value :accessor :parameter-value :initarg :parameter-value :initform nil))
  (:default-initargs
   :reason-code +TB-CONDITION-REASON-CODE-CRITICAL-RFC-INVALID-PARAMETER+
    :reason-text +TB-CONDITION-REASON-TEXT-CRITICAL-RFC-INVALID-PARAMETER+
    :message-nr +TB-CONDITION-MESSAGE-NR-CRITICAL-RFC-INVALID-PARAMETER+
    )
  (:report (lambda (c s)
	     (format s "ThingBone Condition: ~A, Parameter Name: ~S, Parameter Value: ~S."
		     (tb.core:message c)
		     (slot-value c 'parameter-name)
		     (slot-value c 'parameter-value)))))
