;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP RFC BINDINGS (libsapnwrfc)
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.LIBSAPNWRFC")

;;; ---------------------------------------------------------------------------

(defparameter *lock-rfc-condition-log-channel*   (bt:make-recursive-lock))
(defparameter *lock-rfc-condition-log-channel-dispatcher*   (bt:make-recursive-lock))

(defconstant +rfc-condition-log-channel-name+            "RFC CONDITION LOG")
(defconstant +rfc-condition-log-channel-dispatcher-name+ "RFC CONDITION LOG CHANNEL")

(defvar *default-rfc-condition-log-channel-max-size* 1024
  "The default maximum size for a ThingBone RFC Condition Log channel.")

(defvar *rfc-condition-log-channel-max-size* *default-rfc-condition-log-channel-max-size*
  "The maximum size for for the ThingBone RFC Condition Log channel.")

;;; - RFC ERROR MSG CHANNEL -

(defun rfc-condition-log-channel ()
  (bt:with-recursive-lock-held (*lock-rfc-condition-log-channel*)
    (tb.core:global-context-value :rfc-condition-log-channel)))

(defun (setf rfc-condition-log-channel) (channel)
  (bt:with-recursive-lock-held (*lock-rfc-condition-log-channel*)
    (setf (tb.core:global-context-value :rfc-condition-log-channel) channel)))

(defun init-rfc-condition-log-channel (&key (channel nil) (max-size *rfc-condition-log-channel-max-size*) (name +rfc-condition-log-channel-name+))
  (setf (rfc-condition-log-channel) (or channel
					(thingbone.core:make-channel :max-size max-size :name name))))

(defun ensure-rfc-condition-log-channel-initialized ()
  (if (not (rfc-condition-log-channel))
      (init-rfc-condition-log-channel)))

(defun rfc-condition-log-channel-dispatcher ()
  (bt:with-recursive-lock-held (*lock-rfc-condition-log-channel-dispatcher*)
    (tb.core:global-context-value :rfc-condition-log-channel-dispatcher)))

(defun (setf rfc-condition-log-channel-dispatcher) (dispatcher)
  (bt:with-recursive-lock-held (*lock-rfc-condition-log-channel-dispatcher*)
    (setf (tb.core:global-context-value :rfc-condition-log-channel-dispatcher) dispatcher)))

(defun stop-rfc-condition-log-channel-dispatcher ()
  (let ((dispatcher (rfc-condition-log-channel-dispatcher)))
    (if dispatcher
	(tb.core:stop-channel-dispatcher dispatcher))
    (setf (rfc-condition-log-channel-dispatcher) nil)))

(defun start-rfc-condition-log-channel-dispatcher ()
  (ensure-rfc-condition-log-channel-initialized)
  (let ((dispatcher (make-instance 'tb.msgs:msg-dispatcher
				   :name +rfc-condition-log-channel-dispatcher-name+
				   :channel (rfc-condition-log-channel))))
    (setf (rfc-condition-log-channel-dispatcher) dispatcher)
    (tb.core:run-in-thread dispatcher)))

(defun reset-rfc-condition-log-channel ()
  (stop-rfc-condition-log-channel-dispatcher)
  (init-rfc-condition-log-channel)
  (start-rfc-condition-log-channel-dispatcher))
