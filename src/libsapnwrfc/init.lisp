;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP RFC BINDINGS (libsapnwrfc)
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.LIBSAPNWRFC")

;;; ---------------------------------------------------------------------------

(let ((libsapnwrfc-initialized nil))

  (defparameter lock-libsapnwrfc-initialized (bt:make-recursive-lock))

  (defun libsapnwrfc-init ()

    (bt:with-recursive-lock-held (lock-libsapnwrfc-initialized)

      ;; Load SAP RFC Libraries
      (rfc-init)

      (init-rfc-condition-log-channel)

      (start-rfc-condition-log-channel-dispatcher)

      ;; Init completed
      (setq libsapnwrfc-initialized t)
      (log:info "THINGBONE.LIBSAPNWRFC initialized.")
      (values)))

  (defun libsapnwrfc-reset ()
    (bt:with-recursive-lock-held (lock-libsapnwrfc-initialized)
      (rfc-reset nil) ;; Note: this might leak memory in the SAP Netweaver RFC library ...
      (stop-rfc-condition-log-channel-dispatcher)

      ;; Reset completed
      (setq libsapnwrfc-initialized nil)
      (log:info "THINGBONE.LIBSAPNWRFC reset.")))

  (defun libsapnwrfc-initialized-p ()
    (bt:with-recursive-lock-held (lock-libsapnwrfc-initialized)
      libsapnwrfc-initialized))
  )

(defun ensure-libsapnwrfc-initialized ()
  (if (not (libsapnwrfc-initialized-p))
      (libsapnwrfc-init))
  (libsapnwrfc-initialized-p))
