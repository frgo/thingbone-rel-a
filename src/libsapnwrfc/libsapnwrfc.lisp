;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP RFC BINDINGS (libsapnwrfc)
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.LIBSAPNWRFC")

;;; ---------------------------------------------------------------------------

(defparameter *sapnwrfc-foreign-libdir*
  #p "/var/data/swdev/thingbone/extlibs/nwrfc750P_3-70002752/nwrfcsdk/lib/")

(defparameter *loaded-libs* nil)

(defun lib-dir ()
  *sapnwrfc-foreign-libdir*)

(defparameter *sapnwrfc-foreign-libs*
  #+linux
  '("libicudecnumber.so"
    "libicudata.so.50"
    "libicuuc.so.50"
    "libicui18n.so.50"
    "libsapucum.so"
    "libsapnwrfc.so")
  #-linux
  '())

(defun sapnwrfc-foreign-libs (&optional (lib-list *sapnwrfc-foreign-libs*))
  lib-list)

;; (cffi:define-foreign-library sapnwrfc
;;   (t (:default "libsapnwrfc")))
;; (cffi:use-foreign-library sapnwrfc)

(defun load-sapnwrfc-libs (&optional (lib-list *sapnwrfc-foreign-libs*))
  (pushnew (lib-dir) cffi:*foreign-library-directories* :test #'equal)
  (loop for lib in lib-list
     do
       (let ((loaded-lib (cffi:load-foreign-library lib)))
	 (when loaded-lib
	   (pushnew loaded-lib *loaded-libs*)))))

(defun unload-sapnwrfc-libs ()
  (loop for lib in *loaded-libs*
     do
       (progn
	 (ignore-errors
	   (cffi:close-foreign-library lib))
	 (setq *loaded-libs* (remove lib *loaded-libs*)))))
