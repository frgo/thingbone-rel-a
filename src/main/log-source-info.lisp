;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  MAIN
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MAIN")

(defclass log-location ()
  ())

(defclass file-filename-log-location (log-location)
  ((hostname :accessor hostname :initarg :hostname :initform "")
   (filename :accessor filename :initarg :filename :initform "")))

(defmethod jonathan:%to-json ((file-filename-log-location file-filename-log-location))
  (jonathan:write-key "file-location")
  (jonathan:write-value
   (jonathan:with-object
     (jonathan:write-key-value "hostname" (hostname file-filename-log-location))
     (jonathan:write-key-value "filename" (filename file-filename-log-location)))))

(defclass file-directory-log-location (log-location)
  ((hostname :accessor hostname :initarg :hostname :initform "")
   (dir :accessor dir :initarg :dir :initform "")
   (filename-pattern :accessor filename-pattern :initarg :filename-pattern :initform "")))

(defmethod jonathan:%to-json ((file-directory-log-location file-directory-log-location))
  (jonathan:write-key "file-location")
  (jonathan:write-value
   (jonathan:with-object
     (jonathan:write-key-value "hostname" (hostname file-directory-log-location))
     (jonathan:write-key-value "directory" (dir file-directory-log-location))
     (jonathan:write-key-value "filename-pattern" (filename-pattern file-directory-log-location)))))

(defclass log-source-info ()
  ((environment :accessor environment :initarg :environment :initform (tb.core:tb-site))
   (source-class :accessor source-class :initarg :source-class :initform nil)
   (generated-by :accessor generated-by :initarg :generated-by :initform nil)
   (source-format :accessor source-format :initarg :source-format :initform nil)
   (location :accessor location :initarg :location :initform nil)))

(defmethod jonathan:%to-json ((log-source-info log-source-info))
  (jonathan:with-object
    (jonathan:write-key "source-info")
    (jonathan:write-value
     (jonathan:with-object
       (jonathan:write-key-value "environment" (environment log-source-info))
       (jonathan:write-key-value "hostname" (excl.osi:getenv tb.core:+TB-GW-ENV-VAR-CONTAINER-HOST+))
       (jonathan:write-key-value "tbgw-pid" (excl.osi:getpid))
       (jonathan:write-key-value "source-class" (source-class log-source-info))
       (jonathan:write-key-value "generated-by-application" (generated-by log-source-info))
       (jonathan:write-key-value "source-format" (source-format log-source-info))
       (jonathan:%to-json (location log-source-info))))))

(defclass sap-r3-rfc-tracefile-log-source-info (log-source-info)
  ()
  (:default-initargs
   :source-class "file"
    :generated-by "SAP R/3"
    :source-format "sap-r3-trace-file"))

(defun make-sap-r3-rfc-tracefile-log-source-info (hostname directory filename-pattern)
  (let* ((file-location (make-instance 'file-directory-log-location
				       :hostname hostname
				       :dir directory
				       :filename-pattern filename-pattern))
	 (log-source-info (make-instance 'sap-r3-rfc-tracefile-log-source-info
					 :location file-location)))
    log-source-info))

(defclass tbgw-logfile-log-source-info (log-source-info)
  ()
  (:default-initargs
   :source-class "file"
    :generated-by "ThingBone Gateway"
    :source-format "thingbone-gateway-logfile"))

(defun make-tbgw-logfile-log-source-info (hostname filename)
  (let* ((file-location (make-instance 'file-filename-log-location
				       :hostname hostname
				       :filename filename))
	 (log-source-info (make-instance 'tbgw-logfile-log-source-info
					 :location file-location)))
    log-source-info))

(defclass log-source-info-msg (tb.msgs:extended-msg)
  ())

(defun make-log-source-info-msg (log-source-info-list)
  (let* ((data (tb.msgs:make-msg-data :log-source-info log-source-info-list))
 	 (content (tb.msgs:make-msg-extended-content "log-source-info" "update" data nil nil))
	 (sender (tb.aserve::make-msg-sender-for-this-app-instance))
	 (log-source-info-msg (make-instance 'log-source-info-msg
					     :performative "inform"
					     :sender sender
					     :protocol (tb.msgs:extended-msg-protocol)
					     :protocol-version (tb.msgs:extended-msg-protocol-version)
					     :encoding (tb.msgs:extended-msg-encoding)
					     :conversation-id (format nil "~A" (tb.core:make-uuid))
					     :content content)))
    log-source-info-msg))

(defun make-default-log-source-info-list ()
  (let ((tbgw-log-source-info  (make-tbgw-logfile-log-source-info "localhost"
								  (format nil "tbgw-~A-~A.log"
									  (tb.core:tb-site)
									  (excl.osi:getpid))))
	(rfc-trace-log-source-info (make-sap-r3-rfc-tracefile-log-source-info
				    "localhost"
				    (excl.osi:getenv tb.core:+TB-GW-ENV-VAR-LOG-DIR+)
				    "rfc?????-*.trc"
				    )))
    (list tbgw-log-source-info rfc-trace-log-source-info)))

(defun make-default-log-source-info-msg ()
  (make-log-source-info-msg (make-default-log-source-info-list)))


(defun post-log-source-info-to-simple-console (&key (log-source-info-msg (make-default-log-source-info-msg)))
  (log:debug "Posting log source info msg to simple console: ~A" log-source-info-msg)
  (ignore-errors
    (net.aserve.client:do-http-request (format nil "http://~A:~A~A"
  					       (or  (excl.osi:getenv tb.core:+TB-GW-ENV-VAR-SIMPLE-CONSOLE-HOST+)
  						    tb.core:+TB-GW-DEFAULT-SIMPLE-CONSOLE-HOST+)
  					       (or  (excl.osi:getenv tb.core:+TB-GW-ENV-VAR-SIMPLE-CONSOLE-PORT+)
  						    tb.core:+TB-GW-DEFAULT-SIMPLE-CONSOLE-PORT+)
  					       tb.core:+TB-GW-SIMPLE-CONSOLE-API-URL+)
      :method :post
      :content-type "application/json"
      :content (jonathan:to-json log-source-info-msg)
      :timeout 5))
  )

(defclass log-source-info-announcer (tb.msgs:msg-dispatcher)
  ()
  (:default-initargs :name "LOG-SOURCE-INFO-ANNOUNCER"))

(defmethod announce-log-source-info-to-simple-console ((log-source-info-announcer log-source-info-announcer))
  (let ((name (tb.core:name log-source-info-announcer))
	(loop-counter 0)
	(log-source-info-msg (make-default-log-source-info-msg)))
    (tb.msgs:set-keep-listening log-source-info-announcer)
    (tb.core:set-run-status     log-source-info-announcer :RUNNING)
    (do ((keep-listening (tb.msgs:keep-listening-p log-source-info-announcer) (tb.msgs:keep-listening-p log-source-info-announcer)))
	((not (eql keep-listening t))
	 nil)
      (progn
	(bt:thread-yield)
	(if (= loop-counter 0)
	    (post-log-source-info-to-simple-console :log-source-info-msg log-source-info-msg))
	(if (= loop-counter 60)
	    (setf loop-counter 0)
	    (incf loop-counter))
	(sleep 1)))
    (tb.core:set-run-status log-source-info-announcer :HALTED)
    (log:debug "~A: HALTED (exited main loop)." name)
    (values)))

(defmethod start ((log-source-info-announcer log-source-info-announcer))
  (announce-log-source-info-to-simple-console log-source-info-announcer))

(defmethod run-in-thread ((log-source-info-announcer log-source-info-announcer))
  (let* ((thread-name (format nil "~A" (tb.core:name log-source-info-announcer)))
	 (thread (bt:make-thread (lambda ()
				   (tb.main::start log-source-info-announcer)) :name thread-name)))
    (tb.core:set-thread log-source-info-announcer thread)))

(defun make-log-source-info-announcer ()
  (make-instance 'log-source-info-announcer))

(defparameter *log-source-info-announcer* nil)

(defun stop-log-source-info-announcer ()
  (when *log-source-info-announcer*
    (tb.core:stop *log-source-info-announcer*)
    (setf *log-source-info-announcer* nil)))

(defun start-log-source-info-announcer ()
  (let ((task (make-log-source-info-announcer)))
    (setf *log-source-info-announcer* task)
    (tb.main::run-in-thread task)))
