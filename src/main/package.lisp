;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  MAIN
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(defpackage "THINGBONE.MAIN"
  (:use #:cl #:cl-strings)
  (:nicknames "TB.MAIN")
  (:export

   #:*sap-servb-rfc-client-connection-parameters*
   #:*sap-servb-rfc-server-connection-parameters*
   #:*execution-duration*
   #:tb-site
   #:log-filename
   #:config-filename
   #:load-config-file
   #:+TB-GW-ENV-VAR-CONFIG-FILE-PATHNAME+
   #:+TB-GW-DEFAULT-CONFIG-FILE-PATHNAME+
   #:start-thingbone-gateway
   #:stop-thingbone-gateway
   #:main
   ))

;;; ---------------------------------------------------------------------------
