;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  MAIN
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MAIN")

;;; ---------------------------------------------------------------------------

(defparameter *tb-exit-on-error* nil)
(defparameter *swank-port* 4010)

(defparameter *execution-duration* 31536000) ;; 1 year

(defconstant +TB-GW-ENV-VAR-CONFIG-FILE-PATHNAME+ "THINGBONE_GATEWAY_CONFIGFILENAME")
(defconstant +TB-GW-DEFAULT-CONFIG-FILE-PATHNAME+ "/etc/opt/thingbone/gateway/tbgw.config")

(defun config-filename (&optional filename)
  (or filename
      (excl.osi:getenv +TB-GW-ENV-VAR-CONFIG-FILE-PATHNAME+)
      +TB-GW-DEFAULT-CONFIG-FILE-PATHNAME+))

(defun load-stu-test ()
  (log:info "Loading STU test connection parameters (KW1).")
  (format *debug-io* "Loading STU test connection parameters (KW1).")
  (tb.gw.sap-servb::set-stu-kw1-luederh-sap-rfc-connection-params)

  ;; ;; - DEFINE GATEWAY
  ;; (tb.gw.sap-servb:make-and-register-sapr3-gateway :site (tb.core:tb-site) :servb-domain :servb-inbound
  ;; 						   :client-connection-params (tb.gw.sap-servb:sap-servb-client-rfc-connection-params)
  ;; 						   :server-gw-connection-params  (tb.gw.sap-servb:sap-servb-server-rfc-connection-params))

  ;; ;; - DEFINE FUNCTION MODULES AND DATA COMTAINERS

  ;; ;; + Z_MAT_SALES_ORDER_REQUEST

  ;; (log:debug "Defining function module Z_MAT_SALES_ORDER_REQUEST.")
  ;; (tb.gw.sap-servb:define-function-module "Z_MAT_SALES_ORDER_REQUEST" :site-list ("STU" "HAM"))

  ;; ;; + IMPORTING DATA

  ;; (log:debug "Defining data container IS_HEADER for FM Z_MAT_SALES_ORDER_REQUEST.")
  ;; (tb.gw.sap-servb:define-servb-data-container-for-function-module
  ;;     "Z_MAT_SALES_ORDER_REQUEST" "IS_HEADER" :STRUCTURE :IMPORTING :site-list ("STU" "HAM")
  ;;     :slot-definition-list (("ORDER_DATE"         :char     8 :required)
  ;; 			     ("VISIBLE_DISC_ABS"   :char    15 :optional)
  ;; 			     ("UNVISIBLE_DISC_ABS" :char    15 :optional)
  ;; 			     ("VISIBLE_DISC_PRC"   :char     5 :optional)
  ;; 			     ("UNVISIBLE_DISC_PRC" :char     5 :optional)
  ;; 			     ("VISIBLE_SURC_ABS"   :char    15 :optional)
  ;; 			     ("UNVISIBLE_SURC_ABS" :char    15 :optional)
  ;; 			     ("VISIBLE_SURC_PRC"   :char     5 :optional)
  ;; 			     ("UNVISIBLE_SURC_PRC" :char     5 :optional)
  ;; 			     ("RESULTING_PRICE"    :char    15 :required)
  ;; 			     ("CURRENCY"           :char     5 :optional)
  ;; 			     ("DOC_TYPE"           :char     4 :required)
  ;; 			     ("SALES_ORG"          :char     4 :optional)
  ;; 			     ("DISTR_CHAN"         :char     2 :optional)
  ;; 			     ("SALES_OFFICE"       :char     4 :optional)
  ;; 			     ("SALES_GROUP"        :char     3 :optional)
  ;; 			     ("DIVISION"           :char     2 :optional)
  ;; 			     ("PARTNER_ROLE"       :char     2 :optional)
  ;; 			     ("CUSTOMER"           :char    10 :required)
  ;; 			     ("DESIRED_DATE"       :char     8 :optional)
  ;; 			     ("REASON"             :char     3 :optional)
  ;; 			     ("SHIP_WHEN_COMPL"    :char     1 :optional)
  ;; 			     ("PAYMENT_TERM"       :char     4 :optional)
  ;; 			     ("INCOTERMS"          :char     3 :optional)
  ;; 			     ("INCOTERMS_LOC"      :char    28 :optional)
  ;; 			     ("PURCHASE_NR"        :char    35 :optional)
  ;; 			     ("PURCHASE_DATE"      :char     8 :optional)))

  ;; (log:debug "Defining data container IS_ADDRESS for FM Z_MAT_SALES_ORDER_REQUEST.")
  ;; (tb.gw.sap-servb:define-servb-data-container-for-function-module
  ;;     "Z_MAT_SALES_ORDER_REQUEST" "IS_ADDRESS" :STRUCTURE :IMPORTING :site-list ("STU" "HAM")
  ;;     :slot-definition-list (("NAME"               :char    40 :optional)
  ;; 			     ("STREET"             :char    40 :optional)
  ;; 			     ("CITY"               :char    40 :optional)
  ;; 			     ("POSTAL_CODE"        :char    10 :optional)
  ;; 			     ("STATE_CODE"         :char     3 :optional)
  ;; 			     ("COUNTRY"            :char     3 :optional)
  ;; 			     ("REGION"             :char    32 :optional)))

  ;; (log:debug "Defining data container IS_PARTNERS for FM Z_MAT_SALES_ORDER_REQUEST.")
  ;; (tb.gw.sap-servb:define-servb-data-container-for-function-module
  ;;     "Z_MAT_SALES_ORDER_REQUEST" "IT_PARTNERS" :TABLE :IMPORTING :site-list ("STU" "HAM")
  ;;     :slot-definition-list (("ITM_NUMBER"         :char     6 :required)
  ;; 			     ("PARTNER_NR"         :char    10 :required)
  ;; 			     ("PARTNER_ROLE"       :char    64 :required)))

  ;; (log:debug "Defining data container IT_COMMENTS for FM Z_MAT_SALES_ORDER_REQUEST.")
  ;; (tb.gw.sap-servb:define-servb-data-container-for-function-module
  ;;     "Z_MAT_SALES_ORDER_REQUEST" "IT_COMMENTS" :TABLE :IMPORTING :site-list ("STU" "HAM")
  ;;     :slot-definition-list (("ITM_NUMBER"         :char     6 :required)
  ;; 			     ("TEXT_TYPE"          :char    64 :required)
  ;; 			     ("LINE_INDEX"         :int4    10 :required)
  ;; 			     ("COMMENT_TEXT"       :char   132 :required)
  ;; 			     ("LAIS03"             :char     3 :required)))

  ;; (log:debug "Defining data container IT_LINE_ITEMS for FM Z_MAT_SALES_ORDER_REQUEST.")
  ;; (tb.gw.sap-servb:define-servb-data-container-for-function-module
  ;;     "Z_MAT_SALES_ORDER_REQUEST" "IT_LINE_ITEMS" :TABLE :IMPORTING :site-list ("STU" "HAM")
  ;;     :slot-definition-list (("ITM_NUMBER"         :char     6 :required)
  ;; 			     ("MATERIAL"           :char    18 :required)
  ;; 			     ("MATERIAL_VERSION"   :char     4 :optional)
  ;; 			     ("MATERIAL_REVISION"  :char     4 :optional)
  ;; 			     ("MATERIAL_QTY"       :char    13 :required)
  ;; 			     ("MATERIAL_QU"        :char     3 :required)
  ;; 			     ("MATERIAL_PRICE"     :char    15 :optional)
  ;; 			     ("SHOW_LINE_ITEM"     :char     1 :optional)
  ;; 			     ("VISIBL_DISC_ABS"    :char    15 :optional)
  ;; 			     ("UNVISIBL_DISC_ABS"  :char    15 :optional)
  ;; 			     ("VISIBL_DISC_PRC"    :char     5 :optional)
  ;; 			     ("UNVISIBL_DISC_PRC"  :char     5 :optional)
  ;; 			     ("VISIBL_SURC_ABS"    :char    15 :optional)
  ;; 			     ("UNVISIBL_SURC_ABS"  :char    15 :optional)
  ;; 			     ("VISIBL_SURC_PRC"    :char     5 :optional)
  ;; 			     ("UNVISIBL_SURC_PRC"  :char     5 :optional)
  ;; 			     ("RESULTING_PRICE"    :char    15 :required)
  ;; 			     ("CALC_DELIVERY_DATE" :char    10 :optional)
  ;; 			     ("COMM_DELIVERY_DATE" :char    10 :optional)
  ;; 			     ("PAYMENT_TERM"       :char     4 :optional)
  ;; 			     ("INCOTERMS"          :char     3 :optional)
  ;; 			     ("INCOTERMS_LOC"      :char    28 :optional)
  ;; 			     ("PLANT"              :char     4 :required)
  ;; 			     ("LI_PRICE_VISIBLE"   :char     1 :optional)))

  ;; ;; + EXPORTING DATA

  ;; (log:debug "Defining data value EV_SALES_ORDER_NR for FM Z_MAT_SALES_ORDER_REQUEST.")
  ;; (tb.gw.sap-servb:define-servb-data-container-for-function-module
  ;;     "Z_MAT_SALES_ORDER_REQUEST" "" :VALUE :EXPORTING :site-list ("STU" "HAM")
  ;;     :slot-definition-list (("EV_SALES_ORDER_NR"  :char    10)))

  ;; (log:debug "Defining data container ES_SERVB_RETURN for FM Z_MAT_SALES_ORDER_REQUEST.")
  ;; (tb.gw.sap-servb:define-servb-data-container-for-function-module
  ;;     "Z_MAT_SALES_ORDER_REQUEST" "ES_SERVB_RETURN" :STRUCTURE :EXPORTING :site-list ("STU" "HAM")
  ;;     :slot-definition-list (("RETURN_CODE"        :int4    10)
  ;; 			     ("RETURN_MSGNR"       :char     8)
  ;; 			     ("REASON_CODE"        :char     8)
  ;; 			     ("REASON_TEXT"        :string)))

  ;; (log:debug "Defining data container ET_MESSAGES for FM Z_MAT_SALES_ORDER_REQUEST.")
  ;; (tb.gw.sap-servb:define-servb-data-container-for-function-module
  ;;     "Z_MAT_SALES_ORDER_REQUEST" "ET_MESSAGES" :TABLE :EXPORTING :site-list ("STU" "HAM")
  ;;     :slot-definition-list (("MSG_LINE"           :int4    10)
  ;; 			     ("MSG_ITM_NUMBER"     :numc     6)
  ;; 			     ("MSG_LEVEL"          :char     1)
  ;; 			     ("MSG_MESSAGE"        :char   256)
  ;; 			     ("MSG_LANGUAGE"       :char     3)
  ;; 			     ("MSG_SOURCE"         :char    32)
  ;; 			     ("MSG_TIMESTAMP"      :char   21)))

  (values))

(defun load-config-file (&optional filename)

  #+stu-test
  (load-stu-test)

  #-stu-test
  (let ((filename (config-filename filename)))
    (if (cl-fad:file-exists-p filename)
	(progn
	  (log:info "About to load config file ~A..." filename)
	  (load filename)
	  t)
	(progn
	  (log:warn "Config file ~A does not exist or is not readable! NOT loading config file!" filename)
	  nil))))

(defun stop-thingbone-gateway ()
  (log:info "Stopping THINGBONE GATEWAY ...")
  (tb.aserve:stop-aserve)
  (tb.gw.sap-servb:stop-sap-servb)
  (log:info "THINGBONE GATEWAY stopped.")
  (stop-log-source-info-announcer)
  (tb.core:reset-logging)
  (tb.msgs:stop-tb-condition-log-channel-dispatcher)
  (values))

(defun start-thingbone-gateway ()
  (tb.core:init-logging (format nil "~A/tbgw-~A-~A.log"
				(or (tb.core:log-dir) "/tmp")
				(tb.core:tb-site)
				(excl.osi:getpid)))
  (log:info "Starting THINGBONE GATEWAY ...")
  ;;(start-log-source-info-announcer)
  (tb.msgs:start-tb-condition-log-channel-dispatcher)
  (tb.gw.sap-servb:start-sap-servb)
  (tb.aserve:start-restful-api)
  (log:info "THINGBONE GATEWAY started.")
  (values))

(defun signal-2-handler (signal &optional ignore)
  (declare (ignore ignore))
  (log:warn "Received signal ~S. Cleaning up ..." signal)
  (stop-thingbone-gateway)
  ;;(tb-kill-all-threads)
  (log:warn "Exiting ..." signal)
  (excl:exit 1))

(defun signal-10-handler (signal &optional ignore)
  (declare (ignore ignore))
  (log:warn "Received signal ~S. Restarting ThingBone Gateway ..." signal)
  (stop-thingbone-gateway)
  (start-thingbone-gateway)
  )

(defun main (&rest args)

  (tb.core::init-0)

  (if (not (tb.core:tb-site))
      (progn
	(error "THINGBONE_SITE not set as environment variable. THIS IS REQUIRED. Aborting!")
	(if *tb-exit-on-error*
	    (excl:exit -1))))

  (log:info "THINGBONE GATEWAY started. Arguments are: \"~{~S~}\". Site is ~A.~%" args (tb.core:tb-site))

  (excl:add-signal-handler  2 'signal-2-handler)
  (excl:add-signal-handler 10 'signal-10-handler)

  (let ((config-filename (config-filename)))

    (log:info "THINGBONE GATEWAY: Loading config file ~A." config-filename)
    (load-config-file config-filename)

    ;; (log:info "Starting THINGBONE SWANK Server on port ~S ..." *swank-port*)
    ;; (swank:create-server :interface "0.0.0.0" :style :spawn :port *swank-port* :dont-close t)

    (start-thingbone-gateway)

    #+thingbone-production
    (progn
      (log:info "Entering THINGBONE wait loop for ~S seconds..." *execution-duration*)

      (loop for time from 0 to *execution-duration* by 1
	 do
	   (sleep 1)
	   (bt:thread-yield))

      (stop-thingbone-gateway)

      (log:trace "THINGBONE GATEWAY completed.~%~%Exciting...~%~%"))
    )

  #+thingbone-production
  (excl:exit 0)
  )
