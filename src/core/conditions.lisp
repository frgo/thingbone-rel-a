;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  THINGBONE CORE
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.CORE")

;;; ---------------------------------------------------------------------------

(define-condition tb-condition (tb-base condition)
  ((message  :accessor message :initarg :message :initform "")
   (level-nr :accessor level-nr :initarg :level-nr :initform +TB-MSG-LEVEL-UNDEFINED+)
   (timestamp :accessor timestamp :initarg :timestamp :initform (make-timestamp))
   (message-nr :accessor message-nr :initarg :message-nr :initform "")
   (reason-code :accessor reason-code :initarg :reason-code :initform "")
   (reason-text :accessor reason-text :initarg :reason-text :initform "")
   (function-return-code :accessor function-return-code :initarg :function-return-code :initform nil))
  (:report (lambda (c s)
	     (format s "ThingBone Condition: ~A ~S -> ~A, reason code ~S, reason text ~S, function return code ~S"
		     (timestamp c)
		     (msg-level-to-keyword (level-nr c))
		     (message c)
		     (reason-code c)
		     (reason-text c)
		     (function-return-code c)))))

(define-condition tb-emergency (tb-condition error)
  ()
  (:default-initargs
   :level-nr +TB-MSG-LEVEL-EMERGENCY+))

(define-condition tb-alert (tb-condition error)
  ()
  (:default-initargs
   :level-nr +TB-MSG-LEVEL-ALERT+))

(define-condition tb-critical (tb-condition error)
  ()
  (:default-initargs
   :level-nr +TB-MSG-LEVEL-CRITICAL+))

(define-condition tb-error (tb-condition error)
  ()
  (:default-initargs
   :level-nr +TB-MSG-LEVEL-ERROR+))

(define-condition tb-warning (tb-condition warning)
  ()
  (:default-initargs
   :level-nr +TB-MSG-LEVEL-WARNING+))

(define-condition tb-notice (tb-condition warning)
  ()
  (:default-initargs
   :level-nr +TB-MSG-LEVEL-NOTICE+))

(define-condition tb-information (tb-condition)
  ()
  (:default-initargs
   :level-nr +TB-MSG-LEVEL-INFORMATIONAL+))

(define-condition tb-debug (tb-condition)
  ()
  (:default-initargs
   :level-nr +TB-MSG-LEVEL-DEBUG+))

(defun format-tb-condition-for-slack (tb-condition)
  (check-type tb-condition tb.core:tb-condition)
  (with-slots (message message-nr reason-code reason-text) tb-condition
    (format nil "*~A*~%~%~30A ~32A ~30A~%~23A  ~32A ~30A~%~%*Reason Text*:~%~A"
	    message
	    "*Msg Nr*:"
	    "*Reason Code*:"
	    ""
	    message-nr
	    reason-code
	    ""
	    reason-text)))

(defun dispatch-tb-condition (tb-condition)
  (let ((msg (format-tb-condition-for-slack tb-condition))
	(level-nr (tb.core:level-nr tb-condition)))
    (cond
      ((= level-nr tb.core:+TB-MSG-LEVEL-UNDEFINED+)
       nil) ;; NOP
      ((= level-nr tb.core:+TB-MSG-LEVEL-DEBUG+)
       (log:debug "~A" msg))
      ((= level-nr tb.core:+TB-MSG-LEVEL-INFORMATIONAL+)
       (log:info "~A" msg))
      ((or (= level-nr tb.core:+TB-MSG-LEVEL-NOTICE+)
	   (= level-nr tb.core:+TB-MSG-LEVEL-WARNING+))
       (log:warn "~A" msg))
      ((or (= level-nr tb.core:+TB-MSG-LEVEL-ERROR+)
	   (= level-nr tb.core:+TB-MSG-LEVEL-CRITICAL+))
       (log:error "~A" msg))
      ((or (= level-nr tb.core:+TB-MSG-LEVEL-ALERT+)
	   (= level-nr tb.core:+TB-MSG-LEVEL-EMERGENCY+))
       (log:fatal "~A" msg))
      (t (log:fatal "Cannot dispatch ThingBone Condition: Cannot determine log function from level ~S. Message was:~%~%~A" level-nr msg))))
  (values))

(defun signal-tb-emergency (&key
			      (message "")
			      (timestamp (make-timestamp))
			      (message-nr "")
			      (reason-code "")
			      (reason-text "")
			      (function-return-code nil))
  (let ((condition (make-condition 'tb-emergency
				   :message message
				   :timestamp timestamp
				   :message-nr message-nr
				   :reason-code reason-code
				   :reason-text reason-text
				   :function-return-code function-return-code)))
    ;;(log-tb-condition condition)
    (error condition)))

(defun signal-tb-alert (&key
			  (message "")
			  (timestamp (make-timestamp))
			  (message-nr "")
			  (reason-code "")
			  (reason-text "")
			  (function-return-code nil))
  (let ((condition (make-condition 'tb-alert
				   :message message
				   :timestamp timestamp
				   :message-nr message-nr
				   :reason-code reason-code
				   :reason-text reason-text
				   :function-return-code function-return-code)))
    ;;(log-tb-condition condition)
    (error condition)))

(defun signal-tb-critical (&key
			     (message "")
			     (timestamp (make-timestamp))
			     (message-nr "")
			     (reason-code "")
			     (reason-text "")
			     (function-return-code nil))
  (let ((condition (make-condition 'tb-critical
				   :message message
				   :timestamp timestamp
				   :message-nr message-nr
				   :reason-code reason-code
				   :reason-text reason-text
				   :function-return-code function-return-code)))
    ;;(log-tb-condition condition)
    (error condition)))

(defun signal-tb-error (&key
			  (message "")
			  (timestamp (make-timestamp))
			  (message-nr "")
			  (reason-code "")
			  (reason-text "")
			  (function-return-code nil))
  (let ((condition (make-condition 'tb-error
				   :message message
				   :timestamp timestamp
				   :message-nr message-nr
				   :reason-code reason-code
				   :reason-text reason-text
				   :function-return-code function-return-code)))
    ;;(log-tb-condition condition)
    (error condition)))

(defun signal-tb-warning (&key
			    (message "")
			    (timestamp (make-timestamp))
			    (message-nr "")
			    (reason-code "")
			    (reason-text "")
			    (function-return-code nil))
  (let ((condition (make-condition 'tb-warning
				   :message message
				   :timestamp timestamp
				   :message-nr message-nr
				   :reason-code reason-code
				   :reason-text reason-text
				   :function-return-code function-return-code)))
    ;;(log-tb-condition condition)
    (warn condition)))

(defun signal-tb-notice (&key
			   (message "")
			   (timestamp (make-timestamp))
			   (message-nr "")
			   (reason-code "")
			   (reason-text "")
			   (function-return-code nil))
  (let ((condition (make-condition 'tb-notice
				   :message message
				   :timestamp timestamp
				   :message-nr message-nr
				   :reason-code reason-code
				   :reason-text reason-text
				   :function-return-code function-return-code)))
    ;;(log-tb-condition condition)
    (warn condition)))

(defun signal-tb-information (&key
				(message "")
				(timestamp (make-timestamp))
				(message-nr "")
				(reason-code "")
				(reason-text "")
				(function-return-code nil))
  (let ((condition (make-condition 'tb-information
				   :message message
				   :timestamp timestamp
				   :message-nr message-nr
				   :reason-code reason-code
				   :reason-text reason-text
				   :function-return-code function-return-code)))
    ;;(log-tb-condition condition)
    (signal condition)))

(defun signal-tb-debug (&key
			  (message "")
			  (timestamp (make-timestamp))
			  (message-nr "")
			  (reason-code "")
			  (reason-text "")
			  (function-return-code nil))
  (let ((condition (make-condition 'tb-debug
				   :message message
				   :timestamp timestamp
				   :message-nr message-nr
				   :reason-code reason-code
				   :reason-text reason-text
				   :function-return-code function-return-code)))
    ;;(log-tb-condition condition)
    (signal condition)))
