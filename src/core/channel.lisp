;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  THINGBONE CORE
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.CORE")

;;; ---------------------------------------------------------------------------

(defclass channel (finite-queue)
  ((name :reader name :initarg :name :initform "UNNAMED"))
  (:default-initargs
   :max-size *default-finite-queue-max-size*))

(defmethod print-object ((channel channel) stream)
  (print-unreadable-object (channel stream :type t :identity t)
    (format stream "~A :max-size ~S :count ~s" (name channel) (max-size channel) (queue-count channel))))

(deftype channel-size ()
  `(integer 1 ,most-positive-fixnum))

(defun make-channel (&key (name "UNNAMED") (max-size *default-finite-queue-max-size*))
  (check-type max-size channel-size)
  (log:debug "Creating channel ~A with max size ~S." name max-size)
  (make-instance 'channel :max-size max-size :name name))

(defun send (channel object &key (blockp t))
  (declare (ignore blockp)) ;; FIXME -> Need to implement block checking
  (if channel
      (mp:enqueue channel object)
      (log:error "Cannot dispatch message: Channel is NIL!")))

(defun recv (channel &key (wait nil))
  (mp:dequeue channel :wait wait))

(defun channel-full-p (channel)
  (mp:with-locked-queue (channel)
    (>= (queue-count channel) (max-size channel))))

(defun channel-empty-p (channel)
  (mp:with-locked-queue (channel)
    (mp:queue-empty-p channel)))

(defun send-will-block-p (channel)
  (channel-full-p channel)) ;;; FIXME -> Needs better logic ...


(defun start-channel-dispatcher (dispatcher)
  (tb.core:run-in-thread dispatcher))

(defun stop-channel-dispatcher (dispatcher)
  (tb.core:stop dispatcher))
