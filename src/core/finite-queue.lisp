;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  THINGBONE CORE
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.CORE")

;;; ---------------------------------------------------------------------------

(defparameter *lock-channel* (bt:make-recursive-lock))

(defvar *default-finite-queue-max-size* 1024
  "The default maximum size for a finite-queue.")

(defclass finite-queue (mp:queue)
  ((count :documentation "The current size of the queue."
          :initform 0
	  :accessor queue-count)
   (max-size :documentation "The maximum size of the queue."
             :initform *default-finite-queue-max-size*
	     :initarg :max-size
	     :accessor max-size))
  (:documentation "A mp:queue with finite size."))

(define-condition queue-full (simple-error)
  ((queue :initarg :queue
          :reader queue-full-queue))
  (:report (lambda (condition stream)
  	     (let ((queue (queue-full-queue condition)))
               (format stream
               	       "~A is at it's maximum size of ~D."
  		       queue
                       (max-size queue))))))

(defmethod mp:enqueue :around ((queue finite-queue) what)
  (declare (ignorable what))
  (when (>= (queue-count queue) (max-size queue))
    (error 'queue-full :queue queue))
  (excl:incf-atomic (slot-value queue 'count) 1)
  (call-next-method))

(defmethod mp:dequeue :around ((queue finite-queue) &key wait empty-queue-results)
  (declare (ignorable wait empty-queue-results))
  (let ((item (call-next-method)))
    (excl:decf-atomic (slot-value queue 'count) 1)
    item))

;;; ---------------------------------------------------------------------------
;;;    TESTING
;;; ---------------------------------------------------------------------------
