;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  THINGBONE CORE
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.CORE")

;;; ---------------------------------------------------------------------------

(defparameter *tb-core-version* "A.01.11")

(defconstant +TB-ENV-VAR-SITE+ "THINGBONE_SITE")
(defparameter *tb-site* nil)

(eval-when (:compile-toplevel :execute :load-toplevel)

  (defun init-foreign-lib-loading ()

    (format *debug-io* "LD_LIBRARY_PATH=~A~%" (excl.osi:getenv "LD_LIBRARY_PATH"))
    (setq excl:*aclssl-version* '(1 1)) ;; frgo, 2019-08-22: Force OpenSSL 1.1 if not set by env vars

    ;; Handle OpenSSL
    (format *debug-io* "ACL_OPENSSL=~S, ACL_SSL_LIBRARY_NAMES=~S, ACL_OPENSSL_VERSION=~S, *aclssl-version* = ~S~%"
	    (excl.osi:getenv "ACL_OPENSSL")
	    (excl.osi:getenv "ACL_SSL_LIBRARY_NAMES")
	    (excl.osi:getenv "ACL_OPENSSL_VERSION")
	    excl:*aclssl-version*)
    )


  (defun init-0 ()
    (init-foreign-lib-loading)
    (if (not (member :ssl-support *features*))
	(format *debug-io* "ERROR: No SSL support in AllegroCL image!~%")
	(progn
	  (format *debug-io* "Trying to load SSL and Allegroserve ...~%")
	  (require :ssl)
	  (require :aserve)
	  (ignore-errors (format *debug-io* "OpenSSL version loaded: ~A ~%" (excl:openssl-version)))))
    t)

  ;;(init-0)
  )

(defun tb-site ()
  (or *tb-site*
      (excl.osi:getenv +TB-ENV-VAR-SITE+)
      ))

(defun tb-core-version ()
  *tb-core-version*)
