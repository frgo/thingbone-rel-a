;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  THINGBONE CORE
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(defpackage #:thingbone.core
  (:use #:cl #:cl-strings)
  (:nicknames "TB.CORE")
  (:export

   #:+TB-GW-ENV-VAR-LOG-FILE-PATHNAME+
   #:+TB-GW-DEFAULT-LOG-FILE-PATHNAME+
   #:+TB-GW-ENV-VAR-LOG-DIR+

   #:+TB-GW-ENV-VAR-CONTAINER-HOST+

   #:+TB-GW-SIMPLE-CONSOLE-API-URL+
   #:+TB-GW-DEFAULT-SIMPLE-CONSOLE-PORT+
   #:+TB-GW-DEFAULT-SIMPLE-CONSOLE-HOST+
   #:+TB-GW-ENV-VAR-SIMPLE-CONSOLE-PORT+
   #:+TB-GW-ENV-VAR-SIMPLE-CONSOLE-HOST+


   #:+TB-MSG-LEVEL-EMERGENCY+
   #:+TB-MSG-LEVEL-ALERT+
   #:+TB-MSG-LEVEL-CRITICAL+
   #:+TB-MSG-LEVEL-ERROR+
   #:+TB-MSG-LEVEL-WARNING+
   #:+TB-MSG-LEVEL-NOTICE+
   #:+TB-MSG-LEVEL-INFORMATIONAL+
   #:+TB-MSG-LEVEL-DEBUG+
   #:+TB-MSG-LEVEL-UNDEFINED+
   #:msg-level-to-msg-string-en
   #:msg-level-to-keyword

   #:timestamp
   #:message
   #:message-nr
   #:reason-code
   #:reason-text
   #:function-return-code
   #:level-nr

   #:make-channel
   #:channel
   #:send
   #:recv
   #:channel-full-p
   #:channel-empty-p
   #:send-will-block-p
   #:start-channel-dispatcher
   #:stop-channel-dispatcher

   #:format-iso8601-time
   #:parse-iso8601-time
   #:make-iso8601-timestamp-string
   #:make-timestamp
   #:timestamp
   #:make-uuid

   #:tb-base
   #:tb-task
   #:set-run-status
   #:run-status=
   #:wait-until-halted
   #:start
   #:get-thread
   #:set-thread
   #:run-in-thread
   #:stop
   #:make-tb-task-runner
   #:make-tb-task-runner-thread
   #:name
   #:run-status
   #:msg-dispatcher

   #:keywordify
   #:global-context-value

   #:find-mapper
   #:value-mapper
   #:map-value
   #:define-simple-value-mapper
   #:define-bidirectional-simple-value-mapper

   #:init-0
   #:log-dir
   #:init-logging
   #:reset-logging
   #:start-log-source-info-announcer
   #:stop-log-source-info-announcer
   #:tb-site
   #:*tb-site*
   #:tb-core-version

   ;; CONDITIONS
   #:tb-condition
   #:tb-emergency
   #:tb-alert
   #:tb-critical
   #:tb-error
   #:tb-warning
   #:tb-notice
   #:tb-information
   #:tb-debug

   #:signal-tb-emergency
   #:signal-tb-alert
   #:signal-tb-critical
   #:signal-tb-error
   #:signal-tb-warning
   #:signal-tb-notice
   #:signal-tb-information
   #:signal-tb-debug

   ;; PARTICIPANT

   #:tb-participant

   ;; GATEWAY

   #:tb-gateway
   #:site

   ))

;;; ---------------------------------------------------------------------------
