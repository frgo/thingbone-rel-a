;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  THINGBONE CORE
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.CORE")

;;; ---------------------------------------------------------------------------

(defclass tb-task (tb-base ;;simple-tasks:call-task
		   )
  ((name                :reader   name            :initarg :name :initform "UNNAMED TB-TASK")
   (run-status          :accessor run-status                     :initform :HALTED)
   (lock-run-status     :accessor lock-run-status                :initform (bt:make-recursive-lock))
   (thread              :accessor thread                         :initform nil)
   (lock-thread         :accessor lock-thread                    :initform (bt:make-recursive-lock)))
  (:default-initargs
   ;;:func (lambda () nil)
   ))

(defmethod print-object ((tb-task tb-task) stream)
  (print-unreadable-object (tb-task stream :type T :identity T)
    (format stream ":NAME ~S :RUN-STATUS ~s" (name tb-task) (run-status tb-task))))

(defmethod set-run-status ((tb-task tb-task) run-status)
  (bt:with-recursive-lock-held ((lock-run-status tb-task))
    (setf (run-status tb-task) run-status)))

(defmethod run-status= ((tb-task tb-task) run-status)
  (let ((server-run-status (bt:with-recursive-lock-held ((lock-run-status tb-task))
			     (run-status tb-task))))
    (eql server-run-status run-status)))

(defun timed-out? (start-time timeout)
  (declare (type integer start-time timeout))
  (declare (optimize (speed 3) (safety 1) (debug 0) (compilation-speed 0)))
  (if (< timeout 0)
      nil
      (>= (get-internal-real-time) (+ start-time timeout ))))

(defmethod wait-until-halted ((tb-task tb-task) &key (timeout -1)) ;; [ms] Milliseconds
  (log:info "Waiting for task ~S to halt (timeout ~S)..." tb-task timeout)
  (let ((start-time (get-internal-real-time))
	(name (tb.core:name tb-task)))
    (loop until (or (run-status= tb-task :HALTED)
		    (timed-out? start-time timeout))
       do
	 (bt:thread-yield))

    (if (run-status= tb-task :HALTED)
	(log:info "Task ~A halted." name)
	(log:warn "Task ~A could not be halted withing timeout (~S ms)." name timeout)))
  )

(defmethod start ((tb-task tb-task))
  nil)

(defmethod get-thread ((tb-task tb-task))
  (bt:with-recursive-lock-held ((lock-thread tb-task))
    (thread tb-task)))

(defmethod set-thread ((tb-task tb-task) thread)
  (bt:with-recursive-lock-held ((lock-thread tb-task))
    (setf (thread tb-task) thread)))

(defmethod run-in-thread ((tb-task tb-task))
  (let* ((thread-name (format nil "TB-TASK ~A" (name tb-task)))
	 (thread (bt:make-thread (lambda ()
				   (start tb-task)) :name thread-name)))
    (set-thread tb-task thread)))

(defmethod stop ((tb-task tb-task))
  (bt:join-thread (get-thread tb-task)))


(defun make-tb-task-runner ()
  (make-instance 'simple-tasks:runner))

(defun make-tb-task-runner-thread (runner)
  (simple-tasks:make-runner-thread runner))
