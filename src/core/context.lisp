;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  THINGBONE CORE
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.CORE")

(defparameter *lock-global-context* (bt:make-recursive-lock))

(defclass context (tb-base)
  ((name :reader name :initarg :name :initform "UNNAMED")
   (ht :reader ht :initarg :ht :initform (make-hash-table))))

(defparameter *global-context* (make-instance 'context :name "THINGBONE GLOBAL CONTEXT"))

(defun set-global-context (context)
  (bt:with-recursive-lock-held (*lock-global-context*)
    (setq *global-context* context)))

(defun global-context ()
  (bt:with-recursive-lock-held (*lock-global-context*)
    *global-context*))

(defun global-context-value (key)
  (let* ((context (global-context))
	 (ht (ht context)))
    (gethash key ht)))

(defun (setf global-context-value) (value key)
  (let* ((context (global-context))
	 (ht (ht context)))
    (bt:with-recursive-lock-held (*lock-global-context*)
      (setf (gethash key ht) value))))
