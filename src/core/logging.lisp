;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  THINGBONE CORE
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.CORE")

(eval-when (:compile-toplevel :execute :load-toplevel)
  (require :aserve))

(defconstant +TB-GW-ENV-VAR-LOG-FILE-PATHNAME+ "THINGBONE_GATEWAY_LOGFILENAME")
(defconstant +TB-GW-DEFAULT-LOG-FILE-PATHNAME+ "/var/data/thingbone/gateway/logs/tbgw.log")

(defconstant +TB-GW-ENV-VAR-LOG-DIR+ "THINGBONE_GATEWAY_LOGDIR")

(defconstant +TB-GW-ENV-VAR-CONTAINER-HOST+ "THINGBONE_CONTAINER_HOST")

(defconstant +TB-GW-SIMPLE-CONSOLE-API-URL+ "/thingbone/gateway/log/v3")
(defconstant +TB-GW-DEFAULT-SIMPLE-CONSOLE-PORT+ 7189)
(defconstant +TB-GW-DEFAULT-SIMPLE-CONSOLE-HOST+ "bwpstuvm003.bwcinc.org")
(defconstant +TB-GW-ENV-VAR-SIMPLE-CONSOLE-PORT+ "THINGBONE_GATEWAY_SIMPLE_CONSOLE_PORT")
(defconstant +TB-GW-ENV-VAR-SIMPLE-CONSOLE-HOST+ "THINGBONE_GATEWAY_SIMPLE_CONSOLE_HOST")


(defparameter *slack-webhook-url* "https://hooks.slack.com/services/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")
(defparameter *slack-oauth-bearer-token* "xoxp-414361922979-414254515172-822940112615-d104aac71aee7b0f188ce3713cad3e9d")

(defparameter *slack-request-timeout* 5) ;; in seconds

(defparameter *slack-attachment-color-fatal* "b60017") ;; PANTONE 20-0069 TPM
(defparameter *slack-attachment-color-error* "ec4600") ;; PANTONE 20-0059 TPM
(defparameter *slack-attachment-color-warn* "ddc738")  ;; PANTONE 20-0045 TPM
(defparameter *slack-attachment-color-info* "ada59f")  ;; PANTONE 20-0089 TPM
(defparameter *slack-attachment-color-debug* "e6e9e4") ;; PANTONE 20-0081 TPM
(defparameter *slack-attachment-color-trace* "97bbd8") ;; PANTONE 20-0152 TPM

(defparameter *tb-jira-project-id* "BWPS")
(defparameter *tb-jira-url* "https://bwpger.atlassian.net/rest/api/2/issue/")

(defparameter *slack-colors-by-level* (list *slack-attachment-color-fatal*
					    *slack-attachment-color-error*
					    *slack-attachment-color-warn*
					    *slack-attachment-color-info*
					    *slack-attachment-color-debug*
					    *slack-attachment-color-trace*))

(defclass log4cl-slack-appender (log4cl:appender)
  ((name :accessor name :initarg :name :type string :initform :thingbone)
   (slack-webhook-url :accessor slack-webhook-url :initarg :slack-webhook-url :initform *slack-webhook-url*))
  (:default-initargs
   :layout (make-instance 'log4cl:pattern-layout :conversion-pattern "%<{nopretty}%m%>"))
  (:documentation
   "An appender that writes log messages to a slack channel.
The identity of the slack connection is controlled by the :name
initarg and defaults to :thingbone."))

;; (defmethod property-alist ((log4cl-slack-appender log4cl-slack-appender))
;;   (append (call-next-method)
;;           ;; '((:name name :string-skip-whitespace)))
;; 	  '((:string-skip-whitespace))))

(defun level-to-slack-color (level)
  (nth level *slack-colors-by-level*))

;;; curl -D- -u fred:fred -X POST --data {see below} -H "Content-Type: application/json" http://localhost:8090/rest/api/2/issue/

(defclass slack-log-message ()
  ((site :accessor site :initarg :site :initform nil)
   (level :accessor level :initarg :level :initform nil)
   (jira-url :accessor jira-url :initarg :jira-url :initform nil)
   (jira-project-id :accessor jira-project-id :initarg :jira-project-id :initform nil)
   (message :accessor message :initarg :message :initform nil)))

(defmethod print-object ((slack-log-message slack-log-message) stream)
  (print-unreadable-object (slack-log-message stream :identity t :type t)
    (with-slots (site level jira-url jira-project-id message) slack-log-message
      (format stream ":SITE ~S :LEVEL ~S :JIRA-URL ~S :JIRA-PROJECT-ID ~S :MESSAGE ~S"
	      site level jira-url jira-project-id message))))

(defmethod as-value-string ((slack-log-message slack-log-message))
  (format nil "\"~A ~S ~A ~A\""
  	  (tb.core:tb-site)
	  (level slack-log-message)
	  (jira-url slack-log-message)
	  (jira-project-id slack-log-message)
	  (message slack-log-message)))

(defmethod jonathan:%to-json ((slack-log-message slack-log-message))

  (let* ((docker-hostname (excl.osi:getenv +TB-GW-ENV-VAR-CONTAINER-HOST+))
	 (simple-console-port (excl.osi:getenv +TB-GW-ENV-VAR-SIMPLE-CONSOLE-PORT+))
	 (level (level slack-log-message))
	 (tb-core-version (tb.core:tb-core-version)))

    (jonathan:with-object

      (jonathan:write-key "blocks")
      (jonathan:write-value
       (jonathan:with-array

	 (jonathan:write-item
	  (jonathan:with-object ;; divider
	    (jonathan:write-key-value "type" "divider")))

	 (jonathan:write-item
	  (jonathan:with-object ;; Title Line

	    (jonathan:write-key-value "type" "section")

	    (jonathan:write-key "text")

	    (jonathan:write-value
	     (jonathan:with-object
	       (jonathan:write-key-value "type" "mrkdwn")
	       (jonathan:write-key-value "text" (format nil "ThingBone *~A*:" (tb.core:tb-site)))))

	    ;; (jonathan:write-key "accessory")
	    ;; (jonathan:write-value
	    ;;  (jonathan:with-object
	    ;;    (jonathan:write-key-value "type" "button")
	    ;;    (jonathan:write-key "text")
	    ;;    (jonathan:write-value
	    ;; 	(jonathan:with-object
	    ;; 	  (jonathan:write-key-value "type" "plain_text")
	    ;; 	  (jonathan:write-key-value "text" "Create Issue in JIRA")
	    ;; 	  (jonathan:write-key-value "emoji" t)))
	    ;;    (jonathan:write-key "value") (jonathan:write-value (as-value-string slack-log-message))))
	    ))

	 (jonathan:write-item
	  (jonathan:with-object ;; Context Line
	    (jonathan:write-key-value "type" "context")
	    (jonathan:write-key "elements")
	    (jonathan:write-value
	     (jonathan:with-array
	       (jonathan:write-item
		(jonathan:with-object
		  (jonathan:write-key-value "type" "mrkdwn")
		  (jonathan:write-key "text")
		  (jonathan:write-value (format nil "ThingBone Core version ~A running on Docker host ~A. <http://~A:~A/|TBGW Simple Console>."
						tb-core-version docker-hostname docker-hostname simple-console-port))))))))

	 (jonathan:write-item
	  (jonathan:with-object ;; Type and Timestamp Line
	    (jonathan:write-key-value "type" "section")
	    (jonathan:write-key "fields")
	    (jonathan:write-value
	     (jonathan:with-array
	       (jonathan:write-item
		(jonathan:with-object
		  (jonathan:write-key-value "type" "mrkdwn")
		  (jonathan:write-key "text")
		  (jonathan:write-value (format nil "*Type*:~A~A   ~A"
						(string #\Newline)
						(if (< level log4cl:+log-level-info+)
						    ":RED_CIRCLE:"
						    ":WHITE_CIRCLE:")
						(log4cl:log-level-to-string level)))))
	       (jonathan:write-item
		(jonathan:with-object
		  (jonathan:write-key-value "type" "mrkdwn")
		  (jonathan:write-key "text")
		  (jonathan:write-value (format nil "*Timestamp*:~A~A" (string #\Newline) (tb.core:make-timestamp)))))
	       ))))

	 (jonathan:write-item
	  (jonathan:with-object ;; Message Title Line
	    (jonathan:write-key-value "type" "section")

	    (jonathan:write-key "text")
	    (jonathan:write-value
	     (jonathan:with-object
	       (jonathan:write-key-value "type" "mrkdwn")
	       (jonathan:write-key "text")
	       (jonathan:write-value (format nil "*Message*:~A~A" (string #\Newline) (message slack-log-message)))))

	    ;; (jonathan:write-key "accessory")
	    ;; (jonathan:write-value
	    ;;  (jonathan:with-object
	    ;;    (jonathan:write-key-value "type" "image")
	    ;;    (jonathan:write-key "image_url")
	    ;;    (jonathan:write-value "https://giphy.com/stickers/poo-tzOjl8r1SJjO")))

	    )))))))

(defmethod log4cl:appender-do-append ((log4cl-slack-appender log4cl-slack-appender) logger level log-func)

  (let* ((layout (log4cl:appender-layout log4cl-slack-appender))
	 (message (with-output-to-string (stream)
	 	    (log4cl:layout-to-stream layout stream logger level log-func)))
	 (slack-log-message (make-instance 'slack-log-message
					   :jira-project-id *tb-jira-project-id*
					   :jira-url *tb-jira-url*
					   :level level
					   :site (tb.core:tb-site)
					   :message message))
	 (content (jonathan:to-json slack-log-message)))
    (net.aserve.client:do-http-request (slack-webhook-url log4cl-slack-appender)
      :method :post
      :content-type "application/json"
      :content content
      :timeout *slack-request-timeout*)))

(defun log-filename (&optional filename)
  (or filename
      (excl.osi:getenv +TB-GW-ENV-VAR-LOG-FILE-PATHNAME+)
      +TB-GW-DEFAULT-LOG-FILE-PATHNAME+))

(defun log-dir ()
  (excl.osi:getenv +TB-GW-ENV-VAR-LOG-DIR+))

(defun init-logging (&optional filename)
  (log4cl:remove-all-appenders log4cl:*root-logger*)
  (log:config :thread :time :file2 :sane2 :daily (log-filename filename) :backup nil)

  #-thingbone-production
  (log4cl:add-appender log4cl:*root-logger* (make-instance 'log4cl-slack-appender))
  #+thingbone-production
  (format *debug-io* "Logging to file only.~%")

  (log:info "THINGBONE logging intialized.")
  )

(defun reset-logging ()
  (log4cl:remove-all-appenders log4cl:*root-logger*)
  )
