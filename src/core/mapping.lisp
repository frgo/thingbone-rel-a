;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  THINGBONE CORE
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.CORE")

;;; ---------------------------------------------------------------------------
;;;    VALUE MAPPING
;;; ---------------------------------------------------------------------------

;;; SOURCES & DESTINATIONS
;;; :THINGBONE
;;; :BWP-INTEGRATION-LAYER
;;; :SAP-SERVB

(defparameter *mappers* (make-hash-table :test #'equalp))

(defclass mapper-base ()
  ((source      :accessor source      :initarg :source      :initform nil)
   (destination :accessor destination :initarg :destination :initform nil)
   (fn          :accessor fn          :initarg :fn          :initform nil)))

(defun find-mapper (source destination class slot source-value)
  (log:debug "FIND-MAPPER: source = ~S, destination = ~S, class = ~S, slot = ~S, source-value = ~S"
	     source destination class slot source-value)
  (let ((mapper (gethash (list source destination class slot source-value) *mappers*)))
    (log:debug "Mapper = ~S" mapper)
    mapper))

(defclass value-mapper (mapper-base)
  ((input-value  :accessor input-value  :initarg :input-value  :initform nil)
   (output-value :accessor output-value :initarg :output-value :initform nil)))

(defmethod map-it ((value-mapper value-mapper) object slot input)
  (declare (ignore object slot input))
  (output-value value-mapper))

(defun map-value (source destination object slot input-value)
  (let* ((class (class-of object))
	 (mapper (find-mapper source destination class slot input-value)))
    (log:debug "MAP-VALUE: class = ~S, mapper = ~S." class mapper)
    (if mapper
	(map-it mapper object slot input-value)
	input-value)))

(defun define-simple-value-mapper (source destination input-value output-value)
  (let ((mapper (make-instance 'value-mapper
			       :source source
			       :destination destination
			       :input-value input-value
			       :output-value output-value)))
    (setf (gethash (list source destination (class-of nil) nil input-value) *mappers*) mapper))
  (values))

(defun define-bidirectional-simple-value-mapper (side-1 side-2 value-1 value-2)
  (define-simple-value-mapper side-1 side-2 value-1 value-2)
  (define-simple-value-mapper side-2 side-1 value-2 value-1)
  (values))


(defclass slot-mapper (mapper-base)
  ((class-ident :accessor class-ident :initarg :class-ident :initform nil)
   (slot-ident  :accessor slot-ident  :initarg :slot-ident  :initform nil)
   (slot-var    :accessor slot-var    :initarg :slot-var    :initform nil)
   (input-var   :accessor input-var   :initarg :input-var   :initform nil)
   (mapper-code :accessor mapper-code :initarg :mapper-code :initform nil)))

(defmethod print-object ((slot-mapper slot-mapper) stream)
  (print-unreadable-object (slot-mapper stream :identity t :type t)
    (with-slots (source destination class-ident slot-ident slot-var input-var mapper-code) slot-mapper
      (format stream ":SOURCE ~S :DESTINATION ~S :CLASS-IDENT ~S :SLOT-IDENT ~S :SLOT-VAR ~S :INPUT-VAR ~S :MAPPER-CODE ~S"
	      source destination class-ident slot-ident slot-var input-var  mapper-code))))

(defmacro define-slot-mapper (source destination class slot slot-value-var input-var &body body)
  (let ((g-fn (gensym))
	(g-mapper (gensym))
	(g-key (gensym)))
    `(let ((,g-fn (lambda (self ,input-var)
		    (let ((,slot-value-var (slot-value self ',slot)))
		      (progn
			,@body)))))
       (let* ((,g-mapper (make-instance 'slot-mapper
					:class-ident (find-class ',class)
					:slot-ident ',slot
					:slot-var ',slot-value-var
					:input-var ',input-var
					:source ,source
					:destination ,destination
					:mapper-code ',@body
					:fn ,g-fn))
	      (,g-key (list ,source ,destination (find-class ',class) ',slot nil)))
	 (log:debug "key = ~S" ,g-key)
	 (setf (gethash ,g-key *mappers*) ,g-mapper)
	 ,g-mapper))))


;;; === TESTING ====

(defclass mapper-test ()
  ((a :accessor a :initarg :a :initform nil)))

(define-slot-mapper :SOURCE :DESTINATION MAPPER-TEST tb.core::A a input
  (concatenate 'string a "-" input))

(defun test-slot-mapper ()
  (let ((instance (make-instance 'mapper-test :a "FRGO")))
    (map-value :SOURCE :DESTINATION instance 'tb.core::A "TST")))
