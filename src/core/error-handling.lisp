;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  THINGBONE CORE
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.CORE")

;; Levels:
;;
;; 0    Emergency
;; 1    Alert
;; 2    Critical
;; 3    Error
;; 4    Warning
;; 5    Notice
;; 6    Informational
;; 7    Debug

(eval-when (:compile-toplevel :execute :load-toplevel)
  (defconstant +TB-MSG-LEVEL-EMERGENCY+     0)
  (defconstant +TB-MSG-LEVEL-ALERT+         1)
  (defconstant +TB-MSG-LEVEL-CRITICAL+      2)
  (defconstant +TB-MSG-LEVEL-ERROR+         3)
  (defconstant +TB-MSG-LEVEL-WARNING+       4)
  (defconstant +TB-MSG-LEVEL-NOTICE+        5)
  (defconstant +TB-MSG-LEVEL-INFORMATIONAL+ 6)
  (defconstant +TB-MSG-LEVEL-DEBUG+         7)
  (defconstant +TB-MSG-LEVEL-UNDEFINED+     9)

  (defconstant +TB-MSG-LEVEL-STRING-EN-EMERGENCY+     "EMERGENCY")
  (defconstant +TB-MSG-LEVEL-STRING-EN-ALERT+         "ALERT")
  (defconstant +TB-MSG-LEVEL-STRING-EN-CRITICAL+      "CRITICAL")
  (defconstant +TB-MSG-LEVEL-STRING-EN-ERROR+         "ERROR")
  (defconstant +TB-MSG-LEVEL-STRING-EN-WARNING+       "WARNING")
  (defconstant +TB-MSG-LEVEL-STRING-EN-NOTICE+        "NOTICE")
  (defconstant +TB-MSG-LEVEL-STRING-EN-INFORMATIONAL+ "INFORMATIONAL")
  (defconstant +TB-MSG-LEVEL-STRING-EN-DEBUG+         "DEBUG")
  (defconstant +TB-MSG-LEVEL-STRING-EN-UNDEFINED+     "__UNDEFINED__")
  )

(defparameter *tb-msg-level-string-en-dictionary*
  '(#.+TB-MSG-LEVEL-STRING-EN-EMERGENCY+
    #.+TB-MSG-LEVEL-STRING-EN-ALERT+
    #.+TB-MSG-LEVEL-STRING-EN-CRITICAL+
    #.+TB-MSG-LEVEL-STRING-EN-ERROR+
    #.+TB-MSG-LEVEL-STRING-EN-WARNING+
    #.+TB-MSG-LEVEL-STRING-EN-NOTICE+
    #.+TB-MSG-LEVEL-STRING-EN-INFORMATIONAL+
    #.+TB-MSG-LEVEL-STRING-EN-DEBUG+
    #.+TB-MSG-LEVEL-STRING-EN-UNDEFINED+
    #.+TB-MSG-LEVEL-STRING-EN-UNDEFINED+
    ))

(defparameter *tb-msg-level-keyword-dictionary*
  '(:EMERGENCY
    :ALERT
    :CRITICAL
    :ERROR
    :WARNING
    :NOTICE
    :INFORMATIONAL
    :DEBUG
    :__UNDEFINED__
    :__UNDEFINED__
    ))

(defun msg-level-to-string-en (level)
  (if (or (< level 0)
	  (> level 9))
      (error "Invalid level ~S for msg level!" level)
      (nth level *tb-msg-level-string-en-dictionary*)))

(defun msg-level-to-keyword (level)
  (if (or (< level 0)
	  (> level 9))
      (error "Invalid level ~S for msg level!" level)
      (nth level *tb-msg-level-keyword-dictionary*)))

(defun log4cl-level-to-tb-msg-level (log4cl-level)
  log4cl-level)
