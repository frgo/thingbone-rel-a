;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  GATEWAY RESTFUL API
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package #:thingbone.aserve-restful)

#-allegro
(error "THINGBONE.ASERVE-RESTFUL currently is only supported on AllegroCL.")

(eval-when (:compile-toplevel :execute :load-toplevel)
  (require :aserve))

;;; -----------------------------------------------------------------------------
;;;    SIMPLE TESTING
;;; -----------------------------------------------------------------------------

(defun publish-directory (relative-url directory-pathname-or-string)
  (let ((destination (uiop:pathname-directory-pathname directory-pathname-or-string)))
    (net.aserve:publish-directory :prefix relative-url :destination destination)))

(defun publish-json-file (relative-url file-pathname-or-string)
  (net.aserve:publish-file :path relative-url :file file-pathname-or-string :content-type "application/json"))

(defconstant +TB-GW-ENV-VAR-API-PORT+ "THINGBONE_GATEWAY_API_PORT")
(defconstant +TB-GW-DEFAULT-API-PORT+ 7169)
(defconstant +TB-ENV-VAR-CONTAINER-HOST+ "THINGBONE_CONTAINER_HOST")

(defun aserve-port ()
  (let* ((env-var-value (excl.osi:getenv +TB-GW-ENV-VAR-API-PORT+))
	 (port (or env-var-value
		   +TB-GW-DEFAULT-API-PORT+)))
    (log:info "THINGBONE API Port: ~S (env var ~A: ~S), Container Host: ~S."
	      port
	      +TB-GW-ENV-VAR-API-PORT+
	      env-var-value
	      (or (excl.osi:getenv +TB-ENV-VAR-CONTAINER-HOST+)
		  "UNKNOWN"))
    port))

(defun run-aserve-simple (&key (port (aserve-port)))
  (net.aserve:start :host "0.0.0.0" :port port :listeners 1))

(defparameter *json-schema-url* "/integration-layer/schemas/v3/")
(defparameter *json-schema-directory* "/var/data/swdev/integration-layer/schemas/v3/")

(defparameter *json-schema-files* '("core.json"
				    "content.json"
				    "business-partner.json"
				    "item-master-data.json"
				    "item-master-inform.json"))

(defun publish-json-schema-files (&optional (file-list *json-schema-files*))
  (loop for file in file-list
     do
       (publish-json-file (concatenate 'string
				       *json-schema-url*
				       file )
			  (uiop:merge-pathnames*
			   *json-schema-directory*
			   file))))

;;; Note:
;;; https://github.com/bonkzwonil/defrest

(defconstant +TB-ERROR-REASON-CODE-INVALID-METHOD-OBJECT-CLASS-OPERATION-PARAMETER+  1)

(defconstant +TB-ERROR-CODE-CONFIGURATION-ERROR-GATEWAY-NOT-FOUND-FOR-SITE+ 1001)

(defconstant +TB-MSG-NR-SUCCESS+            "TGWI000S")
(defconstant +TB-MSG-NR-INVALID-PARAMETER+  "TGWI100E")



(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ORDER-TYPE+                     "ORDER-TYPE")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-SALES-ORG+                      "SALES-ORG")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-DISTRIBUTION-CHANNEL+           "DISTRIBUTION-CHANNEL")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-BUSINESS-DIVISION+              "BUSINESS-DIVISION")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-BUSINESS-PARTNER-ROLE+          "BUSINESS-PARTNER-ROLE")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-CUSTOMER-NR+                    "CUSTOMER-NR")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-PRICE-DATE+                     "PRICE-DATE")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-DATA-CONTEXT+                   "DATA-CONTEXT")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ENVIRONMENT+                    "ENVIRONMENT")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEMS+                          "ITEMS")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEM-POS-NR+                    "POS-NR")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEM-ITEM-NR+                   "ITEM-NR")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEM-ITEM-VERSION+              "ITEM-VERSION")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEM-ITEM-REVISION+             "ITEM-REVISION")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEM-VALUE-STREAM-CODE+         "VALUE-STREAM-CODE")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEM-QUANTITY+                  "QUANTITY")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEM-UOM+                       "UOM")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEM-PRICING-CONDITION-TYPE+    "PRICING-CONDITION-TYPE")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEM-PRICE+                     "PRICE")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-SALES-MATERIAL-GROUP+           "SALES-MATERIAL-GROUP")


(defclass item-pricing-query-request (tb.msgs:msg-data)
  ((data-context :accessor data-context :initarg :data-context :initform nil)
   (servb-mat-pricing-items-in :accessor servb-mat-pricing-items-in :initarg :servb-mat-pricing-items-in :initform nil)
   ))

(defmethod print-object ((item-pricing-query-request item-pricing-query-request) stream)
  (print-unreadable-object (item-pricing-query-request stream :identity t :type t)
    (format stream ":DATA-CONTEXT ~S :SERVB-MAT-PRICING-ITEMS-IN ~S"
	    (slot-value item-pricing-query-request 'data-context)
	    (slot-value item-pricing-query-request 'servb-mat-pricing-items-in))))

(defclass data-context (tb.msgs:msg-data)
  ((elements :accessor elements :initarg :elements :initform nil)))

(defmethod print-object ((data-context data-context) stream)
  (print-unreadable-object (data-context stream :identity t :type t)
    (format stream ":ELEMENTS ~S"
	    (slot-value data-context 'elements))))

(defmethod jonathan:%to-json ((data-context data-context))
  (jonathan:with-object
    (jonathan:write-key "data-context")
    (jonathan:write-value
     (jonathan:with-object
       (loop for element in (elements data-context)
	  do
	    (jonathan:write-key (key element))
	    (jonathan:write-value (value element)))))))

(defclass key-value-pair (tb.core:tb-base)
  ((key :accessor key :initarg :key :initform "")
   (value :accessor value :initarg :value :initform nil)))

(defmethod print-object ((key-value-pair key-value-pair) stream)
  (print-unreadable-object (key-value-pair stream :identity t :type t)
    (format stream ":KEY ~S :VALUE: ~S"
	    (slot-value key-value-pair 'key)
	    (slot-value key-value-pair 'value))))

(defmethod add-key-value-pair ((data-context data-context) (key-value-pair key-value-pair))
  (push key-value-pair (elements data-context)))

(defun destructure-item-pricing-query-data-context (data-context-from-json-structure)
  (let ((data-context (make-instance 'data-context))
	(env nil))
    (loop for element in data-context-from-json-structure
       do
	 (multiple-value-bind (key value)
	     (destructure-body-element element)
	   (let ((upcased-key (string-upcase key)))
	     (cond
	       ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ENVIRONMENT+)
		(setq env (make-instance 'key-value-pair :key (string-downcase key) :value value)))
	       (t nil))
	     )))
    (if env
	(add-key-value-pair data-context env))
    data-context))

(defun destructure-item-pricing-query-mat-pricing-items-in (items-data-list-from-json-structure)
  (let ((servb-mat-pricing-items-in (make-instance 'tb.gw.sap-servb::servb-mat-pricing-items-in)))
    (loop for item in items-data-list-from-json-structure
       do
	 (let ((sales-material-group   nil)
	       (price                  nil)
	       (pricing-condition-type nil)
	       (uom                    nil)
	       (quantity               nil)
	       (item-nr                nil)
	       (item-version           nil)
	       (item-revision          nil)
	       (value-stream-code      nil)
	       (pos-nr                 nil))
	   (loop for element in item
	      do
		(multiple-value-bind (key value)
		    (destructure-body-element element)
		  (let ((upcased-key (string-upcase key)))
		    (cond
		      ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-SALES-MATERIAL-GROUP+)
		       (setq sales-material-group value))
		      ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEM-PRICE+)
		       (setq price value))
		      ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEM-PRICING-CONDITION-TYPE+)
		       (setq pricing-condition-type value))
		      ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEM-UOM+)
		       (setq uom value))
		      ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEM-QUANTITY+)
		       (setq quantity value))
		      ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEM-ITEM-NR+)
		       (setq item-nr value))
		      ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEM-ITEM-VERSION+)
		       (setq item-version value))
		      ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEM-ITEM-REVISION+)
		       (setq item-revision value))
		      ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEM-VALUE-STREAM-CODE+)
		       (setq value-stream-code value))
		      ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEM-POS-NR+)
		       (setq pos-nr value))
		      (t nil))
		    )))
	   (let ((servb-mat-pricing-item (make-instance 'tb.gw.sap-servb:servb-mat-pricing-item
							:pos-nr pos-nr :item-nr item-nr :item-version item-version :item-revision item-revision :value-stream-code value-stream-code
							:quantity quantity :uom uom :pricing-condition-type pricing-condition-type :price price :sales-material-group sales-material-group)))
	     (tb.gw.sap-servb:add-mat-pricing-item servb-mat-pricing-items-in servb-mat-pricing-item)
	     )))
    servb-mat-pricing-items-in))

;; +++ QUERY ITEM-PRICING +++

(tb.msgs:define-msg-extended-content-destructure-method :item-pricing :query
  (let ((item-pricing-query-request (make-instance 'item-pricing-query-request))
	(data-context               nil)
	(order-type                 nil)
	(sales-org                  nil)
	(distribution-channel       nil)
	(business-division          nil)
	(business-partner-role      nil)
	(business-partner-nr        nil)
	(price-date                 nil)
	(mat-pricing-items-in       nil))

    (loop for element-level-1 in (tb.msgs:data-as-json tb.msgs:msg-extended-content)
       do
	 (multiple-value-bind (key value)
	     (destructure-body-element element-level-1)
	   (log:debug "CONTENT DESTRUCTURING: ELEMENT-LEVEL-1 = ~S, KEY = ~S, VALUE = ~S" element-level-1 key value)

	   (loop for element in element-level-1
	      do
		(multiple-value-bind (key value)
		    (destructure-body-element element)
		  (log:debug "CONTENT DESTRUCTURING: ELEMENT = ~S, KEY = ~S, VALUE = ~S" element key value)
		  (let ((upcased-key (string-upcase key)))
		    (cond
		      ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ORDER-TYPE+)
		       (log:debug "CONTENT DESTRUCTURING: Item Pricing Query => Order Type: ~S" value)
		       (setq order-type value))
		      ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-SALES-ORG+)
		       (log:debug "CONTENT DESTRUCTURING: Item Pricing Query => Sales Org: ~S" value)
		       (setq sales-org value))
		      ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-DISTRIBUTION-CHANNEL+)
		       (log:debug "CONTENT DESTRUCTURING: Item Pricing Query => Distribution Channel: ~S" value)
		       (setq distribution-channel value))
		      ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-BUSINESS-DIVISION+)
		       (log:debug "CONTENT DESTRUCTURING: Item Pricing Query => Business Division: ~S" value)
		       (setq business-division value))
		      ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-BUSINESS-PARTNER-ROLE+)
		       (log:debug "CONTENT DESTRUCTURING: Item Pricing Query => Business Partner Role: ~S" value)
		       (setq business-partner-role value))
		      ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-CUSTOMER-NR+)
		       (log:debug "CONTENT DESTRUCTURING: Item Pricing Query => Customer Nr: ~S" value)
		       (setq business-partner-nr value))
		      ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-PRICE-DATE+)
		       (log:debug "CONTENT DESTRUCTURING: Item Pricing Query => Price Date: ~S" value)
		       (setq price-date value))
		      ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-ITEMS+)
		       (log:debug "CONTENT DESTRUCTURING: Item Pricing Query => Items: ~S" value)
		       (setq mat-pricing-items-in (destructure-item-pricing-query-mat-pricing-items-in value)))
		      ((string= upcased-key +TB-EXTENDED-MSG-KEY-STRING-ITEM-PRICING-QUERY-DATA-CONTEXT+)
		       (log:debug "CONTENT DESTRUCTURING: Item Pricing Query => Data Context: ~S" value)
		       (setq data-context (destructure-item-pricing-query-data-context value)))
		      (t nil)
		      ))))

	   (if mat-pricing-items-in
	       (progn
		 (setf (tb.gw.sap-servb:data-context          mat-pricing-items-in) data-context)
		 (setf (tb.gw.sap-servb:order-type            mat-pricing-items-in) order-type)
		 (setf (tb.gw.sap-servb:sales-org             mat-pricing-items-in) sales-org)
		 (setf (tb.gw.sap-servb:distribution-channel  mat-pricing-items-in) distribution-channel)
		 (setf (tb.gw.sap-servb:division              mat-pricing-items-in) business-division)
		 (setf (tb.gw.sap-servb:partner-role          mat-pricing-items-in) business-partner-role)
		 (setf (tb.gw.sap-servb:business-partner-nr   mat-pricing-items-in) business-partner-nr)
		 (setf (tb.gw.sap-servb:price-date            mat-pricing-items-in) price-date)))
	   (setf (servb-mat-pricing-items-in item-pricing-query-request) mat-pricing-items-in)
	   ))
    item-pricing-query-request))

;; +++ REQUEST SALES-ORDER CREATE +++

(tb.msgs:define-msg-extended-content-destructure-method :sales-order :create
  (let ((data-as-json (tb.msgs:data-as-json tb.msgs:msg-extended-content)))
    (log:debug "About to call make-tb-request-sales-order-create-from-json: data-as-json = ~S" data-as-json)
    (tb.msgs:make-tb-request-sales-order-create-from-json data-as-json)))

;; +++ REQUEST SALES-ORDER UPDATE +++

(tb.msgs:define-msg-extended-content-destructure-method :sales-order :update
  (let ((data-as-json (tb.msgs:data-as-json tb.msgs:msg-extended-content)))
    (log:debug "About to call make-tb-request-sales-order-update-from-json: data-as-json = ~S" data-as-json)
    (tb.msgs:make-tb-request-sales-order-update-from-json data-as-json)))

;; +++ INFORM SALES-ORDER-STATUS UPDATE +++

(tb.msgs:define-msg-extended-content-destructure-method :sales-order-status :update
  (let ((data-as-json (tb.msgs:data-as-json tb.msgs:msg-extended-content)))
    (log:debug "About to call make-tb-inform-sales-order-status-update-from-json: data-as-json = ~S" data-as-json)
    (tb.msgs:make-tb-inform-sales-order-status-update-from-json data-as-json)))

;; =========

(defun destructure-and-make-content (element-data)
  (let ((object-class "")
	(operation "")
	(data-as-json nil))
    (loop for element in element-data
       do
	 (multiple-value-bind (key value)
	     (destructure-body-element element)
	   (let ((upcased-key (string-upcase key)))
	     (cond
	       ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-CONTENT-OBJECT-CLASS+)
		(setq object-class (string-upcase value)))
	       ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-CONTENT-OPERATION+)
		(setq operation (string-upcase value)))
	       ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-CONTENT-DATA+)
		(setq data-as-json value))
	       (t nil)
	       ))))
    (let ((content nil))
      (setq content (tb.msgs:make-msg-extended-content object-class operation nil nil data-as-json))
      (setf (tb.msgs:data content) (tb.msgs:destructure-and-make-content-data content))
      content)))

(defun make-msg-requestor-context-user-id-passcode (credentials-json-string)
  (let ((user-id "")
	(passcode ""))
    (loop for element in credentials-json-string
       do
	 (multiple-value-bind (key value)
	     (destructure-body-element element)
	   (let ((upcased-key (string-upcase key)))
	     (cond
	       ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-CREDENTIALS-USER-ID+)
		(setq user-id value))
	       ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-CREDENTIALS-PASSCODE+)
		(setq passcode value))
	       ))))
    (make-instance 'tb.msgs:msg-requestor-context-user-id-passcode :user-id user-id :passcode passcode)))

(defun make-msg-requestor-context-username-password (credentials-json-string)
  (let ((username "")
	(password ""))
    (loop for element in credentials-json-string
       do
	 (multiple-value-bind (key value)
	     (destructure-body-element element)
	   (let ((upcased-key (string-upcase key)))
	     (cond
	       ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-CREDENTIALS-USERNAME+)
		(setq username value))
	       ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-CREDENTIALS-PASSWORD+)
		(setq password value))
	       ))))
    (make-instance 'tb.msgs:msg-requestor-context-username-password :username username :password password)))

(defun make-msg-requestor-context (kind-json-string credentials-json-string)
  (let ((upcased-kind-json-string (string-upcase kind-json-string)))
    (cond
      ((string= upcased-kind-json-string tb.msgs:+TB-EXTENDED-MSG-VALUE-STRING-REQUESTOR-CONTEXT-KIND-USER-ID-PASSCODE+)
       (make-msg-requestor-context-user-id-passcode credentials-json-string))
      ((string= upcased-kind-json-string tb.msgs:+TB-EXTENDED-MSG-VALUE-STRING-REQUESTOR-CONTEXT-KIND-USERNAME-PASSWORD+)
       (make-msg-requestor-context-username-password credentials-json-string))
      (t nil))))

(defun destructure-and-make-requestor-context (element-data)
  (let ((kind-json-string "")
	(credentials-json-string ""))
    (loop for element in element-data
       do
	 (multiple-value-bind (key value)
	     (destructure-body-element element)
	   (let ((upcased-key (string-upcase key)))
	     (cond
	       ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-CREDENTIALS+)
		(setq credentials-json-string value))
	       ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-REQUESTOR-CONTEXT-KIND+)
		(setq kind-json-string value))
	       (t nil)))))
    (make-msg-requestor-context kind-json-string credentials-json-string)))

(defun destructure-and-make-sender (element-data)
  (let ((app-id nil)
	(app-instance-id nil)
	(o nil)
	(ou nil)
	(support nil))
    (loop for element in element-data
       do
	 (multiple-value-bind (key value)
	     (destructure-body-element element)
	   (let ((upcased-key (string-upcase key)))
	     (cond
	       ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-SENDER-APP-ID+)
		(setq app-id value))
	       ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-SENDER-APP-INSTANCE-ID+)
		(setq app-instance-id value))
	       ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-SENDER-OU+)
		(setq ou value))
	       ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-SENDER-O+)
		(setq o value))
	       ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-SENDER-SUPPORT+)
		(setq support value))
	       (t nil)))))
    (tb.msgs:make-msg-sender app-id app-instance-id nil o ou support)))


(defun set-tb-extended-msg-value (tb-extended-msg key value)

  (check-type tb-extended-msg tb.msgs:extended-msg)

  (let ((upcased-key (string-upcase key)))

    (cond

      ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-CONTENT+)
       (setf (tb.msgs:content tb-extended-msg) (destructure-and-make-content value)))

      ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-REQUESTOR-CONTEXT+)
       (setf (tb.msgs:requestor-context tb-extended-msg) (destructure-and-make-requestor-context value)))

      ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-PERFORMATIVE+)
       (setf (tb.msgs:performative tb-extended-msg) value))

      ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-IN-REPLY-TO+)
       (setf (tb.msgs:in-reply-to tb-extended-msg) value))

      ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-REPLY-BY+)
       (setf (tb.msgs:reply-by tb-extended-msg) value))

      ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-REPLY-WITH+)
       (setf (tb.msgs:reply-with tb-extended-msg) value))

      ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-ENCODING+)
       (setf (tb.msgs:encoding tb-extended-msg) value))

      ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-SENDER+)
       (setf (tb.msgs:sender tb-extended-msg) (destructure-and-make-sender value)))

      ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-TIMESTAMP+)
       (setf (tb.msgs:timestamp tb-extended-msg) value))

      ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-PROTOCOL-VERSION+)
       (setf (tb.msgs:protocol-version tb-extended-msg) value))

      ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-PROTOCOL+)
       (setf (tb.msgs:protocol tb-extended-msg) value))

      ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-LANGUAGE+)
       (setf (tb.msgs:language tb-extended-msg) value))

      ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-CONVERSATION-ID+)
       (setf (tb.msgs:conversation-id tb-extended-msg) value))

      ((string= upcased-key tb.msgs:+TB-EXTENDED-MSG-KEY-STRING-QOS+)
       (setf (tb.msgs:qos tb-extended-msg) value))

      (t nil))
    ))


(defun destructure-body-element (element)
  (typecase element
    (cons (values (car element) (cdr element)))
    (list (values (first element) (second element)))
    (t element)))

(defun %request-body-to-tb-extended-msg (request-body-string)
  (let ((parsed-json (jonathan:parse request-body-string :as :alist))
	(tb-extended-msg (make-instance 'tb.msgs:extended-msg :msg-as-string request-body-string)))
    (loop for element in parsed-json
       do
	 (log:debug ":parsed-json ~S :element ~S" parsed-json element)
	 (multiple-value-bind (key value)
	     (destructure-body-element element)
	   (log:debug ":key ~S :value ~S" key value)
	   (set-tb-extended-msg-value tb-extended-msg key value)))
    tb-extended-msg))

(defun request-body-to-tb-extended-msg (request-body-string)
  (handler-case (%request-body-to-tb-extended-msg request-body-string)
    (error (caught-condition)
      (log:error "While parsing the request to JSON, condition ~S occured!" caught-condition)
      nil)))


;; (let* ((tb-msg-error-info (make-instance 'tb.msgs:msg-error-info
;; 					 :code (+ 10000 +HTTP-RESPONSE-CODE-BAD-REQUEST+)
;; 					 :level level
;; 					 :category :HTTP-REQUEST-METHOD-OR-JSON-REQUEST-OBJECT-CLASS-OR-REQUEST-OPERATION-ERROR))
;;        (tb-extended-msg-out (make-instance 'tb.msgs:extended-msg
;; 					   :conversation-id (format nil "~A" (tb.core:make-uuid))
;; 					   :encoding (tb.msgs:extended-msg-encoding)
;; 					   :protocol (tb.msgs:extended-msg-protocol)
;; 					   :protocol-version (tb.msgs:extended-msg-protocol-version)
;; 					   :in-reply-to (tb.msgs:conversation-id tb-extended-msg-in)
;; 					   :performative "inform"
;; 					   :sender (make-msg-sender-for-this-app-instance)
;; 					   :content nil
;; 					   :error-info-list (list tb-msg-error-info))))



(defun make-tb-extended-msg-for-error (&key code level category reason-code msg-nr text lang text-encoding source-conversation-id)
  (let* ((tb-msg-error-info (make-instance 'tb.msgs:msg-error-info
					   :code code
					   :level level
					   :category category
					   :reason-code reason-code
					   :msg-nr msg-nr
					   :msg (list (tb.msgs:make-msg-text lang text text-encoding))))
	 (tb-extended-msg-out (make-instance 'tb.msgs:extended-msg
					     :conversation-id (format nil "~A" (tb.core:make-uuid))
					     :encoding (tb.msgs:extended-msg-encoding)
					     :protocol (tb.msgs:extended-msg-protocol)
					     :protocol-version (tb.msgs:extended-msg-protocol-version)
					     :in-reply-to source-conversation-id
					     :performative "failure"
					     :sender (tb.msgs:make-msg-sender-for-this-app-instance)
					     :content nil
					     :error-info-list (list tb-msg-error-info))))
    tb-extended-msg-out))

(defmethod handle-tb-extended-msg-using-object-class-and-operation ((tb-extended-msg-in tb.msgs:extended-msg) method object-class operation)
  (make-tb-extended-msg-for-error :code (+ 10000 +HTTP-RESPONSE-CODE-BAD-REQUEST+)
				  :level :ERROR
				  :category :HTTP-REQUEST-METHOD-OR-JSON-REQUEST-OBJECT-CLASS-OR-REQUEST-OPERATION-ERROR
				  :reason-code  +TB-ERROR-REASON-CODE-INVALID-METHOD-OBJECT-CLASS-OPERATION-PARAMETER+
				  :msg-nr +TB-MSG-NR-INVALID-PARAMETER+
				  :text (format nil "Method ~A not allowed or unsupported combination of object-class (= ~A) and operation (= ~A)!" (tb.msgs:stringify-msg-value method) (tb.msgs:stringify-msg-value object-class) (tb.msgs:stringify-msg-value operation))
				  :lang "eng"
				  :text-encoding "UTF-8"
				  :source-conversation-id (tb.msgs:conversation-id tb-extended-msg-in)))

(defmacro define-tb-extended-msg-handler (tb-extended-msg-var method object-class-keyword operation-keyword &body body)
  `(defmethod handle-tb-extended-msg-using-object-class-and-operation ((,tb-extended-msg-var tb.msgs:extended-msg) (method (eql ,method)) (object-class (eql ,object-class-keyword)) (operation (eql ,operation-keyword)))
     (progn
       ,@body)))

(defun handle-tb-extended-msg (method tb-extended-msg)
  (let ((object-class (tb.msgs:object-class tb-extended-msg))
	(operation    (tb.msgs:operation    tb-extended-msg)))
    ;; (handle-tb-extended-msg-using-object-class-and-operation tb-extended-msg method object-class operation)
    (if (log:debug)
    	(handle-tb-extended-msg-using-object-class-and-operation tb-extended-msg method object-class operation)
    	(handler-case (handle-tb-extended-msg-using-object-class-and-operation tb-extended-msg method object-class operation)
    	  (error (caught-condition)
    	    (log:error "While handling the request, condition ~S occured!" caught-condition)
    	    nil)))
    ))

(defun make-configuration-error-msg (code source-conversation-id)
  (make-tb-extended-msg-for-error :code code
				  :level :ERROR
				  :category :THINGBONE-CONFIGURATION-ERROR
				  :msg-nr "TBC001E"
				  :reason-code "00000001"
				  :text "Configuration Error (THINGBONE CONFIGURATION ERROR) occured."
				  :lang "eng"
				  :text-encoding "iso-8859-1"
				  :source-conversation-id source-conversation-id))

(defun make-json-parsing-error-msg (source-conversation-id)
  (make-tb-extended-msg-for-error :code -1
				  :level :ERROR
				  :category :THINGBONE-RUNTIME-ERROR
				  :msg-nr "TBC002E"
				  :reason-code "00000001"
				  :text "Data error (JSON STRUCTURE ERROR) occured."
				  :lang "eng"
				  :text-encoding "iso-8859-1"
				  :source-conversation-id source-conversation-id))

(defun make-handle-request-error-msg (source-conversation-id)
  (make-tb-extended-msg-for-error :code -1
				  :level :ERROR
				  :category :THINGBONE-RUNTIME-ERROR
				  :msg-nr "TBC003E"
				  :reason-code "00000001"
				  :text "ERP error (THINGBONE INTERNAL ERROR) occured."
				  :lang "eng"
				  :text-encoding "iso-8859-1"
				  :source-conversation-id source-conversation-id))

;;; ==== REQUEST DEFINITION ====

;; +++ QUERY ITEM-PRICINMG +++

(define-tb-extended-msg-handler tb-extended-msg-in :get :item-pricing :query
  (tb.gw.sap-servb:ensure-servb-sap-rfc-client-connection-in-global-context)
  (multiple-value-bind (servb-mat-pricing-items-out servb-return-info)
      (tb.gw.sap-servb:sap-direct-mat-get-price (tb.core:global-context-value :sap-servb-client-rfc-connection)
						(servb-mat-pricing-items-in (tb.msgs:data (tb.msgs:content tb-extended-msg-in))))
    (let* ((tb-msg-error-info (tb.gw.sap-servb:servb-return-info-to-tb-msg-error-info servb-return-info))
	   (tb-extended-msg-out (make-instance 'tb.msgs:extended-msg
					       :conversation-id (format nil "~A" (tb.core:make-uuid))
					       :encoding (tb.msgs:extended-msg-encoding)
					       :protocol (tb.msgs:extended-msg-protocol)
					       :protocol-version (tb.msgs:extended-msg-protocol-version)
					       :in-reply-to (tb.msgs:conversation-id tb-extended-msg-in)
					       :performative "response"
					       :sender (tb.msgs::make-msg-sender-for-this-app-instance)
					       :content (make-instance 'tb.msgs:msg-extended-content
								       :object-class :item-pricing
								       :operation :query)
					       :error-info-list (list tb-msg-error-info))))
      (if (not (tb.gw.sap-servb:errorp servb-return-info))
	  (tb.gw.sap-servb:set-tb-extended-msg-content-data-from-servb-mat-pricing-items-out tb-extended-msg-out servb-mat-pricing-items-out))
      tb-extended-msg-out)))

;; +++ REQUEST SALES-ORDER CREATE +++

;; (tb.msgs:define-msg-extended-content-destructure-method :sales-order :create
;;   (tb.msgs:make-tb-request-sales-order-create-from-json (tb.msgs:data-as-json tb.msgs:msg-extended-content)))

;; ==== NON-FUNCTIONAL SOURCE CODE SECTION ===
;;
;;  THE FOLLOW COMMENTED LINES OF CODE ARE BASED ON THE "GATEWAY" PRINCIPLE. THERE ARE DESIGHN ISSUES WITH THAT
;;  PRINCIPLE: HAVING CONFIGURE THE GATEWAY BEFORE THE FUNCTION MODULES, FOR EXAMPLE. ALSO, THERE HAVE BEEN
;;  PERFORMACE ISSUES WITH NON-MEMOIZED RETRIEVING OF SAP RFC HANDLES.
;;
;;  frgo, 2019-10-26.

;; (define-tb-extended-msg-handler tb-extended-msg-request :post :sales-order :create
;;   (let ((gateway (tb.gw.sap-servb:find-servb-gateway-for-site (tb.core:tb-site))))
;;     (if (not gateway)
;; 	(make-configuration-error-msg +TB-ERROR-CODE-CONFIGURATION-ERROR-GATEWAY-NOT-FOUND-FOR-SITE+
;; 				      (tb.msgs:conversation-id tb-extended-msg-request))
;; 	(multiple-value-bind (tb-extended-msg-response servb-return-info)
;; 	    (tb.gw.sap-servb:sap-direct-request-sales-order-create gateway tb-extended-msg-request)
;; 	  (values tb-extended-msg-response servb-return-info)))))

;; === END OF NON-FUNCTIONAL CODE ===

(define-tb-extended-msg-handler extended-msg-request :post :sales-order :create
  (tb.gw.sap-servb:ensure-servb-sap-rfc-client-connection-in-global-context)
  (multiple-value-bind (extended-msg-response servb-return-info)
      (tb.gw.sap-servb:sap-direct-request-sales-order-create (tb.core:global-context-value :sap-servb-client-rfc-connection) extended-msg-request)
    (let ((error-info (tb.gw.sap-servb:servb-return-info-to-tb-msg-error-info servb-return-info)))
      (if extended-msg-response
	  (tb.msgs:add-error-info extended-msg-response error-info))
      (values extended-msg-response servb-return-info))))

;; +++ REQUEST SALES-ORDER UPDATE +++

(define-tb-extended-msg-handler extended-msg-request :post :sales-order :update
  (tb.gw.sap-servb:ensure-servb-sap-rfc-client-connection-in-global-context)
  (multiple-value-bind (extended-msg-response servb-return-info)
      (tb.gw.sap-servb:sap-direct-request-sales-order-update (tb.core:global-context-value :sap-servb-client-rfc-connection) extended-msg-request)
    (let ((error-info (tb.gw.sap-servb:servb-return-info-to-tb-msg-error-info servb-return-info)))
      (if extended-msg-response
	  (tb.msgs:add-error-info extended-msg-response error-info))
      (values extended-msg-response servb-return-info))))

;; +++ INFORM SALES-ORDER-STATUS UPDATE +++

(define-tb-extended-msg-handler extended-msg-inform :post :sales-order-status :update
  (tb.gw.sap-servb:ensure-servb-sap-rfc-client-connection-in-global-context)
  (multiple-value-bind (extended-msg-response servb-return-info)
      (tb.gw.sap-servb:sap-direct-inform-sales-order-status-update (tb.core:global-context-value :sap-servb-client-rfc-connection) extended-msg-inform)
    (let ((error-info (tb.gw.sap-servb:servb-return-info-to-tb-msg-error-info servb-return-info)))
      (if extended-msg-response
	  (tb.msgs:add-error-info extended-msg-response error-info))
      (values extended-msg-response servb-return-info))))


;;; ==== HTTP REQUEST HANDLING ===

;;; Design Notes:
;;;
;;; Inbound HTTP Reqquest -> TB Extended Msg (with Source Info set to
;;; Inbound HTTP Request -> Dispatch TB Extended Msg based on Object Class
;;; and Operation.
;;; Dispatcher: Check if Routing required. A Route specifies a destination
;;; to which the request is to be sent to.
;;; If no route is specified, handle the request locally.
;;; Routing: A Route is defined by using
;;; (define-tb-extended-msg-router 'object-class 'operation
;;;   (...))
;;; This is a macro that expands to:
;;; (defmethod route-using-object-clasa-and-operation ((object-class (eql 'object-class)) (operation (eql 'operation))
;;;   (...))
;;; If #'route returns nil the the message needs to handled #'by handle-tb-extended-msg


(defmethod route-tb-extended-msg-using-object-class-and-operation ((tb-extended-msg tb.msgs:extended-msg) object-class operation)
  (declare (ignore object-class operation))
  nil)

(defmacro define-tb-extended-msg-router (tb-extended-msg-var object-class-keyword operation-keyword &body body)
  `(defmethod route-tb-extended-msg-using-object-class-and-operation ((,tb-extended-msg-var tb.msgs:extended-msg) (object-class (eql ,object-class-keyword)) (operation (eql ,operation-keyword)))
     (progn
       ,@body)))


(defmethod route-tb-extended-msg ((tb-extended-msg tb.msgs:extended-msg))
  (let ((object-class (tb.msgs:object-class tb-extended-msg))
	(operation    (tb.msgs:operation    tb-extended-msg)))
    (route-tb-extended-msg-using-object-class-and-operation tb-extended-msg object-class operation)))


(defun handle-http-request (req ent)

  (let* ((method (net.aserve:request-method req))
	 (request-socket (net.aserve:request-socket req))
	 (request-body (net.aserve:get-request-body req)))

    (log:info "Received request on ~S from ~S port ~S with method ~S."
	      (net.aserve:request-uri req)
	      (socket:ipaddr-to-dotted (socket:remote-host request-socket))
	      (socket:remote-port request-socket)
	      method)
    (log:info "Request body: ~A" request-body)

    (let* ((tb-extended-msg-response nil)
	   (tb-extended-msg-request (request-body-to-tb-extended-msg request-body))
	   (object-class (if tb-extended-msg-request
			     (tb.msgs:object-class tb-extended-msg-request)))
	   (operation    (if tb-extended-msg-request
			     (tb.msgs:operation tb-extended-msg-request))))

      (log:info "Request details: Object Class  ~S - Operation ~S."
		object-class
		operation)

      (if tb-extended-msg-request
	  (progn
	    (setq tb-extended-msg-response (route-tb-extended-msg tb-extended-msg-request))
	    (if (not tb-extended-msg-response)
		(let ((result (handle-tb-extended-msg method tb-extended-msg-request)))
		  (if result
		      (setq tb-extended-msg-response result)
		      (setq tb-extended-msg-response (make-handle-request-error-msg (tb.msgs:conversation-id tb-extended-msg-request)))))))
	  (setq tb-extended-msg-response (make-json-parsing-error-msg nil)))

      (let* ((json-string (jonathan:to-json tb-extended-msg-response))
	     (errorp (or (not tb-extended-msg-response)
			 (tb.msgs:errorp tb-extended-msg-response)))
	     (response-type (if errorp ;; TODO: Make more accurate error return code being given back as HTTP return code
				net.aserve:*response-bad-request*
				net.aserve:*response-ok*)))
	(net.aserve:with-http-response (req ent :response response-type :timeout 60)
	  (net.aserve:with-http-body (req ent)
	    (format (net.aserve:request-reply-stream req) "~A" json-string)))))))

(defun thingbone-v3-http-gateway ()
  (net.aserve:publish :path "/thingbone/api/gateway/v3"
		      :content-type "application/json"
		      :function
		      #'(lambda (req ent)
			  (handle-http-request req ent))))

(defun stop-aserve ()
  (net.aserve:shutdown))


(defun test-aserve ()
  (stop-aserve)
  (net.aserve::debug-on :notrap :xmit)
  (run-aserve-simple)
  (publish-json-schema-files)
  (thingbone-v3-http-gateway))

(defun start-restful-api ()
  (stop-aserve)
  (run-aserve-simple)
  (publish-json-schema-files)
  (thingbone-v3-http-gateway))



;;; -----------------------------------------------------------------------------
;;;   THINGBONE HTTP REQUEST & RESPONSE
;;; -----------------------------------------------------------------------------

;;; HTTP RESPONSE CODES
;;; See http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml

(defconstant +HTTP-RESPONSE-CODE-CONTINUE+                        100)
(defconstant +HTTP-RESPONSE-CODE-SWITCHING-PROTOCOLS+             101)

(defconstant +HTTP-RESPONSE-CODE-OK+                              200)

(defconstant +HTTP-RESPONSE-CODE-BAD-REQUEST+                     400)
(defconstant +HTTP-RESPONSE-CODE-UNAUTHORIZED+                    401)
(defconstant +HTTP-RESPONSE-CODE-FORBIDDEN+                       403)
(defconstant +HTTP-RESPONSE-CODE-NOT-FOUND+                       404)
(defconstant +HTTP-RESPONSE-CODE-METHOD-NOT-ALLOWED+              405)
(defconstant +HTTP-RESPONSE-CODE-NOT-ACCEPTABLE+                  406)
(defconstant +HTTP-RESPONSE-CODE-PROXY-AUTHORIZATION-REQUIRED+    407)
(defconstant +HTTP-RESPONSE-CODE-REQUEST-TIMED-OUT+               408)
(defconstant +HTTP-RESPONSE-CODE-CONFLICT+                        409)
(defconstant +HTTP-RESPONSE-CODE-GONE+                            410)
(defconstant +HTTP-RESPONSE-CODE-LENGTH-REQUIRED+                 411)
(defconstant +HTTP-RESPONSE-CODE-PRECONDITION-FAILED+             412)

(defconstant +HTTP-RESPONSE-CODE-INTERNAL-SERVER-ERROR+           500)
(defconstant +HTTP-RESPONSE-CODE-NOT-IMPLEMEMTED+                 501)
(defconstant +HTTP-RESPONSE-CODE-SERVICE-UNAVAILABLE+             503)


(declaim (inline http-code-informational-p))
(defun http-code-informational-p (code)
  (= (floor (/ code 100)) 1))

(declaim (inline http-code-success-p))
(defun http-code-success-p (code)
  (= (floor (/ code 100)) 2))

(declaim (inline http-code-redirection-p))
(defun http-code-redirection-p (code)
  (= (floor (/ code 100)) 3))

(declaim (inline http-code-client-error-p))
(defun http-code-client-error-p (code)
  (= (floor (/ code 100)) 4))

(declaim (inline http-code-server-error-p))
(defun http-code-server-error-p (code)
  (= (floor (/ code 100)) 5))


(defparameter *tb-http-request-timeout* 60)

(defun tb-http-request-timeout ()
  *tb-http-request-timeout*)

(defclass tb-http-request ()
  ((direction      :accessor direction      :initarg :direction      :initform nil)
   (url            :accessor url            :initarg :url            :initform nil)
   (request-method :accessor request-method :initarg :request-method :initform nil)
   (content-type   :accessor content-type   :initarg :content-type   :initform "application/json")
   (timeout        :accessor timeout        :initarg :timeout        :initform (tb-http-request-timeout))
   (keep-alive     :accessor keep-alive     :initarg :keep-alive     :initform nil)
   (connection     :accessor connection     :initarg :connection     :initform nil)
   (headers        :accessor headers        :initarg :headers        :initform nil)
   (content        :accessor content        :initarg :content        :initform "")))

(defmethod print-object ((tb-http-request tb-http-request) stream)
  (print-unreadable-object (tb-http-request stream :identity t :type t)
    (format stream ":DIRECTION ~S :URL ~S :REQUEST-METHOD ~S :CONTENT-TYPE ~S :TIMEOUT ~S :KEEP-ALIVE ~S :CONNECTION ~S :HEADERS ~S :CONTENT ~A"
	    (direction tb-http-request)
	    (url tb-http-request) (request-method tb-http-request)
	    (content-type tb-http-request) (timeout tb-http-request)
	    (keep-alive tb-http-request) (connection tb-http-request)
	    (headers tb-http-request) (content tb-http-request))))

(defclass tb-http-request-response ()
  ((headers    :accessor headers    :initarg :headers    :initform nil)
   (body       :accessor body       :initarg :body       :initform nil)
   (code       :accessor code       :initarg :code       :initform nil)
   (uri-object :accessor uri-object :initarg :uri-object :initform nil)
   (connection :accessor connection :initarg :connection :initform nil)))

(defmethod print-object ((tb-http-request-response tb-http-request-response) stream)
  (print-unreadable-object (tb-http-request-response stream :identity t :type t)
    (format stream ":BODY ~A :RESPONSE-CODE ~S :HEADERS ~S :URI-OBJECT ~S :CONNECTION ~S"
	    (body tb-http-request-response)
	    (code tb-http-request-response)
	    (headers tb-http-request-response)
	    (uri-object tb-http-request-response)
	    (connection tb-http-request-response)
	    )))

(defmethod do-request ((tb-http-request tb-http-request))
  (multiple-value-bind (response-body response-code response-headers uri-object connection)
      (net.aserve.client:do-http-request (url tb-http-request)
	:method          (request-method tb-http-request)
	:external-format :utf8
	:content-type    (content-type tb-http-request)
	:content         (content tb-http-request)
	:timeout         (timeout tb-http-request)
	:keep-alive      (keep-alive tb-http-request)
	:compress        t
	:connection      (connection tb-http-request)
	:headers         (headers tb-http-request))
    (make-instance 'tb-http-request-response
		   :body response-body
		   :code response-code
		   :headers response-headers
		   :uri-object uri-object
		   :connection connection)))


;;; -----------------------------------------------------------------------------
;;;   INTEGRATION LAYER OUTBOUND INTERFACE
;;; -----------------------------------------------------------------------------

(defparameter *il-hostname-or-ip-address*       "integration-layer.bwpapersystems.com")
(defparameter *il-env*                          "/test")
(defparameter *il-username*                     "bwil_app")
(defparameter *il-password*                     "klsdjf3jfj4wjhep47sdf34h")
(defparameter *il-client-request-content-type*  "application/json")
(defparameter *il-http-request-timeout*         60)

(defparameter *il-authorization-context*        nil)
(defparameter *lock-il-authorization-context*   (bt:make-recursive-lock))

(defparameter *il-api*                          "/erpws/api/erp/import")

(defparameter *il-login-http-request-timeout*   *il-http-request-timeout*)
(defparameter *il-login-default-grace-period*   1200)

(defparameter *il-api-login*                    "/uaa/oauth/token")

(defparameter *il-service-username*             "userERP")
(defparameter *il-service-password*             "jsdkh32623gfgsjKK")
(defparameter *il-service-grant-type*           "password")
(defparameter *il-service-scope*                "openid")

(defun il-http-request-timeout ()
  *il-http-request-timeout*)

;;; === Integration Layer URLs ===

(defun il-api-url-login ()
  (concatenate 'string
	       "https://" *il-username* ":" *il-password* "@" *il-hostname-or-ip-address*
	       ""
	       *il-api-login*))

(defun il-api-url-item-master ()
  (concatenate 'string
	       "https://" *il-hostname-or-ip-address* ;; ":" *il-port*
	       ""
	       *il-api*))

(defun il-api-url-inventory-master ()
  (concatenate 'string
	       "https://" *il-hostname-or-ip-address* ;; ":" *il-port*
	       ""
	       *il-api*))

(defun il-api-url-inventory-update ()
  (concatenate 'string
	       "https://" *il-hostname-or-ip-address* ;; ":" *il-port*
	       ""
	       *il-api*))

(defun il-api-url-sales-info ()
  (concatenate 'string
	       "https://" *il-hostname-or-ip-address* ;; ":" *il-port*
	       ""
	       *il-api*))

(defun il-api-url-business-partner-master ()
  (concatenate 'string
	       "https://" *il-hostname-or-ip-address* ;; ":" *il-port*
	       ""
	       *il-api*))

(defun il-api-url-sales-order-status-update ()
  (concatenate 'string
	       "https://" *il-hostname-or-ip-address* ;; ":" *il-port*
	       ""
	       "/salesorderservice/api/order-sap-update"))

(defun il-username ()
  *il-username*)

(defun il-password ()
  *il-password*)


(defun il-login-query ()
  (list (cons "mode"       "text")
	(cons "username"   *il-service-username*)
	(cons "password"   *il-service-password*)
	(cons "grant_type" *il-service-grant-type*)
	(cons "scope"      *il-service-scope*)))

(defun il-login-body ()
  (let ((query (il-login-query)))
    (net.aserve:query-to-form-urlencoded query :external-format :utf8)))

(defun il-login-timeout ()
  *il-login-http-request-timeout*)

(defun il-login-default-grace-period ()
  *il-login-default-grace-period*)

(defclass il-http-request (tb-http-request)
  ())

(defmethod print-object ((il-http-request il-http-request) stream)
  (print-unreadable-object (il-http-request stream :identity t :type t)
    (format stream ":URL ~S :REQUEST-METHOD ~S :CONTENT-TYPE ~S :TIMEOUT ~S :KEEP-ALIVE ~S :CONNECTION ~S :HEADERS ~S :CONTENT ~S :AUTHORIZATION-CONTEXT ~S"
	    (url il-http-request) (request-method il-http-request)
	    (content-type il-http-request) (timeout il-http-request)
	    (keep-alive il-http-request) (connection il-http-request)
	    (headers il-http-request) (content il-http-request)
	    (authorization-context il-http-request))))

(defclass il-authorization-context ()
  ((access-token              :accessor access-token              :initarg :access-token              :initform nil)
   (refresh-token             :accessor refresh-token             :initarg :refresh-token             :initform nil)
   (token-type                :accessor token-type                :initarg :token-type                :initform nil)
   (expires-in                :accessor expires-in                :initarg :expires-in                :initform nil)
   (scope                     :accessor scope                     :initarg :scope                     :initform nil)
   (iat                       :accessor iat                       :initarg :iat                       :initform nil)
   (jti                       :accessor jti                       :initarg :jti                       :initform nil)
   (last-login-response-code  :accessor last-login-response-code  :initarg :last-login-response-code  :initform +HTTP-RESPONSE-CODE-UNAUTHORIZED+)
   (last-login-timestamp      :accessor last-login-timestamp      :initarg :last-login-timestamp      :initform nil)
   (last-login-universal-time :accessor last-login-universal-time :initarg :last-login-universal-time :initform nil)))

(defmethod print-object ((il-authorization-context il-authorization-context) stream)
  (print-unreadable-object (il-authorization-context stream :identity t :type t)
    (format stream ":ACCESS-TOKEN ~S :REFRESH-TOKEN ~S :TOKEN-TYPE ~S :EXPIRES-IN ~S :SCOPE ~S :IAT ~S :JTI ~S :LAST-LOGIN-RESPONSE-CODE ~S :LAST-LOGIN-TIMESTAMP ~S :LAST-LOGIN-INIVERSAL-TIME ~S"
	    (access-token              il-authorization-context)
	    (refresh-token             il-authorization-context)
	    (token-type                il-authorization-context)
	    (expires-in                il-authorization-context)
	    (scope                     il-authorization-context)
	    (iat                       il-authorization-context)
	    (jti                       il-authorization-context)
	    (last-login-response-code  il-authorization-context)
	    (last-login-timestamp      il-authorization-context)
	    (last-login-universal-time il-authorization-context))))

(defmethod access-token-expired-p ((self il-authorization-context))
  (> (- (get-universal-time) (+ (last-login-universal-time self) (expires-in self))) 0))

(defun il-authorization-context ()
  (bt:with-recursive-lock-held (*lock-il-authorization-context*)
    *il-authorization-context*))

(defun set-il-authorization-context (authorization-context)
  (bt:with-recursive-lock-held (*lock-il-authorization-context*)
    (setq *il-authorization-context* authorization-context)))


(defclass il-outbound-http-request (il-http-request)
  ((authorization-context :accessor authorization-context :initarg :authorization-context :initform nil))
  (:default-initargs
   :request-method :put
    :timeout (il-http-request-timeout)
    :authorization-context (il-authorization-context)
    ))

(defmethod set-authorization-header ((il-outbound-http-request il-outbound-http-request))
  (let ((headers (list (cons "Authorization" (concatenate 'string "Bearer " (access-token (authorization-context il-outbound-http-request)))))))
    (setf (headers il-outbound-http-request) headers)))

(defmethod login-required-p ((request il-outbound-http-request))
  (let* ((authorization-context (authorization-context request))
	 (login-required-p (or (not authorization-context)
			       (access-token-expired-p authorization-context))))
    (log:info "login required? ~S" login-required-p)
    login-required-p))

(defmethod authorization-required-p ((tb-http-request-response tb-http-request-response))
  (eql (code tb-http-request-response) +HTTP-RESPONSE-CODE-UNAUTHORIZED+))

;;; === HTTP REQUESTS TO INTEGRATION LAYER ===

(defclass il-outbound-request-item-master (il-outbound-http-request)
  ()
  (:default-initargs
   :direction :outbound
    :url (il-api-url-item-master)
    ))

(defclass il-outbound-request-inventory-master (il-outbound-http-request)
  ()
  (:default-initargs
   :direction :outbound
    :url (il-api-url-inventory-master)
    ))

(defclass il-outbound-request-inventory-update (il-outbound-http-request)
  ()
  (:default-initargs
   :direction :outbound
    :url (il-api-url-inventory-update)
    ))

(defclass il-outbound-request-sales-info (il-outbound-http-request)
  ()
  (:default-initargs
   :direction :outbound
    :url (il-api-url-sales-info)
    ))

(defclass il-outbound-request-business-partner-master (il-outbound-http-request)
  ()
  (:default-initargs
   :direction :outbound
    :url (il-api-url-business-partner-master)
    ))

(defclass il-outbound-request-sales-order-status-update (il-outbound-http-request)
  ()
  (:default-initargs
   :request-method :post
    :direction :outbound
    :url (il-api-url-sales-order-status-update)
    ))

;;; === HTTP REQUEST EXECUTION ===

;; (defun send-il-login-request ()
;;   (let ((body (il-login-body))
;; 	(url (il-api-url-login)))
;;     (net.aserve.client:do-http-request url
;;       :method :post
;;       :content-type "application/x-www-form-urlencoded"
;;       :accept "application/json"
;;       :external-format :utf8
;;       :content body
;;       :timeout (il-login-timeout))))

(defun send-il-login-request ()
  (let ((login-query (il-login-query)))
    (log:info "IL login query: ~S" login-query)
    (net.aserve.client:do-http-request (il-api-url-login)
      :method :post
      :content-type "application/x-www-form-urlencoded"
      :accept "application/json"
      :external-format :utf8
      :format :text
      :compress t
      :query login-query
      :timeout (il-login-timeout))))

(defun %do-il-login ()
  (let ((authorization-context nil))
    (log:info "Doing login into Integration Layer ...")
    (multiple-value-bind (response-body response-code response-headers uri-object socket)
	(send-il-login-request)
      (log:info "Response for IL LOGIN: response-body = ~A, response-code = ~S, response-headers = ~S, uri-object = ~S, socket = ~S"
		response-body response-code response-headers uri-object socket)
      (if (eql response-code 200)
	  (let ((json-body-object (jonathan:parse response-body))
		(access-token     nil)
		(refresh-token    nil)
		(token-type       nil)
		(expires-in       nil)
		(scope            nil)
		(iat              nil)
		(jti              nil))
	    (do ((object json-body-object))
		((not object))
	      (destructuring-bind (key value &rest rest)
		  object
		(cond
		  ((string= "ACCESS_TOKEN" (string-upcase (princ-to-string key)))
		   (setq access-token value))
		  ((string= "REFRESH_TOKEN" (string-upcase (princ-to-string key)))
		   (setq refresh-token value))
		  ((string= "TOKEN_TYPE" (string-upcase (princ-to-string key)))
		   (setq token-type value))
		  ((string= "EXPIRES_IN" (string-upcase (princ-to-string key)))
		   (setq expires-in value))
		  ((string= "SCOPE" (string-upcase (princ-to-string key)))
		   (setq scope value))
		  ((string= "IAT" (string-upcase (princ-to-string key)))
		   (setq iat value))
		  ((string= "JTI" (string-upcase (princ-to-string key)))
		   (setq jti value))
		  (t (setq rest nil)))
		(setq object rest)))
	    (let ((login-universal-time (- (get-universal-time) 1)))
	      (setq authorization-context (make-instance 'il-authorization-context
							 :last-login-universal-time login-universal-time
							 :access-token  access-token
							 :refresh-token refresh-token
							 :token-type token-type
							 :expires-in expires-in
							 :scope scope
							 :iat iat
							 :jti jti
							 :last-login-response-code response-code
							 :last-login-timestamp (make-iso8601-timestamp-string :time-value login-universal-time :include-timezone-p t)))
	      ))))
    authorization-context))

(defmethod do-il-login ((il-outbound-http-request il-outbound-http-request))
  (let ((authorization-context (%do-il-login)))
    (setf (authorization-context il-outbound-http-request) authorization-context)
    authorization-context))

(defmethod maybe-do-il-login ((il-outbound-http-request il-outbound-http-request))
  (let ((global-authorization-context (il-authorization-context))
	(request-authorization-context (authorization-context il-outbound-http-request)))

    (if (and global-authorization-context
	     (not request-authorization-context))
	(setf (authorization-context il-outbound-http-request) global-authorization-context))

    (if (login-required-p il-outbound-http-request)
	(set-il-authorization-context (do-il-login il-outbound-http-request))))
  (il-authorization-context))

(defmethod do-request :before ((il-outbound-http-request il-outbound-http-request))
  (maybe-do-il-login il-outbound-http-request))

(defmethod do-request ((il-outbound-http-request il-outbound-http-request))
  (set-authorization-header il-outbound-http-request)
  (call-next-method))

;;; REQUEST IMPLEMENTATION

;; Possible/ allowed failure strings to be sent to SAP SERVB
;;
;; SUCCESSFULLY-SENT
;; SERVICE-FAILURE
;; THINGBONE-FAILURE
;; OTHER-FAILURE

(declaim (inline %http-return-code-to-sap-servb-status-string))
(defun %http-return-code-to-sap-servb-status-string (return-code)
  (cond
    ((http-code-success-p return-code)           "SUCCESSFULLY-SENT")
    ((or (http-code-redirection-p  return-code)
	 (http-code-client-error-p return-code)
	 (http-code-server-error-p return-code)) "SERVICE-FAILURE")
    (t "OTHER-FAILURE" )))

(declaim (inline %handle-il-outbound-request-response))
(defun %handle-il-outbound-request-response (tb-http-request-response tb-extended-msg)
  (let ((qos (tb.msgs:qos tb-extended-msg)))
    (if (and qos
	     (tb.msgs:request-status-response-required-p qos))
	(let ((conversation-id (tb.msgs:conversation-id tb-extended-msg))
	      (status (%http-return-code-to-sap-servb-status-string (code tb-http-request-response))))
	  (tb.core:send (tb.gw.sap-servb:sap-servb-inbound-channel)
			(tb.gw.sap-servb:make-tb-sap-servb-set-outb-req-state-msg conversation-id status)))))
  (values))

(declaim (inline %exec-request))
(defun %exec-request (request tb-extended-msg &optional (retry-count 0))
  (log:info "Executing request of type ~S, retry-count ~S." (class-name (class-of request)) retry-count)
  (let ((response (do-request request)))
    (log:info "Got response: ~S." response)
    (cond
      ((and (< retry-count 2)
	    (= 401 (code response)))
       (set-il-authorization-context nil)
       (setf (authorization-context request) nil)
       (incf retry-count)
       (log:info "Retrying request with new login. Retry count now ~S ..." retry-count)
       (%exec-request request tb-extended-msg retry-count))
      (t
       (%handle-il-outbound-request-response response tb-extended-msg))))

  (values))

(tb.msgs:define-extended-msg-dispatch-method :item :create
  (let* ((content (tb.msgs:as-string tb.msgs:extended-msg))
	 (request (make-instance 'il-outbound-request-item-master
				 :content content)))
    (%exec-request request tb.msgs:extended-msg)))

(tb.msgs:define-extended-msg-dispatch-method :item :update
  (let* ((content (tb.msgs:as-string tb.msgs:extended-msg))
	 (request (make-instance 'il-outbound-request-item-master
				 :content content)))
    (%exec-request request tb.msgs:extended-msg)))

(tb.msgs:define-extended-msg-dispatch-method :inventory-master :create
  (let* ((content (tb.msgs:as-string tb.msgs:extended-msg))
	 (request (make-instance 'il-outbound-request-inventory-master
				 :content content)))
    (%exec-request request tb.msgs:extended-msg)))

(tb.msgs:define-extended-msg-dispatch-method :inventory-update :create
  (let* ((content (tb.msgs:as-string tb.msgs:extended-msg))
	 (request (make-instance 'il-outbound-request-inventory-update
				 :content content)))
    (%exec-request request tb.msgs:extended-msg)))

(tb.msgs:define-extended-msg-dispatch-method :inventory-update :update
  (let* ((content (tb.msgs:as-string tb.msgs:extended-msg))
	 (request (make-instance 'il-outbound-request-inventory-update
				 :content content)))
    (%exec-request request tb.msgs:extended-msg)))

(tb.msgs:define-extended-msg-dispatch-method :sales-info :create
  (let* ((content (tb.msgs:as-string tb.msgs:extended-msg))
	 (request (make-instance 'il-outbound-request-sales-info
				 :content content)))
    (%exec-request request tb.msgs:extended-msg)))

(tb.msgs:define-extended-msg-dispatch-method :sales-info :update
  (let* ((content (tb.msgs:as-string tb.msgs:extended-msg))
	 (request (make-instance 'il-outbound-request-sales-info
				 :content content)))
    (%exec-request request tb.msgs:extended-msg)))

(tb.msgs:define-extended-msg-dispatch-method :business-partner :create
  (let* ((content (tb.msgs:as-string tb.msgs:extended-msg))
	 (request (make-instance 'il-outbound-request-business-partner-master
				 :content content)))
    (%exec-request request tb.msgs:extended-msg)))

(tb.msgs:define-extended-msg-dispatch-method :business-partner :update
  (let* ((content (tb.msgs:as-string tb.msgs:extended-msg))
	 (request (make-instance 'il-outbound-request-business-partner-master
				 :content content)))
    (%exec-request request tb.msgs:extended-msg)))

(tb.msgs:define-extended-msg-dispatch-method :sales-order-status :update
  (let* ((content (tb.msgs:as-string tb.msgs:extended-msg))
	 (request (make-instance 'il-outbound-request-sales-order-status-update
				 :content content)))
    (%exec-request request tb.msgs:extended-msg)))
