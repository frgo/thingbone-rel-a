;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  GATEWAY RESTFUL API
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(defpackage "THINGBONE.ASERVE-RESTFUL"
  (:use #:cl
	#:thingbone.core)
  (:nicknames "TB.ASERVE")
  (:export
   #:publish-directory
   #:publish-json-file
   #:run-aserve-simple
   #:publish-json-schema-files

   #:test-aserve
   #:stop-aserve

   #:tb-http-request-timeout
   #:tb-http-request
   #:tb-http-request-response
   #:do-request

   #:*il-hostname-or-ip-address*
   #:*il-port*
   #:*il-username*
   #:*il-password*
   #:*il-api-login*

   #:il-http-request-timeout
   #:il-api-url-login
   #:il-api-url-item-master
   #:il-api-url-inventory-master
   #:il-api-url-inventory-update
   #:il-username
   #:il-password
   #:il-login-body
   #:il-http-request
   #:il-http-request-response
   #:il-authorization-context
   #:set-il-authorization-context
   #:il-outbound-request-item-master
   #:il-outbound-request-inventory-master
   #:il-outbound-request-inventory-update
   #:do-il-login
   #:maybe-do-il-login

   #:start-restful-api
   ))
