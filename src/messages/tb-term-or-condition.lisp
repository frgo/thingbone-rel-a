;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-term-or-condition (msg)
  ((condition-key   :accessor condition-key   :initarg :condition-key  :initform nil)
   (condition-value :accessor condition-value :initarg :conditon-value :initform nil)))

(defmethod print-object ((self tb-term-or-condition) stream)
  (print-unreadable-object (self stream :identity t :type t)
    (format stream ":CONDITION-KEY ~S :CONDITION-VALUE ~S"
	    (slot-value self 'condition-key)
	    (slot-value self 'condition-value)
	    )))

(defmethod jonathan:%to-json ((self tb-term-or-condition))
  (jonathan:with-object
    (jonathan:write-key-value "condition-key"   (condition-key   self))
    (jonathan:write-key-value "condition-value" (condition-value self))
    ))

(defclass tb-payment-terms (tb-term-or-condition)
  ()
  (:default-initargs
   :condition-key "payment-terms-code"))
