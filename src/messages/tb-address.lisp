;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-address (msg tb-validity json-object)
  ((address-kind   :accessor address-kind   :initarg :address-kind   :initform nil)
   (address-format :accessor address-format :initarg :address-format :initform nil)
   (default        :accessor default        :initarg :default        :initform nil)
   (addressee-name :accessor addressee-name :initarg :addressee-name :initform nil)
   (address-line-1 :accessor address-line-1 :initarg :address-line-1 :initform nil)
   (address-line-2 :accessor address-line-2 :initarg :address-line-2 :initform nil)
   (address-line-3 :accessor address-line-3 :initarg :address-line-3 :initform nil)
   (address-line-4 :accessor address-line-4 :initarg :address-line-4 :initform nil)
   (address-line-5 :accessor address-line-5 :initarg :address-line-5 :initform nil)
   (city           :accessor city           :initarg :city           :initform nil)
   (postal-code    :accessor postal-code    :initarg :postal-code    :initform nil)
   (state-code     :accessor state-code     :initarg :state-code     :initform nil)
   (country-code   :accessor country-code   :initarg :country-code   :initform nil)
   (region         :accessor region         :initarg :region         :initform nil)
   (phone          :accessor phone          :initarg :phone          :initform nil)
   (fax            :accessor fax            :initarg :fax            :initform nil)
   (email          :accessor email          :initarg :email          :initform nil)
   (website        :accessor website        :initarg :website        :initform nil)
   (latitude       :accessor latitude       :initarg :latitude       :initform nil)
   (longitude      :accessor longitude      :initarg :longitude      :initform nil)
   (image-url      :accessor image-url      :initarg :image-url      :initform nil)
   )
  (:default-initargs
   :json-object-title nil))

(defmethod print-object ((self tb-address) stream)
  (print-unreadable-object (self stream :identity t :type t)
    (format stream ":ADDRESSEE-NAME ~S :ADDRESS-LINE-4 ~S :CITY ~S :POSTAL-CODE ~S :STATE-CODE ~S :COUNTRY-CODE ~S :REGION ~S"
	    (slot-value self 'addressee-name)
	    (slot-value self 'address-line-4)
	    (slot-value self 'city)
	    (slot-value self 'postal-code)
	    (slot-value self 'state-code)
	    (slot-value self 'country-code)
	    (slot-value self 'region)
	    )))

(define-json-key-slot-list-for-class tb-address
    nil)
