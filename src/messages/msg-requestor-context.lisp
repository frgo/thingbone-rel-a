;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  MESSAGES
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass msg-requestor-context (msg-element)
  ((kind :accessor kind :initarg :kind :initform nil)))

(defmethod print-object ((msg-requestor-context msg-requestor-context) stream)
  (print-unreadable-object (msg-requestor-context stream :type t :identity t)
    (with-slots (kind) msg-requestor-context
      (format stream ":kind ~S" kind))))

(defclass msg-requestor-context-user-id-passcode (msg-requestor-context)
  ((user-id :accessor user-id :initarg :user-id :initform "")
   (passcode :accessor passcode :initarg :passcode :initform nil))
  (:default-initargs
   :kind :user-id-passcode))

(defclass msg-requestor-context-username-password (msg-requestor-context)
  ((username :accessor username :initarg :username :initform "")
   (password :accessor password :initarg :password :initform ""))
  (:default-initargs
   :kind :username-password))

(defmethod print-object ((msg-requestor-context-user-id-passcode msg-requestor-context-user-id-passcode) stream)
  (print-unreadable-object (msg-requestor-context-user-id-passcode stream :type t :identity t)
    (with-slots (kind user-id passcode) msg-requestor-context-user-id-passcode
      (format stream ":kind ~S :user-id ~S :passcode ~S"
	      kind user-id passcode))))

(defmethod print-object ((msg-requestor-context-username-password msg-requestor-context-username-password) stream)
  (print-unreadable-object (msg-requestor-context-username-password stream :type t :identity t)
    (with-slots (kind username password) msg-requestor-context-username-password
      (format stream ":kind ~S :username ~S :password ...not shown..."
	      kind username))))

(defmethod user-id ((msg-requestor-context msg-requestor-context))
  (if (slot-exists-p msg-requestor-context 'user-id)
      (slot-value msg-requestor-context 'user-id)
      ""))

(defmethod username ((msg-requestor-context msg-requestor-context))
  (if (slot-exists-p msg-requestor-context 'username)
      (slot-value msg-requestor-context 'user-name)
      ""))

(defmethod passcode ((msg-requestor-context msg-requestor-context))
  (if (slot-exists-p msg-requestor-context 'passcode)
      (slot-value msg-requestor-context 'passcode)
      ""))

(defmethod password ((msg-requestor-context msg-requestor-context))
  (if (slot-exists-p msg-requestor-context 'password)
      (slot-value msg-requestor-context 'password)
      ""))
