;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-msg (msg json-object)
  ((lang    :accessor lang    :initarg :lang    :initform nil)
   (line-nr :accessor line-nr :initarg :line-nr :initform nil)
   (text    :accessor text    :initarg :text    :initform nil))
  (:default-initargs
   :json-object-title nil))

(defmethod jonathan:%to-json ((msg tb-msg))
  (with-slots (lang line-nr text) msg
    (jonathan:with-object
      (jonathan:write-key-value "line-nr" line-nr)
      (jonathan:write-key-value "lang"    lang)
      (jonathan:write-key-value "text"    text))))
