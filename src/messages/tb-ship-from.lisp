;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-ship-from (msg json-object)
  ((environment  :accessor environment  :initarg :environment  :initform nil)
   (company-nr   :accessor company-nr   :initarg :company-nr   :initform nil)
   (warehouse-nr :accessor warehouse-nr :initarg :warehouse-nr :initform nil)
   (plant-nr     :accessor plant-nr     :initarg :plant-nr     :initform nil)
   )
  (:default-initargs
   :json-object-title "ship-from"))

(define-json-key-slot-list-for-class tb-ship-from
    ("ship-from-environment"   environment)
  ("ship-from-company-nr"    company-nr)
  ("ship-from-warehouse-nr"  warehouse-nr)
  ("ship-from-plant-nr"      plant-nr))

(defun test-from-json-tb-ship-from ()
  (let* ((json$ "{
                  \"ship-from\" : {
                     \"ship-from-environment\": \"STU\",
                     \"ship-from-company-nr\": \"123456\",
                     \"ship-from-warehouse-nr\": \"9\",
                     \"ship-from-plant-nr\": \"2\"
                  }
                }")
	 (json-list (jonathan:parse json$ :as :alist))
	 (element-list (rest (car json-list)))
	 (instance (make-instance 'tb-ship-from)))
    (from-json instance element-list)))
