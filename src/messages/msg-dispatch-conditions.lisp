;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  THINGBONE CORE
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; - TB CONDITION MSG DISPATCH -
(defparameter *lock-tb-condition-log-channel*   (bt:make-recursive-lock))
(defparameter *lock-tb-condition-log-channel-dispatcher*   (bt:make-recursive-lock))

(defconstant +tb-condition-log-channel-name+            "THINGBONE CONDITION LOG")
(defconstant +tb-condition-log-channel-dispatcher-name+ "THINGBONE CONDITION LOG CHANNEL")

(defvar *default-tb-condition-log-channel-max-size* 1024
  "The default maximum size for a ThingBone Condition Log channel.")

(defvar *tb-condition-log-channel-max-size* *default-tb-condition-log-channel-max-size*
  "The maximum size for for the ThingBone Condition Log channel.")

;;; - TB CONDITION MSG CHANNEL -

(defun tb-condition-log-channel ()
  (bt:with-recursive-lock-held (*lock-tb-condition-log-channel*)
    (tb.core:global-context-value :tb-condition-log-channel)))

(defun (setf tb-condition-log-channel) (channel)
  (bt:with-recursive-lock-held (*lock-tb-condition-log-channel*)
    (setf (tb.core:global-context-value :tb-condition-log-channel) channel)))

(defun init-tb-condition-log-channel (&key (channel nil) (max-size *tb-condition-log-channel-max-size*) (name +tb-condition-log-channel-name+))
  (setf (tb-condition-log-channel) (or channel
				       (thingbone.core:make-channel :max-size max-size :name name))))

(defun ensure-tb-condition-log-channel-initialized ()
  (if (not (tb-condition-log-channel))
      (init-tb-condition-log-channel)))

(defun tb-condition-log-channel-dispatcher ()
  (bt:with-recursive-lock-held (*lock-tb-condition-log-channel-dispatcher*)
    (tb.core:global-context-value :tb-condition-log-channel-dispatcher)))

(defun (setf tb-condition-log-channel-dispatcher) (dispatcher)
  (bt:with-recursive-lock-held (*lock-tb-condition-log-channel-dispatcher*)
    (setf (tb.core:global-context-value :tb-condition-log-channel-dispatcher) dispatcher)))

(defun stop-tb-condition-log-channel-dispatcher ()
  (let ((dispatcher (tb-condition-log-channel-dispatcher)))
    (if dispatcher
	(tb.core:stop-channel-dispatcher dispatcher))
    (setf (tb-condition-log-channel-dispatcher) nil)))

(defun start-tb-condition-log-channel-dispatcher ()
  (ensure-tb-condition-log-channel-initialized)
  (let ((dispatcher (make-instance 'tb.msgs:msg-dispatcher
				   :name +tb-condition-log-channel-dispatcher-name+
				   :channel (tb-condition-log-channel))))
    (setf (tb-condition-log-channel-dispatcher) dispatcher)
    (tb.core:run-in-thread dispatcher)))

(defun reset-tb-condition-log-channel ()
  (stop-tb-condition-log-channel-dispatcher)
  (init-tb-condition-log-channel)
  (start-tb-condition-log-channel-dispatcher))
