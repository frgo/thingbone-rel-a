;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  MESSAGES
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass msg-data (msg-element)
  ((kind :accessor kind :initarg :kind :initform nil)
   (data :accessor data :initarg :data :initform nil)))

(defmethod print-object ((msg-data msg-data) stream)
  (print-unreadable-object (msg-data stream :type t :identity t)
    (with-slots (kind data) msg-data
      (format stream ":KIND ~S :DATA ~S" kind data))))

(defun make-msg-data (kind data)
  (make-instance 'msg-data :kind kind :data data))

(defmethod as-string ((msg-data msg-data))
  (error "Subclass must implement method #'AS-STRING !"))

(defclass msg-string-data (msg-data)
  ()
  (:default-initargs
   :kind :string))

(defun make-msg-string-data (string)
  (check-type string string)
  (make-instance 'msg-string-data :data string))

(defmethod as-string ((msg-string-data msg-string-data))
  (data msg-string-data))

(defclass msg-json-object-data (msg-data)
  ()
  (:default-initargs
   :kind :json-object))

(defun make-msg-json-object-data (json-object)
  (make-instance 'msg-json-object-data :data json-object))

(defmethod as-string ((msg-json-object-data msg-json-object-data))
  (jonathan:to-json (data msg-json-object-data)))




(defmethod destructure-and-make-data-using-object-class-and-operation ((msg-extended-content msg-extended-content) object-class operation)
  (let ((err-msg (format nil "~S: DESTRUCTURE-AND-MAKE-DATA could not find specific function for object-class ~S and operation ~S. This most probably is a bug!"
			 msg-extended-content object-class operation)))
    (log:error "~A" err-msg)
    (error "~A" err-msg)))

(defmethod destructure-and-make-content-data ((msg-extended-content msg-extended-content))
  (let ((object-class (object-class msg-extended-content))
	(operation (operation msg-extended-content)))
    (destructure-and-make-data-using-object-class-and-operation msg-extended-content
								(alexandria:make-keyword (cl-strings:clean (string-upcase (princ-to-string object-class))))
								(alexandria:make-keyword (cl-strings:clean (string-upcase (princ-to-string operation)))))))

(defmacro define-msg-extended-content-destructure-method (object-class-keyword operation-keyword &body body)
  `(defmethod destructure-and-make-data-using-object-class-and-operation ((msg-extended-content msg-extended-content) (object-class (eql ,object-class-keyword)) (operation (eql ,operation-keyword)))
     (progn
       ,@body)))
