;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  MESSAGES
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass msg-sender (msg-element)
  ((app-id          :accessor app-id          :initarg :app-id          :initform nil)
   (app-instance-id :accessor app-instance-id :initarg :app-instance-id :initform nil)
   (comment         :accessor comment         :initarg :comment         :initform nil)
   (o               :accessor o               :initarg :o               :initform nil)
   (ou              :accessor ou              :initarg :ou              :initform nil)
   (support         :accessor support         :initarg :support         :initform nil)))

(defmethod print-object ((msg-sender msg-sender) stream)
  (print-unreadable-object (msg-sender stream :type t :identity t)
    (with-slots (app-id app-instance-id comment o ou support) msg-sender
      (format stream ":APP-ID ~S :APP-INSTANCE-ID ~S :COMMENT ~S :O ~S :OU ~S :SUPPORT ~S"
	      app-id app-instance-id comment o ou support))))

(defun make-msg-sender (app-id app-instance-id comment o ou support)
  (make-instance 'msg-sender :app-id app-id :app-instance-id app-instance-id
		 :comment comment :o o :ou ou :support support))

(defmethod jonathan:%to-json ((msg-sender msg-sender))
  (with-slots (app-id app-instance-id comment o ou support) msg-sender
    (jonathan:with-object
      (jonathan:write-key-value "app-id" app-id)
      (jonathan:write-key-value "app-instance-id" app-instance-id)
      (jonathan:write-key-value "__comment" comment)
      (jonathan:write-key-value "o" o)
      (jonathan:write-key-value "ou" ou)
      (jonathan:write-key-value "support" support))))
