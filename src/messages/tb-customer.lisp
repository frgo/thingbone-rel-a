;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-customer (msg json-object)
  ((sold-to        :accessor sold-to        :initarg :sold-to        :initform nil)
   (invoice-to     :accessor invoice-to     :initarg :invoice-to     :initform nil)
   (ship-to        :accessor ship-to        :initarg :ship-to        :initform nil)
   (final-receiver :accessor final-receiver :initarg :final-receiver :initform nil)
   (clearance-from :accessor clearance-from :initarg :clearance-from :initform nil)
   (contact-name   :accessor contact-name   :initarg :contact-name   :initform nil)
   )
  (:default-initargs
   :json-object-title nil))

(define-json-key-slot-list-for-class tb-customer
    ("business-partner-nr-sold-to"        sold-to)
  ("business-partner-nr-invoice-to"     invoice-to)
  ("business-partner-nr-ship-to"        ship-to)
  ("business-partner-nr-final-receiver" final-receiver)
  ("business-partner-nr-clearance-from" clearance-from)
  ("business-partner-contact-name"      contact-name))

(defclass tb-order-global-customer (tb-customer)
  ()
  (:default-initargs
   :json-object-title "order-global-customer"))

(defclass tb-line-item-customer (tb-customer)
  ()
  (:default-initargs
   :json-object-title "line-item-customer"))
