;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-data-context (msg json-object)
  ((environment :accessor environment :initarg :environment :initform nil))
  (:default-initargs
   :json-object-title "data-context"))

(define-json-key-slot-list-for-class tb-data-context
    ("environment" environment))

(defmethod jonathan:%to-json ((tb-data-context tb-data-context))
  (jonathan:with-object
    (jonathan:write-key (json-object-title tb-data-context))
    (jonathan:write-value
     (jonathan:with-object
       (jonathan:write-key-value "environment" (environment tb-data-context))))))
