;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  MESSAGES
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass msg-extended-content (msg-content)
  ((object-class   :accessor object-class   :initarg :object-class   :initform nil)
   (operation      :accessor operation      :initarg :operation      :initform nil)
   (data           :accessor data           :initarg :data           :initform nil)
   (data-as-string :accessor data-as-string :initarg :data-as-string :initform nil)
   (data-as-json   :accessor data-as-json   :initarg :data-as-json   :initform nil)))

(defmethod print-object ((msg-extended-content msg-extended-content) stream)
  (print-unreadable-object (msg-extended-content stream :type t :identity t)
    (with-slots (object-class operation data data-as-string data-as-json) msg-extended-content
      (format stream ":OBJECT-CLASS ~S :OPERATION ~S :DATA ~S :DATA-AS-STRING ~A :DATA-AS-JSON ~S"
	      object-class operation data data-as-string data-as-json))))

(defun make-msg-extended-content (object-class operation data data-as-string data-as-json)
  (let ((object-class-keyword (alexandria:make-keyword (cl-strings:clean (string-upcase (princ-to-string object-class)))))
	(operation-keyword (alexandria:make-keyword (cl-strings:clean (string-upcase (princ-to-string operation))))))
    (make-instance 'msg-extended-content :object-class object-class-keyword :operation operation-keyword
		   :data data :data-as-string data-as-string :data-as-json data-as-json)))

(defmethod jonathan:%to-json ((msg-extended-content msg-extended-content))
  (jonathan:write-key "content")
  (jonathan:with-object
    (jonathan:write-key-value "object-class" (string-downcase (tb.msgs:stringify-msg-value (object-class msg-extended-content))))
    (jonathan:write-key-value "operation"    (string-downcase (tb.msgs:stringify-msg-value (operation msg-extended-content))))
    (let ((content-data (data msg-extended-content)))
      (typecase content-data
	(array (jonathan:write-key "data")
	       (jonathan:write-value
		(jonathan:with-array
		  (loop for index from 0 to (1- (length content-data))
		     do
		       (let ((item (aref content-data index)))
			 (jonathan:write-item item))))))
	(t     (if content-data
		   (progn
		     (jonathan:write-key "data")
		     (jonathan:write-value (data content-data)))
		   (jonathan:write-key-value "data" :null)))))))
