;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  MESSAGES
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass msg-text (msg-element)
  ((lang     :accessor lang     :initarg :lang     :initform nil)
   (value    :accessor value    :initarg :value    :initform nil)
   (encoding :accessor encoding :initarg :encoding :initform nil)))

(defmethod print-object ((msg-text msg-text) stream)
  (print-unreadable-object (msg-text stream :type t :identity t)
    (with-slots (lang value encoding) msg-text
      (format stream ":LANG ~S :VALUE ~S :ENCODING ~S"
	      lang value encoding))))

(defun make-msg-text (lang value encoding)
  (make-instance 'msg-text :lang lang :value value :encoding encoding))

(defmethod jonathan:%to-json ((msg-text msg-text))
  (jonathan:with-object
    (jonathan:write-key-value "lang" (slot-value msg-text 'lang))
    (jonathan:write-key-value "text" (slot-value msg-text 'value))))

(defmethod to-json-string ((msg-text msg-text))
  (jonathan:to-json msg-text))
