;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-contract-owner (msg json-object)
  ((company-nr :accessor company-nr :initarg :company-nr :initform nil)
   )
  (:default-initargs
   :json-object-title "contract-owner"))

(define-json-key-slot-list-for-class tb-contract-owner
    ("company-nr" company-nr))

(defun test-from-json-tb-contract-owner ()
  (let* ((json$
          "{ \"contract-owner\": {
	  \"company-nr\": \"0120120203\"
	   } }"
           )
	 (json-list (jonathan:parse json$ :as :alist))
	 (instance (make-instance 'tb-contract-owner)))
    (from-json instance json-list)))
