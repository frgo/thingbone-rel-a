;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-response-sales-order-create (extended-msg json-object)
  ((request                   :accessor request                   :initarg :request                   :initform nil)
   (data-context              :accessor data-context              :initarg :data-context              :initform nil)
   (sales-order-create-result :accessor sales-order-create-result :initarg :sales-order-create-result :initform nil))
  (:default-initargs
   :json-object-title "response-sales-order-create"))

(define-json-key-slot-list-for-class tb-response-sales-order-create
    ("data-context" data-context :object)
  ("sales-order-create-result" sales-order :object))
