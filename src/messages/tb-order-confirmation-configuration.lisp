;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-order-confirmation-configuration (msg json-object)
  ((show-order-global-discount  :accessor show-order-global-discount  :initarg :show-order-global-discount  :initform t)
   (show-order-global-surcharge :accessor show-order-global-surcharge :initarg :show-order-global-surcharge :initform t)
   (show-line-item-unit-price   :accessor show-line-item-unit-price   :initarg :show-line-item-unit-price   :initform t)
   (show-line-item-discount     :accessor show-line-item-discount     :initarg :show-line-item-discount     :initform t)
   (show-line-item-surcharge    :accessor show-line-item-surcharge    :initarg :show-line-item-surcharge    :initform t)
   )
  (:default-initargs
   :json-object-title "order-confirmation-configuration"))

(define-json-key-slot-list-for-class tb-order-confirmation-configuration
    ("show-order-global-discount" show-order-global-discount)
  ("show-order-global-surcharge" show-order-global-surcharge)
  ("show-line-item-unit-price"   show-line-item-unit-price)
  ("show-line-item-discount"     show-line-item-discount)
  ("show-line-item-surcharge"    show-line-item-surcharge))
