;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  MESSAGES
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(defpackage #:thingbone.messages
  (:use #:cl #:cl-strings)
  (:nicknames "TB.MSGS")
  (:export

   #:+TB-EXTENDED-MSG-KEY-STRING-CONTENT+
   #:+TB-EXTENDED-MSG-KEY-STRING-CONTENT-DATA+
   #:+TB-EXTENDED-MSG-KEY-STRING-CONTENT-OPERATION+
   #:+TB-EXTENDED-MSG-KEY-STRING-CONTENT-OBJECT-CLASS+
   #:+TB-EXTENDED-MSG-KEY-STRING-LEGAL-ENTITY+
   #:+TB-EXTENDED-MSG-KEY-STRING-REQUESTOR-CONTEXT+
   #:+TB-EXTENDED-MSG-KEY-STRING-CREDENTIALS+
   #:+TB-EXTENDED-MSG-KEY-STRING-REQUESTOR-CONTEXT-KIND+
   #:+TB-EXTENDED-MSG-KEY-STRING-PASSWORD+
   #:+TB-EXTENDED-MSG-KEY-STRING-USERNAME+
   #:+TB-EXTENDED-MSG-KEY-STRING-SENDER+
   #:+TB-EXTENDED-MSG-KEY-STRING-SENDER-SUPPORT+
   #:+TB-EXTENDED-MSG-KEY-STRING-SENDER-OU+
   #:+TB-EXTENDED-MSG-KEY-STRING-SENDER-O+
   #:+TB-EXTENDED-MSG-KEY-STRING-SENDER-APP-INSTANCE-ID+
   #:+TB-EXTENDED-MSG-KEY-STRING-SENDER-APP-ID+
   #:+TB-EXTENDED-MSG-KEY-STRING-PERFORMATIVE+
   #:+TB-EXTENDED-MSG-KEY-STRING-IN-REPLY-TO+
   #:+TB-EXTENDED-MSG-KEY-STRING-REPLY-BY+
   #:+TB-EXTENDED-MSG-KEY-STRING-REPLY-WITH+
   #:+TB-EXTENDED-MSG-KEY-STRING-ENCODING+
   #:+TB-EXTENDED-MSG-KEY-STRING-TIMESTAMP+
   #:+TB-EXTENDED-MSG-KEY-STRING-PROTOCOL-VERSION+
   #:+TB-EXTENDED-MSG-KEY-STRING-PROTOCOL+
   #:+TB-EXTENDED-MSG-KEY-STRING-LANGUAGE+
   #:+TB-EXTENDED-MSG-KEY-STRING-CONVERSATION-ID+
   #:+TB-EXTENDED-MSG-KEY-STRING-QOS+
   #:+TB-EXTENDED-MSG-KEY-STRING-CREDENTIALS-USER-ID+
   #:+TB-EXTENDED-MSG-KEY-STRING-CREDENTIALS-PASSCODE+
   #:+TB-EXTENDED-MSG-KEY-STRING-CREDENTIALS-USERNAME+
   #:+TB-EXTENDED-MSG-KEY-STRING-CREDENTIALS-PASSWORD+

   #:+TB-EXTENDED-MSG-VALUE-STRING-REQUESTOR-CONTEXT-KIND-USERNAME-PASSWORD+
   #:+TB-EXTENDED-MSG-VALUE-STRING-REQUESTOR-CONTEXT-KIND-USER-ID-PASSCODE+


   #:msg-base
   #:msg-element
   #:msg-content
   #:msg-extended-content
   #:msg-data
   #:msg-string-data
   #:msg-json-object-data
   #:msg-requestor-context
   #:msg-requestor-context-user-id-passcode
   #:msg-requestor-context-username-password
   #:msg-sender
   #:msg-qos
   #:msg
   #:extended-msg-content
   #:extended-msg
   #:msg-error-info
   #:msg-text

   #:make-msg-data
   #:make-msg-string-data
   #:make-msg-json-object-data
   #:make-msg-extended-content
   #:make-msg-sender
   #:make-msg-requestor-context-user-id-passcode
   #:make-msg-requestor-context-username-password
   #:make-msg-text
   #:make-msg-error-info
   #:add-msg-text
   #:add-error-info

   #:extended-msg-encoding
   #:extended-msg-protocol
   #:extended-msg-protocol-version

   #:requestor-context
   #:language
   #:kind
   #:user-id
   #:passcode
   #:username
   #:password
   #:qos
   #:request-status-response-required-p
   #:conversation-id
   #:encoding
   #:protocol
   #:protocol-version
   #:timestamp
   #:performative
   #:reply-with
   #:reply-by
   #:in-reply-to
   #:content
   #:sender

   #:make-msg-sender-for-this-app-instance

   #:user-kind-msg-requestor-context-p
   #:make-msg-user-id-passwd-requestor-context
   #:operation
   #:object-class
   #:data

   #:stringify-msg-value
   #:as-string
   #:data-as-string
   #:data-as-json

   #:msg-dispatcher
   #:dispatch-msg
   #:dispatch-using-object-class-and-operation
   #:dispatch
   #:first-error-code
   #:errorp
   #:define-extended-msg-dispatch-method
   #:make-msg-dispatcher
   #:get-channel-wait-time
   #:set-channel-wait-time
   #:start
   #:stop
   #:run-in-thread
   #:set-keep-listening
   #:keep-listening-p

   #:tb-condition-log-channel
   #:ensure-tb-condition-log-channel-initialized
   #:tb-condition-log-channel-dispatcher
   #:stop-tb-condition-log-channel-dispatcher
   #:start-tb-condition-log-channel-dispatcher
   #:reset-tb-condition-log-channel

   #:destructure-and-make-data-using-object-class-and-operation
   #:destructure-and-make-content-data
   #:define-msg-extended-content-destructure-method

   #:tb-data-context

   #:tb-request-sales-order-create
   #:data-context
   #:sales-order
   #:make-tb-request-sales-order-create-from-json
   #:order-global-customer
   #:ship-to-address
   #:order-date
   #:order-kind
   #:order-reason
   #:order-currency
   #:order-global-discount-percentage
   #:order-global-discount-absolute-amount
   #:order-global-surcharge-percentage
   #:order-global-surcharge-absolute-amount
   #:order-global-total-price
   #:order-confirmation-configuration
   #:order-logistics-configuration
   #:order-payment-terms-code
   #:order-level-incoterms
   #:order-global-desired-delivery-date
   #:customer-purchase-nr
   #:customer-purchase-date
   #:sales-channel
   #:sales-organisation
   #:sales-office
   #:sales-group
   #:order-global-comments
   #:order-line-items
   #:show-order-global-discount
   #:show-order-global-surcharge
   #:ship-when-complete
   #:terms
   #:location
   #:pos-nr
   #:item-nr
   #:item-version
   #:item-revision
   #:quantity
   #:uom
   #:ship-from
   #:show-line-item-on-order
   #:line-item-calculated-delivery-date
   #:line-item-committed-delivery-date
   #:line-item-payment-terms-code
   #:line-item-level-incoterms
   #:line-item-customer
   #:line-item-price-information
   #:line-item-comments
   #:environment
   #:company-nr
   #:warehouse-nr
   #:plant-nr
   #:unit-price
   #:customer-visible-surcharge-percentage
   #:customer-visible-discount-percentage
   #:customer-invisible-surcharge-percentage
   #:customer-invisible-discount-percentage
   #:customer-visible-surcharge-amount
   #:customer-visible-discount-amount
   #:customer-invisible-surcharge-amount
   #:customer-invisible-discount-amount
   #:line-item-resulting-price
   #:show-line-item-price
   #:sold-to
   #:invoice-to
   #:ship-to
   #:final-receiver
   #:clearance-from
   #:addressee-name
   #:address-line-1
   #:address-line-2
   #:address-line-3
   #:address-line-4
   #:address-line-5
   #:city
   #:postal-code
   #:state-code
   #:country-code
   #:region
   #:lang
   #:comment-line-index-nr
   #:comment-category
   #:text

   #:tb-response-sales-order-create

   #:tb-sales-order
   #:tb-ship-to-address
   #:tb-customer
   #:tb-order-line-item
   #:tb-sales-order-create-result
   #:tb-request-result-info
   #:tb-order-global-status-info
   #:tb-order-line-item-status-info
   #:request-result-info
   #:sales-order-create-result
   #:order-nr
   #:order-global-tax-amount
   #:request
   #:tb-sales-order-status
   #:sales-order-status
   #:cancellation-status
   #:confirmation-status
   #:invoice-booking-statius
   #:credit-check-status
   #:delay-status
   #:invoice-blocking-status
   #:goods-received-status
   #:commissioning-status
   #:packing-status
   #:delivery-status
   #:revenue-booking-status
   #:staging-status
   #:order-global-status-info
   #:order-line-items-results
   #:tb-inform-sales-order-status-update
   #:make-tb-inform-sales-order-status-update-from-json
   #:order-line-item-status-info

   #:tb-request-sales-order-update
   #:make-tb-request-sales-order-update-from-json
   #:tb-response-sales-order-update
   #:tb-sales-order-update-result
   #:sales-order-update-result


   ))

(provide :thingbone.messages)


;;; ---------------------------------------------------------------------------
