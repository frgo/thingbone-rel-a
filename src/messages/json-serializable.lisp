;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  MESSAGES
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass json-serializable ()
  ())

(defclass json-object (json-serializable)
  ((json-object-title :accessor json-object-title :initarg :json-object-title :initform (error "JSON-OBJECT-TITLE required!"))))

(defclass json-array (json-serializable)
  ((json-array-element-class :accessor json-array-element-class :initarg :json-array-element-class :initform (error "JSON-ARRAY-ELEMENT-CLASS required!"))))

;;; JSON Handling

(defmacro define-json-key-slot-list-for-class (class &rest key-slot-pairs)
  (let ((list-ensured-key-slot-pairs (if (and key-slot-pairs
					      (listp key-slot-pairs)
					      (= (length key-slot-pairs) 2)
					      (not (listp (first key-slot-pairs))))
					 (list key-slot-pairs)
					 key-slot-pairs)))
    `(defmethod json-key-slot-list ((self ,class))
       '(,@list-ensured-key-slot-pairs))))

(defun find-keys-and-values-for-json (instance)
  (loop for element in (json-key-slot-list instance)
     collect (list (first element) (slot-value instance (second element)))))

(defmethod from-json ((self json-serializable) json-alist)
  (declare (ignore json-alist))
  self)

(defmethod from-json ((self json-object) json-alist)
  (set-slot-values-from-json-object self json-alist)
  self)

(defmethod jonathan:%to-json ((self json-object))
  (flet ((write-json-object (self)
	   (jonathan:with-object
	     (loop for element in (find-keys-and-values-for-json self)
		do
		  (jonathan:write-key-value (first element) (second element))))))
    (let ((json-object-title (json-object-title self)))
      (if json-object-title
	  (progn
	    (jonathan:write-key (json-object-title self))
	    (jonathan:write-value
	     (write-json-object self)))
	  (write-json-object self)))
    self))

(defun msg-class-from-json-key (key)
  (find-class (find-symbol (format nil "TB-~A" (string-upcase key)) :thingbone.messages)))

(defun array-from-json (object-class json-list)
  (let* ((nr-elements (length json-list))
	 (array (make-array nr-elements :fill-pointer 0 :initial-element nil)))
    (loop for json-element in json-list
       do
	 (vector-push-extend (from-json (make-instance object-class) json-element) array))
    array))

(defmethod set-slot-values-from-json-object ((self json-object) json-alist)
  (let ((key-slot-list (json-key-slot-list self))
	(json-list (if (not (listp (first json-alist)))
		       (list json-alist)
		       json-alist)))
    (loop for json-element in json-list
       do
	 (let* ((key                 (format nil "~A" (car json-element)))
		(value               (cdr json-element))
		(key-slot-def        (find-if (lambda (x)
						(string= (car x) key))
					      key-slot-list))
		(key-slot-def-length (length key-slot-def))
		(slot                (if key-slot-def
					 (cadr key-slot-def)))
		(object-kind         (if (and key-slot-def
					      (>= (length key-slot-def) 3))
					 (third key-slot-def)
					 :simple-value))
		(object-class        (ecase object-kind
				       (:simple-value nil)
				       (:object (if (> key-slot-def-length 3)
						    (fourth key-slot-def)
						    (msg-class-from-json-key key)))
				       (:array (if (> key-slot-def-length 3)
						   (fourth key-slot-def)
						   (error "MISSING ARRAY ELEMENT OBJECT CLASS DEFINITION IN ~S" key-slot-def))))))
	   (log:debug "set-slot-values-from-json-object - :KEY ~S :VALUE ~S :KEY-SLOT-DEF ~S :SLOT ~S :OBJECT-KIND ~S :OBJECT-CLASS ~S"
		      key value key-slot-def slot object-kind object-class)
	   (if (not slot)
	       (error "Could not handle JSON element (slot definition not found -> Class Name: ~S, Key: ~S, Value: ~S, Key Slot Definition: ~S, Object Kind: ~S, Object Class: ~S)!"
		      (class-name (class-of self)) key value key-slot-def object-kind object-class)
	       (ecase object-kind
		 (:simple-value (setf (slot-value self slot) value))
		 (:object       (setf (slot-value self slot) (from-json (make-instance object-class) value)))
		 (:array        (setf (slot-value self slot) (array-from-json object-class value))))))))
  self)

(defun make-extended-msg-from-json (performative object-class operation json-list)
  (let* ((fn-name (string-upcase (format nil "make-tb-~A-~A-~A-from-json" performative object-class operation)))
	 (fn-symbol (find-symbol fn-name))
	 (extended-msg (funcall fn-symbol json-list)))
    (setf (object-class (content extended-msg)) object-class)
    (setf (operation (content extended-msg)) operation)
    extended-msg))
