;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  MESSAGES
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass msg-source-info (msg-element)
  ((kind :accessor kind :initarg :kind :initform nil)
   ))

(defmethod print-object ((msg-source-info msg-source-info) stream)
  (print-unreadable-object (msg-source-info stream :type t :identity t)
    (format stream ":KIND ~S"
	    (kind msg-source-info))))


;; (defclass msg-http-request-source-info (msg-source-info)
;;   ((http-request :accessor http-request :initarg :http-request :initform nil))
;;   (:default-initargs
;;    :kind :http-request-source-info))

;; (defclass msg-sap-servb-outbound-source-info (msg-source-info)
;;   ((servb-outbound :accessor servb-outbound :initarg :servb-outbound :initform nil))
;;   (:default-initargs
;;    :kind :sap-servb-outbound-source-info))
