;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-request-sales-order-update (extended-msg json-object)
  ((data-context :accessor data-context :initarg :data-context :initform nil)
   (sales-order  :accessor sales-order  :initarg :sales-order  :initform nil))
  (:default-initargs
   :json-object-title "request-sales-order-update"))

(define-json-key-slot-list-for-class tb-request-sales-order-update
    ("data-context" data-context :object)
  ("sales-order" sales-order :object))

(defmethod from-json ((self tb-request-sales-order-update) json-list)

  ;; Note:
  ;; This function expects a complete object list where the first
  ;; element is the object "title itself:
  ;; { "data" : { ... } }
  ;; (let ((element-list (first (cdr json-list))))
  ;;   (loop for element in element-list
  ;;      do
  ;; 	 (set-slot-values-from-json-object self element-list)
  ;; 	 ))
  (log:debug "In from-json: ~S" self)

  (set-slot-values-from-json-object self (first (cdr json-list)))

  (let ((msg-data (data (content self))))
    (setf (kind msg-data) :tb-request-sales-order-update)
    (setf (data msg-data) (list (data-context self) (sales-order self))))
  self)

(defun make-tb-request-sales-order-update-from-json (json-list)
  (let* ((instance (make-instance 'tb-request-sales-order-update))
	 (from-json (from-json instance json-list)))
    from-json))
