;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-incoterms (msg json-object)
  ((location :accessor location :initarg :location :initform nil)
   (terms    :accessor terms    :initarg :terms    :initform nil)
   )
  (:default-initargs
   :json-object-title nil))

(defmethod print-object ((self tb-incoterms) stream)
  (print-unreadable-object (self stream :identity t :type t)
    (format stream ":LOCATION ~S :TERMS ~S"
	    (slot-value self 'location)
	    (slot-value self 'terms)
	    )))

(define-json-key-slot-list-for-class tb-incoterms
    ("incoterms-location" location)
  ("incoterms-terms" terms))

(defclass tb-order-level-incoterms (tb-incoterms)
  ()
  (:default-initargs
   :json-object-title "order-level-incoterms"))

(defclass tb-line-item-level-incoterms (tb-incoterms)
  ()
  (:default-initargs
   :json-object-title "line-item-level-incoterms"))
