;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  MESSAGES
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defparameter *tb-extended-msg-encoding*         "iso-8859-1")
(defparameter *tb-extended-msg-protocol*         "bwp-proprietary")
(defparameter *tb-extended-msg-protocol-version* "3")

(defun extended-msg-encoding ()
  *tb-extended-msg-encoding*)

(defun extended-msg-protocol ()
  *tb-extended-msg-protocol*)

(defun extended-msg-protocol-version ()
  *tb-extended-msg-protocol-version*)

(defclass extended-msg (msg)
  ((conversation-id   :accessor conversation-id   :initarg :conversation-id   :initform nil)
   (encoding          :accessor encoding          :initarg :encoding          :initform nil)
   (language          :accessor language          :initarg :language          :initform "json")
   (protocol          :accessor protocol          :initarg :protocol          :initform nil)
   (protocol-version  :accessor protocol-version  :initarg :protocol-version  :initform nil)
   (timestamp         :accessor timestamp         :initarg :timestamp         :initform nil)
   (requestor-context :accessor requestor-context :initarg :requestor-context :initform nil)
   (sender            :accessor sender            :initarg :sender            :initform nil)
   (reply-with        :accessor reply-with        :initarg :reply-with        :initform nil)
   (in-reply-to       :accessor in-reply-to       :initarg :in-reply-to       :initform nil)
   (reply-by          :accessor reply-by          :initarg :reply-by          :initform nil)
   (performative      :accessor performative      :initarg :performative      :initform nil)
   (qos               :accessor qos               :initarg :qos               :initform nil)
   (content           :accessor content           :initarg :content           :initform (make-instance 'msg-content))
   ))

(defmethod jonathan:%to-json ((extended-msg extended-msg))
  (with-slots (conversation-id
	       encoding
	       language
	       protocol
	       protocol-version
	       timestamp
	       sender
	       requestor-context
	       reply-with
	       in-reply-to
	       reply-by
	       performative
	       qos
	       content
	       error-info-list) extended-msg
    (jonathan:with-object
      (jonathan:write-key-value "conversation-id"    (or conversation-id  :null))
      (jonathan:write-key-value "encoding"           (or encoding         :null))
      (jonathan:write-key-value "language"           (or language         :null))
      (jonathan:write-key-value "protocol"           (or protocol         :null))
      (jonathan:write-key-value "protocol-version"   (or protocol-version :null))
      (jonathan:write-key-value "timestamp"          (or timestamp        :null))
      (jonathan:write-key       "sender")            (jonathan:write-value sender)
      (jonathan:write-key       "requestor-context") (if requestor-context
							 (jonathan:write-value requestor-context)
							 (jonathan:write-value :null))
      (jonathan:write-key-value "reply-with"         (or reply-with       :null))
      (jonathan:write-key-value "in-reply-to"        (or in-reply-to      :null))
      (jonathan:write-key-value "reply-by"           (or reply-by         :null))
      (jonathan:write-key-value "performative"       (or performative     :null))
      ;; (jonathan:write-key-value "qos"                (or qos              :null))
      (jonathan:write-key       "content")           (if content
							 (jonathan:write-value content)
							 (jonathan:write-value :null))
      (jonathan:write-key       "error-info")        (if error-info-list
							 (jonathan:write-value error-info-list)
							 (jonathan:write-value :null)))))


(defmethod print-object ((extended-msg extended-msg) stream)
  (print-unreadable-object (extended-msg stream :type t :identity t)
    (with-slots (conversation-id
		 encoding
		 language
		 protocol
		 protocol-version
		 timestamp sender
		 requestor-context
		 reply-with
		 in-reply-to
		 reply-by
		 performative
		 qos
		 content
		 error-info-list) extended-msg
      (format stream ":CONVERSATION-ID ~S :ENCODING ~S :LANGUAGE ~S :PROTOCOL ~S :PROTOCOL-VERSION ~S :TIMESTAMP ~S :SENDER ~S :REQUESTOR-CONTEXT ~S :REPLY-WITH ~S :IN-REPLY-TO ~S :REPLY-BY ~S :PERFORMATIVE ~S :QOS ~S :SENDER ~S :CONTENT ~S :ERROR-INFO-LIST ~S"
	      conversation-id encoding language protocol protocol-version timestamp sender requestor-context reply-with in-reply-to reply-by performative qos sender content error-info-list))))

(defmethod operation ((extended-msg extended-msg))
  (let ((content (content extended-msg)))
    (if content
	(operation content))))

(defmethod object-class ((extended-msg extended-msg))
  (let ((content (content extended-msg)))
    (if content
	(object-class content))))

(defmethod make-and-set-msg-as-string ((extended-msg extended-msg))
  (let ((string "{}"))
    (setf (msg-as-string extended-msg) string)
    string)) ;; FIXME: Needs proper construction of string representation! frgo, 2019-05-19

(defmethod as-string ((extended-msg extended-msg))
  (let ((string (msg-as-string extended-msg)))
    (if (not string)
	(setq string (make-and-set-msg-as-string extended-msg)))
    string))

(defmethod dispatch-using-object-class-and-operation ((extended-msg extended-msg) object-class operation)
  (let ((err-msg (format nil "~S: Dispatch could not find specific function for object-class ~S and operation ~S. This most probably is a bug!"
			 extended-msg object-class operation)))
    (log:error "~A" err-msg)
    (error "~A" err-msg)))

(defmethod dispatch ((extended-msg extended-msg) &key &allow-other-keys)
  (let ((object-class (object-class extended-msg))
	(operation (operation extended-msg)))
    (log:debug "DISPATCH EXTENDED-MSG: object-class: ~S, operation: ~S" object-class operation)
    (dispatch-using-object-class-and-operation
     extended-msg
     (alexandria:make-keyword (cl-strings:clean (string-upcase (princ-to-string object-class))))
     (alexandria:make-keyword (cl-strings:clean (string-upcase (princ-to-string operation)))))))

(defmacro define-extended-msg-dispatch-method (object-class-keyword operation-keyword &body body)
  `(defmethod dispatch-using-object-class-and-operation ((extended-msg extended-msg) (object-class (eql ,object-class-keyword)) (operation (eql ,operation-keyword)))
     (progn
       ,@body)))


;;; === THESE ARE QUICK HACKS AND REALLY SHOULD BE CONFIGURATION VALUES ... ===

(defun tb-this-sender-app-id ()
  "6378EFBC-C739-4DB8-9513-24BEA0E86622")

(defun tb-this-sender-app-instance-id ()
  "8AA47AA9-DE2F-437D-B40B-ED58DF76A4E0")

(defun tb-this-sender-comment ()
  "BW Papersystems THINGBONE Messaging Gateway")

(defun tb-this-sender-o ()
  "BW Papersystems")

(defun tb-this-sender-ou ()
  "BW Papersystems Stuttgart GmbH")

(defun tb-this-sender-support ()
  "it.stuttgart@bwpapersystems.com")


(defun make-msg-sender-for-this-app-instance ()
  (tb.msgs:make-msg-sender (tb-this-sender-app-id)
			   (tb-this-sender-app-instance-id)
			   (tb-this-sender-comment)
			   (tb-this-sender-o)
			   (tb-this-sender-ou)
			   (tb-this-sender-support)))
