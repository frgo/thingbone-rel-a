;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-inform-sales-order-status-update (extended-msg json-object)
  ((request            :accessor request            :initarg :request            :initform nil)
   (data-context       :accessor data-context       :initarg :data-context       :initform nil)
   (sales-order-status :accessor sales-order-status :initarg :sales-order-status :initform nil))
  (:default-initargs
   :json-object-title "sales-order-status"))

(define-json-key-slot-list-for-class tb-inform-sales-order-status-update
    ("data-context" data-context :object)
  ("sales-order-status" sales-order-status :object))


(defmethod from-json ((self tb-inform-sales-order-status-update) json-list)

  ;; Note:
  ;; This function expects a complete object list where the first
  ;; element is the object "title itself:
  ;; { "data" : { ... } }
  ;; (let ((element-list (first (cdr json-list))))
  ;;   (loop for element in element-list
  ;;      do
  ;; 	 (set-slot-values-from-json-object self element-list)
  ;; 	 ))
  (log:debug "In from-json: ~S" self)

  (set-slot-values-from-json-object self (first (cdr json-list)))

  (let ((msg-data (data (content self))))
    (setf (kind msg-data) :tb-request-sales-order-create)
    (setf (data msg-data) (list (data-context self) (sales-order-status self))))
  self)

(defmethod as-string ((inform-sales-order-status-update tb-inform-sales-order-status-update))
  (setf (msg-as-string inform-sales-order-status-update) (jonathan:to-json inform-sales-order-status-update)))

(defun make-tb-inform-sales-order-status-update-from-json (json-list)
  (let* ((instance (make-instance 'tb-inform-sales-order-status-update))
	 (from-json (from-json instance json-list)))
    from-json))
