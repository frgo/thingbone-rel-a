;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  MESSAGES
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defconstant +TB-EXTENDED-MSG-KEY-STRING-CONTENT+                 "CONTENT")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-CONTENT-DATA+            "DATA")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-CONTENT-OPERATION+       "OPERATION")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-CONTENT-OBJECT-CLASS+    "OBJECT-CLASS")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-LEGAL-ENTITY+            "LEGAL-ENTITY")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-REQUESTOR-CONTEXT+       "REQUESTER-CONTEXT")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-CREDENTIALS+             "CREDENTIALS")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-REQUESTOR-CONTEXT-KIND+  "REQUESTER-CONTEXT-KIND")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-CREDENTIALS-PASSWORD+    "PASSWORD")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-CREDENTIALS-USERNAME+    "USERNAME")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-CREDENTIALS-PASSCODE+    "PASSCODE")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-CREDENTIALS-USER-ID+     "USER-ID")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-SENDER+                  "SENDER")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-SENDER-SUPPORT+          "SUPPORT")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-SENDER-OU+               "OU")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-SENDER-O+                "O")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-SENDER-APP-INSTANCE-ID+  "APP-INSTANCE-ID")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-SENDER-APP-ID+           "APP-ID")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-PERFORMATIVE+            "PERFORMATIVE")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-IN-REPLY-TO+             "IN-REPLY-TO")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-REPLY-BY+                "REPLY-BY")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-REPLY-WITH+              "REPLY-WITH")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-ENCODING+                "ENCODING")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-TIMESTAMP+               "TIMESTAMP")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-PROTOCOL-VERSION+        "PROTOCOL-VERSION")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-PROTOCOL+                "PROTOCOL")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-LANGUAGE+                "LANGUAGE")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-CONVERSATION-ID+         "CONVERSATION-ID")
(defconstant +TB-EXTENDED-MSG-KEY-STRING-QOS+                     "QOS")

(defconstant +TB-EXTENDED-MSG-VALUE-STRING-REQUESTOR-CONTEXT-KIND-USERNAME-PASSWORD+ "USERNAME-PASSWORD")
(defconstant +TB-EXTENDED-MSG-VALUE-STRING-REQUESTOR-CONTEXT-KIND-USER-ID-PASSCODE+  "USER-ID-PASSCODE")
