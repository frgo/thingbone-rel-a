;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-sales-order-update-result (msg json-object)
  ((order-nr                 :accessor order-nr                 :initarg :order-nr                 :initform nil)
   (last-changed-on          :accessor last-chaned-on           :initarg :last-changed-on          :initform nil)
   (order-global-tax-amount  :accessor order-global-tax-amount  :initarg :order-global-tax-amount  :initform nil)
   (order-global-status-info :accessor order-global-status-info :initarg :order-global-status-info :initform nil)
   (request-result-info      :accessor request-result-info      :initarg :request-result-info      :initform (make-array 10 :adjustable t :fill-pointer 0))
   (order-line-items-results :accessor order-line-items-results :initarg :order-line-items-results :initform (make-array 10 :adjustable t :fill-pointer 0)))
  (:default-initargs
   :json-object-title "sales-order-update-result"))

(defmethod jonathan:%to-json ((sales-order-update-result tb-sales-order-update-result))
  (with-slots (order-nr
	       last-changed-on
	       order-global-tax-amount
	       order-global-status-info
	       request-result-info
	       order-line-items-results) sales-order-update-result

    (jonathan:with-object
      (jonathan:write-key (json-object-title sales-order-update-result))

      (jonathan:write-value
       (jonathan:with-object
	 (jonathan:write-key-value "order-nr"                (or order-nr                :null))
	 (jonathan:write-key-value "order-global-tax-amount" (or order-global-tax-amount :null))
	 (jonathan:write-key-value "last-changed-on"         (or last-changed-on         :null))

	 (if order-global-status-info
	     (progn
	       (jonathan:write-key "order-global-status-info")
	       (jonathan:write-value order-global-status-info))
	     (jonathan:write-key-value "order-global-status-info" :null))

	 (jonathan:write-key "request-result-info")
	 (if request-result-info
	     (let ((length (length request-result-info)))
	       (if (> length 0)
	 	   (jonathan:write-value
	 	    (jonathan:with-array
	 	      (loop for index from 0 to (1- (length request-result-info))
	 		 do
	 		   (let ((element (aref request-result-info index)))
			     (if element
	 			 (jonathan:write-item element))))))
		   (jonathan:write-value :null)))
	     (jonathan:write-value :null))

	 (jonathan:write-key "order-line-item-results")
	 (if order-line-items-results
	     (let ((length (length order-line-items-results)))
	       (if (> length 0)
		   (jonathan:write-value
		    (jonathan:with-array
		      (loop for index from 0 to (1- (length order-line-items-results))
			 do
	 		   (let ((element (aref order-line-items-results index)))
	 		     (if element
	 			 (jonathan:write-item element))))))
		   (jonathan:write-value :null)))
	     (jonathan:write-value :null)))))))
