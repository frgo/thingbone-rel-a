;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-classification (msg json-object)
  ((classification-key :accessor classification-key :initarg :classification-key :initform nil)
   (classification-value :accessor classification-value :initarg :classification-value :initform nil))
  (:default-initargs
   :json-object-title nil))

(define-json-key-slot-list-for-class tb-classification
    nil)

(defmethod jonathan:%to-json ((self tb-classification))
  (jonathan:write-key-value (classification-key self) (classification-value self)))

(defun test-classification-from-json ()
  (let ((json$  " { \"business-partner-classifications\": [
		  {
		    \"credit-limit\": {
		      \"amount\": 12345.00,
		      \"currency\": \"EUR\",
		      \"valid-from\": \"2020-01-01T00:00:00Z\"
		    }
		  },
		  {
		    \"credit-status\": \"credit-hold\"
		  },
		  {
		    \"partial-ship\": true
		  },
		  {
		    \"purchase-order-required\": true
		  },
		  {
		    \"priority-code\": \"80\"
	 	  }
		] }"))
    (jonathan:parse json$ :as :alist )))

#| =>
(("business-partner-classifications"
(("credit-limit" ("valid-from" . "2020-01-01T00:00:00Z")
("currency" . "EUR") ("amount" . 12345.0)))
(("credit-status" . "credit-hold")) (("partial-ship" . T))
(("purchase-order-required" . T)) (("priority-code" . "80"))))
|#					;
