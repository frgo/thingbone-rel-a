;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-order-address (tb-address)
  ()
  (:default-initargs
   :json-object-title nil))

(define-json-key-slot-list-for-class tb-order-address
    nil)

(defclass tb-ship-to-address (tb-order-address)
  ()
  (:default-initargs
   :json-object-title "ship-to-address"))

(define-json-key-slot-list-for-class tb-ship-to-address
    ("addressee-name" addressee-name)
  ("address-line-1" address-line-1)
  ("address-line-2" address-line-2)
  ("address-line-3" address-line-3)
  ("address-line-4" address-line-4)
  ("address-line-5" address-line-5)
  ("city" city)
  ("postal-code" postal-code)
  ("state-code" state-code)
  ("country-code" country-code)
  ("region" region))
