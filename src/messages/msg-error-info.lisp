;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  MESSAGES
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass msg-error-info (msg-element)
  ((code        :accessor code        :initarg :code        :initform nil)
   (level       :accessor level       :initarg :level       :initform nil)
   (category    :accessor category    :initarg :category    :initform nil)
   (reason-code :accessor reason-code :initarg :reason-code :initform nil)
   (msg-nr      :accessor msg-nr      :initarg :msg-nr      :initform nil)
   (msg         :accessor msg         :initarg :msg         :initform '())))

(defmethod print-object ((msg-error-info msg-error-info) stream)
  (print-unreadable-object (msg-error-info stream :type t :identity t)
    (with-slots (code level category reason-code msg-nr msg) msg-error-info
      (format stream ":CODE ~S :LEVEL ~S :CATEGORY ~S :REASON-CODE ~S :MSG-NR ~S :MSG ~S"
	      code level category reason-code msg-nr msg))))

(defun make-msg-error-info (code level category reason-code msg-nr msg)
  (make-instance 'msg-error-info
		 :code code :level level :category category
		 :reason-code reason-code :msg-nr msg-nr :msg msg))

(defmethod jonathan:%to-json ((msg-error-info msg-error-info))
  (jonathan:with-object
    (jonathan:write-key-value "code"        (or (slot-value msg-error-info 'code)        :null))
    (jonathan:write-key-value "level"       (or (slot-value msg-error-info 'level)       :null))
    (jonathan:write-key-value "category"    (or (slot-value msg-error-info 'category)    :null))
    (jonathan:write-key-value "reason-code" (or (slot-value msg-error-info 'reason-code) :null))
    (jonathan:write-key-value "msg-nr"      (or (slot-value msg-error-info 'msg-nr)      :null))
    (jonathan:write-key-value "msg"         (jonathan:with-array
					      (loop for text in (msg msg-error-info)
						 do
						   (jonathan:write-item text))))))

(defmethod add-msg-text ((msg-error-info msg-error-info) (msg-text msg-text))
  (setf (msg msg-error-info) (append (msg msg-error-info) (alexandria:ensure-list msg-text))))

(defmethod add-error-info ((msg msg) (msg-error-info msg-error-info))
  (setf (error-info-list msg) (cons msg-error-info (error-info-list msg))))
