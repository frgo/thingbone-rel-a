u;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-sales-order (msg json-object)
  ((contract-owner                         :accessor contract-owner                         :initarg :contract-owner                         :initform nil)
   (order-global-customer                  :accessor order-global-customer                  :initarg :order-global-customer                  :initform nil)
   (ship-to-address                        :accessor ship-to-address                        :initarg :ship-to-address                        :initform nil)
   (order-date                             :accessor order-date                             :initarg :order-date                             :initform nil)
   (order-class                            :accessor order-class                            :initarg :order-class                            :initform nil)
   (order-kind                             :accessor order-kind                             :initarg :order-kind                             :initform nil)
   (order-reason                           :accessor order-reason                           :initarg :order-reason                           :initform nil)
   (order-currency                         :accessor order-currency                         :initarg :order-currency                         :initform nil)
   (order-global-nr                        :accessor order-global-nr                        :initarg :order-global-nr                        :initform nil)
   (order-nr                               :accessor order-nr                               :initarg :order-nr                               :initform nil)
   (order-id                               :accessor order-id                               :initarg :order-id                               :initform nil)
   (order-global-title                     :accessor order-global-title                     :initarg :order-global-title                     :initform nil)
   (order-global-discount-percentage       :accessor order-global-discount-percentage       :initarg :order-global-discount-percentage       :initform nil)
   (order-global-discount-absolute-amount  :accessor order-global-discount-absolute-amount  :initarg :order-global-discount-absolute-amount  :initform nil)
   (order-global-surcharge-percentage      :accessor order-global-surcharge-percentage      :initarg :order-global-surcharge-percentage      :initform nil)
   (order-global-surcharge-absolute-amount :accessor order-global-surcharge-absolute-amount :initarg :order-global-surcharge-absolute-amount :initform nil)
   (order-global-total-price               :accessor order-global-total-price               :initarg :order-global-total-price               :initform nil)
   (order-confirmation-configuration       :accessor order-confirmation-configuration       :initarg :order-confirmation-configuration       :initform nil)
   (order-logistics-configuration          :accessor order-logistics-configuration          :initarg :order-logistics-configuration          :initform nil)
   (order-payment-terms-code               :accessor order-payment-terms-code               :initarg :order-payment-terms-code               :initform nil)
   (order-level-incoterms                  :accessor order-level-incoterms                  :initarg :order-level-incoterms                  :initform nil)
   (order-global-desired-delivery-date     :accessor order-global-desired-delivery-date     :initarg :order-global-desired-delivery-date     :initform nil)
   (customer-purchase-nr                   :accessor customer-purchase-nr                   :initarg :customer-purchase-nr                   :initform nil)
   (customer-purchase-date                 :accessor customer-purchase-date                 :initarg :customer-purchase-date                 :initform nil)
   (sales-channel                          :accessor sales-channel                          :initarg :sales-channel                          :initform nil)
   (sales-organisation                     :accessor sales-organisation                     :initarg :sales-organisation                     :initform nil)
   (sales-office                           :accessor sales-office                           :initarg :sales-office                           :initform nil)
   (sales-group                            :accessor sales-group                            :initarg :sales-group                            :initform nil)
   (order-global-comments                  :accessor order-global-comments                  :initarg :order-global-comments                  :initform nil)
   (order-line-items                       :accessor order-line-items                       :initarg :order-line-items                       :initform nil)
   )
  (:default-initargs
   :json-object-title "sales-order"))

(defmethod print-object ((self tb-sales-order) stream)
  (print-unreadable-object (self stream :identity t :type t)
    (format stream ":ORDER-GLOBAL-NR ~S :ORDER-ID: ~S :ORDER-NR ~S"
	    (slot-value self 'order-global-nr)
	    (slot-value self 'order-id)
	    (slot-value self 'order-nr)
	    )))

(define-json-key-slot-list-for-class tb-sales-order
    ("contract-owner"                       contract-owner                           :object)
  ("order-global-customer"                  order-global-customer                    :object tb-customer)
  ("ship-to-address"                        ship-to-address                          :object tb-ship-to-address)
  ("order-date"                             order-date)
  ("order-class"                            order-class)
  ("order-kind"                             order-kind)
  ("order-reason"                           order-reason)
  ("order-currency"                         order-currency)
  ("order-global-nr"                        order-global-nr)
  ("order-id"                               order-id)
  ("order-nr"                               order-nr)
  ("order-global-title"                     order-global-title)
  ("order-global-discount-percentage"       order-global-discount-percentage)
  ("order-global-discount-absolute-amount"  order-global-discount-absolute-amount)
  ("order-global-surcharge-percentage"      order-global-surcharge-percentage)
  ("order-global-surcharge-absolute-amount" order-global-surcharge-absolute-amount)
  ("order-global-total-price"               order-global-total-price)
  ("order-confirmation-configuration"       order-confirmation-configuration         :object)
  ("order-logistics-configuration"          order-logistics-configuration            :object)
  ("order-payment-terms-code"               order-payment-terms-code)
  ("order-level-incoterms"                  order-level-incoterms                    :object tb-incoterms)
  ("order-global-desired-delivery-date"     order-global-desired-delivery-date)
  ("customer-purchase-nr"                   customer-purchase-nr)
  ("customer-purchase-date"                 customer-purchase-date)
  ("sales-channel"                          sales-channel)
  ("sales-organisation"                     sales-organisation)
  ("sales-office"                           sales-office)
  ("sales-group"                            sales-group)
  ("order-global-comments"                  order-global-comments                    :array tb-comment)
  ("order-line-items"                       order-line-items                         :array tb-order-line-item))
