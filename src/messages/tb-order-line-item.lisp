;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-order-line-item (msg tb-version-info json-object)
  ((site                               :accessor site                               :initarg :site                               :initform nil)
   (pos-nr                             :accessor pos-nr                             :initarg :pos-nr                             :initform nil)
   (pos-type                           :accessor pos-type                           :initarg :pos-type                           :initform nil)
   (order-reason                       :accessor order-reason                       :initarg :order-reason                       :initform nil)
   (item-nr                            :accessor item-nr                            :initarg :item-nr                            :initform nil)
   (item-version                       :accessor item-version                       :initarg :item-version                       :initform nil)
   (item-revision                      :accessor item-revision                      :initarg :item-revision                      :initform nil)
   (value-stream-code                  :accessor value-stream-code                  :initarg :value-stream-code                  :initform nil)
   (quantity                           :accessor quantity                           :initarg :quantity                           :initform nil)
   (uom                                :accessor uom                                :initarg :uom                                :initform nil)
   (ship-to-address                    :accessor ship-to-address                    :initarg :ship-to-address                    :initform nil)
   (ship-from                          :accessor ship-from                          :initarg :ship-from                          :initform nil)
   (show-line-item-on-order            :accessor show-line-item-on-order            :initarg :show-line-item-on-order            :initform nil)
   (line-item-calculated-delivery-date :accessor line-item-calculated-delivery-date :initarg :line-item-calculated-delivery-date :initform nil)
   (line-item-committed-delivery-date  :accessor line-item-committed-delivery-date  :initarg :line-item-committed-delivery-date  :initform nil)
   (line-item-payment-terms-code       :accessor line-item-payment-terms-code       :initarg :line-item-payment-terms-code       :initform nil)
   (line-item-price-information        :accessor line-item-price-information        :initarg :line-item-price-information        :initform nil)
   (line-item-level-incoterms          :accessor line-item-level-incoterms          :initarg :line-item-level-incoterms          :initform nil)
   (line-item-customer                 :accessor line-item-customer                 :initarg :line-item-customer                 :initform nil)
   (line-item-comments                 :accessor line-item-comments                 :initarg :line-item-comments                 :initform nil)
   )
  (:default-initargs
   :json-object-title nil))

(define-json-key-slot-list-for-class tb-order-line-item
    ("site" site)
  ("pos-nr" pos-nr)
  ("pos-type" pos-type)
  ("order-reason" order-reason)
  ("item-nr" item-nr)
  ("item-version" item-version)
  ("item-revision" item-revision)
  ("value-stream-code" value-stream-code)
  ("quantity" quantity)
  ("uom" uom)
  ("ship-to-address" ship-to-address :object tb-ship-to-address)
  ("ship-from" ship-from :object)
  ("show-line-item-on-order" show-line-item-on-order)
  ("line-item-calculated-delivery-date" line-item-calculated-delivery-date)
  ("line-item-committed-delivery-date" line-item-committed-delivery-date)
  ("line-item-payment-terms-code" line-item-payment-terms-code)
  ("line-item-price-information" line-item-price-information :object)
  ("line-item-level-incoterms" line-item-level-incoterms :object tb-incoterms)
  ("line-item-customer" line-item-customer :object tb-customer)
  ("line-item-comments" line-item-comments :array tb-comment))
