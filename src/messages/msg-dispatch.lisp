;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  MESSAGES
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defun dispatch-msg (tb-msg)
  (tb.msgs:dispatch tb-msg))

(defparameter *msg-dispatch-channel-wait-time* 1)

(defclass msg-dispatcher (tb.core:tb-task)
  ((channel                :accessor channel                :initarg :channel                :initform nil)
   (channel-wait-time      :accessor channel-wait-time      :initarg :channel-wait-time      :initform *msg-dispatch-channel-wait-time*)
   (lock-channel-wait-time :accessor lock-channel-wait-time :initarg :lock-channel-wait-time :initform (bt:make-recursive-lock))
   (keep-listening         :accessor keep-listening         :initarg :keep-listening         :initform t)
   (lock-keep-listening    :accessor lock-keep-listening    :initarg :lock-keep-listening    :initform (bt:make-recursive-lock)))
  (:default-initargs
   :name "THINGBONE MSG DISPATCHER"))

(defmethod print-object ((msg-dispatcher msg-dispatcher) stream)
  (print-unreadable-object (msg-dispatcher stream :identity t :type t)
    (with-slots (name channel channel-wait-time keep-listening run-status) msg-dispatcher
      (format stream "~A :CHANNEL ~S :CHANNEL-WAIT-TIME ~S :KEEP-LISTENING ~S :RUN-STATUS ~S"
	      name channel channel-wait-time keep-listening run-status))))

(defun make-msg-dispatcher (name channel &optional (channel-wait-time *msg-dispatch-channel-wait-time*))
  (make-instance 'msg-dispatcher :name name :channel channel :channel-wait-time channel-wait-time))

(defmethod keep-listening-p ((msg-dispatcher msg-dispatcher))
  (bt:with-recursive-lock-held ((lock-keep-listening msg-dispatcher))
    (keep-listening msg-dispatcher)))

(defmethod set-keep-listening ((msg-dispatcher msg-dispatcher))
  (bt:with-recursive-lock-held ((lock-keep-listening msg-dispatcher))
    (setf (keep-listening msg-dispatcher) t)))

(defmethod get-channel-wait-time ((msg-dispatcher msg-dispatcher))
  (bt:with-recursive-lock-held ((lock-channel-wait-time msg-dispatcher))
    (channel-wait-time msg-dispatcher)))

(defmethod set-channel-wait-time ((msg-dispatcher msg-dispatcher) wait-time)
  (bt:with-recursive-lock-held ((lock-channel-wait-time msg-dispatcher))
    (setf (channel-wait-time msg-dispatcher) wait-time)))

(defmethod stop-listening ((msg-dispatcher msg-dispatcher))
  (bt:with-recursive-lock-held ((lock-keep-listening msg-dispatcher))
    (setf (keep-listening msg-dispatcher) nil)))

(defmethod listen-and-dispatch ((msg-dispatcher msg-dispatcher))
  (let ((channel           (channel msg-dispatcher))
	(channel-wait-time (get-channel-wait-time msg-dispatcher))
	(name              (tb.core:name msg-dispatcher)))
    (if (not channel)
	(error "No channel defined in msg dispatcher ~A." name))
    (set-keep-listening msg-dispatcher)
    (tb.core:set-run-status     msg-dispatcher :RUNNING)
    (do ((keep-listening (keep-listening-p msg-dispatcher) (keep-listening-p msg-dispatcher)))
	((not (eql keep-listening t))
	 nil)
      (progn
	(bt:thread-yield)
	(let ((msg (tb.core:recv channel :wait channel-wait-time)))
	  (if msg
	      (dispatch-msg msg)))))
    (tb.core:set-run-status msg-dispatcher :HALTED)
    (log:debug "~A: HALTED (exited listen-and-dispatch main loop)." name)
    (values)))

(defmethod reset ((msg-dispatcher msg-dispatcher))
  (stop-listening msg-dispatcher)
  (tb.core:wait-until-halted msg-dispatcher :timeout 500)
  t)

(defmethod tb.core:start ((msg-dispatcher msg-dispatcher))
  (log:info "Starting ~S ..." (tb.core:name msg-dispatcher))
  (listen-and-dispatch msg-dispatcher))

(defmethod tb.core:run-in-thread ((msg-dispatcher msg-dispatcher))
  (let* ((thread-name (format nil "MSG-DISPATCHER ~A" (tb.core:name msg-dispatcher)))
	 (thread (bt:make-thread (lambda ()
				   (tb.core:start msg-dispatcher)) :name thread-name)))
    (tb.core:set-thread msg-dispatcher thread)))

(defmethod tb.core:stop ((msg-dispatcher msg-dispatcher))
  (log:info "Stopping ~S ..." (tb.core:name msg-dispatcher))
  (reset msg-dispatcher)
  (bt:join-thread (tb.core:get-thread msg-dispatcher)))

(defun log-tb-condition (condition)
  (tb.core:send (tb.msgs:tb-condition-log-channel) condition))

(defmethod tb.msgs:dispatch ((tb-condition tb.core:tb-condition) &key &allow-other-keys)
  (dispatch-tb-condition tb-condition))
