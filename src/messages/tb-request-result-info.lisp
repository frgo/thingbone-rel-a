;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-request-result-info (msg json-object)
  ((pos-nr      :accessor pos-nr      :initarg :pos-nr      :initform nil)
   (line-nr     :accessor line-nr     :initarg :line-nr     :initform nil)
   (code        :accessor code        :initarg :code        :initform nil)
   (level       :accessor level       :initarg :level       :initform nil)
   (category    :accessor category    :initarg :category    :initform nil)
   (reason-code :accessor reason-code :initarg :reason-code :initform nil)
   (msg-nr      :accessor msg-nr      :initarg :msg-nr      :initform nil)
   (lang        :accessor lang        :initarg :lang        :initform nil)
   (text        :accessor text        :initarg :text        :initform nil)
   (source      :accessor source      :initarg :source      :initform nil)
   (timestamp   :accessor timestamp   :initarg :timestamp   :initform nil)
   )
  (:default-initargs
   :json-object-title "request-result-info"))

(defmethod jonathan:%to-json ((request-result-info tb-request-result-info))
  (with-slots (code level category reason-code msg-nr line-nr lang text source) request-result-info
    (jonathan:with-object
      (jonathan:write-key-value "code"        (or code 0))
      (jonathan:write-key-value "level"       (typecase level
						(string level)
						(integer (tb.core:msg-level-to-keyword level))
						(t (princ-to-string level))))
      (jonathan:write-key-value "category"    (or category :null))
      (jonathan:write-key-value "reason-code" (or reason-code :null))
      (jonathan:write-key-value "msg-nr"      (or msg-nr :null))
      (jonathan:write-key-value "line-nr"     (or line-nr 0))
      (jonathan:write-key-value "lang"        (or lang :null))
      (jonathan:write-key-value "text"        (or text :null))
      (jonathan:write-key-value "source"      (or source :null)))))
