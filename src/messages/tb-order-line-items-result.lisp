;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-order-line-items-result (msg json-object)
  ((pos-nr                                    :accessor pos-nr                                    :initarg :pos-nr                                    :initform nil)
   (order-line-item-cancellation-status       :accessor order-line-item-cancellation-status       :initarg :order-line-item-cancellation-status       :initform nil)
   (order-line-item-confirmation-status       :accessor order-line-item-confirmation-status       :initarg :order-line-item-confirmation-status       :initform nil)
   (order-line-item-invoice-booking-status    :accessor order-line-item-invoice-booking-status    :initarg :order-line-item-invoice-booking-status    :initform nil)
   (order-line-item-credit-check-status       :accessor order-line-item-credit-check-status       :initarg :order-line-item-credit-check-status       :initform nil)
   (order-line-item-delay-status              :accessor order-line-item-delay-status              :initarg :order-line-item-delay-status              :initform nil)
   (order-line-item-invoice-blocking-status   :accessor order-line-item-invoice-blocking-status   :initarg :order-line-item-invoice-blocking-status   :initform nil)
   (order-line-item-goods-received-status     :accessor order-line-item-goods-received-status     :initarg :order-line-item-goods-received-status     :initform nil)
   (order-line-item-commisioning-status       :accessor order-line-item-commissioning-status      :initarg :order-line-item-commissioning-status      :initform nil)
   (order-line-item-packing-status            :accessor order-line-item-packing-status            :initarg :order-line-item-delivery-status           :initform nil)
   (order-line-item-delivery-status           :accessor order-line-item-delivery-status           :initarg :order-line-item-delivery-status           :initform nil)
   (order-line-item-delivery-completed-status :accessor order-line-item-delivery-completed-status :initarg :order-line-item-delivery-completed-status :initform nil)
   (order-line-item-revenue-booking-status    :accessor order-line-item-revenue-booking-status    :initarg :order-line-item-revenue-booking-status    :initform nil)
   (order-line-item-staging-status            :accessor order-line-item-staging-status            :initarg :order-line-item-staging-status            :initform nil)
   (request-result-info                       :accessor request-result-info                       :initarg :request-result-info                       :initform nil))
  (:default-initargs
   :json-object-title "order-line-items-result"))
