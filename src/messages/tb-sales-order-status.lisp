;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-sales-order-status (msg json-object)
  ((order-nr                    :accessor order-nr                    :initarg :order-nr                    :initform nil)
   (order-global-nr             :accessor order-global-nr             :initarg :order-global-nr             :initform nil)
   (order-id                    :accessor order-id                    :initarg :order-id                    :initform nil)
   (order-global-status-info    :accessor order-global-status-info    :initarg :order-global-status-info    :initform nil)
   (order-line-item-status-info :accessor order-line-item-status-info :initarg :order-line-item-status-info :initform nil)
   (last-changed-on             :accessor last-changed-on             :initarg :last-changed-on             :initform nil)
   (last-changed-by             :accessor last-changed-by             :initarg :last-changed-by             :initform nil)
   ;; --------------------------------------------------------------------------------------------------------------
   (pos-nr                      :accessor pos-nr                      :initarg :pos-nr                      :initform nil)
   (item-nr                     :accessor item-nr                     :initarg :item-nr                     :initform nil)
   (item-version                :accessor item-version                :initarg :item-version                :initform nil)
   (item-revision               :accessor item-revision               :initarg :item-revision               :initform nil)
   (lang                        :accessor lang                        :initarg :lang                        :initform nil)
   (cancellation-status         :accessor cancellation-status         :initarg :cancellation-status         :initform nil)
   (confirmation-status         :accessor confirmation-status         :initarg :confirmation-status         :initform nil)
   (invoice-booking-status      :accessor invoice-booking-status      :initarg :invoice-booking-status      :initform nil)
   (credit-check-status         :accessor credit-check-status         :initarg :credit-check-status         :initform nil)
   (delay-status                :accessor delay-status                :initarg :delay-status                :initform nil)
   (invoice-blocking-status     :accessor invoice-blocking-status     :initarg :invoice-blocking-status     :initform nil)
   (goods-received-status       :accessor goods-received-status       :initarg :goods-received-status       :initform nil)
   (commisioning-status         :accessor commissioning-status        :initarg :commissioning-status        :initform nil)
   (packing-status              :accessor packing-status              :initarg :packing-status              :initform nil)
   (delivery-status             :accessor delivery-status             :initarg :delivery-status             :initform nil)
   (delivery-completed-status   :accessor delivery-completed-status   :initarg :delivery-completed-status   :initform nil)
   (revenue-booking-status      :accessor revenue-booking-status      :initarg :revenue-booking-status      :initform nil)
   (staging-status              :accessor staging-status              :initarg :staging-status              :initform nil)
   (request-result-info         :accessor request-result-info         :initarg :request-result-info         :initform nil))
  (:default-initargs
   :json-object-title nil))

(define-json-key-slot-list-for-class tb-sales-order-status
    ("order-nr"                  order-nr)
  ("order-global-nr"           order-global-nr)
  ("order-id"                  order-id)
  ("order-global-status-info"  order-global-status-info :object)
  ("last-changed-on"           last-changed-on)
  ("last-changed-by"           last-changed-by)
  )

(defmethod print-object ((tb-sales-order-status tb-sales-order-status) stream)
  (print-unreadable-object (tb-sales-order-status stream :identity t :type t)
    (with-slots (pos-nr item-nr item-version item-revision staging-status) tb-sales-order-status
      (format stream ":POS-NR ~S :ITEM-NR ~S :ITEM-VERSION ~S :ITEM-REVISION ~S :STAGING-STATUS ~S"
	      pos-nr item-nr item-version item-revision staging-status))))


(defmethod jonathan:%to-json ((sales-order-status tb-sales-order-status))
  (with-slots (order-nr
	       order-global-nr
	       order-id
	       order-global-status-info
	       order-line-item-status-info
	       last-changed-on
	       last-changed-by
	       ) sales-order-status
    (jonathan:with-object
      (jonathan:write-key "sales-order-status")
      (jonathan:write-value
       (jonathan:with-object
	 (jonathan:write-key-value "order-nr" (or order-nr :null))
	 (jonathan:write-key-value "order-id" (or order-id :null))
	 (jonathan:write-key-value "order-global-nr" (or order-global-nr :null))
	 (jonathan:write-key-value "last-changed-on" (or last-changed-on :null))
	 (jonathan:write-key-value "last-changed-by" (or last-changed-by :null))
	 (jonathan:write-key "order-global-status-info")
	 (jonathan:write-value order-global-status-info)
	 (jonathan:write-key "order-line-items-status-info")
	 (if order-line-item-status-info
	     (let ((length (length order-line-item-status-info)))
	       (if (> length 0)
	 	   (jonathan:write-value
	 	    (jonathan:with-array
	 	      (loop for index from 0 to (1- length)
	 		 do
	 		   (let ((element (aref order-line-item-status-info index)))
			     (if element
	 			 (jonathan:write-item element))))))))
	     (jonathan:write-value :null)))))))


(defclass tb-order-global-status-info (tb-sales-order-status)
  ()
  (:default-initargs
   :json-object-title "order-global-status-info"))

(define-json-key-slot-list-for-class tb-order-global-status-info
    ("staging-status"  staging-status)
  ("lang"            lang))

(defmethod jonathan:%to-json ((self tb-order-global-status-info))
  (with-slots (lang
	       cancellation-status
	       confirmation-status
	       invoice-booking-status
	       credit-check-status
	       delay-status
	       invoice-blocking-status
	       goods-received-status
	       commisioning-status
	       packing-status
	       delivery-status
	       delivery-completed-status
	       revenue-booking-status
	       staging-status) self
    (jonathan:with-object
      (jonathan:write-key-value "lang"                      (or lang :null))
      (jonathan:write-key-value "cancellation-status"       (or cancellation-status :null))
      (jonathan:write-key-value "confirmation-status"       (or confirmation-status :null))
      (jonathan:write-key-value "invoice-booking-status"    (or invoice-booking-status :null))
      (jonathan:write-key-value "credit-check-status"       (or credit-check-status :null))
      (jonathan:write-key-value "delay-status"              (or delay-status :null))
      (jonathan:write-key-value "invoice-blocking-status"   (or invoice-blocking-status :null))
      (jonathan:write-key-value "goods-received-status"     (or goods-received-status :null))
      (jonathan:write-key-value "commisioning-status"       (or commisioning-status :null))
      (jonathan:write-key-value "packing-status"            (or packing-status :null))
      (jonathan:write-key-value "delivery-status"           (or delivery-status :null))
      (jonathan:write-key-value "delivery-completed-status" (or delivery-completed-status :null))
      (jonathan:write-key-value "revenue-booking-status"    (or revenue-booking-status :null))
      (jonathan:write-key-value "staging-status"            (or staging-status :null)))))


(defclass tb-order-line-item-status-info (tb-sales-order-status)
  ()
  (:default-initargs
   :json-object-title "order-line-item-status-info"))

(defmethod jonathan:%to-json ((self tb-order-line-item-status-info))
  (with-slots (pos-nr
	       item-nr
	       item-version
	       item-revision
	       lang
	       cancellation-status
	       confirmation-status
	       invoice-booking-status
	       credit-check-status
	       delay-status
	       invoice-blocking-status
	       goods-received-status
	       commisioning-status
	       packing-status
	       delivery-status
	       delivery-completed-status
	       revenue-booking-status
	       staging-status
	       request-result-info) self
    (jonathan:with-object
      (jonathan:write-key-value "site" (tb.core:tb-site))
      (jonathan:write-key-value "pos-nr" (tb.msgs:stringify-msg-value pos-nr))
      (jonathan:write-key-value "item-nr" (or item-nr :null))
      (jonathan:write-key-value "item-version" (or item-version :null))
      (jonathan:write-key-value "item-revision" (or item-revision :null))
      (jonathan:write-key "order-line-item-status-info")
      (jonathan:write-value
       (jonathan:with-object
	 (jonathan:write-key-value "lang"                      (or lang :null))
	 (jonathan:write-key-value "cancellation-status"       (or cancellation-status :null))
	 (jonathan:write-key-value "confirmation-status"       (or confirmation-status :null))
	 (jonathan:write-key-value "invoice-booking-status"    (or invoice-booking-status :null))
	 (jonathan:write-key-value "credit-check-status"       (or credit-check-status :null))
	 (jonathan:write-key-value "delay-status"              (or delay-status :null))
	 (jonathan:write-key-value "invoice-blocking-status"   (or invoice-blocking-status :null))
	 (jonathan:write-key-value "goods-received-status"     (or goods-received-status :null))
	 (jonathan:write-key-value "commisioning-status"       (or commisioning-status :null))
	 (jonathan:write-key-value "packing-status"            (or packing-status :null))
	 (jonathan:write-key-value "delivery-status"           (or delivery-status :null))
	 (jonathan:write-key-value "delivery-completed-status" (or delivery-completed-status :null))
	 (jonathan:write-key-value "revenue-booking-status"    (or revenue-booking-status :null))
	 (jonathan:write-key-value "staging-status"            (or staging-status :null))))
      (jonathan:write-key "request-result-info")
      (if request-result-info
	  (let ((length (length request-result-info)))
	    (if (> length 0)
	 	(jonathan:write-value
	 	 (jonathan:with-array
	 	   (loop for index from 0 to (1- (length request-result-info))
	 	      do
	 		(let ((element (aref request-result-info index)))
			  (if element
	 		      (jonathan:write-item element))))))))
	  (jonathan:write-value :null))

      )))
