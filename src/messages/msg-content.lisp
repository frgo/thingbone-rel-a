;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  MESSAGES
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass msg-content (msg-element)
  ((object-class :accessor object-class :initarg :object-class :initform nil)
   (operation    :accessor operation    :initarg :operation    :initform nil)
   (data         :accessor data         :initarg :data         :initform (make-instance 'msg-data))))


(defmethod jonathan:%to-json ((msg-content msg-content))
  (break)
  (with-slots (object-class operation data) msg-content
    (jonathan:with-object
      (jonathan:write-key-value "object-class" object-class)
      (jonathan:write-key-value "operation"    operation)
      (jonathan:with-array
	(loop for index from 0 to (1- (length data))
	   do
	     (jonathan:write-item (aref data index)))))))
