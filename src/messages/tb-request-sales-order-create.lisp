;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-request-sales-order-create (extended-msg json-object)
  ((data-context :accessor data-context :initarg :data-context :initform nil)
   (sales-order  :accessor sales-order  :initarg :sales-order  :initform nil))
  (:default-initargs
   :json-object-title "request-sales-order-create"))

(define-json-key-slot-list-for-class tb-request-sales-order-create
    ("data-context" data-context :object)
  ("sales-order" sales-order :object))

(defmethod from-json ((self tb-request-sales-order-create) json-list)

  ;; Note:
  ;; This function expects a complete object list where the first
  ;; element is the object "title itself:
  ;; { "data" : { ... } }
  ;; (let ((element-list (first (cdr json-list))))
  ;;   (loop for element in element-list
  ;;      do
  ;; 	 (set-slot-values-from-json-object self element-list)
  ;; 	 ))
  (log:debug "In from-json: ~S" self)

  (set-slot-values-from-json-object self (first (cdr json-list)))

  (let ((msg-data (data (content self))))
    (setf (kind msg-data) :tb-request-sales-order-create)
    (setf (data msg-data) (list (data-context self) (sales-order self))))
  self)

(defun make-tb-request-sales-order-create-from-json (json-list)
  (let* ((instance (make-instance 'tb-request-sales-order-create))
	 (from-json (from-json instance json-list)))
    from-json))

;; (defun test-make-data-from-json-request-sales-order-create ()
;;   (let* ((performative "request")
;; 	 (object-class "sales-order")
;; 	 (operation    "create")
;; 	 (json-list (jonathan:parse "{ \"data\": [
;; 	 {
;; 	 \"data-context\": {
;; 	 \"environment\": \"TT\"
;; 	 }
;; 	 },

;; 	 {
;; 	 \"sales-order\": {

;; 	 \"contract-owner\": {
;; 	 \"company-nr\": \"0120120203\"
;; 	 },
;; 	 \"order-global-customer\": {
;; 	 \"business-partner-nr-sold-to\": \"100749\",
;; 	 \"business-partner-nr-invoice-to\": \"100749\",
;; 	 \"business-partner-nr-ship-to\": \"100749\",
;; 	 \"business-partner-nr-final-receiver\": \"100749\",
;; 	 \"business-partner-nr-clearance-from\": \"100749\",
;; 	 \"business-partner-contact-name\": \"Franky goes to Holluwood\"
;; 	 },
;; 	 \"ship-to-address\": {
;; 	 \"addressee-name\": \"Mr. Han Solo\",
;; 	 \"address-line-1\": \"\",
;; 	 \"address-line-2\": \"Appartment 911\",
;; 	 \"address-line-3\": \"Waikiki Beach Hotel\",
;; 	 \"address-line-4\": \"101 Know-It-All Blvd\",
;; 	 \"address-line-5\": \"\",
;; 	 \"city\": \"Chunchucmil\",
;; 	 \"postal-code\": \"66688\",
;; 	 \"state-code\": \"YU\",
;; 	 \"country-code\": \"MX\",
;; 	 \"region\": \"\"
;; 	 },
;; 	 \"order-date\": \"2019-06-01Z\",
;; 	 \"order-class\": \"spare-part\",
;; 	 \"order-kind\": \"ET\",
;; 	 \"order-reason\": \"replenishment\",
;; 	 \"order-currency\": \"EUR\",
;; 	 \"order-global-nr\": \"O-9019820912\",
;; 	 \"order-global-title\": \"Order für 20 coole Schrauben\",
;; 	 \"order-global-discount-percentage\": 10,
;; 	 \"order-global-discount-absolute-amount\": 1000,
;; 	 \"order-global-surcharge-percentage\": 10,
;; 	 \"order-global-surcharge-absolute-amount\": 1000,
;; 	 \"order-global-total-price\": 3200.34,
;; 	 \"order-confirmation-configuration\": {
;; 	 \"show-order-global-discount\": true,
;; 	 \"show-order-global-surcharge\": true,
;; 	 \"show-line-item-unit-price\": false,
;; 	 \"show-line-item-discount\": true,
;; 	 \"show-line-item-surcharge\": true
;; 	 },
;; 	 \"order-logistics-configuration\": {
;; 	 \"ship-when-complete\": true
;; 	 },
;; 	 \"order-payment-terms-code\": \"30T+10SAT+60D\",
;; 	 \"order-level-incoterms\": {
;; 	 \"incoterms-location\": \"Chunchucmil\",
;; 	 \"incoterms-terms\": \"EXW\"
;; 	 },
;; 	 \"order-global-desired-delivery-date\": \"2019-08-04Z\",
;; 	 \"customer-purchase-nr\": \"\",
;; 	 \"customer-purchase-date\": \"\",
;; 	 \"sales-channel\": \"\",
;; 	 \"sales-organisation\": \"\",
;; 	 \"sales-office\": \"\",
;; 	 \"sales-group\": \"\",
;; 	 \"order-global-comments\": [
;; 	 {
;; 	 \"lang\": \"ger\",
;; 	 \"comment-line-index-nr\": 1,
;; 	 \"comment-category\": \"\",
;; 	 \"text\": \"Wir lieben es!\"
;; 	 },
;; 	 {
;; 	 \"lang\": \"eng\",
;; 	 \"comment-line-index-nr\": 2,
;; 	 \"comment-category\": \"\",
;; 	 \"text\": \"It's not that you didn't know it!\"
;; 	 }
;; 	 ],
;; 	 \"order-line-items\": [
;; 	 {
;; 	 \"site\": \"TT\",
;; 	 \"pos-nr\": \"10\",
;; 	 \"pos-type\": \"item\",
;; 	 \"order-reason\": \"replenishment\",
;; 	 \"item-nr\": \"0000079031\",
;; 	 \"item-version\": \"\",
;; 	 \"item-revision\": \"B10\",
;; 	 \"value-stream-code\": \"C\",
;; 	 \"quantity\": 1,
;; 	 \"uom\": \"u\",
;; 	 \"ship-from\": {
;; 	 \"ship-from-environment\": \"STU\",
;; 	 \"ship-from-company-nr\": \"12122344\",
;; 	 \"ship-from-warehouse-nr\": \"2\",
;; 	 \"ship-from-plant-nr\": \"2\"
;; 	 },
;; 	 \"show-line-item-on-order\": true,
;; 	 \"line-item-calculated-delivery-date\": \"2019-08-04Z\",
;; 	 \"line-item-committed-delivery-date\": \"2019-08-07Z\",
;; 	 \"line-item-payment-terms-code\": \"PRE100\",
;; 	 \"line-item-price-information\": {
;; 	 \"unit-price\": 200.00,
;; 	 \"customer-visible-surcharge-percentage\": 10,
;; 	 \"customer-visible-discount-percentage\": 10,
;; 	 \"customer-invisible-surcharge-percentage\": 10,
;; 	 \"customer-invisible-discount-percentage\": 10,
;; 	 \"customer-visible-surcharge-amount\": 0,
;; 	 \"customer-visible-discount-amount\": 0,
;; 	 \"customer-invisible-surcharge-amount\": 0,
;; 	 \"customer-invisible-discount-amount\": 0,
;; 	 \"line-item-resulting-price\": 2000.00
;; 	 },
;; 	 \"line-item-level-incoterms\": {
;; 	 \"incoterms-location\": \"Nuertingen\",
;; 	 \"incoterms-terms\": \"EXW\"
;; 	 },
;; 	 \"line-item-customer\": {
;; 	 \"business-partner-nr-sold-to\": \"100749\",
;; 	 \"business-partner-nr-invoice-to\": \"100749\",
;; 	 \"business-partner-nr-ship-to\": \"100749\",
;; 	 \"business-partner-nr-final-receiver\": \"100749\",
;; 	 \"business-partner-nr-clearance-from\": \"100749\",
;; 	 \"business-partner-contact-name\": \"Franky goes to Holluwood\"
;; 	 },
;; 	 \"line-item-comments\": [
;; 	 {
;; 	 \"lang\": \"fre\",
;; 	 \"comment-line-index-nr\": 1,
;; 	 \"comment-category\": \"\",
;; 	 \"text\": \"C'est impossible !\"
;; 	 },
;; 	 {
;; 	 \"lang\": \"fre\",
;; 	 \"comment-line-index-nr\": 2,
;; 	 \"comment-category\": \"\",
;; 	 \"text\": \"Qu'est-çe c'est que ça ?\"
;; 	 }
;; 	 ]
;; 	 }
;; 	 ]
;; 	 }
;; 	 }
;; 	 ]
;; 	 }
;; }" :as :aslist)))

;;     (make-extended-msg-from-json performative object-class operation json-list)))
