;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  MESSAGES
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass msg (msg-base)
  ((source-info-list :accessor source-info-list :initarg :source-info-list :initform '())
   (error-info-list  :accessor error-info-list  :initarg :error-info-list  :initform nil)
   (msg-as-string    :accessor msg-as-string    :initarg :msg-as-string    :initform nil)))

;; (defmethod publish (transport (msg msg))
;; (declare (ignore transport))
;; (let ((log-str (format nil "Class ~S or one of its superclasses must implement method PUBLSH."
;; 		       (class-name (class-of msg)) msg)))
;;   (log:error "~A" log-str)
;;   (error "~A" log-str)))

(defmethod dispatch ((msg msg) &key &allow-other-keys)
  (let ((err-msg (format nil "DISPATCH: Subclass must implement #'dispatch method!")))
    (log:error "~A" err-msg)
    (error "~A" err-msg)))

(defmethod first-error-code-and-level ((msg msg))
  (let ((error-info-list (error-info-list msg)))
    (or (loop for error-info in error-info-list
	   do
	     (let ((code (code error-info))
		   (level (level error-info)))
	       (if (or (not (= code 0))
		       (not (or (eql level :WARNING)
				(eql level :INFORMATIONAL)
				(eql level :DEBUG))))
		   (return (values code level)))))
	(values 0 :INFORMATIONAL))))

(defmethod errorp ((msg msg))
  (multiple-value-bind (code level)
      (first-error-code-and-level msg)
    (or (not (= code 0))
	(not (or (eql level :WARNING)
		 (eql level :INFORMATIONAL)
		 (eql level :DEBUG))))))
