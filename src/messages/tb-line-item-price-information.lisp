;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.MESSAGES")

;;; ---------------------------------------------------------------------------

(defclass tb-line-item-price-information (msg json-object)
  ((unit-price                              :accessor unit-price                              :initarg :unit-price                              :initform nil)
   (customer-visible-surcharge-percentage   :accessor customer-visible-surcharge-percentage   :initarg :customer-visible-surcharge-percentage   :initform nil)
   (customer-visible-discount-percentage    :accessor customer-visible-dicsount-percentage    :initarg :customer-visible-discount-percentage    :initform nil)
   (customer-invisible-surcharge-percentage :accessor customer-invisible-surcharge-percentage :initarg :customer-invisible-surcharge-percentage :initform nil)
   (customer-invisible-discount-percentage  :accessor customer-invisible-dicsount-percentage  :initarg :customer-invisible-discount-percentage  :initform nil)
   (customer-visible-surcharge-amount       :accessor customer-visible-surcharge-amount       :initarg :customer-visible-surcharge-amount       :initform nil)
   (customer-visible-discount-amount        :accessor customer-visible-dicsount-amount        :initarg :customer-visible-discount-amount        :initform nil)
   (customer-invisible-surcharge-amount     :accessor customer-invisible-surcharge-amount     :initarg :customer-invisible-surcharge-amount     :initform nil)
   (customer-invisible-discount-amount      :accessor customer-invisible-dicsount-amount      :initarg :customer-invisible-discount-amount      :initform nil)
   (line-item-resulting-price               :accessor line-item-resulting-price               :initarg :line-item-resulting-price               :initform nil)
   (show-line-item-price                    :accessor show-line-item-price                    :initarg :show-line-item-price                    :initform nil)
   )
  (:default-initargs
   :json-object-title "line-item-price-information"))

(define-json-key-slot-list-for-class tb-line-item-price-information
    ("unit-price"                             unit-price)
  ("customer-visible-surcharge-percentage"    customer-visible-surcharge-percentage)
  ("customer-visible-discount-percentage"     customer-visible-discount-percentage)
  ("customer-invisible-surcharge-percentage"  customer-invisible-surcharge-percentage)
  ("customer-invisible-discount-percentage"   customer-invisible-discount-percentage)
  ("customer-visible-surcharge-amount"        customer-visible-surcharge-amount)
  ("customer-visible-discount-amount"         customer-visible-discount-amount)
  ("customer-invisible-surcharge-amount"      customer-invisible-surcharge-amount)
  ("customer-invisible-discount-amount"       customer-invisible-discount-amount)
  ("line-item-resulting-price"                line-item-resulting-price)
  ("show-line-item-price"                     show-line-item-price))
