;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  ASDF REGISTERING
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "CL-USER")

(asdf:defsystem #:thingbone.messages
  :description ""
  :author "Frank Goenninger <frank.goenninger@bwpapersystems.com>"
  :maintainer "BWP STU IT <it.stuttgart@bwpapersystems.com>"
  :license  "Proprietary. All Rights reserved."
  :version "0.0.1"
  :depends-on (:jonathan
	       :thingbone.core
	       )
  :serial t
  :components
  ((:module thingbone.messages
	    :pathname "src/messages/"
	    :components
	    ((:file "package")
	     (:file "constants")
	     (:file "msg-utils")
	     (:file "json-serializable")
	     (:file "msg-base")
	     (:file "msg-element")
             (:file "msg-source-info")
	     (:file "msg")
	     (:file "msg-text")
	     (:file "msg-sender")
	     (:file "msg-qos")
	     (:file "msg-requestor-context")
	     (:file "msg-content")
	     (:file "msg-error-info")
	     (:file "msg-extended-content")
	     (:file "msg-data")
	     (:file "extended-msg")
	     (:file "msg-dispatch")
	     (:file "msg-dispatch-conditions")

	     (:file "tb-text")
	     (:file "tb-validity")
	     (:file "tb-version-info")

	     (:file "tb-address")
	     (:file "tb-business-partner-address")
	     (:file "tb-classification")
	     (:file "tb-contract-owner")
	     (:file "tb-comment")
	     (:file "tb-comments")
	     (:file "tb-credit-limit")
	     (:file "tb-customer")
	     (:file "tb-data-context")
	     (:file "tb-incoterms")
	     (:file "tb-line-item-price-information")
	     (:file "tb-order-address")
	     (:file "tb-order-confirmation-configuration")
	     (:file "tb-order-logistics-configuration")
	     (:file "tb-order-line-item")
             (:file "tb-request-result-info")
	     (:file "tb-sales-order")
	     (:file "tb-request-sales-order-create")
	     (:file "tb-ship-from")
	     (:file "tb-term-or-condition")
	     (:file "tb-sales-order-status")
	     (:file "tb-sales-order-create-result")
	     (:file "tb-sales-order-update-result")
	     (:file "tb-response-sales-order-create")
	     (:file "tb-response-sales-order-update")
	     (:file "tb-request-sales-order-update")
	     (:file "tb-inform-sales-order-status-update")

	     ))))
