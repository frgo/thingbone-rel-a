(in-package "CL-USER")

(load "/opt/common-lisp/quicklisp/setup.lisp")
(load "/var/data/swdev/thingbone/register.lisp")

(ql:quickload "cl-fad")
(ql:quickload "thingbone.core")
(ql:quickload "thingbone.libsapnwrfc")
(ql:quickload "thingbone.gateway.sap-servb")
(ql:quickload "thingbone.aserve-restful")
;;(ql:quickload "swank")
(ql:quickload "thingbone.main")

(tb.core::init-0)

(setq *restart-app-function* 'thingbone.main:main)

(thingbone.libsapnwrfc:rfc-reset nil)
(thingbone.libsapnwrfc:rfc-init)
