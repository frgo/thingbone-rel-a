;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  LISP APPLICATION GENERATION
;;;
;;; =====================================================================


(in-package "CL-USER")

(load "/opt/common-lisp/quicklisp/setup.lisp")
(load "/var/data/swdev/thingbone/register.lisp")

(eval-when (:load-toplevel :execute :compile-toplevel)
  (require :ssl)
  (require :aserve))

(push :thingbone-production cl:*features*)
;; (ql:quickload "swank")
(ql:quickload "thingbone.main")

(excl:exit 0)
