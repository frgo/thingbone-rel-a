;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  LISP APPLICATION GENERATION
;;;
;;; =====================================================================

(in-package "CL-USER")

(load "/opt/common-lisp/quicklisp/setup.lisp")
(load "/var/data/swdev/thingbone/register.lisp")
(ql:quickload "cl-fad")

(defun gen-tb-gw-application ()

  ;;(setq *restart-app-function* 'thingbone.main:main)

  ;;(thingbone.libsapnwrfc:rfc-reset nil)
  ;;(thingbone.libsapnwrfc:rfc-init)

  (let ((destination-directory (cl-fad:pathname-as-directory #p"/var/data/swdev/thingbone/delivery/output/thingbone-gateway/")))

    (if (cl-fad:directory-exists-p destination-directory)
	(cl-fad:delete-directory-and-files destination-directory))

    (excl:generate-application "tbgw"
			       destination-directory
			       '("/var/data/swdev/thingbone/delivery/tools/load-tb-gw-app.lisp")
			       :application-type :exe
			       :copy-shared-libraries t
			       :include-locales t
			       :include-compiler nil
			       :include-tpl nil
			       :debug t
			       :verbose t
			       )))

(push :thingbone-production cl:*features*)
(gen-tb-gw-application)
(excl:exit 0)
