;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  CONFIG
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "CL-USER")

;;; --- MODIFY TO YOUR NEEDS ---

;;; - SAP-SERVB CONNECTION PARAMETERS: CLIENT -

;;; STU: SAPROUTER = "/H/bwpstusap.bwcinc.org/S/5890"
;;; HAM: SAPROUTER = "/H/bwphamsap.bwcinc.org/S/56080"


;;; === T E S T I N G ===

(defparameter *bwp-stu-kw1-connection-params*
  (let* ((sap-router     "/H/10.79.128.1/S/5890")
	 (sap-app-server "wudkw1ci")
	 (sap-sysnr      "00")
	 (sap-client     "500")
	 (sap-lang       "EN")
	 (sap-user       "THINGBONE")
	 (sap-pw         "t3hdi5ncg5b8o4nfe3a24e9e2136fd6a6daf731e")
	 (sap-trace      "3")
	 (connection-params (list (list "SAPROUTER" sap-router)
				  (list "ASHOST"    sap-app-server)
				  (list "SYSNR"     sap-sysnr)
				  (list "CLIENT"    sap-client)
				  (list "LANG"      sap-lang)
				  (list "USER"      sap-user)
				  (list "PASSWD"    sap-pw)
				  (list "TRACE"     sap-trace)
				  )))
    connection-params))

(defparameter *bwp-stu-kw1-luederh-connection-params*
  (let* ((sap-router     "/H/10.79.128.1/S/5890")
	 (sap-app-server "wudkw1ci")
	 (sap-sysnr      "00")
	 (sap-client     "500")
	 (sap-lang       "EN")
	 (sap-user       "luederh")
	 (sap-pw         "kl23060")
	 (sap-trace      "3")
	 (connection-params (list (list "SAPROUTER" sap-router)
				  (list "ASHOST"    sap-app-server)
				  (list "SYSNR"     sap-sysnr)
				  (list "CLIENT"    sap-client)
				  (list "LANG"      sap-lang)
				  (list "USER"      sap-user)
				  (list "PASSWD"    sap-pw)
				  (list "TRACE"     sap-trace)
				  )))
    connection-params))

(if (string= (tb.core:tb-site) "STU")
    (setf (tb.gw.sap-servb:sap-servb-client-rfc-connection-params)
	  *bwp-stu-kw1-luederh-connection-params*))

(log:debug "T E S T I N G with the following SAP connection parameters: ~S" (tb.gw.sap-servb:sap-servb-client-rfc-connection-params))

;;; === P R O D U C T I O N ===

;; (if (string= (tb.core:tb-site) "STU")
;;     (setf (tb.gw.sap-servb:sap-servb-client-rfc-connection-params)
;; 	  (list (list "SAPROUTER" "/H/10.79.128.1/S/5890")
;; 		(list "ASHOST"    "wudkw2ci")
;; 		(list "SYSNR"     "02")
;; 		(list "CLIENT"    "500")
;; 		(list "LANG"      "EN")
;; 		(list "USER"      "THINGBONE")
;; 		(list "PASSWD"    "t3hdi5ncg5b8o4nfe3a24e9e2136fd6a6daf731e")
;; 		(list "TRACE"     "1"))))

;; (if (string= (tb.core:tb-site) "HAM")
;;     (setf (tb.gw.sap-servb:sap-servb-client-rfc-connection-params)
;; 	  (list (list "SAPROUTER" "/H/10.78.176.27/S/56080")
;; 		(list "ASHOST"    "echp10ci")
;; 		(list "SYSNR"     "00")
;; 		(list "CLIENT"    "100")
;; 		(list "LANG"      "EN")
;; 		(list "USER"      "USR_SERVBUS")
;; 		(list "PASSWD"    "Hamburg#1")
;; 		(list "TRACE"     "1"))))

(if (string= (tb.core:tb-site) "HAM")
    (setf (tb.gw.sap-servb:sap-servb-client-rfc-connection-params)
	  (list (list "SAPROUTER" "/H/10.78.176.27/S/56080")
		(list "ASHOST"    "echq10ci")
		(list "SYSNR"     "02")
		(list "CLIENT"    "100")
		(list "LANG"      "EN")
		(list "USER"      "USR_SERVBUS")
		(list "PASSWD"    "Hamburg#1")
		(list "TRACE"     "1"))))

;;; - SAP-SERVB CONNECTION PARAMETERS: SERVER / SAP GW -

(if (string= (tb.core:tb-site) "STU")
    (setf (tb.gw.sap-servb:sap-servb-server-rfc-connection-params)
	  (list (list "SAPROUTER"  "/H/10.79.128.1/S/5890")
		(list "GWHOST"     "10.218.2.103")
		(list "GWSERV"     "sapgw00")
		(list "PROGRAM_ID" "TB_GW_SAP_SERVB")
		(list "TRACE"      "1"))))

(if (string= (tb.core:tb-site) "HAM")
    (setf (tb.gw.sap-servb:sap-servb-server-rfc-connection-params)
	  (list (list "SAPROUTER"  "/H/10.78.176.27/S/56080")
		(list "GWHOST"     "10.218.2.103")
		(list "GWSERV"     "sapgw00")
		(list "PROGRAM_ID" "TB_GW_SAP_SERVB")
		(list "TRACE"      "1"))))

;;; - THINGBONE API PORT -

;; (if (string= (tb.core:tb-site) "HAM")
;;     (excl.osi:setenv "THINGBONE_GATEWAY_API_PORT" 7190 t))

;; (if (string= (tb.core:tb-site) "STU")
;;     (excl.osi:setenv "THINGBONE_GATEWAY_API_PORT" 7191 t))


;;; --- GATEWAYS ---
;; THIS DOES NOT WORK YET!
;; Create SAP R/3 Gateway
;; (tb.gw.sap-servb:make-and-register-sapr3-gateway :site (tb.core:tb-site) :servb-domain :servb-inbound
;; 						 :client-connection-params (tb.gw.sap-servb:sap-servb-client-rfc-connection-params)
;; 						 :server-gw-connection-params  (tb.gw.sap-servb:sap-servb-server-rfc-connection-params))

;; (tb.gw.sap-servb:define-function-module "Z_MAT_SALES_ORDER_REQUEST" :site-list ("STU" "HAM"))

;; (tb.gw.sap-servb:define-servb-data-container-for-function-module "Z_MAT_SALES_ORDER_REQUEST" "IS_HEADER" :STRUCTURE :IMPORTING :site-list ("STU" "HAM")
;; 								 :slot-definition-list (("ORDER_DATE" :char 8 :required)
;; 											("VISIBLE_DISC_ABS" :char 15)
;; 											("RESULTING_PRICE" :char 15 :required)))
