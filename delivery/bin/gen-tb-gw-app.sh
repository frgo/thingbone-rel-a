#!/bin/bash

PATH=/opt/common-lisp/lang/allegrocl/acl10.1.64/:$PATH
export PATH

# LIBCRYPTO="`ldconfig -v -p | grep "libcrypto.so.1.0.2" | awk '{ print $1 }' -`"
# LIBSSL="`ldconfig -v -p | grep "libssl.so.1.0.2" | awk '{ print $1 }' -`"

# if [ "x$LIBCRYPTO" != "x" -a "x$LIBSSL" != "x" ]
# then
#   THINGBONE_SSL_LIBRARY_NAMES="$LIBCRYPTO $LIBSSL"
#   export THINGBONE_SSL_LIBRARY_NAMES
#   echo "THINGBONE_SSL_LIBRARY_NAMES=$THINGBONE_SSL_LIBRARY_NAMES"
#   export ACL_SSL_LIBRARY_NAMES="$THINGBONE_SSL_LIBRARY_NAMES"
# fi

alisp -s /var/data/swdev/thingbone/delivery/tools/compile-tb-gw-app.lisp
alisp -s /var/data/swdev/thingbone/delivery/tools/gen-tb-gw-app.lisp
