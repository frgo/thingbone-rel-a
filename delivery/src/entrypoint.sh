#!/bin/bash -e

# --- SEPCIAL VARS ---

LOGFILE=/tmp/entrypoint.log

QUIET=0
DEBUG=1

TAG=`basename $0`
FAC="local0"

# =============================================================================
# function:     logit
# -----------------------------------------------------------------------------
# parameter:    $1      priority for syslog messages
#               $2      message string
# purpose:      writes message to syslog and optinally to log file
# =============================================================================

logit()
{
    logger -i -t $TAG -p $FAC.$1 "$2"

    DATE=`date "+%b %d %T"`
    MSG="$DATE $HOST $TAG[$$]: $2"

    if [ "$LOGFILE" != "" ]
    then
      echo "$MSG"   >>$LOGFILE
    fi

    if [ $QUIET -ne 1 ]
    then
      echo "$2"
    fi

    return
}

# =============================================================================
# function:     cleanup
# -----------------------------------------------------------------------------
# parameter:    ./.
# purpose:      Cleans up files that may have been created until receiving
#               a signal or up until an error occured.
# =============================================================================

cleanup()
{
    logit INFO "cleaning up..."

    rm -f $LOCKFILE >/dev/null 2>&1

    return
}

# =============================================================================
# function:     existsproc
# -----------------------------------------------------------------------------
# parameter:    $1      command string of process
# purpose:      checks process list for existence of specified process
# =============================================================================

existsproc()
{
    if [ "$1" = "" ]
    then
	return 1
    fi

    pid=`ps -ef | grep "$1" | grep -v "grep" | awk '{ print $2 }  `

    if [ "$pid" != "" -a "$pid" != "$$" ]
    then
	return 0
    else
	return 1
    fi
}

# =============================================================================
# function:     killproc
# -----------------------------------------------------------------------------
# parameter:    $1      command string of process
# purpose:      kills specified process in graceful way:
#               first send SIGTERM, wait 2 seconds, send SIGKILL
# =============================================================================

killproc()
{
    pid=`ps -ef | grep "$1" | grep -v "grep" | awk '{ print $2 }  `

    if [ "$pid" != "" ]
    then

	kill $pid >/dev/null 2>&1
	sleep 2
	if ps -p $pid >/dev/null 2>&1
	then
	    kill -9 $pid >/dev/null 2>&1
	fi
	echo "$MSG_PREFIX \"$1\" stopped"

    else

	echo "$MSG_PREFIX \"$1\" already stopped"

    fi
}

# =============================================================================
# function:     terminate
# -----------------------------------------------------------------------------
# parameter:    ./.
# purpose:      Prints INFO message and terminates the process
# =============================================================================

terminate()
{
    logit INFO "Shutting down Docker Entrypoint..."
    killproc alisp
    exit 0
}

# =============================================================================
# function:     kill_multiple_proc
# -----------------------------------------------------------------------------
# parameter:    $1      command string/regex of process(es)
# purpose:      kills specified process(es) in graceful way:
#               first send SIGTERM, wait 2 seconds, send SIGKILL
# =============================================================================

kill_multiple_proc()
{
  integer COUNT=0

  ps -ef | grep "$1" | grep -v "grep" | awk '{ print $2 }' | \
  while read pid
  do

      if [ "$pid" != "" ]
      then
	  kill $pid >/dev/null 2>&1
	  sleep 2
	  if ps -p $pid >/dev/null 2>&1
	  then
              kill -9 $pid >/dev/null 2>&1
	  fi
	  let COUNT+=1
      fi

  done

  echo "$COUNT process(es) matching \"$1\" killed"
}

# =============================================================================
# function:     kill_user
# -----------------------------------------------------------------------------
# parameter:    $1      user whose processes will be killed
# purpose:      kills processes in graceful way:
#               first send SIGTERM, wait 2 seconds, send SIGKILL
# =============================================================================

kill_user()
{
  integer RC=0

  ps -fu "$1" 2>/dev/null | awk '{ print $2 } ' |      \
    while read pid
  do
    if [ "$pid" != "" ]
    then
      kill $pid >/dev/null 2>&1
    fi
  done

  sleep 2

  ps -fu "$1" 2>/dev/null | awk '{ print $2 } ' |      \
    while read pid
  do
    if [ "$pid" != "" ]
    then
      kill -9 $pid >/dev/null 2>&1
      if ps -p $pid >/dev/null 2>&1
      then
        RC=RC+1
      fi
    fi
  done

  return $RC
}

# =============================================================================
# function:     trap_handler
# -----------------------------------------------------------------------------
# parameter:    $1      signal number
# purpose:      Called when a signal is received (e.g. Ctrl-C). Cleans up
#               files that may have been created until receiving the signal.
# =============================================================================

trap_handler()
{
  logit INFO "Signal $1 caught..."

  case "$1" in

      15) logit INFO "exiting..."

          ;;

      *)  logit INFO "aborting..."
          ;;

  esac

  terminate
  cleanup

  exit $1
}

# --- SETUP SIGNAL HANDLING ---------------------------------------------------

trap "trap_handler 2"   2
trap "trap_handler 3"   3
trap "trap_handler 4"   4
trap "trap_handler 5"   5
trap "trap_handler 6"   6
trap "trap_handler 7"   7
trap "trap_handler 8"   8
trap "trap_handler 9"   9
trap "trap_handler 10" 10
trap "trap_handler 11" 11
trap "trap_handler 12" 12
trap "trap_handler 13" 13
trap "trap_handler 14" 14
trap "trap_handler 15" 15

# --- START ALLEGROCL ---------------------------------------------------------

CMD="sudo -u $THINGBONE_GATEWAY_APPUSER -E -- /home/$THINGBONE_GATEWAY_APPUSER/run-tbgw.sh"
logit INFO "About to execute command \"$CMD\" ..."
$CMD
