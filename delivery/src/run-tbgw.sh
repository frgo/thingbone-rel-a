#!/bin/bash -e

# --- SEPCIAL VARS ---

LOGFILE=${TBGW_RUN_LOGFILE:-/tmp/run-tbgw.log}
EXE=${THINGBONE_GATEWAY_EXE:-tbgw}
RFC_TRACE_DIR=${THINGBONE_GATEWAY_LOGDIR:-/var/data/thingbone/gateway/logs}

QUIET=0
DEBUG=1

MSG_PREFIX=`basename $0`
TAG=${MSG_PREFIX}
FAC="local0"

# =============================================================================
# function:     logit
# -----------------------------------------------------------------------------
# parameter:    $1      priority for syslog messages
#               $2      message string
# purpose:      writes message to syslog and optinally to log file
# =============================================================================

logit()
{
    logger -i -t $TAG -p $FAC.$1 "$2"

    DATE=`date "+%b %d %T"`
    MSG="$DATE $HOST $TAG[$$]: $2"

    if [ "$LOGFILE" != "" ]
    then
      echo "$MSG"   >>$LOGFILE
    fi

    if [ $QUIET -ne 1 ]
    then
      echo "$2"
    fi

    return
}

# =============================================================================
# function:     cleanup
# -----------------------------------------------------------------------------
# parameter:    ./.
# purpose:      Cleans up files that may have been created until receiving
#               a signal or up until an error occured.
# =============================================================================

cleanup()
{
    logit INFO "cleaning up..."

    rm -f $LOCKFILE >/dev/null 2>&1

    return
}

# =============================================================================
# function:     existsproc
# -----------------------------------------------------------------------------
# parameter:    $1      command string of process
# purpose:      checks process list for existence of specified process
# =============================================================================

existsproc()
{
    if [ "$1" = "" ]
    then
	return 1
    fi

    pid=`ps -ef | grep "$1" | grep -v "grep" | awk '{ print $2 }  `

    if [ "$pid" != "" -a "$pid" != "$$" ]
    then
	return 0
    else
	return 1
    fi
}

# =============================================================================
# function:     killproc
# -----------------------------------------------------------------------------
# parameter:    $1      command string of process
# purpose:      kills specified process in graceful way:
#               first send SIGTERM, wait 2 seconds, send SIGKILL
# =============================================================================

killproc()
{
    pid=`ps -ef | grep "$1" | grep -v "grep" | awk '{ print $2 }  `

    if [ "$pid" != "" ]
    then

	kill $pid >/dev/null 2>&1
	sleep 2
	if ps -p $pid >/dev/null 2>&1
	then
	    kill -9 $pid >/dev/null 2>&1
	fi
	echo "$MSG_PREFIX \"$1\" stopped"

    else

	echo "$MSG_PREFIX \"$1\" already stopped"

    fi
}

# =============================================================================
# function:     terminate
# -----------------------------------------------------------------------------
# parameter:    ./.
# purpose:      Prints INFO message and terminates TBGW
# =============================================================================

terminate()
{
    logit INFO "Shutting down TBGW..."
    killproc tbgw
    exit 0
}

# =============================================================================
# function:     kill_multiple_proc
# -----------------------------------------------------------------------------
# parameter:    $1      command string/regex of process(es)
# purpose:      kills specified process(es) in graceful way:
#               first send SIGTERM, wait 2 seconds, send SIGKILL
# =============================================================================

kill_multiple_proc()
{
  integer COUNT=0

  ps -ef | grep "$1" | grep -v "grep" | awk '{ print $2 }' | \
  while read pid
  do

      if [ "$pid" != "" ]
      then
	  kill $pid >/dev/null 2>&1
	  sleep 2
	  if ps -p $pid >/dev/null 2>&1
	  then
              kill -9 $pid >/dev/null 2>&1
	  fi
	  let COUNT+=1
      fi

  done

  echo "$COUNT process(es) matching \"$1\" killed"
}

# =============================================================================
# function:     kill_user
# -----------------------------------------------------------------------------
# parameter:    $1      user whose processes will be killed
# purpose:      kills processes in graceful way:
#               first send SIGTERM, wait 2 seconds, send SIGKILL
# =============================================================================

kill_user()
{
  integer RC=0

  ps -fu "$1" 2>/dev/null | awk '{ print $2 } ' |      \
    while read pid
  do
    if [ "$pid" != "" ]
    then
      kill $pid >/dev/null 2>&1
    fi
  done

  sleep 2

  ps -fu "$1" 2>/dev/null | awk '{ print $2 } ' |      \
    while read pid
  do
    if [ "$pid" != "" ]
    then
      kill -9 $pid >/dev/null 2>&1
      if ps -p $pid >/dev/null 2>&1
      then
        RC=RC+1
      fi
    fi
  done

  return $RC
}

# =============================================================================
# function:     trap_handler
# -----------------------------------------------------------------------------
# parameter:    $1      signal number
# purpose:      Called when a signal is received (e.g. Ctrl-C). Cleans up
#               files that may have been created until receiving the signal.
# =============================================================================

trap_handler()
{
  title $QUIET
  logit INFO "Signal $1 caught..."

  case "$1" in

      15) logit INFO "exiting..."

          ;;

      *)  logit INFO "aborting..."
          ;;

  esac

  terminate
  cleanup

  exit $1
}

# --- SETUP SIGNAL HANDLING ---------------------------------------------------

trap "trap_handler 2"   2
trap "trap_handler 3"   3
trap "trap_handler 4"   4
trap "trap_handler 5"   5
trap "trap_handler 6"   6
trap "trap_handler 7"   7
trap "trap_handler 8"   8
trap "trap_handler 9"   9
trap "trap_handler 10" 10
trap "trap_handler 11" 11
trap "trap_handler 12" 12
trap "trap_handler 13" 13
trap "trap_handler 14" 14
trap "trap_handler 15" 15

# --- SANITY CHECKS ---

# - Make sure shared memory requirements are met.
SHM_SIZE=$(df -P /dev/shm | grep -v Filesystem | awk '{print $2}')

if [ "$SHM_SIZE" -lt 1048576 ]
then
    logit ERROR "The container for TBGW must be started with a shared memory size larger than 1 GB (e.g. by using option --shm-size 1g)!"
    cleanup
    exit 1
fi

# - Check if exe is actually executable

if [ ! -x $THINGBONE_GATEWAY_BINDIR/$THINGBONE_GATEWAY_EXE ]
then
    logit ERROR "Thingbone Gateway exe file $THINGBONE_GATEWAY_BINDIR/$THINGBONE_GATEWAY_EXE not found or not executable!"
    cleanup
    exit 1
fi

# --- START ALLEGROCL ---------------------------------------------------------

echo "========================================================================"
echo
echo "*** THINGBONE GATEWAY ***"
echo

export LD_LIBRARY_PATH=$THINGBONE_GATEWAY_BINDIR:/usr/lib/x86_64-linux-gnu:$LD_LIBRARY_PATH
logit INFO "ENV LD_LIBRARY_PATH                   = $LD_LIBRARY_PATH"

# LIBCRYPTO="`ldconfig -v -p | grep "libcrypto.so.1.0.2" | awk '{ print $1 }' -`"
# LIBSSL="`ldconfig -v -p | grep "libssl.so.1.0.2" | awk '{ print $1 }' -`"

# if [ "x$LIBCRYPTO" != "x" -a "x$LIBSSL" != "x" ]
# then
#   THINGBONE_SSL_LIBRARY_NAMES="$LIBCRYPTO $LIBSSL"
#   export THINGBONE_SSL_LIBRARY_NAMES
#   export ACL_SSL_LIBRARY_NAMES="$THINGBONE_SSL_LIBRARY_NAMES"
# fi

export OPENSSL_VERSION=`openssl version`
logit INFO "ENV THINGBONE_SSL_LIBRARY_NAMES       = \"$THINGBONE_SSL_LIBRARY_NAMES\""
logit INFO "ENV OPENSSL_VERSION                   = $OPENSSL_VERSION"

export RFC_TRACE_DIR

logit INFO "ENV THINGBONE_GATEWAY_INSTALLDIR      = $THINGBONE_GATEWAY_INSTALLDIR"
logit INFO "ENV THINGBONE_GATEWAY_BINDIR          = $THINGBONE_GATEWAY_BINDIR"
logit INFO "ENV THINGBONE_GATEWAY_LIBDIR          = $THINGBONE_GATEWAY_LIBDIR"
logit INFO "ENV THINGBONE_GATEWAY_CONFIGFILENAME  = $THINGBONE_GATEWAY_CONFIGFILENAME"
logit INFO "ENV THINGBONE_GATEWAY_CONFIGDIR       = $THINGBONE_GATEWAY_CONFIGDIR"
logit INFO "ENV THINGBONE_GATEWAY_DATADIR         = $THINGBONE_GATEWAY_DATADIR"
logit INFO "ENV THINGBONE_GATEWAY_LOGDIR          = $THINGBONE_GATEWAY_LOGDIR"
logit INFO "ENV THINGBONE_GATEWAY_LOGFILENAME     = $THINGBONE_GATEWAY_LOGFILENAME"
logit INFO "ENV THINGBONE_GATEWAY_APPUSER         = $THINGBONE_GATEWAY_APPUSER"
logit INFO "ENV THINGBONE_GATEWAY_APPUID          = $THINGBONE_GATEWAY_APPUID"
logit INFO "ENV THINGBONE_GATEWAY_APPGROUP        = $THINGBONE_GATEWAY_APPGROUP"
logit INFO "ENV THINGBONE_GATEWAY_APPGID          = $THINGBONE_GATEWAY_APPGID"
logit INFO "ENV THINGBONE_GATEWAY_SWANK_PORT      = $THINGBONE_GATEWAY_SWANK_PORT"
logit INFO "ENV THINGBONE_GATEWAY_API_PORT        = $THINGBONE_GATEWAY_API_PORT"
logit INFO "ENV THINGBONE_GATEWAY_CONSOLE_PORT    = $THINGBONE_GATEWAY_CONSOLE_PORT"
logit INFO "ENV THINGBONE_GATEWAY_EXE             = $THINGBONE_GATEWAY_EXE"
logit INFO "ENV THINGBONE_SITE                    = $THINGBONE_SITE"
logit INFO "ENV RFC_TRACE_DIR                     = $RFC_TRACE_DIR"

echo

CMD="$THINGBONE_GATEWAY_BINDIR/$THINGBONE_GATEWAY_EXE"
logit INFO "About to execute command \"$CMD\" ..."
$CMD
