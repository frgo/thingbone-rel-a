# -*- Mode:Makefile; Coding:utf-8 -*-
# =====================================================================
#
#  T H I N G B O N E  -   The BackBONE for all THINGs
#
#  A data-centric messaging middleware for real-time application
#  integration and IoT integration based on OMG DDS and Common Lisp.
#
#  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
#  For licensing information see file license.md.
#
#  All Rights Reserved. German law applies exclusively in all cases.
#
#  Author: Frank Gönninger <frgo@me.com>
#  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
#              <support@goenninger.net>
#
# =====================================================================
#
#  PROVISIONING MAKEFILE
#
# =====================================================================


# --- CUSTOMIZING SECTION ---

THINGBONE_GATEWAY_VERSION = a0105
PLATFORM = linux-x64
ACCOUNT = bwpger

# --- CMD SETUP ---

PS = ps
CD = cd
PWD = pwd
TAR = tar
RM = rm -f
DOCKER = docker

# --- IMPLEMENTATION ---

THINGBONE_SWDEVDIR ?= /var/data/swdev/thingbone
THINGBONE_DELIVERYDIR = $(THINGBONE_SWDEVDIR)/delivery
THINGBONE_DELIVERYBINDIR ?= $(THINGBONE_DELIVERYDIR)/bin

# Strip any '.rcN' or '.tN' from VERSION.
THINGBONE_GATEWAY_FINAL_VERSION=$(shell echo $(THINGBONE_GATEWAY_VERSION) | sed -e 's/\.rc.*$$//' -e 's/\.t[0-9]$$//')

SERVICE = thingbone-gateway
TAG = $(ACCOUNT)/$(SERVICE):v$(THINGBONE_GATEWAY_VERSION)
LATEST_TAG = $(ACCOUNT)/$(SERVICE):latest

THINGBONE_GATEWAY_PKGNAME = tbgw
THINGBONE_GATEWAY_TAR = $(THINGBONE_GATEWAY_PKGNAME)-$(THINGBONE_GATEWAY_FINAL_VERSION)-$(PLATFORM).tar
THINGBONE_GATEWAY_EXE = tbgw
THINGBONE_GATEWAY_CONFIGFILE = tbgw.config
THINGBONE_GATEWAY_CONFIGFILE_TEMPLATE = tbgw.config.template
THINGBONE_GATEWAY_CONFIGFILE_SOURCEDIR = $(THINGBONE_DELIVERYDIR)/config

THINGBONE_OUTPUTDIR = /var/data/swdev/thingbone/delivery/output

THINGBONE_GATEWAY_INSTALLDIR = \/opt\/thingbone\/gateway\/$(THINGBONE_GATEWAY_VERSION)
THINGBONE_GATEWAY_BINDIR = \/opt\/thingbone\/gateway\/$(THINGBONE_GATEWAY_VERSION)\/thingbone-gateway
THINGBONE_GATEWAY_LIBDIR = \/opt\/thingbone\/gateway\/$(THINGBONE_GATEWAY_VERSION)\/thingbone-gateway
THINGBONE_GATEWAY_CONFIGDIR = \/etc\/opt\/thingbone\/gateway
THINGBONE_GATEWAY_CONFIGFILENAME = $(THINGBONE_GATEWAY_CONFIGDIR)\/tbgw.config
THINGBONE_GATEWAY_DATADIR = \/var\/data\/thingbone\/gateway
THINGBONE_GATEWAY_LOGDIR = $(THINGBONE_GATEWAY_DATADIR)\/logs
THINGBONE_GATEWAY_LOGFILENAME = $(THINGBONE_GATEWAY_DATADIR)\/logs\/tbgw.log
THINGBONE_GATEWAY_APPUSER = thingbone
THINGBONE_GATEWAY_APPUID = 42006
THINGBONE_GATEWAY_APPGROUP = app
THINGBONE_GATEWAY_APPGID = 20006

THINGBONE_GATEWAY_SWANK_PORT ?= 4010
THINGBONE_GATEWAY_API_PORT ?= 7169
THINGBONE_GATEWAY_CONSOLE_PORT ?= 3000

RUNNING_DOCKER_CONTAINER = $(shell $(DOCKER) container list | grep $(SERVICE) | awk '{print $$1}')
EXISTING_DOCKER_CONTAINER = $(shell $(DOCKER) container list -a | grep $(SERVICE) | awk '{print $$1}')

.PHONY:	exe clean run

exe:
	$(THINGBONE_DELIVERYBINDIR)/make-tbgw-exe.sh

pkg: Dockerfile exe
	$(TAR) -cf $(THINGBONE_GATEWAY_TAR) -C $(THINGBONE_OUTPUTDIR) $(SERVICE) -C $(THINGBONE_GATEWAY_CONFIGFILE_SOURCEDIR) $(THINGBONE_GATEWAY_CONFIGFILE_TEMPLATE)

Dockerfile: dockerfile.in
	sed -e 's/__THINGBONE_GATEWAY_TBZ2__/$(THINGBONE_GATEWAY_TBZ2)/g' \
            -e 's/__THINGBONE_GATEWAY_TAR__/$(THINGBONE_GATEWAY_TAR)/g' \
	    -e 's/__THINGBONE_GATEWAY_VERSION__/$(TINGBONE_GATEWAY_VERSION)/g' \
	    -e 's/__THINGBONE_GATEWAY_FINAL_VERSION__/$(THINGBONE_GATEWAY_FINAL_VERSION)/g' \
	    -e 's/__THINGBONE_GATEWAY_INSTALLDIR__/$(THINGBONE_GATEWAY_INSTALLDIR)/g' \
	    -e 's/__THINGBONE_GATEWAY_BINDIR__/$(THINGBONE_GATEWAY_BINDIR)/g' \
	    -e 's/__THINGBONE_GATEWAY_LIBDIR__/$(THINGBONE_GATEWAY_LIBDIR)/g' \
	    -e 's/__THINGBONE_GATEWAY_CONFIGFILE__/$(THINGBONE_GATEWAY_CONFIGFILE)/g' \
	    -e 's/__THINGBONE_GATEWAY_CONFIGDIR__/$(THINGBONE_GATEWAY_CONFIGDIR)/g' \
	    -e 's/__THINGBONE_GATEWAY_CONFIGFILE_TEMPLATE__/$(THINGBONE_GATEWAY_CONFIGFILE_TEMPLATE)/g' \
	    -e 's/__THINGBONE_GATEWAY_CONFIGFILENAME__/$(THINGBONE_GATEWAY_CONFIGFILENAME)/g' \
	    -e 's/__THINGBONE_GATEWAY_DATADIR__/$(THINGBONE_GATEWAY_DATADIR)/g' \
	    -e 's/__THINGBONE_GATEWAY_LOGDIR__/$(THINGBONE_GATEWAY_LOGDIR)/g' \
	    -e 's/__THINGBONE_GATEWAY_LOGFILENAME__/$(THINGBONE_GATEWAY_LOGFILENAME)/g' \
	    -e 's/__THINGBONE_GATEWAY_APPUSER__/$(THINGBONE_GATEWAY_APPUSER)/g' \
	    -e 's/__THINGBONE_GATEWAY_APPUID__/$(THINGBONE_GATEWAY_APPUID)/g' \
	    -e 's/__THINGBONE_GATEWAY_APPGROUP__/$(THINGBONE_GATEWAY_APPGROUP)/g' \
	    -e 's/__THINGBONE_GATEWAY_APPGID__/$(THINGBONE_GATEWAY_APPGID)/g' \
	    -e 's/__THINGBONE_GATEWAY_SWANK_PORT__/$(THINGBONE_GATEWAY_SWANK_PORT)/g' \
	    -e 's/__THINGBONE_GATEWAY_API_PORT__/$(THINGBONE_GATEWAY_API_PORT)/g' \
	    -e 's/__THINGBONE_GATEWAY_CONSOLE_PORT__/$(THINGBONE_GATEWAY_CONSOLE_PORT)/g' \
	    -e 's/__THINGBONE_GATEWAY_EXE__/$(THINGBONE_GATEWAY_EXE)/g' \
	    -e 's/__SERVICE__/$(SERVICE)/g' \
	    Dockerfile.in > Dockerfile

docker-image: Dockerfile pkg
	docker build -t $(TAG) .
	docker tag $(TAG) $(LATEST_TAG)

ifeq "$(strip $(RUNNING_DOCKER_CONTAINER))" ""
clean-kill-container:
	@echo "No container to be killed."
else
clean-kill-container:
	$(DOCKER) container kill $(RUNNING_DOCKER_CONTAINER)
endif

ifeq "$(strip $(EXISTING_DOCKER_CONTAINER))" ""
clean-rm-container:
	@echo "No container to be removed."
else
clean-rm-container:
	$(DOCKER) container rm $(EXISTING_DOCKER_CONTAINER)
endif

clean-rm:
	$(RM) Dockerfile
	$(RM) $(THINGBONE_GATEWAY_TAR)

clean:	clean-kill-container clean-rm-container clean-rm


release: docker-image
	docker push $(TAG)
	docker push $(LATEST_TAG)

ifndef THINGBONE_SITE
run:
	@echo "ERROR: THINGBONE_SITE not set! This is required for $(SERVICE) running in Docker container!"
else
run:	Dockerfile
	docker run --name=$(SERVICE) --shm-size=1g -p $(THINGBONE_GATEWAY_SWANK_PORT):$(THINGBONE_GATEWAY_SWANK_PORT) -p $(THINGBONE_GATEWAY_API_PORT):$(THINGBONE_GATEWAY_API_PORT) -p $(THINGBONE_GATEWAY_CONSOLE_PORT):$(THINGBONE_GATEWAY_CONSOLE_PORT) -e THINGBONE_GATEWAY_API_PORT=$(THINGBONE_GATEWAY_API_PORT)  -e THINGBONE_GATEWAY_SWANK_PORT=$(THINGBONE_GATEWAY_SWANK_PORT) -e THINGBONE_GATEWAY_CONSOLE_PORT=$(THINGBONE_GATEWAY_CONSOLE_PORT) -e THINGBONE_SITE=$(THINGBONE_SITE) $(LATEST_TAG)
endif
