# BWP STU THINGBONE SERVICES #

## Service Domains

ThingBone's[^1] services architecture follows a domain-driven design. Domains form the boundary of services. Each service in itself is divided into a set of basic functionality that is availbale to consumers of a service.

The domains are modeled as use case domains:

	* Gateway: Provides access from outside the defined security perimeter
	* Management: ThingBone administration, logging and monitoring
	* Persistence: Storing and retrieving messages

## API Domains and their URLs

The URI string of a service is formed as a combination of base URL plus service domain and version of the API of that domain:

	1.	/thingbone/api/gateway/v3
	2.  /thingbone/api/mgmt/v1
	3.  /thingbone/api/persistence/v1

### Object Management Domain

This domain defines services that allow creation,updating, deletion, quering and searching of objects of a specified class.

The following service methods are provided by a conforming service:

	* CREATE
	* QUERY/READ
	* UPDATE
	* DELETE
	* SEARCH

### CREATE Service Method
TBC.


## Hosts and their Connection Info ##

	BWPSTUVM001 VMWARE Linux Host / Oracle DB (hda 100GB)                 10.79.10.76   255.255.240.0   10.79.0.1   10.79.15.255   20   Static
	BWPSTUVM002 VMWARE Linux Host / Alegrograph (hda 80GB)                10.79.10.77   255.255.240.0   10.79.0.1   10.79.15.255   20   Static
	BWPSTUVM003 VMWARE Linux Host / Admin-Utils (Ansible…) (hda 80 GB)    10.79.10.78   255.255.240.0   10.79.0.1   10.79.15.255   20   Static

  lvol0 16GB /
  lvol1 32GB /var
  lvol2 8 GB /tmp
  lvol3 16 GB /opt
 lvol4  13GB swap

rootpwd: Dt619pa7!$$$
goenningerf: Dt619pa7ie4viydt!



[^1]: THINGBONE: BWP STU's Information Backbone
