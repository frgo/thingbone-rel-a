# BWP STU THINGBONE SERVICES #

## Service Domains

ThingBone's[^1] Microservices Architecture follows a Domain-driven Design. Domains form the boundary of Services. Each Service in itself is divided into a set of basic functionality that is availbale to Consumers of a service.

The Domains are modeled as business domains. Examples for such business domains are:

	* Business Functions: Sales, Inside Sales, R&D, Purchasing, Order Fulfillment, Logistics, Commissioning, Field Service
	* Business Processes: Quoting, Selling, Purchasing, Invoicing, Dispatching of Resouces, ...
	* Business Support Functions: Reporting, Data Cleasning, ...

In addition, there are pure DevOps service that concern status of the software landscape, error querying and error recovery etc.

The URI string of a real service is then formed as a combination of business object + domain:

Spare Part + Quoting = "/thingbone/api/sparepart-quote/v1"
New Machine + Sales (booking a customer order) = "/thingbone/api/newequipment-customerorder/v2"
Field Service + Dispatch a Technician = "/thingbone/api/fieldservice-dispatch/v1"

The last piece of the



### Object Management Domain

This domain defines services that allow creatio,,updating, deletion, quering and searching of objects of a specified class.

The following service methods are provided by a conforming service:

	* CREATE
	* QUERY/READ
	* UPDATE
	* DELETE
	* SEARCH

### CREATE Service Method




## Service Classes ##

Within ThingBone, there is a differentation between several classes of services. A specifc service may be an instance of one or more classes at the same time, prviding connectivity and/or functionality of the respective class.

The following classes and sub-classes of services are known to ThingBone:

	1. Connectivity/ Protocol
		1.1 Asynchronous HTTPS
	2. Functionality



## Services and their Connection Info ##

[^1]: THINGBONE: BWP STU's Information Backbone
