;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  ASDF REGISTERING
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(in-package :cl-user)

#-quicklisp
(error "Quicklisp required ...")

(eval-when (:load-toplevel :execute)
  (progn
    (when (ql:quickload :log4cl)
      (pushnew :log4cl cl:*features*))
    (ql:quickload :cl-fad)))

(eval-when (:load-toplevel :compile-toplevel :execute)
  (let* ((truename *load-pathname*)
	 (path (if truename
		   (cl-fad:canonical-pathname (cl-fad:merge-pathnames-as-directory truename))
		   nil)))
    (when path
      (pushnew path asdf:*central-registry* :test #'eql)
      #+log4cl
      (log:info "Path ~a registered in ASDF central registry." path))))
