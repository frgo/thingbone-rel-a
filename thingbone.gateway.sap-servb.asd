;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  ASDF REGISTERING
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "CL-USER")

(asdf:defsystem #:thingbone.gateway.sap-servb
  :description "The ThingBone <-> SAP Service Bus via RFC Gateway."
  :author "Frank Goenninger <frank.goenninger@bwpapersystems.com>"
  :maintainer "Frank Goenninger <frank.goenninger@bwpapersystems.com>"
  :license  "Proprietary. All Rights reserved."
  :version "0.0.1"
  :depends-on (:cl-strings
	       :thingbone.core
	       :thingbone.libsapnwrfc
	       :thingbone.messages)
  :serial t
  :components
  ((:module thingbone.gateway.sap-servb
	    :pathname "gateways/sap-servb/src/"
	    :components
	    ((:file "package")
	     (:file "channels")
	     (:file "init")
	     (:file "bapiret2")
	     (:file "conditions")
	     (:file "servb-return-info")
	     (:file "servb-rfc-connection")
	     (:file "servb-data")
	     (:file "servb-qos")
	     (:file "servb-is-header")
	     (:file "servb-iv-data")
	     (:file "servb-iv-event")
	     (:file "servb-iv-object-key")
	     (:file "servb-iv-object")
	     (:file "servb-outbound-msg")
	     (:file "servb-appl-log-msg")
	     ;; TASK HANDLING
	     (:file "servb-control-task")
	     ;; (:file "servb-inbound-task")
	     (:file "servb-outbound-task")
	     ;; SERVB SERVER + RFC SERVER
	     (:file "servb-server-function")
	     (:file "servb-server")
	     (:file "servb-gateway")
	     ;; CLIENT FUNCTION MODULES
	     (:file "servb-mat-common")
	     (:file "servb-version")
	     (:file "servb-ping")
	     (:file "servb-set-outb-req-state")
	     (:file "servb-mat-get-price")
	     (:file "servb-mat-sales-order-create")
	     (:file "servb-mat-sales-order-update")
	     (:file "servb-mat-sales-order-status-update")
	     ;; MAPPINGS
	     (:file "servb-mapping")
	     ;; SERVER FUNCTION MODULES
	     (:file "z-servb-outbound-server-fm")
	     (:file "z-servb-appl-log-msg-fm")
	     (:file "z-mat-s-o-s-upd-req")
	     ;; TESTING AND DEBUGGING AIDS
	     (:file "servb-connection-params")
	     ))))
