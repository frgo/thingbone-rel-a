;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  ASDF REGISTERING
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "CL-USER")

(asdf:defsystem #:thingbone.main
  :description ""
  :author "Frank Goenninger <frank.goenninger@goenninger.net>"
  :maintainer "Frank Goenninger <frank.goenninger@goenninger.net>"
  :license  "Proprietary. All Rights reserved."
  :version "0.0.1"
  :depends-on (:thingbone.core
	       :thingbone.messages
	       :thingbone.libsapnwrfc
	       :thingbone.gateway.sap-servb
	       :thingbone.aserve-restful)
  :serial t
  :components
  ((:module thingbone.main
	    :pathname "src/main/"
	    :components
	    ((:file "package")
	     (:file "log-source-info")
	     (:file "main")
	     ))))
