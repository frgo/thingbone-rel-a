# <img src="https://github.com/GOENNINGERBT/thingbone/raw/master/images/thingbone-1.gif" width="200" height="107"/>  THINGBONE

<p><h2><b>THINGBONE</b></h2>

## What Is This?
ThingBone is a data-centric middleware solution for application integration and IoT solutions based on OMG DDS, HTTP RESTful APIs and other protocols and technologies. It is based on RTI Connext DDS as the DDS implementation and makes extensive use of Common Lisp using Franz AllegroCL.<br><br>

## Licensing

### License and Rights To Use
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

### Commercial Use

For commercial licensing options, please contact Gönninger B&T's sales at [sales@goenninger.net](mailto:sales@goenninger.net) - Thank you.

## Reporting Problems
Generally you can report problems by [opening an issue](https://github.com/GOENNINGERBT/thingbone/issues). You should have the following pieces handy in order for us to be able to help you out as quickly and painlessly as possible:

* The application or module that is affected.
* The branch of ThingBone you are using.
* A paste of the build log or failure message/ backtrace of the execution point you reached.
* Your operating system name and version.
* A short description of the actions that produced the issue.
* Patience.

## Contact
Currently, this repository is managed by Frank Goenninger (a.k.a. as "frgo"). He may be reached via email at [frank.goenninger@goenninger.net](mailto:frank.goenninger@goenninger.net) or via mobile phone on +49 175 4321058.
