;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(defpackage #:thingbone.gateway.sap-servb
  (:use #:cl
	#:cl-strings
	#:thingbone.libsapnwrfc)
  (:nicknames "TB.GW.SAP-SERVB")
  (:export
   #:bapiret2
   #:set-sap-bapiret2-from-servb-bapiret2
   #:set-servb-bapiret2-from-sap-bapiret2
   #:bapiret2-error-p

   #:sap-servb-inbound-channel
   #:sap-servb-outbound-channel
   #:sap-servb-appl-log-channel
   #:init-sap-servb-inbound-channel
   #:init-sap-servb-outbound-channel
   #:init-sap-servb-appl-log-channel
   #:ensure-sap-servb-inbound-channel-initialized
   #:ensure-sap-servb-outbound-channel-initialized
   #:ensure-sap-servb-appl-log-channel-initialized
   #:start-sap-servb-inbound-channel-dispatcher
   #:start-sap-servb-outbound-channel-dispatcher
   #:start-sap-servb-appl-log-channel-dispatcher
   #:stop-sap-servb-inbound-channel-dispatcher
   #:stop-sap-servb-outbound-channel-disptacher
   #:stop-sap-servb-appl-log-channel-dispatcher

   #:sap-servb-init
   #:sap-servb-reset
   #:sap-servb-initialized-p
   #:ensure-sap-servb-initialized
   #:start-sap-servb
   #:stop-sap-servb

   #:servb-data

   #:servb-qos
   #:tb-extended-msg-to-servb-qos
   #:get-qos-structure-handle-from-container-handle
   #:set-sap-qos-from-servb-qos
   #:set-servb-qos-from-sap-qos
   #:make-servb-qos-from-sap-container-handle

   #:servb-is-header
   #:tb-extended-msg-to-servb-is-header
   #:set-sap-is-header-from-servb-is-header
   #:set-servb-is-header-from-sap-is-header
   #:make-servb-is-header-from-sap-container-handle

   #:servb-iv-data
   #:set-sap-iv-data-from-servb-iv-data
   #:set-servb-iv-data-from-sap-iv-data
   #:make-servb-iv-data-from-sap-container-handle

   #:servb-iv-event
   #:set-sap-iv-event-from-servb-iv-event
   #:set-servb-iv-event-from-sap-iv-event
   #:make-servb-iv-event-from-sap-container-handle

   #:servb-iv-object-key
   #:set-sap-iv-object-key-from-servb-iv-object-key
   #:set-servb-iv-object-key-from-sap-iv-object-key
   #:make-servb-iv-object-key-from-sap-container-handle

   #:servb-iv-object
   #:set-sap-iv-object-from-servb-iv-object
   #:set-servb-iv-object-from-sap-iv-object
   #:make-servb-iv-object-from-sap-container-handle

   #:servb-outbound-msg
   #:make-requestor-context-from-servb-outbound-msg
   #:make-sender-from-servb-outbound-msg
   #:make-msg-data-from-servb-outbound-msg
   #:make-msg-content-from-servb-outbound-msg
   #:to-tb-extended-msg

   #:sap-servb-rfc-ping

   #:servb-sap-rfc-connection
   #:close-connection
   #:open-connection
   #:connection-ok-p
   #:connection-closed-p
   #:ensure-connection
   #:ensure-servb-sap-rfc-client-connection-in-global-context
   #:sap-servb-client-rfc-connection-params
   #:sap-servb-server-rfc-connection-params

   #:servb-return-info
   #:rfc-return-code
   #:return-type
   #:return-id
   #:return-number
   #:return-message
   #:make-servb-return-info
   #:sap-servb-es-return-to-servb-return-info
   #:errorp
   #:servb-return-info-to-tb-msg-error-info

   #:servb-server-function
   #:install
   #:print-servb-server-function-registry
   #:reset-servb-server-function-registry
   #:register-servb-server-function-in-registry
   #:count-servb-server-functions-by-domain
   #:get-servb-server-function-by-remote-fm-name
   #:get-servb-server-functions-by-domain
   #:define-servb-server-function
   #:install-servb-server-functions-for-domain
   #:reset-servb-server-functions-for-domain

   #:order-type
   #:sales-org
   #:distribution-channel
   #:division
   #:partner-role
   #:business-partner-nr
   #:price-date
   #:mat-pricing-items
   #:data-context

   #:servb-server
   #:stop-listening
   #:reset
   #:register
   #:intialize
   #:listen-and-dispatch
   #:run-in-thread
   #:start
   #:stop

   #:servb-version
   #:as-string

   #:reset-servb-gateway-registry
   #:servb-gateway
   #:register-servb-gateway
   #:deregister-servb-gateway
   #:make-servb-gateway
   #:make-and-register-servb-gateway
   #:make-sapr3-gateway
   #:make-and-register-sapr3-gateway
   #:find-servb-gateway-for-site
   #:ensure-client-connection
   #:ensure-server-connection

   #:servb-data-element

   #:servb-function-module
   #:make-function-module
   #:add-function-module
   #:init-function-module
   #:add-and-init-function-module
   #:function-handle
   #:function-module
   #:define-function-module
   #:container

   #:servb-container-slot

   #:servb-container
   #:init-container
   #:container-handle
   #:make-servb-container
   #:add-container
   #:define-servb-data-container-for-function-module

   #:servb-sap-write-value

   #:structure-handle-from-function-module-and-structure
   #:table-handle-from-function-module-and-table
   #:slot-from-function-module-and-container

   #:servb-write-field
   #:servb-write-structure
   #:servb-write-table


   ;; CONDITIONS

   #:sap-rfc-error
   #:servb-parameter-error

   ;; ---

   ;;; SERB SAP RFC SERVER

   #:z-servb-outbound-server-fm
   #:make-tb-sap-servb-set-outb-req-state-msg

   ;; ---

   ;; Z_MAT_GET_PRICING


   #:servb-mat-pricing-item
   #:*servb-mat-pricing-items-in-nr-array-elements*
   #:servb-mat-pricing-items-in
   #:nr-of-mat-pricing-items
   #:add-mat-pricing-item
   #:nth-mat-pricing-item
   #:servb-mat-pricing-item-out
   #:servb-mat-pricing-items-out
   #:nr-of-mat-pricing-items-out
   #:add-mat-pricing-item-out
   #:nth-mat-pricing-item-out
   #:get-it-line-items-table-handle-from-function-handle
   #:get-et-line-items-table-handle-from-function-handle
   #:set-sap-it-line-items-from-servb-mat-pricing-items-in
   #:set-servb-mat-pricing-items-out-from-sap-et-line-items
   #:z-mat-get-price
   #:sap-direct-mat-get-price
   #:set-tb-extended-msg-content-data-from-servb-mat-pricing-items-out

   ;; MAT COMMON

   #:sapr3-get-t-sales-order-return
   #:sapr3-write-single-ship-to-address

   ;; Z_MAT_S_O_CRE_REQ

   #:sap-direct-request-sales-order-create

   ;; Z_MAT_S_O_UPD_REQ

   #:sap-direct-request-sales-order-update

   ;; Z_MAT_S_O_S_UPD_REQ

   #:sap-direct-inform-sales-order-status-update

   ))

(provide :thingbone.gateway.sap-servb)
