;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

(defun z-servb-set-outb-req-state (connection-handle conversation-id-string state-string &key (sap-uc-encoding (rfc-sap-uc-encoding)))

  ;; FUNCTION Z_SERVB_SET_OUTB_REQ_STATE.
  ;; *“----------------------------------------------------------------------
  ;; *“*”Lokale Schnittstelle:
  ;; *”  IMPORTING
  ;; *”     VALUE(IV_CONVERSATION_ID) TYPE  ZSERVB_DE_CONVERSATION_ID
  ;; *”     VALUE(IV_STATE) TYPE  ZSERVB_DE_STATE
  ;; *”  EXPORTING
  ;; *”     VALUE(ES_RETURN) TYPE  BAPIRET2
  ;; *“----------------------------------------------------------------------
  ;; IV_CONVERSATION_ID (CAHR,36)
  ;; IV_STATE (CHAR,32)

  (log:trace "Z-SERVB-SET-OUTB-REQ-STATE: connection-handle = ~S, conversation-id-string = ~S, state-string = ~S "
	     connection-handle conversation-id-string state-string)

  (let* ((func-handle (rfc-function connection-handle "Z_SERVB_SET_OUTB_REQ_STATE"))
	 (rc (rfc-set-chars func-handle "IV_CONVERSATION_ID" conversation-id-string (length conversation-id-string) sap-uc-encoding))
	 (rc (rfc-set-chars func-handle "IV_STATE" state-string (length state-string) sap-uc-encoding))
	 (rc (rfc-invoke connection-handle func-handle))
	 (servb-return-info (get-servb-return-info-from-function-handle func-handle)))
    (setf (rfc-return-code servb-return-info) rc)
    (setf (fm-name servb-return-info) "Z-SERVB-SET-OUTB-REQ-STATE")

    (log:info "Z-SERVB-OUTB-REQ-STATE: Conversation ID: ~S, State: ~S -> Result: ~S."
	      conversation-id-string state-string servb-return-info)

    (tb.core:send (sap-servb-return-info-log-channel) servb-return-info)

    ;; Second value always servb-return-info
    (values nil servb-return-info)))

(defun sap-servb-set-outb-req-state (servb-sap-rfc-connection conversation-id state)
  (check-type servb-sap-rfc-connection servb-sap-rfc-connection)
  (check-type conversation-id string)
  (ensure-connection servb-sap-rfc-connection)
  (z-servb-set-outb-req-state (connection-handle servb-sap-rfc-connection)
			      conversation-id
			      (princ-to-string state)))

(defclass tb-sap-servb-set-outb-req-state-msg (tb.msgs:msg)
  ((conversation-id :accessor conversation-id :initarg :conversation-id :initform nil)
   (state           :accessor state           :initarg :state           :initform nil)
   ))

(defmethod print-object ((tb-sap-servb-set-outb-req-state-msg tb-sap-servb-set-outb-req-state-msg) stream)
  (print-unreadable-object (tb-sap-servb-set-outb-req-state-msg stream :identity t :type t)
    (format stream ":CONVERSATION-ID ~S :STATE ~S"
	    (conversation-id tb-sap-servb-set-outb-req-state-msg)
	    (state           tb-sap-servb-set-outb-req-state-msg))))

(defun make-tb-sap-servb-set-outb-req-state-msg (conversation-id state)
  (make-instance 'tb-sap-servb-set-outb-req-state-msg
		 :conversation-id conversation-id
		 :state state))

(defmethod tb.msgs:dispatch ((tb-sap-servb-set-outb-req-state-msg tb-sap-servb-set-outb-req-state-msg) &key &allow-other-keys)
  (ensure-servb-sap-rfc-client-connection-in-global-context)
  (let ((sap-rfc-connection (tb.core:global-context-value :sap-servb-client-rfc-connection)))
    (sap-servb-set-outb-req-state sap-rfc-connection
				  (conversation-id tb-sap-servb-set-outb-req-state-msg)
				  (state tb-sap-servb-set-outb-req-state-msg))))
