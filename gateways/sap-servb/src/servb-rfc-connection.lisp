;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

(defclass servb-sap-rfc-connection ()
  ((connection-handle :accessor connection-handle :initarg :connection-handle :initform (cffi:null-pointer))
   (name              :accessor name              :initarg :name              :initform "UNNAMED")
   (status            :accessor status            :initarg :status            :initform :CLOSED)
   (connection-params :accessor connection-params :initarg :connection-params :initform '())
   ))

(defmethod print-object ((servb-sap-rfc-connection servb-sap-rfc-connection) stream)
  (print-unreadable-object (servb-sap-rfc-connection stream :identity t :type t)
    (with-slots (connection-handle name status connection-params) servb-sap-rfc-connection
      (format stream "~A :CONNECTION-HANDLE #x~X :STATUS ~S :CONNECTION-PARAMS ~S"
	      name connection-handle status connection-params))))

(defun make-servb-sap-rfc-connection (name connection-params)
  (make-instance 'servb-sap-rfc-connection :name name :connection-params connection-params))

(defmethod close-connection ((servb-sap-rfc-connection servb-sap-rfc-connection))
  (if (not (cffi:null-pointer-p (connection-handle servb-sap-rfc-connection)))
      (progn
	(unwind-protect
	     (rfc-close-connection (connection-handle servb-sap-rfc-connection))
	  (setf (connection-handle servb-sap-rfc-connection) (cffi:null-pointer)))
	(setf (status servb-sap-rfc-connection) :CLOSED)))
  (status servb-sap-rfc-connection))

(defmethod open-connection ((servb-sap-rfc-connection servb-sap-rfc-connection) &key (if-already-open :do-nothing))
  (cond
    ((eql (status servb-sap-rfc-connection) :CLOSED)
     (progn
       (setf (connection-handle servb-sap-rfc-connection) (rfc-open-connection (connection-params servb-sap-rfc-connection)))
       (if (not (cffi:null-pointer-p (connection-handle servb-sap-rfc-connection)))
	   (setf (status servb-sap-rfc-connection) :OPEN))))
    ((and (eql (status servb-sap-rfc-connection) :OPEN) (eql if-already-open :re-open))
     (if (not (cffi:null-pointer-p (connection-handle servb-sap-rfc-connection)))
	 (progn
	   (close-connection servb-sap-rfc-connection)
	   (let ((connection-handle (rfc-open-connection (connection-params servb-sap-rfc-connection))))
	     (setf (connection-handle servb-sap-rfc-connection) connection-handle)
	     (if (not (cffi:null-pointer-p connection-handle))
		 (setf (status servb-sap-rfc-connection) :OPEN)))))))
  (status servb-sap-rfc-connection))

(defmethod connection-ok-p ((servb-sap-rfc-connection servb-sap-rfc-connection))
  (if (not (rfc-connection-handle-valid-p (connection-handle servb-sap-rfc-connection)))
      (progn
	;; Reset connection-handle and status forcibly
	(setf (connection-handle servb-sap-rfc-connection) (cffi:null-pointer))
	(setf (status servb-sap-rfc-connection) :CLOSED)
	nil)
      t))

(defmethod connection-closed-p ((servb-sap-rfc-connection servb-sap-rfc-connection))
  (not (connection-ok-p servb-sap-rfc-connection)))

(defmethod connection-open-p ((servb-sap-rfc-connection servb-sap-rfc-connection))
  (connection-ok-p servb-sap-rfc-connection))

(defmethod ensure-connection ((servb-sap-rfc-connection servb-sap-rfc-connection))
  (rfc-init)
  (if (not (connection-ok-p servb-sap-rfc-connection))
      (eql (open-connection servb-sap-rfc-connection) :OPEN)
      t))

(defun sap-servb-client-rfc-connection-params ()
  (tb.core:global-context-value :sap-servb-client-rfc-connection-params))

(defun (setf sap-servb-client-rfc-connection-params) (rfc-connection-params)
  (setf (tb.core:global-context-value :sap-servb-client-rfc-connection-params) rfc-connection-params))

(defun sap-servb-server-rfc-connection-params ()
  (tb.core:global-context-value :sap-servb-server-rfc-connection-params))

(defun (setf sap-servb-server-rfc-connection-params) (rfc-connection-params)
  (setf (tb.core:global-context-value :sap-servb-server-rfc-connection-params) rfc-connection-params))

(defun ensure-servb-sap-rfc-client-connection-in-global-context ()
  (let ((sap-rfc-connection (tb.core:global-context-value :sap-servb-client-rfc-connection)))
    (if (not sap-rfc-connection)
	(let ((sap-servb-client-rfc-connection-params (tb.core:global-context-value :sap-servb-client-rfc-connection-params)))
	  (log:debug "Connection params: ~S" sap-servb-client-rfc-connection-params)
	  (if (not sap-servb-client-rfc-connection-params)
	      (error 'servb-missing-sap-rfc-client-connection-parameters)
	      (let ((sap-servb-client-connection (make-instance 'servb-sap-rfc-connection
								:connection-params sap-servb-client-rfc-connection-params)))
		(setf (tb.core:global-context-value :sap-servb-client-rfc-connection) sap-servb-client-connection)))))))

(defmacro with-rfc-connection-ensured ((servb-sap-rfc-connection fm-name) &body body)
  `(if (ensure-connection ,servb-sap-rfc-connection)
       (progn
	 ,@body)
       (values nil (make-servb-return-info ,fm-name
					   :RFC-COMMUNICATION-FAILURE
					   +TB-CONDITION-LEVEL-SAP-SERVB-ERROR-RFC-COMMUNICATION-FAULURE+
					   +TB-CONDITION-MESSAGE-NR-SAP-SERVB-ERROR-RFC-COMMUNICATION-FAILURE+
					   +TB-CONDITION-REASON-CODE-SAP-SERVB-ERROR-RFC-COMMUNICATION-FAILURE+
					   +TB-CONDITION-REASON-TEXT-SAP-SERVB-ERROR-RFC-COMMUNICATION-FAILURE+))))
