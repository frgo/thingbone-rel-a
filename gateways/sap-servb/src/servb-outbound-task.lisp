;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

;;; OUTBOUND TASK RUNNER

(defparameter *lock-sap-servb-outbound-task-runner* (bt:make-recursive-lock))
(defparameter *sap-servb-outbound-task-runner* nil)

(defparameter *lock-sap-servb-outbound-task-runner-thread* (bt:make-recursive-lock))
(defparameter *sap-servb-outbound-task-runner-thread* nil)

(defconstant +sap-servb-outbound-task-runner-name+ "THINGBONE SAP SERVICE BUS OUTBOUND TASK RUNNER")

(defun init-sap-servb-outbound-task-runner ()
  (bt:with-recursive-lock-held (*lock-sap-servb-outbound-task-runner*)
    (setq *sap-servb-outbound-task-runner* (make-tb-task-runner))))

(defun sap-servb-outbound-task-runner ()
  (bt:with-recursive-lock-held (*lock-sap-servb-outbound-task-runner*)
    *sap-servb-outbound-task-runner*))

(defun init-sap-servb-outbound-task-runner-thread (&key (runner (sap-servb-outbound-task-runner)))
  (bt:with-recursive-lock-held (*lock-sap-servb-outbound-task-runner-thread*)
    (setq *sap-servb-outbound-task-runner-thread* (make-tb-task-runner-thread runner))))

(defun sap-servb-outbound-task-runner-thread ()
  (bt:with-recursive-lock-held (*lock-sap-servb-outbound-task-runner-thread*)
    *sap-servb-outbound-task-runner-thread*))

(defun init-sap-servb-outbound-task-runner ()
  (init-sap-servb-outbound-task-runner)
  (init-sap-servb-outbound-task-runner-thread)
  (values (sap-servb-outbound-task-runner) (sap-servb-outbound-task-runner-thread)))
