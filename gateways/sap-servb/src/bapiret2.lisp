;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

;;; STANDARD SAP STRUCTURE: BAPIRET2

;; IMPLEMENTATION NOTE: BAPIRET2 STRUCTURE DEFINITION
;;
;; TYPE	        BAPI_MTYPE	CHAR	1	0	Meldungstyp: S Success, E Error, W Warning, I Info, A Abort
;; ID	        SYMSGID	        CHAR	20	0	Nachrichtenklasse
;; NUMBER	SYMSGNO	        NUMC	3	0	Nachrichtennummer
;; MESSAGE	BAPI_MSG	CHAR	220	0	Meldungstext
;; LOG_NO	BALOGNR	        CHAR	20	0	Anwendungs-Log: Protokollnummer
;; LOG_MSG_NO	BALMNR	        NUMC	6	0	Anwendungs-Log: interne laufende Nummer der Meldung
;; MESSAGE_V1	SYMSGV	        CHAR	50	0	Nachrichtenvariable
;; MESSAGE_V2	SYMSGV	        CHAR	50	0	Nachrichtenvariable
;; MESSAGE_V3	SYMSGV	        CHAR	50	0	Nachrichtenvariable
;; MESSAGE_V4	SYMSGV	        CHAR	50	0	Nachrichtenvariable
;; PARAMETER	BAPI_PARAM	CHAR	32	0	Parametername
;; ROW	        BAPI_LINE	INT4	10	0	Zeile im Parameter
;; FIELD	BAPI_FLD	CHAR	30	0	Feld im Parameter
;; SYSTEM	BAPILOGSYS	CHAR	10	0	System (logisches System) aus dem die Nachricht stammt

(defclass bapiret2 ()
  ((message-type   :accessor message-type   :initarg :message-type   :initform "S")
   (message-id     :accessor message-id     :initarg :message-id     :initform "Z_BWP_STU_SERVBUS")
   (message-number :accessor message-number :initarg :message-number :initform "000")
   (message        :accessor message        :initarg :message        :initform "")
   (log-nr         :accessor log-nr         :initarg :log-nr         :initform "")
   (log-msg-nr     :accessor log-msg-nr     :initarg :log-msg-nr     :initform "000000")
   (message-v1     :accessor message-v1     :initarg :message-v1     :initform "")
   (message-v2     :accessor message-v2     :initarg :message-v2     :initform "")
   (message-v3     :accessor message-v3     :initarg :message-v3     :initform "")
   (message-v4     :accessor message-v4     :initarg :message-v4     :initform "")
   (parameter      :accessor parameter      :initarg :parameter      :initform "")
   (row            :accessor row            :initarg :row            :initform 0)
   (field          :accessor field          :initarg :field          :initform "")
   (system         :accessor system         :initarg :system         :initform "")
   ))

(defmethod print-object ((bapiret2 bapiret2) stream)
  (print-unreadable-object (bapiret2 stream :type t :identity t)
    (format stream ":MESSAGE-TYPE ~S :MESSAGE-ID ~S :MESSAGE-NUMBER ~S :MESSAGE ~S :LOG-NR ~S :LOG-MSG-NR ~S :MESSAGE-V1 ~S :MESSAGE-V2 ~S :MESSAGE-V3 ~S :MESSAGE-V4 ~S :PARAMETER ~S :ROW ~S :FIELD ~S :SYSTEM ~S"
	    (message-type   bapiret2)
	    (message-id     bapiret2)
	    (message-number bapiret2)
	    (message        bapiret2)
	    (log-nr         bapiret2)
	    (log-msg-nr     bapiret2)
	    (message-v1     bapiret2)
	    (message-v2     bapiret2)
	    (message-v3     bapiret2)
	    (message-v4     bapiret2)
	    (parameter      bapiret2)
	    (row            bapiret2)
	    (field          bapiret2)
	    (system         bapiret2))))

(defun set-sap-bapiret2-from-servb-bapiret2 (servb-bapiret2 bapiret2-struct-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (rfc-set-chars bapiret2-struct-handle "TYPE"       (message-type servb-bapiret2)   (length (message-type servb-bapiret2))   sap-uc-encoding)
  (rfc-set-chars bapiret2-struct-handle "ID"         (message-id servb-bapiret2)     (length (message-id servb-bapiret2))     sap-uc-encoding)
  (rfc-set-chars bapiret2-struct-handle "NUMBER"     (message-number servb-bapiret2) (length (message-number servb-bapiret2)) sap-uc-encoding)
  (rfc-set-chars bapiret2-struct-handle "LOG_NO"     (log-nr servb-bapiret2)         (length (log-nr servb-bapiret2))         sap-uc-encoding)
  (rfc-set-chars bapiret2-struct-handle "LOG_MSG_NO" (log-msg-nr servb-bapiret2)     (length (log-msg-nr servb-bapiret2))     sap-uc-encoding)
  (rfc-set-chars bapiret2-struct-handle "MESSAGE_V1" (message-v1 servb-bapiret2)     (length (message-v1 servb-bapiret2))     sap-uc-encoding)
  (rfc-set-chars bapiret2-struct-handle "MESSAGE_V2" (message-v2 servb-bapiret2)     (length (message-v2 servb-bapiret2))     sap-uc-encoding)
  (rfc-set-chars bapiret2-struct-handle "MESSAGE_V3" (message-v3 servb-bapiret2)     (length (message-v3 servb-bapiret2))     sap-uc-encoding)
  (rfc-set-chars bapiret2-struct-handle "MESSAGE_V4" (message-v4 servb-bapiret2)     (length (message-v4 servb-bapiret2))     sap-uc-encoding)
  (rfc-set-chars bapiret2-struct-handle "PARAMETER"  (parameter servb-bapiret2)      (length (parameter servb-bapiret2))      sap-uc-encoding)
  (rfc-set-int   bapiret2-struct-handle "ROW"        (row servb-bapiret2)                                                     sap-uc-encoding)
  (rfc-set-chars bapiret2-struct-handle "FIELD"      (field servb-bapiret2)          (length (field servb-bapiret2))          sap-uc-encoding)
  (rfc-set-chars bapiret2-struct-handle "SYSTEM"     (system servb-bapiret2)         (length (system servb-bapiret2))         sap-uc-encoding)
  (values))

(defun set-servb-bapiret2-from-sap-bapiret2 (servb-bapiret2 bapiret2-struct-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (setf (message-type   servb-bapiret2) (cl-strings:clean (rfc-get-chars bapiret2-struct-handle "TYPE"           1 sap-uc-encoding)))
  (setf (message-id     servb-bapiret2) (cl-strings:clean (rfc-get-chars bapiret2-struct-handle "ID"            20 sap-uc-encoding)))
  (setf (message-number servb-bapiret2) (cl-strings:clean (rfc-get-chars bapiret2-struct-handle "NUMBER"         3 sap-uc-encoding)))
  (setf (message        servb-bapiret2) (cl-strings:clean (rfc-get-chars bapiret2-struct-handle "MESSAGE"      220 sap-uc-encoding)))
  (setf (log-nr         servb-bapiret2) (cl-strings:clean (rfc-get-chars bapiret2-struct-handle "LOG_NO"        20 sap-uc-encoding)))
  (setf (log-msg-nr     servb-bapiret2) (cl-strings:clean (rfc-get-chars bapiret2-struct-handle "LOG_MSG_NO"     6 sap-uc-encoding)))
  (setf (message-v1     servb-bapiret2) (cl-strings:clean (rfc-get-chars bapiret2-struct-handle "MESSAGE_V1"    50 sap-uc-encoding)))
  (setf (message-v2     servb-bapiret2) (cl-strings:clean (rfc-get-chars bapiret2-struct-handle "MESSAGE_V2"    50 sap-uc-encoding)))
  (setf (message-v3     servb-bapiret2) (cl-strings:clean (rfc-get-chars bapiret2-struct-handle "MESSAGE_V3"    50 sap-uc-encoding)))
  (setf (message-v4     servb-bapiret2) (cl-strings:clean (rfc-get-chars bapiret2-struct-handle "MESSAGE_V4"    50 sap-uc-encoding)))
  (setf (parameter      servb-bapiret2) (cl-strings:clean (rfc-get-chars bapiret2-struct-handle "PARAMETER"     32 sap-uc-encoding)))
  (setf (row            servb-bapiret2) (rfc-get-int bapiret2-struct-handle "ROW"))
  (setf (field          servb-bapiret2) (cl-strings:clean (rfc-get-chars bapiret2-struct-handle "FIELD"         30 sap-uc-encoding)))
  (setf (system         servb-bapiret2) (cl-strings:clean (rfc-get-chars bapiret2-struct-handle "SYSTEM"        10 sap-uc-encoding)))
  servb-bapiret2)

(defun bapiret2-error-p (bapiret2-struct-handle)
  (let ((result-type (rfc-get-string bapiret2-struct-handle "TYPE" 1)))
    (if (or (string= result-type "I")
	    (string= result-type "S"))
	nil
	t)))
