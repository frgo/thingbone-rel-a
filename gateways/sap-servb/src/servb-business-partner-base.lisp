;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

(defclass servb-business-partner (servb-data servb-data-validity-mixin)
  ((business-partner-nr          :accessor business-partner-nr          :initarg :business-partner-nr       :initform nil)
   (business-partner-kind        :accessor business-partner-kind        :initarg :business-partner-kind     :initform nil)
   (business-partner-id          :accessor business-partner-id          :initarg :business-partner-id       :initform nil)
   (business-partner-name        :accessor business-partner-name        :initarg :business-partner-name     :initform "")
   (search-term                  :accessor search-term                  :initarg :search-term               :initform "")
   (business-partner-status      :accessor business-partner-status      :initarg :business-partner-status   :initform nil)
   (lang                         :accessor lang                         :initarg :lang                      :initform nil)
   (currency                     :accessor currency                     :initarg :currency                  :initform nil)
   (vat-registration-number      :accessor vat-registration-number      :initarg :vat-registration-number   :initform nil)
   (duns-number                  :accessor duns-number                  :initarg :duns-number               :initform nil)
   (incoterms                    :accessor incoterms                    :initarg :incoterms                 :initform nil)
   (incoterms-location           :accessor incoterms-loaction           :initarg :incoterms-location        :initform nil)
   ()

   ))

(defmethod print-object ((servb-mat-pricing-item servb-mat-pricing-item) stream)
  (print-unreadable-object (servb-mat-pricing-item stream :identity t :type t)
    (format stream ":SITE ~S :POS-NR ~S :ITEM-NR ~S :ITEM-VERSION ~S :ITEM-REVISION ~S :VALUE-STREAM-CODE ~S :QUANTITY ~S :UOM ~S :PRICING-CONDITION-TYPE ~S :PRICE ~S :SALES-MATERIAL-GROUP ~S"
	    )))
