;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

;;; SAP-SERVB - SPECIFIC DATA STRUCTURES

;; IMPLEMENTATION NOTE: IV_EVENT DEFINITION
;;
;; IV_EVENT	STRING

(eval-when (:compile-toplevel :execute :load-toplevel)
  (defconstant +default-servb-iv-event-data-string-length+ (* 1 1024))
  (defparameter *servb-iv-event-data-string-length* +default-servb-iv-event-data-string-length+))

(defclass servb-iv-event (servb-data)
  ((data :accessor data :initarg :data :initform nil)))

(defmethod print-object ((servb-iv-event servb-iv-event) stream)
  (print-unreadable-object (servb-iv-event stream :identity t :type t)
    (format stream ":DATA ~S" (data servb-iv-event))))

(defun set-sap-iv-event-from-servb-iv-event (servb-iv-event container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let ((data (data servb-iv-event)))
    (rfc-set-string container-handle "IV_EVENT" data (length data) sap-uc-encoding)
    (values)))

(defun set-servb-iv-event-from-sap-iv-event (servb-iv-event container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (setf (data servb-iv-event) (rfc-get-string container-handle "IV_EVENT" *servb-iv-event-data-string-length* sap-uc-encoding))
  (log:debug "SET-SERVB-IV-EVENT-FROM-SAP-IV-EVENT - ~S" servb-iv-event)
  servb-iv-event)

(defun make-servb-iv-event-from-sap-container-handle (container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (if (not (cffi:null-pointer-p container-handle))
      (let ((servb-iv-event (make-instance 'servb-iv-event)))
	(set-servb-iv-event-from-sap-iv-event servb-iv-event container-handle :sap-uc-encoding sap-uc-encoding)
	servb-iv-event)))
