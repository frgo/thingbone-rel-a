;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

(defmethod servb-to-sap ((self tb-incoterms) func-handle container-handle  &key (sap-uc-encoding (rfc-sap-uc-encoding)))

  (let ((%container-handle (sap-container-handle self func-handle container-handle)))

    (with-slots (location terms) self

      (when location
	(rfc-set-chars %container-handle "INCOTERMS_LOC" location (servb-length 28 (length location)) sap-uc-encoding))

      (when terms
	(rfc-set-chars %container-handle "INCOTERMS" location (servb-length 28 (length location)) sap-uc-encoding))

      ))

  self)

(defmethod sap-to-servb ((self servb-incoterms) func-handle container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let ((%container-handle (sap-container-handle self func-handle container-handle)))
    )
  self)


(defclass servb-order-level-incoterms (servb-incoterms)
  ())

(defmethod sap-container-handle-from-sap-func-handle ((self servb-order-level-incoterms) func-handle)
  (rfc-get-structure func-handle "IS_HEADER"))
