;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;; FUNCTION Z_MAT_SALES_ORDER_REQUEST .
;; *"----------------------------------------------------------------------
;; *"*"Lokale Schnittstelle:
;; *"  IMPORTING
;; *"     VALUE(IS_HEADER) TYPE  ZMAT_S_SALES_ORDER_HEADER
;; *"     VALUE(IT_LINE_ITEMS) TYPE  ZMAT_T_SALES_ORDER_ITEMS OPTIONAL
;; *"     VALUE(IT_COMMENTS) TYPE  ZMAT_T_SALES_ORDER_COMMENT_LI OPTIONAL
;; *"     VALUE(IT_PARTNERS) TYPE  ZMAT_T_SALES_ORDER_PARTNERS OPTIONAL
;; *"     VALUE(IS_ADDRESS) TYPE  ZMAT_S_SALES_ORDER_ADDRESS OPTIONAL
;; *"     VALUE(IV_WO_COMMIT) TYPE  FLAG OPTIONAL
;; *"  EXPORTING
;; *"     VALUE(ES_SERVB_RETURN) TYPE  ZSERVB_S_RETURN
;; *"     VALUE(EV_SALES_ORDER_NR) TYPE  VBELN_VA
;; *"     VALUE(ET_MESSAGES) TYPE  ZMAT_T_SALES_ORDER_RETURN
;; *"     VALUE(ET_DETAIL_STATUS) TYPE  ZMAT_T_SALES_ORDER_STATUS
;; *"----------------------------------------------------------------------


(defun make-sales-order-status-update-agree-from-extended-msg (extended-msg)
  (let* ((data-context (make-instance 'tb.msgs:tb-data-context :environment (tb.core:tb-site)))
	 ;; (data (let ((data (tb.msgs:make-msg-data :response-sales-order-create
	 ;; 					  (make-array 2 :adjustable t :fill-pointer 0 :initial-element nil))))
	 ;; 	 (vector-push-extend data-context (tb.msgs:data data))
	 ;; 	 (vector-push-extend (make-instance 'tb.msgs:tb-sales-order-create-result) (tb.msgs:data data))
	 ;; 	 data))
	 (content (make-instance 'tb.msgs:msg-extended-content
				 :object-class :sales-order
				 :operation :update
				 :data ;;data
				 nil))
	 (in-reply-to (tb.msgs:conversation-id extended-msg))
	 (request (tb.msgs:data (tb.msgs:content extended-msg)))
	 (response (make-instance 'tb.msgs:tb-inform-sales-order-status-update
				  :conversation-id (format nil "~A" (tb.core:make-uuid))
				  :encoding (tb.msgs:extended-msg-encoding)
				  :protocol (tb.msgs:extended-msg-protocol)
				  :protocol-version (tb.msgs:extended-msg-protocol-version)
				  :in-reply-to in-reply-to
				  :performative "agree"
				  :sender (tb.msgs::make-msg-sender-for-this-app-instance)
				  :content content
				  :data-context data-context
				  :request request
				  :sales-order-status nil)))
    response))

;; FUNCTION Z_MAT_S_O_S_UPD_REQ .
;; *"----------------------------------------------------------------------
;; *"*"Lokale Schnittstelle:
;; *"  IMPORTING
;; *"     VALUE(IV_SALES_ORDER) TYPE  VBELN_VA
;; *"     VALUE(IV_STAGING_STATUS) TYPE  Z_DE_ZZ_ST_STATUS
;; *"     VALUE(IV_WITH_GET_STATUS) TYPE  FLAG OPTIONAL
;; *"  EXPORTING
;; *"     VALUE(ES_SERVB_RETURN) TYPE  ZSERVB_S_RETURN
;; *"     VALUE(ET_MESSAGES) TYPE  ZMAT_T_SALES_ORDER_RETURN
;; *"  CHANGING
;; *"     VALUE(CT_DETAIL_STATUS) TYPE  ZMAT_T_SALES_ORDER_STATUS OPTIONAL
;; *"----------------------------------------------------------------------

;; +++ IMPORTING +++

;; Importing-Parameter
;;
;; Parameter	        Data element	        Type	Length	Hinweis
;; IV_SALES_ORDER	VBELN_VA	        CHAR	10	Sales Order Nummer
;; IV_STAGING_STATUS	Z_DE_ZZ_ST_STATUS	NUMV	2	Staging Status
;; IV_WITH_GET_STATUS	FLAG	                CHAR	1	"X' --> Status Liste wird zurückgegeben
;;                                                               <leer> --> Status Liste wird nicht zurückgegeben"

(defun sapr3-write-iv-sales-order-status-from-inform-sales-order-status-update (container-handle inform-sales-order-status-update &key (sap-uc-encoding (rfc-sap-uc-encoding)))

  (let* ((sales-order-update       (tb.msgs:sales-order-status inform-sales-order-status-update))
	 (order-nr                 (tb.msgs:order-nr sales-order-update))
	 (order-global-status-info (tb.msgs:order-global-status-info sales-order-update))
	 (staging-status           (tb.msgs:staging-status order-global-status-info)))
    (sapr3-write-chars container-handle "IV_SALES_ORDER"     order-nr        10 sap-uc-encoding)
    (sapr3-write-chars container-handle "IV_STAGING_STATUS"  staging-status   2 sap-uc-encoding))

  (values))

;; +++ EXPORTING +++

;; Only need ES_SERVB_RETURN

;; +++ FUNCTION MODULE +++

;; FUNCTION Z_MAT_SALES_ORDER_REQUEST .
;; *"----------------------------------------------------------------------
;; *"*"Lokale Schnittstelle:
;; *"  IMPORTING
;; *"     VALUE(IS_HEADER) TYPE  ZMAT_S_SALES_ORDER_HEADER
;; *"     VALUE(IT_LINE_ITEMS) TYPE  ZMAT_T_SALES_ORDER_ITEMS OPTIONAL
;; *"     VALUE(IT_COMMENTS) TYPE  ZMAT_T_SALES_ORDER_COMMENT_LI OPTIONAL
;; *"     VALUE(IT_PARTNERS) TYPE  ZMAT_T_SALES_ORDER_PARTNERS OPTIONAL
;; *"     VALUE(IS_ADDRESS) TYPE  ZMAT_S_SALES_ORDER_ADDRESS OPTIONAL
;; *"     VALUE(IV_WO_COMMIT) TYPE  FLAG OPTIONAL
;; *"  EXPORTING
;; *"     VALUE(ES_SERVB_RETURN) TYPE  ZSERVB_S_RETURN
;; *"     VALUE(EV_SALES_ORDER_NR) TYPE  VBELN_VA
;; *"     VALUE(EV_TAX) TYPE  NETWR
;; *"     VALUE(ET_MESSAGES) TYPE  ZMAT_T_SALES_ORDER_RETURN
;; *"     VALUE(ET_DETAIL_STATUS) TYPE  ZMAT_T_SALES_ORDER_STATUS
;; *"----------------------------------------------------------------------

(defun z-mat-sales-order-status-update (connection-handle extended-msg &key (sap-uc-encoding (rfc-sap-uc-encoding)))

  (let* ((inform-sales-order-status-update (tb.msgs:data (tb.msgs:content extended-msg)))
  	 (agree-sales-order-status-update (make-sales-order-status-update-agree-from-extended-msg extended-msg))
  	 (fm-name "Z_MAT_S_O_S_UPD_REQ")
	 (func-handle (let ((handle (rfc-function connection-handle fm-name)))
			(if (null handle)
			    (tb.sapnwrfc:signal-rfc-invalid-parameter "FM NAME" "Z_MAT_S_O_S_UPD_REQ"))
			handle)))

    ;; WRITE DATA INTO IMPORTING STRUCTURES AND TABLES

    (sapr3-write-iv-sales-order-status-from-inform-sales-order-status-update func-handle inform-sales-order-status-update :sap-uc-encoding sap-uc-encoding)

    ;; INVOKE FUNCTION

    (let* ((rc                (rfc-invoke connection-handle func-handle))
	   (servb-return-info (get-servb-return-info-from-function-handle func-handle)))
      (setf (rfc-return-code servb-return-info) rc)
      (setf (fm-name servb-return-info) fm-name)

      (log:info "Z-MAT-SALES-ORDER-STATUS-UPDATE: agree-sales-order-status-update: ~S." agree-sales-order-status-update)
      (tb.core:send (sap-servb-return-info-log-channel) servb-return-info)
      (values agree-sales-order-status-update servb-return-info))))


(defun sap-direct-inform-sales-order-status-update (servb-sap-rfc-connection extended-msg-with-inform-sales-order-status-update)
  (check-type servb-sap-rfc-connection servb-sap-rfc-connection)
  (check-type extended-msg-with-inform-sales-order-status-update tb.msgs:extended-msg)
  (with-rfc-connection-ensured (servb-sap-rfc-connection "SAP-DIRECT-INFORM-SALES-ORDER-STATUS-UPDATE")
    (multiple-value-bind (agree-sales-order-status-update servb-return-info)
	(z-mat-sales-order-status-update (connection-handle servb-sap-rfc-connection) extended-msg-with-inform-sales-order-status-update)
      (setf (tb.msgs:data (tb.msgs:content agree-sales-order-status-update))
	    (make-array 1 :initial-contents (list (tb.msgs:data-context agree-sales-order-status-update))))
      (values agree-sales-order-status-update servb-return-info))))
