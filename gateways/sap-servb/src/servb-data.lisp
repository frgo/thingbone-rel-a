;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

;;; SAP-SERVB - SPECIFIC DATA STRUCTURES

(defclass servb-data ()
  ())

(defclass servb-data-validity-mixin ()
  ((valid-from  :accessor valid-from  :initarg :valid-from  :initform nil)
   (valid-until :accessor valid-until :initarg :valid-until :initform nil)))

(defmethod servb-length (max-length (s string))
  (let ((len (length s)))
    (if (> len max-length)
	max-length
	len)))

(defmethod servb-length (max-length (n integer))
  (if (> n max-length)
      max-length
      n))

(declaim (inline sap-container-handle))
(defun sap-container-handle (servb-object func-handle container-handle)
  (or container-handle
      (sap-container-handle-from-sap-func-handle servb-object func-handle)))
