;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

;;; SAP-SERVB - SPECIFIC DATA STRUCTURES

;; IMPLEMENTATION NOTE: IV_DATA DEFINITION
;;
;; IV_DATA	STRING	JSON-String

(defconstant +default-servb-iv-data-data-string-length+ (* 1 1024 1024))
(defparameter *servb-iv-data-data-string-length* +default-servb-iv-data-data-string-length+)

(defclass servb-iv-data (servb-data)
  ((environment :accessor environment :initarg :environment :initform (tb.core:tb-site))
   (data :accessor data :initarg :data :initform nil)))

(defmethod print-object ((servb-iv-data servb-iv-data) stream)
  (print-unreadable-object (servb-iv-data stream :identity t :type t)
    (format stream ":DATA ~S" (data servb-iv-data))))

(defun set-sap-iv-data-from-servb-iv-data (servb-iv-data container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let ((data (data servb-iv-data)))
    (rfc-set-string container-handle "IV_DATA" data (length data) sap-uc-encoding)
    (values)))

(defun set-servb-iv-data-from-sap-iv-data (servb-iv-data container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (setf (data servb-iv-data) (rfc-get-string container-handle "IV_DATA" *servb-iv-data-data-string-length* sap-uc-encoding))
  (log:debug "SET-SERVB-IV-DATA-FROM-SAP-IV-DATA - ~S" servb-iv-data)
  servb-iv-data)

(defun make-servb-iv-data-from-sap-container-handle (container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (if (not (cffi:null-pointer-p container-handle))
      (let ((servb-iv-data (make-instance 'servb-iv-data)))
	(set-servb-iv-data-from-sap-iv-data servb-iv-data container-handle :sap-uc-encoding sap-uc-encoding)
	servb-iv-data)))
