;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

;;; das ist die USER FUNCTION !!
(defun z-servb-appl-log-msg-fm (connection-handle func-handle error-info-ptr)

  (declare (ignore connection-handle error-info-ptr))

  (let ((rc :RFC-OK))
    (log:trace "Z-SERVB-APPL-LOG-MSG-FM: Entry.")

    (let* ((servb-bal-s-header-information (make-servb-bal-s-header-information-from-sap-container-handle func-handle))
	   (it-messages-table-handle       (get-it-messages-table-handle-from-function-handle func-handle))
	   (nr-msgs                        (rfc-get-row-count it-messages-table-handle)))

      (log:debug "Z-SERVB-APPL-LOG-MSG-FM: ~S messages to be sent to Slack." nr-msgs)

      (if (not (cffi:null-pointer-p it-messages-table-handle))
	  (progn
	    (let ((channel (sap-servb-appl-log-channel)))
	      (loop for index from 0 to (1- nr-msgs)
		 do
		   (rfc-move-to it-messages-table-handle index)
		   (let ((row-handle (rfc-get-current-row it-messages-table-handle)))
		     (if (not (cffi:null-pointer-p row-handle))
			 (let* ((bapiret2 (set-servb-bapiret2-from-sap-bapiret2 (make-instance 'bapiret2) row-handle))
				(appl-log-msg (make-tb-sap-servb-appl-log-msg servb-bal-s-header-information bapiret2)))
			   (tb.core:send channel appl-log-msg))))))))

      (set-sap-servb-es-servb-return-from-params-in-func func-handle
							 "Z-SERVB-APPL-LOG-MSG-FM"
							 :rfc-ok
							 log4cl:+log-level-info+
							 "00000000"
							 "TBGS001S"
							 (format nil "SUCCESS. HANDLED ~S INDIVIDUAL MESSAGES." nr-msgs)))

    (log:trace "Z-SERVB-OUTBOUND-SERVER-FM: Exit (rc = ~S)." rc)
    (cffi:foreign-enum-value 'rfc-rc rc)))

(define-servb-server-function Z_SERVB_APPL_LOG_MSG_FM :servb-outbound 'z-servb-appl-log-msg-fm)
