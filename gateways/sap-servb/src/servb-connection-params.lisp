;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

;;; ============================================================================
;;;   SAP CONNECTION PARAMS
;;; ============================================================================

;; ;;; SAP RFC CLIENT CONNECTION PARAMS

(defparameter *bwp-stu-kw0-connection-params*
  (let* ((sap-router     "/H/10.79.128.1/S/5890")
	 (sap-app-server "wudkw0ci")
	 (sap-sysnr      "00")
	 (sap-client     "500")
	 (sap-lang       "EN")
	 (sap-user       "THINGBONE")
	 (sap-pw         "t3hdi5ncg5b8o4nfe3a24e9e2136fd6a6daf731e")
	 (sap-trace      "3")
	 (connection-params (list (list "SAPROUTER" sap-router)
				  (list "ASHOST"    sap-app-server)
				  (list "SYSNR"     sap-sysnr)
				  (list "CLIENT"    sap-client)
				  (list "LANG"      sap-lang)
				  (list "USER"      sap-user)
				  (list "PASSWD"    sap-pw)
				  (list "TRACE"     sap-trace)
				  )))
    connection-params))

(defparameter *bwp-stu-kw1-connection-params*
  (let* ((sap-router     "/H/10.79.128.1/S/5890")
	 (sap-app-server "wudkw1ci")
	 (sap-sysnr      "00")
	 (sap-client     "500")
	 (sap-lang       "EN")
	 (sap-user       "THINGBONE")
	 (sap-pw         "t3hdi5ncg5b8o4nfe3a24e9e2136fd6a6daf731e")
	 (sap-trace      "3")
	 (connection-params (list (list "SAPROUTER" sap-router)
				  (list "ASHOST"    sap-app-server)
				  (list "SYSNR"     sap-sysnr)
				  (list "CLIENT"    sap-client)
				  (list "LANG"      sap-lang)
				  (list "USER"      sap-user)
				  (list "PASSWD"    sap-pw)
				  (list "TRACE"     sap-trace)
				  )))
    connection-params))

(defparameter *bwp-stu-kw1-luederh-connection-params*
  (let* ((sap-router     "/H/10.79.128.1/S/5890")
	 (sap-app-server "wudkw1ci")
	 (sap-sysnr      "00")
	 (sap-client     "500")
	 (sap-lang       "EN")
	 (sap-user       "luederh")
	 (sap-pw         "kl23060")
	 (sap-trace      "3")
	 (connection-params (list (list "SAPROUTER" sap-router)
				  (list "ASHOST"    sap-app-server)
				  (list "SYSNR"     sap-sysnr)
				  (list "CLIENT"    sap-client)
				  (list "LANG"      sap-lang)
				  (list "USER"      sap-user)
				  (list "PASSWD"    sap-pw)
				  (list "TRACE"     sap-trace)
				  )))
    connection-params))

(defparameter *bwp-stu-kw2-connection-params*
  (let* ((sap-router     "/H/10.79.128.1/S/5890")
	 (sap-app-server "wudkw2ci")
	 (sap-sysnr      "02")
	 (sap-client     "500")
	 (sap-lang       "EN")
	 (sap-user       "THINGBONE")
	 (sap-pw         "t3hdi5ncg5b8o4nfe3a24e9e2136fd6a6daf731e")
	 (sap-trace      "1")
	 (connection-params (list (list "SAPROUTER" sap-router)
				  (list "ASHOST"    sap-app-server)
				  (list "SYSNR"     sap-sysnr)
				  (list "CLIENT"    sap-client)
				  (list "LANG"      sap-lang)
				  (list "USER"      sap-user)
				  (list "PASSWD"    sap-pw)
				  (list "TRACE"     sap-trace)
				  )))
    connection-params))

(defparameter *bwp-ham-t10-connection-params*
  (let* ((sap-router     "/H/10.78.176.27/S/56080")
	 (sap-app-server "echt10ci")
	 (sap-sysnr      "00")
	 (sap-client     "600")
	 (sap-lang       "EN")
	 (sap-user       "USR_SERVBUS")
	 (sap-pw         "Hamburg#1")
	 (sap-trace      "3")
	 (connection-params (list (list "SAPROUTER" sap-router)
				  (list "ASHOST"    sap-app-server)
				  (list "SYSNR"     sap-sysnr)
				  (list "CLIENT"    sap-client)
				  (list "LANG"      sap-lang)
				  (list "USER"      sap-user)
				  (list "PASSWD"    sap-pw)
				  (list "TRACE"     sap-trace)
				  )))
    connection-params))

(defparameter *bwp-ham-q10-connection-params*
  (let* ((sap-router     "/H/10.78.176.27/S/56080")
	 (sap-app-server "echq10ci")
	 (sap-sysnr      "02")
	 (sap-client     "600")
	 (sap-lang       "EN")
	 (sap-user       "USR_SERVBUS")
	 (sap-pw         "Hamburg#1")
	 (sap-trace      "3")
	 (connection-params (list (list "SAPROUTER" sap-router)
				  (list "ASHOST"    sap-app-server)
				  (list "SYSNR"     sap-sysnr)
				  (list "CLIENT"    sap-client)
				  (list "LANG"      sap-lang)
				  (list "USER"      sap-user)
				  (list "PASSWD"    sap-pw)
				  (list "TRACE"     sap-trace)
				  )))
    connection-params))

(defparameter *bwp-ham-p10-connection-params*
  (let* ((sap-router     "/H/10.78.176.27/S/56080")
	 (sap-app-server "echp10ci")
	 (sap-sysnr      "00")
	 (sap-client     "100")
	 (sap-lang       "EN")
	 (sap-user       "USR_SERVBUS")
	 (sap-pw         "Hamburg#1")
	 (sap-trace      "1")
	 (connection-params (list (list "SAPROUTER" sap-router)
				  (list "ASHOST"    sap-app-server)
				  (list "SYSNR"     sap-sysnr)
				  (list "CLIENT"    sap-client)
				  (list "LANG"      sap-lang)
				  (list "USER"      sap-user)
				  (list "PASSWD"    sap-pw)
				  (list "TRACE"     sap-trace)
				  )))
    connection-params))

;;; SAP RFC SERVER CONNECTION PARAMS

(defparameter *bwp-stu-sapgw00-server-gw-connection-params*
  (let* ((sap-router     "/H/10.79.128.1/S/5890")
	 (sap-gwhost     "10.218.2.103")
	 (sap-gwserv     "sapgw00")
	 (sap-program-id "TB_GW_SAP_SERVB")
	 (sap-trace      "1")
	 (server-gw-connection-params (list (list "SAPROUTER"  sap-router)
					    (list "GWHOST"     sap-gwhost)
					    (list "GWSERV"     sap-gwserv)
					    (list "PROGRAM_ID" sap-program-id)
					    (list "TRACE"      sap-trace)
					    )))
    server-gw-connection-params))

(defparameter *bwp-stu-sapgw02-server-gw-connection-params*
  (let* ((sap-router     "/H/10.79.128.1/S/5890")
	 (sap-gwhost     "10.218.2.105")
	 (sap-gwserv     "sapgw02")
	 (sap-program-id "TB_GW_SAP_SERVB")
	 (sap-trace      "1")
	 (server-gw-connection-params (list (list "SAPROUTER"  sap-router)
					    (list "GWHOST"     sap-gwhost)
					    (list "GWSERV"     sap-gwserv)
					    (list "PROGRAM_ID" sap-program-id)
					    (list "TRACE"      sap-trace)
					    )))
    server-gw-connection-params))

(defparameter *bwp-ham-sapgw00-server-gw-connection-params*
  (let* ((sap-router     "/H/10.78.176.27/S/56080")
	 (sap-gwhost     "10.218.2.103")
	 (sap-gwserv     "sapgw00")
	 (sap-program-id "TB_GW_SAP_SERVB")
	 (sap-trace      "1")
	 (server-gw-connection-params (list (list "SAPROUTER"  sap-router)
					    (list "GWHOST"     sap-gwhost)
					    (list "GWSERV"     sap-gwserv)
					    (list "PROGRAM_ID" sap-program-id)
					    (list "TRACE"      sap-trace)
					    )))
    server-gw-connection-params))


(defun set-ham-t10-sap-rfc-connection-params ()
  (setf (tb.core:global-context-value :sap-servb-client-rfc-connection) nil)
  (let ((client-conn-params *bwp-ham-t10-connection-params*)
	(server-conn-params *bwp-ham-sapgw00-server-gw-connection-params*))
    (setf (sap-servb-client-rfc-connection-params) client-conn-params)
    (setf (sap-servb-server-rfc-connection-params) server-conn-params)
    ))

(defun set-ham-q10-sap-rfc-connection-params ()
  (setf (tb.core:global-context-value :sap-servb-client-rfc-connection) nil)
  (let ((client-conn-params *bwp-ham-q10-connection-params*)
	(server-conn-params *bwp-ham-sapgw00-server-gw-connection-params*))
    (setf (sap-servb-client-rfc-connection-params) client-conn-params)
    (setf (sap-servb-server-rfc-connection-params) server-conn-params)
    ))

(defun set-ham-p10-sap-rfc-connection-params ()
  (setf (tb.core:global-context-value :sap-servb-client-rfc-connection) nil)
  (let ((client-conn-params *bwp-ham-p10-connection-params*)
	(server-conn-params *bwp-ham-sapgw00-server-gw-connection-params*))
    (setf (sap-servb-client-rfc-connection-params) client-conn-params)
    (setf (sap-servb-server-rfc-connection-params) server-conn-params)
    ))

(defun set-stu-kw1-sap-rfc-connection-params ()
  (setf (tb.core:global-context-value :sap-servb-client-rfc-connection) nil)
  (let ((client-conn-params *bwp-stu-kw1-connection-params*)
	(server-conn-params *bwp-stu-sapgw00-server-gw-connection-params*))
    (setf (sap-servb-client-rfc-connection-params) client-conn-params)
    (setf (sap-servb-server-rfc-connection-params) server-conn-params)
    ))

(defun set-stu-kw1-luederh-sap-rfc-connection-params ()
  (setf (tb.core:global-context-value :sap-servb-client-rfc-connection) nil)
  (let ((client-conn-params *bwp-stu-kw1-luederh-connection-params*)
	(server-conn-params *bwp-stu-sapgw00-server-gw-connection-params*))
    (setf (sap-servb-client-rfc-connection-params) client-conn-params)
    (setf (sap-servb-server-rfc-connection-params) server-conn-params)
    ))

(defun set-stu-kw2-sap-rfc-connection-params ()
  (setf (tb.core:global-context-value :sap-servb-client-rfc-connection) nil)
  (let ((client-conn-params *bwp-stu-kw2-connection-params*)
	(server-conn-params *bwp-stu-sapgw02-server-gw-connection-params*))
    (setf (sap-servb-client-rfc-connection-params) client-conn-params)
    (setf (sap-servb-server-rfc-connection-params) server-conn-params)
    ))
