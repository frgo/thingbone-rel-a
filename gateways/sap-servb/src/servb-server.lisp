;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

(defclass servb-server (tb.core:tb-task)
  ((domain                      :reader   domain                      :initarg :domain                      :initform (error "DOMAIN required!"))
   (handle-domain-server-fns-p  :accessor handle-domain-server-fns-p  :initarg :handle-domain-server-fns-p  :initform t)
   (registration-status         :accessor registration-status         :initarg :registration-status         :initform :UNREGISTERED)
   (client-connection-params    :accessor client-connection-params    :initarg :client-connection-params    :initform '())
   (client-rfc-connection       :accessor client-rfc-connection       :initarg :client-rfc-connection       :initform nil)
   (server-gw-connection-params :accessor server-gw-connection-params :initarg :server-gw-connection-params :initform '())
   (rfc-timeout                 :accessor rfc-timeout                 :initarg :rfc-timeout                 :initform 1) ;; [s]
   (rfc-server-handle           :accessor rfc-server-handle           :initarg :rfc-server-handle           :initform (cffi:null-pointer))
   (keep-listening              :accessor keep-listening              :initarg :keep-listening              :initform t)
   (lock-keep-listening         :accessor lock-keep-listening                                               :initform (bt:make-recursive-lock))
   )
  (:default-initargs
   :name "SAP SERVB SERVER"))

(defmethod keep-listening-p ((servb-server servb-server))
  (bt:with-recursive-lock-held ((lock-keep-listening servb-server))
    (keep-listening servb-server)))

(defmethod print-object ((servb-server servb-server) stream)
  (print-unreadable-object (servb-server stream :identity t :type t)
    (format stream "~A :DOMAIN ~S :REGISTRATION-STATUS ~S :RUN-STATUS ~S :KEEP-LISTENING ~S :CLIENT-CONNECTION-PARAMS ~S :SERVER-GW-CONNECTION-PARAMS ~S :RFC-TIMEOUT ~S :RFC-SERVER-HANDLE ~S"
	    (tb.core::name servb-server)
	    (domain servb-server)
	    (registration-status servb-server)
	    (tb.core::run-status servb-server)
	    (keep-listening-p servb-server)
	    (client-connection-params servb-server)
	    (server-gw-connection-params servb-server)
	    (rfc-timeout servb-server)
	    (rfc-server-handle servb-server))))

(defmethod stop-listening ((servb-server servb-server))
  (bt:with-recursive-lock-held ((lock-keep-listening servb-server))
    (setf (keep-listening servb-server) nil)))

(defmethod reset-servb-server-functions ((servb-server servb-server))
  (log:trace "SERVB-SERVER::RESET-SERVB-SERVER-FUNCTIONS: Entered.")
  (if (handle-domain-server-fns-p servb-server)
      (reset-servb-server-functions-for-domain (domain servb-server))))

(defmethod install-servb-server-functions ((servb-server servb-server) client-servb-rfc-connection)
  (log:trace "SERVB-SERVER::INSTALL-SERVB-SERVER-FUNCTIONS: Entered.")
  (let ((domain (domain servb-server))
	(server-name (tb.core::name servb-server)))
    (if (handle-domain-server-fns-p servb-server)
	(progn
	  (log:debug "SERVB-SERVER ~A: Handling domain server functions ..." server-name)
	  (log:info "SERVB-SERVER ~A: Domain ~S with ~S functions to install."
		    server-name domain (count-servb-server-functions-by-domain domain))
	  (install-servb-server-functions-for-domain (domain servb-server) client-servb-rfc-connection)
	  t)
	(progn
	  (log:warn "SERVB-SERVER ~A: NOT handling domain server functions!" server-name)
	  nil))))

(defmethod reset ((servb-server servb-server))
  (reset-servb-server-functions servb-server)
  (stop-listening servb-server)
  (let ((client-rfc-connection (client-rfc-connection servb-server)))
    (if client-rfc-connection
	(progn
	  (close-connection client-rfc-connection)
	  (setf (client-rfc-connection servb-server) nil))))
  (tb.core:wait-until-halted servb-server :timeout 5000)
  (setf (rfc-server-handle servb-server) (cffi:null-pointer))
  (setf (registration-status servb-server) :UNREGISTERED)
  t)

(defun register-servb-server (server-rfc-server-gw-connection-params)
  (rfc-register-server server-rfc-server-gw-connection-params))

(defmethod register ((servb-server servb-server) &key (force nil))
  (log:trace "SERVB-SERVER::REGISTER: Entered.")
  (cond
    ((or (eql (registration-status servb-server) :UNREGISTERED)
	 force)
     (progn
       (if force
	   (ignore-errors
	     (let ((server-handle (register-servb-server (server-gw-connection-params servb-server))))
	       (setf (rfc-server-handle servb-server) server-handle)
	       (setf (registration-status servb-server) :REGISTERED)))
	   (let ((server-handle (register-servb-server (server-gw-connection-params servb-server))))
	     (setf (rfc-server-handle servb-server) server-handle)
	     (setf (registration-status servb-server) :REGISTERED)))
       ))))

(defmethod initialize ((servb-server servb-server))
  (log:trace "SERVB-SERVER::INITIALIZE: Entered.")
  (let ((client-rfc-connection (make-instance 'servb-sap-rfc-connection
					      :connection-params (client-connection-params servb-server))))
    (setf (client-rfc-connection servb-server) client-rfc-connection)
    (ensure-connection client-rfc-connection)
    (install-servb-server-functions servb-server client-rfc-connection))
  (register servb-server))

(defmethod listen-and-dispatch ((servb-server servb-server))
  (log:trace "SERVB-SEVER::LISTEN-AND-DISPATCH: Entered.")
  (let ((server-handle nil)
	(timeout       (rfc-timeout servb-server))
	(server-name   (tb.core::name servb-server))
	(result        nil))
    (setf (keep-listening servb-server) t)
    (setq result (do ((rc :rfc-ok)
		      (keep-listening (keep-listening-p servb-server) (keep-listening-p servb-server)))
		     ((not (and (eql keep-listening t)
				(or (eql rc :rfc-ok)
				    (eql rc :rfc-retry)
				    (eql rc :rfc-abap-exception))))
		      rc)
		   (progn
		     (tb.core:set-run-status servb-server :RUNNING)
		     (bt:thread-yield)
		     ;; (log:debug "~A: Listening for requests ... (keep-listening: ~S, rc: ~S, timeout: ~S)" server-name keep-listening rc timeout)
		     (setq server-handle (rfc-server-handle servb-server))
		     (setq rc (rfc-listen-and-dispatch server-handle timeout))
		     ;; (log:debug "~A: RFC-LISTEN-AND-DISPATCH: rc = ~S." server-name rc)
		     (let ((reconnect nil))
		       (case rc
			 (:rfc-ok
			  (log:debug "~A: User function completed with :RFC-OK." server-name))
			 (:rfc-retry
			  ;;(log:debug "~A: No RFC request within ~S seconds." server-name timeout)
			  )
			 (:rfc-abap-exception
			  (log:error "~A: ABAP Exception in user function!" server-name))
			 (:rfc-not-found
			  (log:error "~A: Unknown Function Module!" server-name)
			  (setq reconnect t))
			 (:rfc-external-failure
			  (log:error "~A: SYSTEM_FAILURE has been sent to backend!" server-name)
			  (setq reconnect t))
			 (:rfc-communication-failure
			  (log:error "~A: RFC communication failure!" server-name)
			  (setq reconnect t))
			 (:rfc-abap-message
			  (log:error "~A: ABAP error message has been sent to backend!" server-name)
			  (setq reconnect t))
			 (otherwise
			  (log:info "~A: RFC-LISTEN-AND-DISPATCH: Unhandled RC ~S!" server-name rc)
			  (setq reconnect t))
			 )
		       (if reconnect
			   (progn
			     (log:info "~A: Trying to reconnect ..." server-name)
			     (register servb-server :force t)
			     (setq rc :rfc-ok)))))))
    (log:debug "~A LISTEN-AND-DISPATCH: result = ~S." server-name result)
    (tb.core:set-run-status servb-server :HALTED)
    (log:debug "~A: HALTED (exited listen-and-dispatch main loop)." server-name)
    (log:trace "SERVB-SEVER::LISTEN-AND-DISPATCH: Exiting.")
    result))

(defun %start-servb-server (self)
  (log:info "Starting ~S ..." (tb.core:name self))
  (initialize self)
  (listen-and-dispatch self))

(defmethod tb.core:start ((servb-server servb-server))
  (handler-case (%start-servb-server servb-server)
    (error (condition)
      (log:error "*** Caught error ~S. Task ~S failed! Aborting this task!!!" condition (tb.core:name servb-server))
      condition)
    (:no-error ()
      t)))

(defmethod tb.core:stop ((servb-server servb-server))
  (log:info "Stopping ~S ..." (tb.core:name servb-server))
  (reset servb-server)
  (bt:join-thread (tb.core:get-thread servb-server)))


(defun sap-servb-server ()
  (tb.core:global-context-value :sap-servb-server))

(defun (setf sap-servb-server) (servb-server)
  (setf (tb.core:global-context-value :sap-servb-server) servb-server))


(defun start-sap-servb-outbound-server (&key (rfc-client-connnection-params (sap-servb-client-rfc-connection-params))
					  (rfc-server-connnection-params (sap-servb-server-rfc-connection-params)))
  (setf (sap-servb-server) (make-instance 'tb.gw.sap-servb:servb-server
					  :name "SAP SERVB OUTBOUND SERVER"
					  :domain :servb-outbound
					  :client-connection-params    rfc-client-connnection-params
					  :server-gw-connection-params rfc-server-connnection-params))
  (tb.core:run-in-thread (sap-servb-server)))

(defun stop-sap-servb-outbound-server (&key (servb-server (sap-servb-server)))
  (if servb-server
      (tb.core:stop servb-server)))
