;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

(defparameter *lock-sap-servb-inbound-channel*         (bt:make-recursive-lock))
(defparameter *lock-sap-servb-outbound-channel*        (bt:make-recursive-lock))
(defparameter *lock-sap-servb-appl-log-channel*        (bt:make-recursive-lock))
(defparameter *lock-sap-servb-return-info-log-channel* (bt:make-recursive-lock))
(defparameter *lock-sap-servb-rfc-error-log-channel*   (bt:make-recursive-lock))

(defparameter *lock-sap-servb-inbound-channel-dispatcher*         (bt:make-recursive-lock))
(defparameter *lock-sap-servb-outbound-channel-dispatcher*        (bt:make-recursive-lock))
(defparameter *lock-sap-servb-appl-log-channel-dispatcher*        (bt:make-recursive-lock))
(defparameter *lock-sap-servb-return-info-log-channel-dispatcher* (bt:make-recursive-lock))
(defparameter *lock-sap-servb-rfc-error-log-channel-dispatcher*   (bt:make-recursive-lock))

(defconstant +sap-servb-inbound-channel-name+         "SAP SERVICE BUS INBOUND")
(defconstant +sap-servb-outbound-channel-name+        "SAP SERVICE BUS OUTBOUND")
(defconstant +sap-servb-appl-log-channel-name+        "SAP SERVICE BUS APPLICATION LOG")
(defconstant +sap-servb-return-info-log-channel-name+ "SAP SERVICE BUS RETURN INFO LOG")
(defconstant +sap-servb-rfc-error-log-channel-name+   "SAP SERVICE BUS RFC ERROR LOG")

(defconstant +sap-servb-inbound-channel-dispatcher-name+         "SAP SERVICE BUS INBOUND CHANNEL DISPATCHER")
(defconstant +sap-servb-outbound-channel-dispatcher-name+        "SAP SERVICE BUS OUTBOUND CHANNEL DISPATCHER")
(defconstant +sap-servb-appl-log-channel-dispatcher-name+        "SAP SERVICE BUS APPLICATION LOG CHANNEL DISPATCHER")
(defconstant +sap-servb-return-info-log-channel-dispatcher-name+ "SAP SERVICE BUS RETURN INFO LOG CHANNEL DISPATCHER")
(defconstant +sap-servb-rfc-error-log-channel-dispatcher-name+   "SAP SERVICE BUS RFC ERROR LOG CHANNEL DISPATCHER")

(defvar *default-sap-servb-channel-max-size* 1024
  "The default maximum size for a SAP SERVB channel.")

(defvar *sap-servb-inbound-channel-max-size* *default-sap-servb-channel-max-size*
  "The maximum size for for the SAP INBOUND SERVB channel.")
(defvar *sap-servb-outbound-channel-max-size* *default-sap-servb-channel-max-size*
  "The maximum size for for the SAP OUTBOUND SERVB channel.")

(defvar *sap-servb-appl-log-channel-max-size* *default-sap-servb-channel-max-size*
  "The maximum size for for the SAP APPL LOG SERVB channel.")

(defvar *sap-servb-return-info-log-channel-max-size* *default-sap-servb-channel-max-size*
  "The maximum size for for the SAP RETURN INFO LOG SERVB channel.")

(defvar *sap-servb-rfc-error-log-channel-max-size* *default-sap-servb-channel-max-size*
  "The maximum size for for the SAP RFC ERROR LOG SERVB channel.")

;;; - OUTBOUND MSG CHANNEL -

(defun sap-servb-outbound-channel ()
  (bt:with-recursive-lock-held (*lock-sap-servb-outbound-channel*)
    (tb.core:global-context-value :sap-servb-outbound-channel)))

(defun (setf sap-servb-outbound-channel) (channel)
  (bt:with-recursive-lock-held (*lock-sap-servb-outbound-channel*)
    (setf (tb.core:global-context-value :sap-servb-outbound-channel) channel)))

(defun init-sap-servb-outbound-channel (&key (channel nil) (max-size *sap-servb-outbound-channel-max-size*) (name +sap-servb-outbound-channel-name+))
  (setf (sap-servb-outbound-channel) (or channel
					 (thingbone.core:make-channel :max-size max-size :name name))))

(defun ensure-sap-servb-outbound-channel-initialized ()
  (if (not (sap-servb-outbound-channel))
      (init-sap-servb-outbound-channel)))

(defun sap-servb-outbound-channel-dispatcher ()
  (bt:with-recursive-lock-held (*lock-sap-servb-outbound-channel-dispatcher*)
    (tb.core:global-context-value :sap-servb-outbound-channel-dispatcher)))

(defun (setf sap-servb-outbound-channel-dispatcher) (dispatcher)
  (bt:with-recursive-lock-held (*lock-sap-servb-outbound-channel-dispatcher*)
    (setf (tb.core:global-context-value :sap-servb-outbound-channel-dispatcher) dispatcher)))

(defun stop-sap-servb-outbound-channel-dispatcher ()
  (let ((dispatcher (sap-servb-outbound-channel-dispatcher)))
    (if dispatcher
	(tb.core:stop-channel-dispatcher dispatcher))
    (setf (sap-servb-outbound-channel-dispatcher) nil)))

(defun start-sap-servb-outbound-channel-dispatcher ()
  (ensure-sap-servb-outbound-channel-initialized)
  (let ((dispatcher (make-instance 'tb.msgs:msg-dispatcher
				   :name +sap-servb-outbound-channel-dispatcher-name+
				   :channel (sap-servb-outbound-channel))))
    (setf (sap-servb-outbound-channel-dispatcher) dispatcher)
    (tb.core:run-in-thread dispatcher)))

(defun reset-sap-servb-outbound-channel ()
  (stop-sap-servb-outbound-channel-dispatcher)
  (init-sap-servb-outbound-channel)
  (start-sap-servb-outbound-channel-dispatcher))

;;; - INBOUND MSG CHANNEL -

(defun sap-servb-inbound-channel ()
  (bt:with-recursive-lock-held (*lock-sap-servb-inbound-channel*)
    (tb.core:global-context-value :sap-servb-inbound-channel)))

(defun (setf sap-servb-inbound-channel) (channel)
  (bt:with-recursive-lock-held (*lock-sap-servb-inbound-channel*)
    (setf (tb.core:global-context-value :sap-servb-inbound-channel) channel)))

(defun init-sap-servb-inbound-channel (&key (channel nil) (max-size *sap-servb-inbound-channel-max-size*) (name +sap-servb-inbound-channel-name+))
  (setf (sap-servb-inbound-channel) (or channel
					(thingbone.core:make-channel :max-size max-size :name name))))

(defun ensure-sap-servb-inbound-channel-initialized ()
  (if (not (sap-servb-inbound-channel))
      (init-sap-servb-inbound-channel)))

(defun sap-servb-inbound-channel-dispatcher ()
  (bt:with-recursive-lock-held (*lock-sap-servb-inbound-channel-dispatcher*)
    (tb.core:global-context-value :sap-servb-inbound-channel-dispatcher)))

(defun (setf sap-servb-inbound-channel-dispatcher) (dispatcher)
  (bt:with-recursive-lock-held (*lock-sap-servb-inbound-channel-dispatcher*)
    (setf (tb.core:global-context-value :sap-servb-inbound-channel-dispatcher) dispatcher)))

(defun stop-sap-servb-inbound-channel-dispatcher ()
  (let ((dispatcher (sap-servb-inbound-channel-dispatcher)))
    (if dispatcher
	(tb.core:stop-channel-dispatcher dispatcher))
    (setf (sap-servb-inbound-channel-dispatcher) nil)))

(defun start-sap-servb-inbound-channel-dispatcher ()
  (ensure-sap-servb-inbound-channel-initialized)
  (let ((dispatcher (make-instance 'tb.msgs:msg-dispatcher
				   :name +sap-servb-inbound-channel-dispatcher-name+
				   :channel (sap-servb-inbound-channel))))
    (setf (sap-servb-inbound-channel-dispatcher) dispatcher)
    (tb.core:run-in-thread dispatcher)))

(defun reset-sap-servb-inbound-channel ()
  (stop-sap-servb-inbound-channel-dispatcher)
  (init-sap-servb-inbound-channel)
  (start-sap-servb-inbound-channel-dispatcher))

;;; - SAP APPL LOG MSG CHANNEL -

(defun sap-servb-appl-log-channel ()
  (bt:with-recursive-lock-held (*lock-sap-servb-appl-log-channel*)
    (tb.core:global-context-value :sap-servb-appl-log-channel)))

(defun (setf sap-servb-appl-log-channel) (channel)
  (bt:with-recursive-lock-held (*lock-sap-servb-appl-log-channel*)
    (setf (tb.core:global-context-value :sap-servb-appl-log-channel) channel)))

(defun init-sap-servb-appl-log-channel (&key (channel nil) (max-size *sap-servb-appl-log-channel-max-size*) (name +sap-servb-appl-log-channel-name+))
  (setf (sap-servb-appl-log-channel) (or channel
					 (thingbone.core:make-channel :max-size max-size :name name))))

(defun ensure-sap-servb-appl-log-channel-initialized ()
  (if (not (sap-servb-appl-log-channel))
      (init-sap-servb-appl-log-channel)))

(defun sap-servb-appl-log-channel-dispatcher ()
  (bt:with-recursive-lock-held (*lock-sap-servb-appl-log-channel-dispatcher*)
    (tb.core:global-context-value :sap-servb-appl-log-channel-dispatcher)))

(defun (setf sap-servb-appl-log-channel-dispatcher) (dispatcher)
  (bt:with-recursive-lock-held (*lock-sap-servb-appl-log-channel-dispatcher*)
    (setf (tb.core:global-context-value :sap-servb-appl-log-channel-dispatcher) dispatcher)))

(defun stop-sap-servb-appl-log-channel-dispatcher ()
  (let ((dispatcher (sap-servb-appl-log-channel-dispatcher)))
    (if dispatcher
	(tb.core:stop-channel-dispatcher dispatcher))
    (setf (sap-servb-appl-log-channel-dispatcher) nil)))

(defun start-sap-servb-appl-log-channel-dispatcher ()
  (ensure-sap-servb-appl-log-channel-initialized)
  (let ((dispatcher (make-instance 'tb.msgs:msg-dispatcher
				   :name +sap-servb-appl-log-channel-dispatcher-name+
				   :channel (sap-servb-appl-log-channel))))
    (setf (sap-servb-appl-log-channel-dispatcher) dispatcher)
    (tb.core:run-in-thread dispatcher)))

(defun reset-sap-servb-appl-log-channel ()
  (stop-sap-servb-appl-log-channel-dispatcher)
  (init-sap-servb-appl-log-channel)
  (start-sap-servb-appl-log-channel-dispatcher))

;;; - RETURN-INFO MSG CHANNEL -

(defun sap-servb-return-info-log-channel ()
  (bt:with-recursive-lock-held (*lock-sap-servb-return-info-log-channel*)
    (tb.core:global-context-value :sap-servb-return-info-log-channel)))

(defun (setf sap-servb-return-info-log-channel) (channel)
  (bt:with-recursive-lock-held (*lock-sap-servb-return-info-log-channel*)
    (setf (tb.core:global-context-value :sap-servb-return-info-log-channel) channel)))

(defun init-sap-servb-return-info-log-channel (&key (channel nil) (max-size *sap-servb-return-info-log-channel-max-size*) (name +sap-servb-return-info-log-channel-name+))
  (setf (sap-servb-return-info-log-channel) (or channel
						(thingbone.core:make-channel :max-size max-size :name name))))

(defun ensure-sap-servb-return-info-log-channel-initialized ()
  (if (not (sap-servb-return-info-log-channel))
      (init-sap-servb-return-info-log-channel)))

(defun sap-servb-return-info-log-channel-dispatcher ()
  (bt:with-recursive-lock-held (*lock-sap-servb-return-info-log-channel-dispatcher*)
    (tb.core:global-context-value :sap-servb-return-info-log-channel-dispatcher)))

(defun (setf sap-servb-return-info-log-channel-dispatcher) (dispatcher)
  (bt:with-recursive-lock-held (*lock-sap-servb-return-info-log-channel-dispatcher*)
    (setf (tb.core:global-context-value :sap-servb-return-info-log-channel-dispatcher) dispatcher)))

(defun stop-sap-servb-return-info-log-channel-dispatcher ()
  (let ((dispatcher (sap-servb-return-info-log-channel-dispatcher)))
    (if dispatcher
	(tb.core:stop-channel-dispatcher dispatcher))
    (setf (sap-servb-return-info-log-channel-dispatcher) nil)))

(defun start-sap-servb-return-info-log-channel-dispatcher ()
  (ensure-sap-servb-return-info-log-channel-initialized)
  (let ((dispatcher (make-instance 'tb.msgs:msg-dispatcher
				   :name +sap-servb-return-info-log-channel-dispatcher-name+
				   :channel (sap-servb-return-info-log-channel))))
    (setf (sap-servb-return-info-log-channel-dispatcher) dispatcher)
    (tb.core:run-in-thread dispatcher)))

(defun reset-sap-servb-return-info-log-channel ()
  (stop-sap-servb-return-info-log-channel-dispatcher)
  (init-sap-servb-return-info-log-channel)
  (start-sap-servb-return-info-log-channel-dispatcher))

;;; - RFC ERROR MSG CHANNEL -

(defun sap-servb-rfc-error-log-channel ()
  (bt:with-recursive-lock-held (*lock-sap-servb-rfc-error-log-channel*)
    (tb.core:global-context-value :sap-servb-rfc-error-log-channel)))

(defun (setf sap-servb-rfc-error-log-channel) (channel)
  (bt:with-recursive-lock-held (*lock-sap-servb-rfc-error-log-channel*)
    (setf (tb.core:global-context-value :sap-servb-rfc-error-log-channel) channel)))

(defun init-sap-servb-rfc-error-log-channel (&key (channel nil) (max-size *sap-servb-rfc-error-log-channel-max-size*) (name +sap-servb-rfc-error-log-channel-name+))
  (setf (sap-servb-rfc-error-log-channel) (or channel
						(thingbone.core:make-channel :max-size max-size :name name))))

(defun ensure-sap-servb-rfc-error-log-channel-initialized ()
  (if (not (sap-servb-rfc-error-log-channel))
      (init-sap-servb-rfc-error-log-channel)))

(defun sap-servb-rfc-error-log-channel-dispatcher ()
  (bt:with-recursive-lock-held (*lock-sap-servb-rfc-error-log-channel-dispatcher*)
    (tb.core:global-context-value :sap-servb-rfc-error-log-channel-dispatcher)))

(defun (setf sap-servb-rfc-error-log-channel-dispatcher) (dispatcher)
  (bt:with-recursive-lock-held (*lock-sap-servb-rfc-error-log-channel-dispatcher*)
    (setf (tb.core:global-context-value :sap-servb-rfc-error-log-channel-dispatcher) dispatcher)))

(defun stop-sap-servb-rfc-error-log-channel-dispatcher ()
  (let ((dispatcher (sap-servb-rfc-error-log-channel-dispatcher)))
    (if dispatcher
	(tb.core:stop-channel-dispatcher dispatcher))
    (setf (sap-servb-rfc-error-log-channel-dispatcher) nil)))

(defun start-sap-servb-rfc-error-log-channel-dispatcher ()
  (ensure-sap-servb-rfc-error-log-channel-initialized)
  (let ((dispatcher (make-instance 'tb.msgs:msg-dispatcher
				   :name +sap-servb-rfc-error-log-channel-dispatcher-name+
				   :channel (sap-servb-rfc-error-log-channel))))
    (setf (sap-servb-rfc-error-log-channel-dispatcher) dispatcher)
    (tb.core:run-in-thread dispatcher)))

(defun reset-sap-servb-rfc-error-log-channel ()
  (stop-sap-servb-rfc-error-log-channel-dispatcher)
  (init-sap-servb-rfc-error-log-channel)
  (start-sap-servb-rfc-error-log-channel-dispatcher))
