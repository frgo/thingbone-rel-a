;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

;;; SAP-SERVB - SPECIFIC DATA STRUCTURES

;; CONVERSATION_ID	CHAR	36	Service Bus Conversation ID
;; IV_STATE	         CHAR	21	Status
;;
;; Gültige Werte für IV_STATE:		SUCCESSFULLY-SENT
;;                                      HTTP-CONNECTION-FAILURE
;;                                      SERVICE-FAILURE

(defconstant +SERVB-QOS-STATUS-RESPONSE-QOS-REQUIRED+      "REQUIRED")
(defconstant +SERVB-QOS-STATUS-RESPONSE-QOS-NOT-REQUIRED+  "NOT_REQUIRED")

(defclass servb-qos (servb-data)
  ((status-response-qos :accessor status-response-qos :initarg :status-response-qos :initform +SERVB-QOS-STATUS-RESPONSE-QOS-NOT-REQUIRED+)
   ))

(defmethod print-object ((servb-qos servb-qos) stream)
  (print-unreadable-object (servb-qos stream :identity t :type t)
    (format stream ":STATUS-RESPONSE-QOS ~S"
	    (status-response-qos servb-qos))))

(defmethod request-status-response-required-p ((servb-qos servb-qos))
  (string= (status-response-qos servb-qos) +SERVB-QOS-STATUS-RESPONSE-QOS-REQUIRED+))

(defmethod tb-extended-msg-to-servb-qos ((extended-msg thingbone.messages:extended-msg))
  (let* ((qos (qos extended-msg)))
    (make-instance 'servb-qos
		   :status-response-qos (if (tb.msgs:request-status-response-required-p qos)
					    +SERVB-QOS-STATUS-RESPONSE-QOS-REQUIRED+
					    +SERVB-QOS-STATUS-RESPONSE-QOS-NOT-REQUIRED+))))

(defun get-qos-structure-handle-from-container-handle (container-handle)
  (rfc-get-structure container-handle "QOS"))

(defun set-sap-qos-from-servb-qos (servb-qos qos-struct-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let ((status-response-qos (status-response-qos servb-qos)))
    (rfc-set-chars qos-struct-handle "STATUS_RESPONSE" status-response-qos (length status-response-qos) sap-uc-encoding)
    (values)))

(defun set-servb-qos-from-sap-qos (qos-struct-handle servb-qos &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (setf (status-response-qos servb-qos) (cl-strings:clean (rfc-get-chars qos-struct-handle "STATUS_RESPONSE" 128 sap-uc-encoding)))
  (log:debug "SET-SERVB-QOS-FROM-SAP-QOS: servb-qos = ~S" servb-qos)
  servb-qos)

(defun make-servb-qos-from-sap-container-handle (container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let ((servb-qos (make-instance 'servb-qos))
	(struct-handle (get-qos-structure-handle-from-container-handle container-handle)))
    (log:debug "MAKE-SERVB-QOS-FROM-SAP-CONTAINER-HANDLE: container-handle = ~S" container-handle)
    (if (not (cffi:null-pointer-p struct-handle))
	(set-servb-qos-from-sap-qos struct-handle servb-qos :sap-uc-encoding sap-uc-encoding ))))
