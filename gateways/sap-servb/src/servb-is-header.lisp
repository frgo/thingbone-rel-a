;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

;;; SAP-SERVB - SPECIFIC DATA STRUCTURES

;; IMPLEMENTATION NOTE: IS_HEADER STRUCTURE DEFINITION
;;
;; CONVERSATION_ID	ZSERVB_DE_CONVERSATION_ID	CHAR	36	0	Service Bus Conversation ID
;; ENCODING	        ZSERVB_DE_ENCODING	        CHAR	32	0	Service Bus Encoding
;; LANGUAGE	        ZSERVB_DE_LANGUAGE	        CHAR	16	0	Service Bus Language
;; PROTOCOL	        ZSERVB_DE_PROTOCOL	        CHAR	32	0	Service Bus Protocol
;; PROTOCOL_VERSION	ZSERVB_DE_PROTOCOL_VERSION	CHAR	3	0	Service Bus Protocol version
;; TIMESTAMP	        ZSERVB_DE_TIMESTAMP_UTC	        CHAR	64	0	Service Bus Timestamp UTC
;; APP_ID	        ZSERVB_DE_APP_ID	        CHAR	36	0	Service Bus App ID
;; APP_INSTANCE_ID	ZSERVB_DE_APP_INSTANCE_ID	CHAR	36	0	Service Bus App Instance ID
;; ZCOMMENT	        ZSERVB_DE_COMMENT	        CHAR	128	0	Service Bus Comment
;; O	                ZSERVB_DE_O	                CHAR	128	0	Service Bus Organisation
;; OU	                ZSERVB_DE_OU	                CHAR	128	0	Service Bus Organsation unit
;; SUPPORT	        ZSERVB_DE_SUPPORT	        CHAR	128	0	Service Bus Support
;; REPLY_WITH	        ZSERVB_DE_REPLY_WITH	        CHAR	36	0	Service Bus Reply code
;; REPLY_BY	        ZSERVB_DE_REPLY_BY	        CHAR	64	0	Service Bus Reply frist
;; PERFORMATIVE	        ZSERVB_DE_PERFORMATIVE	        CHAR	16	0	Service Bus Performative
;; TYPE	                ZSERVB_DE_TYPE	                CHAR	16	0	Service Bus Credential type
;; USER_ID	        ZSERVB_DE_USER_ID	        CHAR	36	0	Service Bus User id
;; PASSCODE	        ZSERVB_DE_PASSCODE	        CHAR	40	0	Service Bus Passcode
;; OPERATION	        ZSERVB_DE_OPERATION	        CHAR	16	0	Service Bus Operation
;; OBJECT_CLASS	        ZSERVB_DE_OBJECT_CLASS	        CHAR	64	0	Service Bus Object Class
;; QOS			<structure Type: ZSERVB_S_QUALITY_OF_SERVICE>
;;
;; ZSERVB_S_QUALITY_OF_SERVICE STRUCTURE DEFINITION
;;
;; STATUS_RESPONSE	                                CHAR	128	Status Response required (values: REQUIRED; NOT-REQUIRED)


(defclass servb-is-header (servb-data)
  ((conversation-id    :accessor conversation-id    :initarg :conversation-id    :initform nil)
   (encoding           :accessor encoding           :initarg :encoding           :initform nil)
   (language           :accessor language           :initarg :language           :initform nil)
   (protocol           :accessor protocol           :initarg :protocol           :initform nil)
   (protocol-version   :accessor protocol-version   :initarg :protocol-version   :initform nil)
   (timestamp          :accessor timestamp          :initarg :timestamp          :initform nil)
   (app-id             :accessor app-id             :initarg :app-id             :initform nil)
   (app-instance-id    :accessor app-instance-id    :initarg :app-instance-id    :initform nil)
   (comment            :accessor comment            :initarg :comment            :initform nil)
   (o                  :accessor o                  :initarg :o                  :initform nil)
   (ou                 :accessor ou                 :initarg :ou                 :initform nil)
   (support            :accessor support            :initarg :support            :initform nil)
   (reply-with         :accessor reply-with         :initarg :reply-with         :initform nil)
   (in-reply-to        :accessor in-reply-to        :initarg :in-reply-to        :initform nil)
   (reply-by           :accessor reply-by           :initarg :reply-by           :initform nil)
   (performative       :accessor performative       :initarg :performative       :initform nil)
   (credentials-type   :accessor credentials-type   :initarg :credentials-type   :initform nil)
   (requestor-user-id  :accessor requestor-user-id  :initarg :requestor-user-id  :initform nil)
   (requestor-passcode :accessor requestor-passcode :initarg :requestor-passcode :initform nil)
   (operation          :accessor operation          :initarg :operation          :initform nil)
   (object-class       :accessor object-class       :initarg :object-class       :initform nil)
   (qos                :accessor qos                :initarg :qos                :initform nil)
   ))

(defmethod print-object ((servb-is-header servb-is-header) stream)
  (print-unreadable-object (servb-is-header stream :identity t :type t)
    (with-slots (conversation-id
		 encoding
		 language
		 protocol
		 protocol-version
		 timestamp
		 app-id
		 app-instance-id
		 comment
		 o
		 ou
		 support
		 reply-with
		 in-reply-to
		 reply-by
		 performative
		 credentials-type
		 requestor-user-id
		 requestor-passcode
		 operation
		 object-class
		 qos) servb-is-header
      (format stream ":CONVERSATION-ID ~S :ENCODING ~S :LANGUAGE ~S :PROTOCOL ~S :PROTOCOL-VERSION ~S :TIMESTAMP ~S :APP-ID ~S :APP-INSTANCE-ID ~S :COMMENT ~S :O ~S :OU ~S :SUPPORT ~S :REPLY-WITH ~S :IN-REPLY-TO ~S :REPLY-BY ~S :PERFORMATIVE ~S :CREDENTIALS-TYPE ~S :REQUESTOR-USER-ID ~S :REQUESTOR-PASSCODE ~S :OPERATION ~S :OBJECT-CLASS ~S :QOS ~S"
	      conversation-id
	      encoding
	      language
	      protocol
	      protocol-version
	      timestamp
	      app-id
	      app-instance-id
	      comment
	      o
	      ou
	      support
	      reply-with
	      in-reply-to
	      reply-by
	      performative
	      credentials-type
	      requestor-user-id
	      requestor-passcode
	      operation
	      object-class
	      qos))))

(defmethod tb-extended-msg-to-servb-is-header ((extended-msg thingbone.messages:extended-msg))

  (with-slots (tb.msgs:conversation-id tb.msgs:encoding tb.msgs:language tb.msgs:protocol tb.msgs:protocol-version tb.msgs:timestamp tb.msgs:requestor-context tb.msgs:reply-with tb.msgs:reply-by tb.msgs:in-reply-to tb.msgs:performative) extended-msg

    (let* ((requestor-context (tb.msgs::requestor-context extended-msg))
	   (requestor-type (if requestor-context
			       (tb.msgs::kind requestor-context)))
	   (requestor-kind-is-user-id-passwd-p (if requestor-context
						   (tb.msgs:user-kind-msg-requestor-context-p requestor-context)))
	   (requestor-user-id (if requestor-kind-is-user-id-passwd-p
				  (tb.msgs:user-id requestor-context)))
	   (requestor-passcode (if requestor-kind-is-user-id-passwd-p
				   (tb.msgs::passcode requestor-context)))

	   (operation (operation extended-msg))
	   (object-class (object-class extended-msg)))

      (make-instance 'servb-is-header
		     :conversation-id    (tb.msgs:stringify-msg-value tb.msgs:conversation-id)
		     :encoding           (tb.msgs:stringify-msg-value tb.msgs:encoding)
		     :language           (tb.msgs:stringify-msg-value tb.msgs:language)
		     :protocol           (tb.msgs:stringify-msg-value tb.msgs:protocol)
		     :protocol-version   (tb.msgs:stringify-msg-value tb.msgs:protocol-version)
		     :timestamp          (tb.msgs:stringify-msg-value tb.msgs:timestamp)
		     :credentials-type   (tb.msgs:stringify-msg-value requestor-type)
		     :requestor-user-id  (tb.msgs:stringify-msg-value requestor-user-id)
		     :requestor-passcode (tb.msgs:stringify-msg-value requestor-passcode)
		     :reply-with         (tb.msgs:stringify-msg-value tb.msgs:reply-with)
		     :reply-by           (tb.msgs:stringify-msg-value tb.msgs:reply-by)
		     :in-reply-to        (tb.msgs:stringify-msg-value tb.msgs:in-reply-to)
		     :performative       (tb.msgs:stringify-msg-value tb.msgs:performative)
		     :requestor-type     (tb.msgs:stringify-msg-value requestor-type)
		     :requestor-user-id  (tb.msgs:stringify-msg-value requestor-user-id)
		     :requestor-passcode (tb.msgs:stringify-msg-value requestor-passcode)
		     :operation          (tb.msgs:stringify-msg-value operation)
		     :object-class       (tb.msgs:stringify-msg-value object-class)))))

(defun get-is-header-structure-handle-from-function-handle (func-handle)
  (rfc-get-structure func-handle "IS_HEADER"))

(defun set-sap-is-header-from-servb-is-header (servb-is-header is-header-struct-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (with-slots (conversation-id
	       encoding
	       language
	       protocol
	       protocol-version
	       timestamp
	       app-id
	       app-instance-id
	       comment
	       o
	       ou
	       support
	       reply-with
	       reply-by
	       performative
	       credentials-type
	       requestor-user-id
	       requestor-passcode
	       operation
	       object-class) servb-is-header
    (rfc-set-chars is-header-struct-handle "CONVERSATION_ID"  conversation-id    (length conversation-id)    sap-uc-encoding)
    (rfc-set-chars is-header-struct-handle "ENCODING"         encoding           (length encoding)           sap-uc-encoding)
    (rfc-set-chars is-header-struct-handle "LANGUAGE"         language           (length language)           sap-uc-encoding)
    (rfc-set-chars is-header-struct-handle "PROTOCOL"         protocol           (length protocol)           sap-uc-encoding)
    (rfc-set-chars is-header-struct-handle "PROTOCOL_VERSION" protocol-version   (length protocol-version)   sap-uc-encoding)
    (rfc-set-chars is-header-struct-handle "TIMESTAMP"        timestamp          (length timestamp)          sap-uc-encoding)
    (rfc-set-chars is-header-struct-handle "APP_ID"           app-id             (length app-id)             sap-uc-encoding)
    (rfc-set-chars is-header-struct-handle "APP_INSTANCE_ID"  app-instance-id    (length app-instance-id)    sap-uc-encoding)
    (rfc-set-chars is-header-struct-handle "ZCOMMENT"         comment            (length comment)            sap-uc-encoding)
    (rfc-set-chars is-header-struct-handle "O"                o                  (length o)                  sap-uc-encoding)
    (rfc-set-chars is-header-struct-handle "OU"               ou                 (length ou)                 sap-uc-encoding)
    (rfc-set-chars is-header-struct-handle "SUPPORT"          support            (length support)            sap-uc-encoding)
    (rfc-set-chars is-header-struct-handle "REPLY_WITH"       reply-with         (length reply-with)         sap-uc-encoding)
    (rfc-set-chars is-header-struct-handle "REPLY_BY"         reply-by           (length reply-by)           sap-uc-encoding)
    (rfc-set-chars is-header-struct-handle "PERFORMATIVE"     performative       (length performative)       sap-uc-encoding)
    (rfc-set-chars is-header-struct-handle "TYPE"             credentials-type   (length credentials-type)   sap-uc-encoding)
    (rfc-set-chars is-header-struct-handle "USER_ID"          requestor-user-id  (length requestor-user-id)  sap-uc-encoding)
    (rfc-set-chars is-header-struct-handle "PASSCODE"         requestor-passcode (length requestor-passcode) sap-uc-encoding)
    (rfc-set-chars is-header-struct-handle "OPERATION"        operation          (length operation)          sap-uc-encoding)
    (rfc-set-chars is-header-struct-handle "OBJECT_CLASS"     object-class       (length object-class)       sap-uc-encoding)
    ;; FIXME: QOS is missing here!!! frgo, 2019-05-25
    (values)))

(defun set-servb-is-header-from-sap-is-header (is-header-struct-handle servb-is-header  &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (setf (conversation-id     servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "CONVERSATION_ID"    36 sap-uc-encoding)))
  (setf (encoding            servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "ENCODING"           32 sap-uc-encoding)))
  (setf (language            servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "LANGUAGE"           16 sap-uc-encoding)))
  (setf (protocol            servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "PROTOCOL"           32 sap-uc-encoding)))
  (setf (protocol-version    servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "PROTOCOL_VERSION"   16 sap-uc-encoding)))
  (setf (timestamp           servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "TIMESTAMP"          64 sap-uc-encoding)))
  (setf (app-id              servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "APP_ID"             36 sap-uc-encoding)))
  (setf (app-instance-id     servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "APP_INSTANCE_ID"    36 sap-uc-encoding)))
  (setf (comment             servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "ZCOMMENT"          128 sap-uc-encoding)))
  (setf (o                   servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "O"                 128 sap-uc-encoding)))
  (setf (ou                  servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "OU"                128 sap-uc-encoding)))
  (setf (support             servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "SUPPORT"           128 sap-uc-encoding)))
  (setf (reply-with          servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "REPLY_WITH"         36 sap-uc-encoding)))
  (setf (reply-by            servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "REPLY_BY"           64 sap-uc-encoding)))
  (setf (performative        servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "PERFORMATIVE"       16 sap-uc-encoding)))
  (setf (credentials-type    servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "TYPE"               16 sap-uc-encoding)))
  (setf (requestor-user-id   servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "USER_ID"            36 sap-uc-encoding)))
  (setf (requestor-passcode  servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "PASSCODE"           40 sap-uc-encoding)))
  (setf (operation           servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "OPERATION"          16 sap-uc-encoding)))
  (setf (object-class        servb-is-header) (cl-strings:clean (rfc-get-chars is-header-struct-handle "OBJECT_CLASS"       64 sap-uc-encoding)))
  (setf (qos                 servb-is-header) (make-servb-qos-from-sap-container-handle is-header-struct-handle :sap-uc-encoding sap-uc-encoding))
  (log:debug "SET-SERVB-IS-HEADER-FROM-SAP-IS-HEADER - ~S" servb-is-header)
  servb-is-header)

(defun make-servb-is-header-from-sap-container-handle (container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let ((servb-is-header (make-instance 'servb-is-header))
	(struct-handle (rfc-get-structure container-handle "IS_HEADER")))
    (if (not (cffi:null-pointer-p struct-handle))
	(set-servb-is-header-from-sap-is-header struct-handle servb-is-header :sap-uc-encoding sap-uc-encoding ))))
