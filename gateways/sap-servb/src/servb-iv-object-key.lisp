;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

;;; SAP-SERVB - SPECIFIC DATA STRUCTURES

;; IMPLEMENTATION NOTE: IV_OBJECT_KEY DEFINITION
;;
;; IV_OBJECT_KEY	STRING

(eval-when (:compile-toplevel :execute :load-toplevel)
  (defconstant +default-servb-iv-object-key-data-string-length+ (* 1 1024))
  (defparameter *servb-iv-object-key-data-string-length* +default-servb-iv-object-key-data-string-length+))

(defclass servb-iv-object-key (servb-data)
  ((data :accessor data :initarg :data :initform nil)))

(defmethod print-object ((servb-iv-object-key servb-iv-object-key) stream)
  (print-unreadable-object (servb-iv-object-key stream :identity t :type t)
    (format stream ":DATA ~S" (data servb-iv-object-key))))

(defun set-sap-iv-object-key-from-servb-iv-object-key (servb-iv-object-key container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let ((data (data servb-iv-object-key)))
    (rfc-set-string container-handle "IV_OBJECT_KEY" data (length data) sap-uc-encoding)
    (values)))

(defun set-servb-iv-object-key-from-sap-iv-object-key (servb-iv-object-key container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (setf (data servb-iv-object-key) (rfc-get-string container-handle "IV_OBJECT_KEY" *servb-iv-object-key-data-string-length* sap-uc-encoding))
  (log:debug "SET-SERVB-IV-OBJECT-KEY-FROM-SAP-IV-OBJECT-KEY - ~S" servb-iv-object-key)
  servb-iv-object-key)

(defun make-servb-iv-object-key-from-sap-container-handle (container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (if (not (cffi:null-pointer-p container-handle))
      (let ((servb-iv-object-key (make-instance 'servb-iv-object-key)))
	(set-servb-iv-object-key-from-sap-iv-object-key servb-iv-object-key container-handle :sap-uc-encoding sap-uc-encoding)
	servb-iv-object-key)))
