;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

;;; SAP-SERVB - SPECIFIC DATA STRUCTURES

(defparameter *servb-server-function-registry* (make-hash-table :size 1024 :test 'string=))

(defclass servb-server-function ()
  ((remote-fm-name       :accessor remote-fm-name       :initarg :remote-fm-name       :initform (error "REMOTE-FM-NAME required!"))
   (lisp-fn              :accessor lisp-fn              :initarg :lisp-fn              :initform (error "LISP-FN required!"))
   (domain               :accessor domain               :initarg :domain               :initform (error "DOMAIN required!"))
   (callback-foreign-ptr :accessor callback-foreign-ptr :initarg :callback-foreign-ptr :initform (cffi:null-pointer))
   (status               :accessor status               :initarg :status               :initform :UN-INSTALLED)
   (rfc-func-handle      :accessor rfc-func-handle      :initarg :rfc-func-handle      :initform (cffi:null-pointer))
   ))

(defmethod print-object ((servb-server-function servb-server-function) stream)
  (print-unreadable-object (servb-server-function stream :identity t :type t)
    (with-slots (remote-fm-name lisp-fn domain callback-name callback-foreign-ptr status rfc-func-handle) servb-server-function
      (format stream "~A :STATUS ~S :LISP-FN ~S :DOMAIN ~S :CALLBACK-FOREIGN-PTR ~S :RFC-FUNC-HANDLE ~S" remote-fm-name status lisp-fn domain callback-foreign-ptr rfc-func-handle))))

(defmethod reset ((servb-server-function servb-server-function))
  (if (not (cffi:null-pointer-p (rfc-func-handle servb-server-function)))
      (progn
	(ignore-errors
	  (rfc-destroy-function (rfc-func-handle servb-server-function)))
	(setf (rfc-func-handle servb-server-function) (cffi:null-pointer))))
  (setf (status servb-server-function) :UN-INSTALLED))

(defmethod install ((servb-server-function servb-server-function) client-servb-rfc-connection)
  (log:trace "servb-server-function::install -> ENTRY.")
  (ensure-connection client-servb-rfc-connection)
  (log:info "About to install  server function ~A (Lisp function ~A)."
	    (remote-fm-name servb-server-function) (lisp-fn servb-server-function))
  (rfc-install-server-function nil
			       (rfc-get-function-desc (connection-handle client-servb-rfc-connection) (remote-fm-name servb-server-function))
			       (callback-foreign-ptr servb-server-function))
  (log:info "Installed server function ~A (Lisp function ~A)."
	    (remote-fm-name servb-server-function) (lisp-fn servb-server-function))
  (setf (status servb-server-function) :INSTALLED)
  (log:trace "servb-server-function::install -> EXIT.")
  servb-server-function)

(defun print-servb-server-function-registry (stream)
  (format stream "<SERVB-SERVER-FUNCTION-REGISTRY (~S ENTRIES)" (hash-table-count *servb-server-function-registry*))
  (maphash #'(lambda (key value)
	       (format stream "~%:KEY ~S :VALUE ~S " key value)) *servb-server-function-registry*)
  (format stream ">~&"))

(defun reset-servb-server-function-registry ()
  (clrhash *servb-server-function-registry*))

(defun register-servb-server-function-in-registry (servb-server-function)
  (check-type servb-server-function servb-server-function)
  (setf (gethash (remote-fm-name servb-server-function) *servb-server-function-registry*) servb-server-function))

(defun count-servb-server-functions-by-domain (domain)
  (loop
     with counter = 0
     for value being the hash-values of *servb-server-function-registry*
     using (hash-key key)
     when (eql (domain value) domain)
     do
       (incf counter)
     finally (return counter)))

(defun get-servb-server-function-by-remote-fm-name (remote-fm-name)
  (gethash remote-fm-name *servb-server-function-registry*))

(defun get-servb-server-functions-by-domain (domain)
  (loop
     for value being the hash-values of *servb-server-function-registry*
     using (hash-key key)
     when (eql (domain value) domain)
     collect value))

(defmacro define-servb-server-function (remote-fm-name domain lisp-fn)
  `(progn
     ;;(check-type ,domain :keyword) -- FIXME: -> Implement keyword-p
     (cffi:defcallback ,remote-fm-name rfc-rc
	 ((connection-handle rfc-connection-handle)
	  (func-handle rfc-connection-handle)
	  (error-info-ptr (:pointer (:struct rfc-error-info))))
       (funcall ,lisp-fn connection-handle func-handle error-info-ptr))
     (let* ((remote-fm-name-str (string-upcase (princ-to-string ',remote-fm-name)))
	    (servb-server-function (make-instance 'servb-server-function
						  :remote-fm-name remote-fm-name-str
						  :domain ,domain
						  :callback-foreign-ptr (cffi:get-callback ',remote-fm-name)
						  :lisp-fn ,lisp-fn)))
       (register-servb-server-function-in-registry servb-server-function)
       servb-server-function)))

(defun install-servb-server-functions-for-domain (domain client-servb-rfc-connection)
  (loop
     for servb-server-function in (get-servb-server-functions-by-domain domain)
     do
       (install servb-server-function client-servb-rfc-connection)))

(defun reset-servb-server-functions-for-domain (domain)
  (loop
     for servb-server-function in (get-servb-server-functions-by-domain domain)
     do
       (reset servb-server-function)))
