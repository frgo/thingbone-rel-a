;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

(defun z-servb-rfc-ping (connection-handle)

  ;; "Z_SERVB_PING"

  (let* ((func-handle (rfc-function connection-handle "Z_SERVB_RFC_PING"))
	 (rc (rfc-invoke connection-handle func-handle))
	 (servb-return-info (get-servb-return-info-from-function-handle func-handle)))
    (setf (rfc-return-code servb-return-info) rc)
    (log:debug "Z-SERVB-RFC-PING completed with return code ~S. Results: ~S."
	       rc servb-return-info)
    (values nil servb-return-info)))

(defun sap-servb-rfc-ping (servb-sap-rfc-connection)
  (check-type servb-sap-rfc-connection servb-sap-rfc-connection)
  (ensure-connection servb-sap-rfc-connection)
  (z-servb-rfc-ping (connection-handle servb-sap-rfc-connection)))
