;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

(defun %do-outbound-server-fm (func-handle)

  (let* ((servb-outbound-msg (make-instance 'servb-outbound-msg))
	 (is-header          (make-servb-is-header-from-sap-container-handle     func-handle))
	 (iv-data            (make-servb-iv-data-from-sap-container-handle       func-handle))
	 (iv-object          (make-servb-iv-object-from-sap-container-handle     func-handle))
	 (iv-object-key      (make-servb-iv-object-key-from-sap-container-handle func-handle))
	 (iv-event           (make-servb-iv-event-from-sap-container-handle      func-handle))
	 (tb-extended-msg    nil))

    (setf (is-header     servb-outbound-msg) is-header)
    (setf (iv-data       servb-outbound-msg) iv-data)
    (setf (iv-object     servb-outbound-msg) iv-object)
    (setf (iv-object-key servb-outbound-msg) iv-object-key)
    (setf (iv-event      servb-outbound-msg) iv-event)

    (setq tb-extended-msg (to-tb-extended-msg servb-outbound-msg))

    (log:info "Z-SERV-OUTBOUND-SERVER-FM: Received from SAP: ~S." servb-outbound-msg)

    (tb.core:send (sap-servb-outbound-channel) tb-extended-msg)
    )

  (values))

;;; das ist die USER FUNCTION !!
(defun z-servb-outbound-server-fm (connection-handle func-handle error-info-ptr)

  (declare (ignore connection-handle error-info-ptr))

  (log:trace "Z-SERVB-OUTBOUND-SERVER-FM: Entry.")

  (let ((rc :rfc-ok)
	(condition nil))

    (handler-case (%do-outbound-server-fm func-handle)
      (error (caught-condition)
	(log:error "Z-SERVB-OUTBOUND-SERVER-FM *** Caught error ~S: ~A !"
		   caught-condition
		   (format nil (slot-value condition 'format-control) (slot-value condition 'format-arguments)))
	(setq condition caught-condition)
	(setq rc :rfc-external-failure))
      (:no-error ()
	(values)))

    (ecase rc
      (:rfc-ok
       (set-sap-servb-es-servb-return-from-params-in-func func-handle
							  "Z-SERVB-OUTBOUND-SERVER-FM"
							  :rfc-ok
							  log4cl:+log-level-info+
							  "00000000"
							  "TBGS000S"
							  "SUCCESS."))
      (:rfc-abap-runtime-failure
       (set-sap-servb-es-servb-return-from-params-in-func func-handle
							  "Z-SERVB-OUTBOUND-SERVER-FM"
							  :rfc-abap-runtime-failure
							  log4cl:+log-level-error+
							  "00000001"
							  "TBGS001E"
							  "ERROR: RUNTIME FAILURE!"))
      (:rfc-abap-exception
       (set-sap-servb-es-servb-return-from-params-in-func func-handle
							  "Z-SERVB-OUTBOUND-SERVER-FM"
							  :rfc-abap-exception
							  log4cl:+log-level-error+
							  "00000002"
							  "TBGS001E"
							  "ERROR: ABAP EXCEPTION !"))
      (:rfc-external-failure
       (set-sap-servb-es-servb-return-from-params-in-func func-handle
							  "Z-SERVB-OUTBOUND-SERVER-FM"
							  :rfc-external-failure
							  log4cl:+log-level-error+
							  "00000003"
							  "TBGS001E"
							  "ERROR: EXTERNAL FAILURE!"))
      (otherwise
       (log:error "Z-SERVB-OUTBOUND-SERVER-FM: RFC Error ~S." rc)
       (set-sap-servb-es-servb-return-from-params-in-func func-handle
							  "Z-SERVB-OUTBOUND-SERVER-FM"
							  :rfc-external-failure
							  log4cl:+log-level-error+
							  "00000001"
							  "TBGS002E"
							  "ERROR: UNKNOWN ERROR!"))
      )

    (log:trace "Z-SERVB-OUTBOUND-SERVER-FM: Exit (rc = ~S)." rc)
    (cffi:foreign-enum-value 'rfc-rc :rfc-ok)))

(define-servb-server-function Z_SERVB_OUTBOUND_SERVER_FM :servb-outbound 'z-servb-outbound-server-fm)
