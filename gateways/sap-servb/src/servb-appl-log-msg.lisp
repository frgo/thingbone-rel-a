;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

;;; SAP-SERVB - SPECIFIC DATA STRUCTURES

;; IMPLEMENTATION NOTE: ZBAL_S_HEADER_INFORMATION STRUCTURE DEFINITION
;;
;; EXTNUMBER	CHAR	100	Anwendungs-Log: Externe Identifikation
;; OBJECT	CHAR	20	Anwendungs-Log: Objektname (Applikationskürzel)
;; SUBOBJECT	CHAR	20	Anwendungs-Log: Unterobjekt
;; ALDATE	DATS	8	Anwendungs-Log: Datum
;; ALTIME	TIMS	6	Anwendungs-Log: Uhrzeit
;; ALUSER	CHAR	12	Anwendungs-Log: Benutzername
;; ALTCODE	CHAR	20	Anwendungs-Log: Transaktionscode
;; ALPROG	CHAR	40	Anwendungs-Log: Programmname
;; ALMODE	CHAR	1	Anwendungs-Log: Betriebsmodus (Batch,Batch-Input,Dialog)


(defclass servb-bal-s-header-information (servb-data)
  ((extnumber :accessor extnumber :initarg :extnumber :initform nil)
   (object    :accessor object    :initarg :object    :initform nil)
   (subobject :accessor subobject :initarg :subobject :initform nil)
   (aldate    :accessor aldate    :initarg :aldate    :initform nil)
   (altime    :accessor altime    :initarg :altime    :initform nil)
   (aluser    :accessor aluser    :initarg :aluser    :initform nil)
   (altcode   :accessor altcode   :initarg :altcode   :initform nil)
   (alprog    :accessor alprog    :initarg :alprog    :initform nil)
   (almode    :accessor almode    :initarg :almode    :initform nil)
   ))

(defmethod print-object ((servb-bal-s-header-information servb-bal-s-header-information) stream)
  (print-unreadable-object (servb-bal-s-header-information stream :identity t :type t)
    (with-slots (extnumber object subobject aldate altime aluser altcode alprog almode) servb-bal-s-header-information
      (format stream ":EXTNUMBER ~S :OBJECT ~S :SUBOBJECT ~S :ALDATE ~S :ALTIME ~S :ALUSER ~S :ALTCODE ~S :ALPROG ~S :ALMODE ~S"
	      extnumber object subobject aldate altime aluser altcode alprog almode))))


(defun get-bal-s-header-information-structure-handle-from-function-handle (func-handle)
  (rfc-get-structure func-handle "IS_HEADER"))

(defun get-it-messages-table-handle-from-function-handle (func-handle)
  (rfc-get-table func-handle "IT_MESSAGES"))

(defun set-servb-bal-s-header-information-from-sap-is-header (is-header-struct-handle servb-bal-s-header-information &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (setf (extnumber servb-bal-s-header-information) (cl-strings:clean (rfc-get-chars is-header-struct-handle "EXTNUMBER" 100 sap-uc-encoding)))
  (setf (object    servb-bal-s-header-information) (cl-strings:clean (rfc-get-chars is-header-struct-handle "OBJECT"     20 sap-uc-encoding)))
  (setf (subobject servb-bal-s-header-information) (cl-strings:clean (rfc-get-chars is-header-struct-handle "SUBOBJECT"  20 sap-uc-encoding)))
  (setf (aldate    servb-bal-s-header-information) (cl-strings:clean (rfc-get-chars is-header-struct-handle "ALDATE"      8 sap-uc-encoding)))
  (setf (altime    servb-bal-s-header-information) (cl-strings:clean (rfc-get-chars is-header-struct-handle "ALTIME"      6 sap-uc-encoding)))
  (setf (aluser    servb-bal-s-header-information) (cl-strings:clean (rfc-get-chars is-header-struct-handle "ALUSER"     12 sap-uc-encoding)))
  (setf (altcode   servb-bal-s-header-information) (cl-strings:clean (rfc-get-chars is-header-struct-handle "ALTCODE"    20 sap-uc-encoding)))
  (setf (alprog    servb-bal-s-header-information) (cl-strings:clean (rfc-get-chars is-header-struct-handle "ALPROG"     40 sap-uc-encoding)))
  (setf (almode    servb-bal-s-header-information) (cl-strings:clean (rfc-get-chars is-header-struct-handle "ALMODE"      1 sap-uc-encoding)))
  servb-bal-s-header-information)

(defun make-servb-bal-s-header-information-from-sap-container-handle (container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let ((servb-bal-s-header-information (make-instance 'servb-bal-s-header-information))
	(struct-handle (rfc-get-structure container-handle "IS_HEADER")))
    (if (not (cffi:null-pointer-p struct-handle))
	(set-servb-bal-s-header-information-from-sap-is-header struct-handle servb-bal-s-header-information :sap-uc-encoding sap-uc-encoding ))))

(defclass tb-sap-servb-appl-log-msg (tb.msgs:msg)
  ((servb-bal-s-header-information :accessor servb-bal-s-header-information :initarg :servb-bal-s-header-information :initform nil)
   (bapiret2                       :accessor bapiret2                       :initarg :bapiret2                       :initform nil)))

(defun make-tb-sap-servb-appl-log-msg (servb-bal-s-header-information bapiret2)
  (make-instance 'tb-sap-servb-appl-log-msg
		 :servb-bal-s-header-information servb-bal-s-header-information
		 :bapiret2 bapiret2))

(defmethod format-msg-for-slack ((tb-sap-servb-appl-log-msg tb-sap-servb-appl-log-msg))
  (with-slots (extnumber object subobject aldate altime aluser altcode alprog almode) (servb-bal-s-header-information tb-sap-servb-appl-log-msg)
    (with-slots (message-type message-id message-number message system) (bapiret2 tb-sap-servb-appl-log-msg)
      (format nil "*SAP SERVB: SAP APPL LOG ENTRY*~%~A~%~%*Object*: ~20A *Sub Object*: ~20A~%*User*: ~12A *Tcode*: ~20A *Prog*: ~40A~%*ID*: ~20A *NR*: ~3A~%~%~A"
	      extnumber
	      object
	      subobject
	      aluser
	      altcode
	      alprog
	      message-id
	      message-number
	      message))))

(defmethod tb.msgs:dispatch ((tb-sap-servb-appl-log-msg tb-sap-servb-appl-log-msg) &key &allow-other-keys)
  (let ((message-type (message-type (bapiret2 tb-sap-servb-appl-log-msg)))
	(msg (format-msg-for-slack tb-sap-servb-appl-log-msg)))
    (cond
      ((or (string= message-type "S")
	   (string= message-type "I"))
       (log:info "~A" msg))
      ((string= message-type "W")
       (log:warn "~A" msg))
      ((string= message-type "E")
       (log:error "~A" msg))
      ((string= message-type "A")
       (log:fatal "~A" msg))
      (t (log:fatal "Cannot dispatch TB-SAP-SERVB-APPL-LOG-MSG: Cannot determine log function from message type ~S. Message was:~%~%~A" message-type msg))))
  (values))

(defun test-tb-sap-servb-appl-log-msg-dispatch ()
  (let* ((bapiret2 (make-instance 'bapiret2 :message "Hallo Du da!" :message-type "X"))
	 (servb-bal-s-header-information (make-instance 'servb-bal-s-header-information
							:almode "D"
							:aluser "LUEDERH"
							:aldate "20190625"
							:altime "101506"
							:alprog "SAPLZ_SERVB_IOBOUND_RFC"
							:object "ZPLMFW"
							:subobject "BASIS"
							:extnumber "Outbound Event: 20190625081506.984523"))
	 (appl-log-msg (make-tb-sap-servb-appl-log-msg servb-bal-s-header-information bapiret2)))
    (tb.msgs:dispatch appl-log-msg)))
