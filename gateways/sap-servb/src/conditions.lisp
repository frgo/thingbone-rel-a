;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

;;; SAP RFC ERRORS

(defconstant +TB-CONDITION-LEVEL-SAP-SERVB-ERROR-RFC-MISC-FAULURE+                    tb.core:+TB-MSG-LEVEL-ERROR+)
(defconstant +TB-CONDITION-MESSAGE-NR-SAP-SERVB-ERROR-RFC-MISC-FAILURE+               "TGWS101E")
(defconstant +TB-CONDITION-REASON-CODE-SAP-SERVB-ERROR-RFC-MISC-FAILURE+              "00000001")
(defconstant +TB-CONDITION-REASON-TEXT-SAP-SERVB-ERROR-RFC-MISC-FAILURE+              "An unspecified SAP RFC error occured.")

(defconstant +TB-CONDITION-LEVEL-SAP-SERVB-ERROR-RFC-COMMUNICATION-FAULURE+           tb.core:+TB-MSG-LEVEL-ERROR+)
(defconstant +TB-CONDITION-MESSAGE-NR-SAP-SERVB-ERROR-RFC-COMMUNICATION-FAILURE+      "TGWS101E")
(defconstant +TB-CONDITION-REASON-CODE-SAP-SERVB-ERROR-RFC-COMMUNICATION-FAILURE+     "00000002")
(defconstant +TB-CONDITION-REASON-TEXT-SAP-SERVB-ERROR-RFC-COMMUNICATION-FAILURE+     "An application-level SAP RFC communication failure between the ThingBone Gateway and SAP occured.")

(defconstant +TB-CONDITION-LEVEL-SAP-SERVB-ERROR-RFC-INVALID-PARAMETER+               tb.core:+TB-MSG-LEVEL-CRITICAL+)
(defconstant +TB-CONDITION-MESSAGE-NR-SAP-SERVB-ERROR-RFC-INVALID-PARAMETER+          "TGWS101E")
(defconstant +TB-CONDITION-REASON-CODE-SAP-SERVB-ERROR-RFC-INVALID-PARAMETER+         "00000003")
(defconstant +TB-CONDITION-REASON-TEXT-SAP-SERVB-ERROR-RFC-INVALID-PARAMETER+         "Invalid parameter or invalid number of parameters passed to an SAP RFC function module. Most likely, this is a bug in the ThingBone SAP Gateway module.")

(defconstant +TB-CONDITION-LEVEL-SAP-SERVB-ERROR-RFC-CONVERSION-FAILURE+              tb.core:+TB-MSG-LEVEL-ERROR+)
(defconstant +TB-CONDITION-MESSAGE-NR-SAP-SERVB-ERROR-RFC-CONVERSION-FAILURE+         "TGWS101E")
(defconstant +TB-CONDITION-REASON-CODE-SAP-SERVB-ERROR-RFC-CONVERSION-FAILURE+         "00000004")
(defconstant +TB-CONDITION-REASON-TEXT-SAP-SERVB-ERROR-RFC-CONVERSION-FAILURE+        "A value could not be converted to the required format to be passed to a SAP function module.")

;;; SERVB EXECUTION ERRORS ORIGINATING IN SAP

(defconstant +TB-CONDITION-LEVEL-SAP-SERVB-ERROR-FUNCTION-MODULE-EXEC-ERROR+          tb.core:+TB-MSG-LEVEL-ERROR+)
(defconstant +TB-CONDITION-MESSAGE-NR-SAP-SERVB-ERROR-FUNCTION-MODULE-EXEC-ERROR+     "TGWS201E")
(defconstant +TB-CONDITION-REASON-CODE-SAP-SERVB-ERROR-FUNCTION-MODULE-EXEC-ERROR+    "00000001")
(defconstant +TB-CONDITION-REASON-TEXT-SAP-SERVB-ERROR-FUNCTION-MODULE-EXEC-ERROR+    "While executing an SAP request an error occurred on application level.")

;;; SERVB CONFIG ERRORS

(defconstant +TB-CONDITION-LEVEL-SAP-SERVB-ERROR-MISSING-SAP-RFC-CLIENT-CONNECTION-PARAMETERS+          tb.core:+TB-MSG-LEVEL-CRITICAL+)
(defconstant +TB-CONDITION-MESSAGE-NR-SAP-SERVB-ERROR-MISSING-SAP-RFC-CLIENT-CONNECTION-PARAMETERS+     "TGWS301F")
(defconstant +TB-CONDITION-REASON-CODE-SAP-SERVB-ERROR-MISSING-SAP-RFC-CLIENT-CONNECTION-PARAMETERS+    "00000001")
(defconstant +TB-CONDITION-REASON-TEXT-SAP-SERVB-ERROR-MISSING-SAP-RFC-CLIENT-CONNECTION-PARAMETERS+    "Missing SAP RFC client connection configuration (params not found in global context).")

(defconstant +TB-CONDITION-LEVEL-SAP-SERVB-ERROR-MISSING-SAP-RFC-SERVER-CONNECTION-PARAMETERS+          tb.core:+TB-MSG-LEVEL-CRITICAL+)
(defconstant +TB-CONDITION-MESSAGE-NR-SAP-SERVB-ERROR-MISSING-SAP-RFC-SERVER-CONNECTION-PARAMETERS+     "TGWS302F")
(defconstant +TB-CONDITION-REASON-CODE-SAP-SERVB-ERROR-MISSING-SAP-RFC-SERVER-CONNECTION-PARAMETERS+    "00000001")
(defconstant +TB-CONDITION-REASON-TEXT-SAP-SERVB-ERROR-MISSING-SAP-RFC-SERVER-CONNECTION-PARAMETERS+    "Missing SAP RFC client connection configuration (params not found in global context).")


;;; CONDITION DEFINITIONS

(define-condition servb-error (tb.core:tb-error)
  ())

(define-condition servb-sap-rfc-error (servb-error)
  ()
  (:default-initargs
   :level-nr tb.core:+TB-MSG-LEVEL-ERROR+)
  (:report (lambda (c s)
	     (format s "ThingBone Condition: ~A"
		     (tb.core:message c)))))

(define-condition servb-sap-rfc-misc-failure (servb-sap-rfc-error)
  ()
  (:default-initargs
   :reason-code +TB-CONDITION-REASON-CODE-SAP-SERVB-ERROR-RFC-MISC-FAILURE+
    :reason-text +TB-CONDITION-REASON-TEXT-SAP-SERVB-ERROR-RFC-MISC-FAILURE+
    :message-nr +TB-CONDITION-MESSAGE-NR-SAP-SERVB-ERROR-RFC-MISC-FAILURE+))

(define-condition servb-sap-rfc-communication-failure (servb-sap-rfc-error)
  ()
  (:default-initargs
   :reason-code +TB-CONDITION-REASON-CODE-SAP-SERVB-ERROR-RFC-COMMUNICATION-FAILURE+
    :reason-text +TB-CONDITION-REASON-TEXT-SAP-SERVB-ERROR-RFC-COMMUNICATION-FAILURE+
    :message-nr +TB-CONDITION-MESSAGE-NR-SAP-SERVB-ERROR-RFC-COMMUNICATION-FAILURE+
    ))

(define-condition servb-sap-rfc-invalid-parameter (servb-sap-rfc-error)
  ((parameter-name  :accessor :parameter-name  :initarg :parameter-name  :initform nil)
   (parameter-value :accessor :parameter-value :initarg :parameter-value :initform nil))
  (:default-initargs
   :level-nr +TB-CONDITION-LEVEL-SAP-SERVB-ERROR-RFC-INVALID-PARAMETER+
    :reason-code +TB-CONDITION-REASON-CODE-SAP-SERVB-ERROR-RFC-INVALID-PARAMETER+
    :reason-text +TB-CONDITION-REASON-TEXT-SAP-SERVB-ERROR-RFC-INVALID-PARAMETER+
    :message-nr +TB-CONDITION-MESSAGE-NR-SAP-SERVB-ERROR-RFC-INVALID-PARAMETER+
    )
  (:report (lambda (c s)
	     (format s "ThingBone Condition: ~A, Parameter Name: ~S, Parameter Value: ~S."
		     (tb.core:message c)
		     (slot-value c 'parameter-name)
		     (slot-value c 'parameter-value)))))

(define-condition servb-missing-sap-rfc-client-connection-parameters (servb-sap-rfc-error)
  ()
  (:default-initargs
   :reason-code +TB-CONDITION-REASON-CODE-SAP-SERVB-ERROR-MISSING-SAP-RFC-CLIENT-CONNECTION-PARAMETERS+
    :reason-text +TB-CONDITION-REASON-TEXT-SAP-SERVB-ERROR-MISSING-SAP-RFC-CLIENT-CONNECTION-PARAMETERS+
    :message-nr +TB-CONDITION-MESSAGE-NR-SAP-SERVB-ERROR-MISSING-SAP-RFC-CLIENT-CONNECTION-PARAMETERS+
    ))

(define-condition servb-missing-sap-rfc-server-connection-parameters (servb-sap-rfc-error)
  ()
  (:default-initargs
   :reason-code +TB-CONDITION-REASON-CODE-SAP-SERVB-ERROR-MISSING-SAP-RFC-SERVER-CONNECTION-PARAMETERS+
    :reason-text +TB-CONDITION-REASON-TEXT-SAP-SERVB-ERROR-MISSING-SAP-RFC-SERVER-CONNECTION-PARAMETERS+
    :message-nr +TB-CONDITION-MESSAGE-NR-SAP-SERVB-ERROR-MISSING-SAP-RFC-SERVER-CONNECTION-PARAMETERS+
    ))
