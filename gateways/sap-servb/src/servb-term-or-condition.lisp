;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

(defclass servb-term-or-condition (servb-data)
  ((condition-key   :accessor condition-key   :initarg :condition-key  :initform nil)
   (condition-value :accessor condition-value :initarg :conditon-value :initform nil)))

(defmethod print-object ((self servb-term-or-condition) stream)
  (print-unreadable-object (self stream :identity t :type t)
    (format stream ":CONDITION-KEY ~S :CONDITION-VALUE ~S"
	    (slot-value self 'condition-key)
	    (slot-value self 'condition-value)
	    )))

(defmethod jonathan:%to-json ((servb-term-or-condition servb-term-or-condition))
  (jonathan:with-object
    (jonathan:write-key-value "condition-key"   (condition-key servb-term-or-condition))
    (jonathan:write-key-value "condition-value" (condition-value servb-term-or-condition))
    ))

(defclass servb-payment-terms (servb-term-or-condition)
  ()
  (:default-initargs
   :condition-key "payment-terms-code"))

(defmethod servb-to-sap ((servb-payment-terms servb-payment-terms) func-handle container-handle  &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (declare (ignore func-handle))
  (let ((value (condition-value servb-payment-terms)))
    (rfc-set-chars container-handle "PAYMENT_TERM" (servb-length 4 value) sap-uc-encoding))
  servb-payment-terms)

(defmethod sap-to-servb ((servb-payment-terms servb-payment-terms) func-handle container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (declare (ignore func-handle))
  (setf (condition-value servb-payment-terms) (cl-strings:clean (rfc-get-chars container-handle "PAYMENT_TERM" 4 sap-uc-encoding)))
  servb-payment-terms)

(defun make-servb-payment-terms-from-sap (func-handle container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let ((instance (make-instance 'servb-payment-terms)))
    (sap-to-servb instance func-handle container-handle sap-uc-encoding)))
