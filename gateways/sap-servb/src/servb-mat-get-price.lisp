;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

(defclass servb-mat-pricing-item (servb-data)
  ((site                   :accessor site                   :initarg :site                   :initform nil)
   (pos-nr                 :accessor pos-nr                 :initarg :pos-nr                 :initform nil)
   (item-nr                :accessor item-nr                :initarg :item-nr                :initform nil)
   (item-version           :accessor item-version           :initarg :item-version           :initform nil)
   (item-revision          :accessor item-revision          :initarg :item-revision          :initform nil)
   (value-stream-code      :accessor value-stream-code      :initarg :value-stream-code      :initform nil)
   (quantity               :accessor quantity               :initarg :quantity               :initform nil)
   (uom                    :accessor uom                    :initarg :uom                    :initform nil)
   (pricing-condition-type :accessor pricing-condition-type :initarg :pricing-condition-type :initform nil)
   (price                  :accessor price                  :initarg :price                  :initform nil)
   (sales-material-group   :accessor sales-material-group   :initarg :sales-material-group   :initform nil)))

(defmethod print-object ((servb-mat-pricing-item servb-mat-pricing-item) stream)
  (print-unreadable-object (servb-mat-pricing-item stream :identity t :type t)
    (format stream ":SITE ~S :POS-NR ~S :ITEM-NR ~S :ITEM-VERSION ~S :ITEM-REVISION ~S :VALUE-STREAM-CODE ~S :QUANTITY ~S :UOM ~S :PRICING-CONDITION-TYPE ~S :PRICE ~S :SALES-MATERIAL-GROUP ~S"
	    (slot-value servb-mat-pricing-item 'site)
	    (slot-value servb-mat-pricing-item 'pos-nr)
	    (slot-value servb-mat-pricing-item 'item-nr)
	    (slot-value servb-mat-pricing-item 'item-version)
	    (slot-value servb-mat-pricing-item 'item-revision)
	    (slot-value servb-mat-pricing-item 'value-stream-code)
	    (slot-value servb-mat-pricing-item 'quantity)
	    (slot-value servb-mat-pricing-item 'uom)
	    (slot-value servb-mat-pricing-item 'pricing-condition-type)
	    (slot-value servb-mat-pricing-item 'price)
	    (slot-value servb-mat-pricing-item 'sales-material-group)
	    )))

(defparameter *servb-mat-pricing-items-in-nr-array-elements* 32)

(defclass servb-mat-pricing-items-in (servb-data)
  ((data-context         :accessor data-context         :initarg :data-context         :initform nil)
   (mat-pricing-items    :accessor mat-pricing-items    :initarg :mat-pricing-items    :initform (make-array *servb-mat-pricing-items-in-nr-array-elements* :element-type 'servb-mat-pricing-item :adjustable t :fill-pointer 0))
   (order-type           :accessor order-type           :initarg :order-type           :initform nil)
   (sales-org            :accessor sales-org            :initarg :sales-org            :initform nil)
   (distribution-channel :accessor distribution-channel :initarg :distribution-channel :initform nil)
   (division             :accessor division             :initarg :division             :initform nil)
   (partner-role         :accessor partner-role         :initarg :partner-role         :initform nil)
   (business-partner-nr  :accessor business-partner-nr  :initarg :business-partner-nr  :initform nil)
   (price-date           :accessor price-date           :initarg :price-date           :initform nil)))

(defmethod nr-of-mat-pricing-items ((servb-mat-pricing-items-in servb-mat-pricing-items-in))
  (loop for index from 0 to (1- (length (mat-pricing-items servb-mat-pricing-items-in)))
     with nr-of-mat-pricing-items = 0
     do
       (if (aref (mat-pricing-items servb-mat-pricing-items-in) index)
	   (incf nr-of-mat-pricing-items))
     finally
       (return nr-of-mat-pricing-items)))

(defmethod print-object ((servb-mat-pricing-items-in servb-mat-pricing-items-in) stream)
  (print-unreadable-object (servb-mat-pricing-items-in stream :identity t :type t)
    (with-slots (data-context business-partner-nr order-type sales-org distribution-channel division partner-role price-date) servb-mat-pricing-items-in
      (format stream ":DATA-CONTEXT ~S :BUSINESS-PARTNER-NR ~S :ORDER-TYPE ~S :SALES-ORG ~S :DISTRIBUTION-CHANNEL ~S :DIVISION ~S :PARTNER-ROLE ~S :PRICE-DATE ~S (~S MAT PRICING ITEMS)"
	      data-context business-partner-nr order-type sales-org distribution-channel division partner-role price-date
	      (nr-of-mat-pricing-items servb-mat-pricing-items-in)))))

(defmethod add-mat-pricing-item ((servb-mat-pricing-items-in servb-mat-pricing-items-in) (servb-mat-pricing-item servb-mat-pricing-item))
  (vector-push-extend servb-mat-pricing-item (mat-pricing-items servb-mat-pricing-items-in)))

(defmethod nth-mat-pricing-item ((servb-mat-pricing-items-in servb-mat-pricing-items-in) nth)
  (aref (mat-pricing-items servb-mat-pricing-items-in) nth))


(defclass servb-mat-pricing-item-out (servb-data)
  ((site               :accessor site               :initarg :site               :initform (tb.core:tb-site))
   (pos-nr             :accessor pos-nr             :initarg :pos-nr             :initform nil)
   (item-nr            :accessor item-nr            :initarg :item-nr            :initform nil)
   (price              :accessor price              :initarg :price              :initform nil)
   (quantity           :accessor quantity           :initarg :quantity           :initform nil)
   (currency           :accessor currency           :initarg :currency           :initform nil)
   (pricing-conditions :accessor pricing-conditions :initarg :pricing-conditions :initform nil)))

(defmethod print-object ((servb-mat-pricing-item-out servb-mat-pricing-item-out) stream)
  (print-unreadable-object (servb-mat-pricing-item-out stream :identity t :type t)
    (format stream ":SITE ~A :POS-NR ~S :ITEM-NR ~S :QUANTITY ~S :CURRENCY ~S :PRICE ~S :PRICING-CONDITIONS ~S"
	    (site               servb-mat-pricing-item-out)
	    (pos-nr             servb-mat-pricing-item-out)
	    (item-nr            servb-mat-pricing-item-out)
	    (quantity           servb-mat-pricing-item-out)
	    (currency           servb-mat-pricing-item-out)
	    (price              servb-mat-pricing-item-out)
	    (pricing-conditions servb-mat-pricing-item-out))))

(defmethod jonathan:%to-json ((servb-mat-pricing-item-out servb-mat-pricing-item-out))
  (with-slots (site pos-nr item-nr quantity currency price pricing-conditions) servb-mat-pricing-item-out
    (jonathan:with-object
      (jonathan:write-key-value "site"     site)
      (jonathan:write-key-value "pos-nr"   pos-nr)
      (jonathan:write-key-value "item-nr"  item-nr)
      (jonathan:write-key-value "quantity" quantity)
      (jonathan:write-key-value "currency" currency)
      (jonathan:write-key-value "price"    price)
      (jonathan:write-key "pricing-conditions")
      (jonathan:write-value
       (jonathan:with-array
	 (loop for item-cond in (pricing-conditions servb-mat-pricing-item-out)
	    do
	      (jonathan:write-item item-cond)))))))

(defparameter *servb-mat-pricing-items-out-nr-array-elements* 32)

(defclass servb-mat-pricing-items-out (servb-data)
  ((data-context :accessor data-context :initarg :data-context :initform nil)
   (mat-pricing-items-out :accessor mat-pricing-items-out :initarg :mat-pricing-items-out :initform (make-array *servb-mat-pricing-items-out-nr-array-elements* :element-type 'servb-mat-pricing-item-out :adjustable t :fill-pointer 0))))

(defmethod nr-of-mat-pricing-items-out ((servb-mat-pricing-items-out servb-mat-pricing-items-out))
  (loop for index from 0 to (1- (length (mat-pricing-items-out servb-mat-pricing-items-out)))
     with nr-of-mat-pricing-items-out = 0
     do
       (if (aref (mat-pricing-items-out servb-mat-pricing-items-out) index)
	   (incf nr-of-mat-pricing-items-out))
     finally
       (return nr-of-mat-pricing-items-out)))

(defmethod print-object ((servb-mat-pricing-items-out servb-mat-pricing-items-out) stream)
  (print-unreadable-object (servb-mat-pricing-items-out stream :identity t :type t)
    (format stream ":DATA-CONTEXT ~S (~S MAT PRICING ITEMS OUT)"
	    (slot-value servb-mat-pricing-items-out 'data-context)
	    (nr-of-mat-pricing-items-out servb-mat-pricing-items-out))))

(defmethod %print-instance ((servb-mat-pricing-items-out servb-mat-pricing-items-out) stream)
  (loop for index from 0 to (1- (nr-of-mat-pricing-items-out servb-mat-pricing-items-out))
     do
       (format stream "~S" (aref (mat-pricing-items-out servb-mat-pricing-items-out) index))))

(defmethod add-mat-pricing-item-out ((servb-mat-pricing-items-out servb-mat-pricing-items-out) (servb-mat-pricing-item-out servb-mat-pricing-item-out))
  (vector-push-extend servb-mat-pricing-item-out (mat-pricing-items-out servb-mat-pricing-items-out)))

(defmethod nth-mat-pricing-item-out ((servb-mat-pricing-items-out servb-mat-pricing-items-out) nth)
  (aref (mat-pricing-items-out servb-mat-pricing-items-out) nth))

(defmethod jonathan:%to-json ((servb-mat-pricing-items-out servb-mat-pricing-items-out))
  (jonathan:with-array
    (jonathan:write-item (data-context servb-mat-pricing-items-out))
    (jonathan:write-item
     (jonathan:with-object
       (jonathan:write-key "items")
       (jonathan:write-value
	(jonathan:with-array
	  (loop for index from 0 to (1- (length (mat-pricing-items-out servb-mat-pricing-items-out)))
	     do
	       (let ((mat-pricing-item-out (nth-mat-pricing-item-out servb-mat-pricing-items-out index)))
		 (jonathan:write-item mat-pricing-item-out)))))))))


(defun set-tb-extended-msg-content-data-from-servb-mat-pricing-items-out (tb-extended-msg servb-mat-pricing-items-out)
  (check-type tb-extended-msg tb.msgs:extended-msg)
  (check-type servb-mat-pricing-items-out servb-mat-pricing-items-out)
  (let ((msg-data (make-instance 'tb.msgs:msg-data :kind :SERVB-MAT-PRICING-ITEMS-OUT :data servb-mat-pricing-items-out)))
    (setf (tb.msgs:data (tb.msgs:content tb-extended-msg)) msg-data))
  tb-extended-msg)

(defclass servb-mat-pricing-item-cond (servb-data)
  ((pos-nr                         :accessor pos-nr                         :initarg :pos-nr                         :initform nil)
   (condition-index-nr             :accessor condition-index-nr             :initarg :condition-index-nr             :initform nil)
   (condition-type                 :accessor condition-type                 :initarg :condition-type                 :initform nil)
   (condition-value                :accessor condition-value                :initarg :condition-value                :initform nil)
   (condition-currency             :accessor condition-currency             :initarg :condition-currency             :initform nil)
   (uom                            :accessor uom                            :initarg :uom                            :initform nil)
   (condition-price                :accessor condition-price                :initarg :condition-price                :initform nil)
   (condition-price-currency       :accessor condition-price-currency       :initarg :condition-price-currency       :initform nil)
   (condition-price-date           :accessor condition-price-date           :initarg :condition-price-date           :initform nil)
   (condition-description-language :accessor condition-description-language :initarg :condition-description-language :initform nil)
   (condition-description-text     :accessor condition-description-text     :initarg :condition-description-text     :initform nil)
   ))

(defmethod print-object ((servb-mat-pricing-item-cond servb-mat-pricing-item-cond) stream)
  (print-unreadable-object (servb-mat-pricing-item-cond stream :identity t :type t)
    (format stream ":POS-NR ~S :CONDITION-INDEX-NR ~S :CONDITION-TYPE ~S :CONDITION-VALUE ~S :CONDITION-CURRENCY ~S :UOM ~S :CONDITION-PRICE ~S :CONDITION-PRICE-CURRENCY ~S :CONDITION-PRICE-DATE ~S :CONDITION-DESCRIPTION-LANGUAGE ~S :CONDITION-DESCRIPTION-TEXT ~S"
	    (pos-nr                         servb-mat-pricing-item-cond)
	    (condition-index-nr             servb-mat-pricing-item-cond)
	    (condition-type                 servb-mat-pricing-item-cond)
	    (condition-value                servb-mat-pricing-item-cond)
	    (condition-currency             servb-mat-pricing-item-cond)
	    (uom                            servb-mat-pricing-item-cond)
	    (condition-price                servb-mat-pricing-item-cond)
	    (condition-price-currency       servb-mat-pricing-item-cond)
	    (condition-price-date           servb-mat-pricing-item-cond)
	    (condition-description-language servb-mat-pricing-item-cond)
	    (condition-description-text     servb-mat-pricing-item-cond)
	    )))

(defmethod jonathan:%to-json ((servb-mat-pricing-item-cond servb-mat-pricing-item-cond))
  (with-slots (condition-index-nr condition-type condition-value condition-currency uom condition-price condition-price-currency condition-price-date condition-description-language condition-description-text) servb-mat-pricing-item-cond
    (jonathan:with-object
      (jonathan:write-key-value "condition-index-nr"       condition-index-nr)
      (jonathan:write-key-value "condition-type"           condition-type)
      (jonathan:write-key "condition-type-description")
      (jonathan:write-value
       (jonathan:with-array
	 (jonathan:write-item
	  (jonathan:with-object
	    (jonathan:write-key-value "lang" condition-description-language)
	    (jonathan:write-key-value "text" condition-description-text)))))
      (jonathan:write-key-value "condition-value"          condition-value)
      (jonathan:write-key-value "condition-currency"       condition-currency)
      (jonathan:write-key-value "uom"                      (tb.core:map-value :THINGBONE :BW-INTEGRATION-LAYER nil nil uom))
      (jonathan:write-key-value "condition-price"          condition-price)
      (jonathan:write-key-value "condition-price-currency" condition-price-currency)
      (jonathan:write-key-value "condition-price-date"     condition-price-date))))

(defparameter *servb-mat-pricing-item-conds-nr-array-elements* 256)

(defclass servb-mat-pricing-item-conds-out (servb-data)
  ((mat-pricing-item-conds :accessor mat-pricing-item-conds :initarg :mat-pricing-item-conds :initform (make-array *servb-mat-pricing-item-conds-nr-array-elements* :element-type 'servb-mat-pricing-item-cond :adjustable t :fill-pointer 0))))

(defmethod print-object ((servb-mat-pricing-item-conds-out servb-mat-pricing-item-conds-out) stream)
  (print-unreadable-object (servb-mat-pricing-item-conds-out stream :identity t :type t)
    (format stream "(~S MAT PRICING ITEM CONDITIONS)"
	    (length servb-mat-pricing-item-conds-out))))

(defmethod add-mat-pricing-item-cond ((servb-mat-pricing-item-conds-out servb-mat-pricing-item-conds-out) (servb-mat-pricing-item-cond servb-mat-pricing-item-cond))
  (vector-push-extend servb-mat-pricing-item-cond (mat-pricing-item-conds servb-mat-pricing-item-conds-out)))

(defmethod nth-mat-pricing-item-cond ((servb-mat-pricing-item-conds-out servb-mat-pricing-item-conds-out) nth)
  (aref (mat-pricing-item-conds servb-mat-pricing-item-conds-out) nth))

;;; ---

(defun get-it-line-items-table-handle-from-function-handle (func-handle)
  (rfc-get-table func-handle "IT_LINE_ITEMS"))

(defun get-et-line-items-table-handle-from-function-handle (func-handle)
  (rfc-get-table func-handle "ET_LINE_ITEMS"))

(defun get-et-line-items-cond-table-handle-from-function-handle (func-handle)
  (rfc-get-table func-handle "ET_LINE_ITEMS_COND"))

(defun set-sap-it-line-items-from-servb-mat-pricing-items-in (servb-mat-pricing-items-in
							      it-line-items-table-handle
							      &key (sap-uc-encoding (rfc-sap-uc-encoding)))

  ;; Tabelle ZMAT_T_PRICING_ITEMS / Struktur ZMAT_S_PRICING_ITEMS
  ;;
  ;; Attribut	        Format	Länge	Dez.-Stellen	Beschreibung
  ;; ITM_NUMBER	        NUMC	6	0	        Verkaufsbelegposition
  ;; MATERIAL	        CHAR	18	0	        Materialnummer
  ;; MATERIAL_QTY	QUAN	13	3	        Zielmenge in Verkaufsmengeneinheit
  ;; MATERIAL_QU	UNIT	3	0	        Zielmengeneinheit
  ;; COND_TYPE	        CHAR	4	0	        Konditionsart
  ;; MATERIAL_PRICE	CURR	15	2	        Nettowert in Belegwährung

  ;; PREPARE TABLE

  (rfc-delete-all-rows it-line-items-table-handle)

  (let ((nr-items (nr-of-mat-pricing-items servb-mat-pricing-items-in)))
    (if (> nr-items 0)
	(progn
	  (rfc-append-new-rows it-line-items-table-handle nr-items)
	  (rfc-move-to-first-row it-line-items-table-handle)

	  ;; INSERT LINES INTO TABLE

	  (loop for index from 0 to (1- nr-items)
	     do
	       (rfc-move-to it-line-items-table-handle index)
	       (let ((row-handle (rfc-get-current-row it-line-items-table-handle))
		     (mat-line-item (aref (mat-pricing-items servb-mat-pricing-items-in) index)))

		 (log:debug "MAT-LINE-ITEM-IN = ~S" mat-line-item)
		 (if (not (cffi:null-pointer-p row-handle))
		     (with-slots (pos-nr item-nr quantity uom pricing-condition-type price sales-material-group) mat-line-item
		       (if pos-nr
			   (rfc-set-chars row-handle "ITM_NUMBER" pos-nr (length pos-nr) sap-uc-encoding))
		       (if item-nr
			   (rfc-set-chars row-handle "MATERIAL" item-nr (length item-nr) sap-uc-encoding))
		       (if quantity
			   (let ((value-str (etypecase quantity
					      (integer (format nil "~d" quantity))
					      (float   (format nil "~3$" quantity))
					      (string  quantity))))
			     (rfc-set-chars row-handle "MATERIAL_QTY" value-str (length value-str) sap-uc-encoding)))
		       (if uom
			   (let ((uom (tb.core:map-value :THINGBONE :SAP-SERVB nil nil uom)))
			     (rfc-set-chars row-handle "MATERIAL_QU" uom (length uom) sap-uc-encoding)))
		       (if pricing-condition-type
			   (rfc-set-chars row-handle "COND_TYPE" pricing-condition-type (length pricing-condition-type) sap-uc-encoding))
		       (if price
			   (let ((value-str (etypecase price
					      (integer (format nil "~2$" (coerce price 'float)))
					      (float   (format nil "~2$" price))
					      (string  price))))
			     (rfc-set-chars row-handle "MATERIAL_PRICE" value-str (length value-str) sap-uc-encoding))
			   (rfc-set-chars row-handle "MATERIAL_PRICE" "0.00" (length "0.00") sap-uc-encoding))
		       (if sales-material-group
			   (rfc-set-chars row-handle "SALES_MATERIAL_GROUP" sales-material-group (length sales-material-group) sap-uc-encoding))
		       )))))))
  (values))




(defun set-servb-mat-pricing-items-out-from-sap-et-line-items (servb-mat-pricing-items-out
							       et-line-items-table-handle
							       &key (sap-uc-encoding (rfc-sap-uc-encoding)))

  ;; Tabelle ZMAT_T_PRICING_ITEMS_OUT / Struktur ZMAT_S_PRICING_ITEMS_OUT
  ;;
  ;; Attribut	        Format	Länge	Dez.-Stellen	Beschreibung
  ;; ITM_NUMBER	        NUMC	6	0	        Verkaufsbelegposition
  ;; MATERIAL	        CHAR	18	0	        Materialnummer
  ;; MATERIAL_PRICE	DEC	23	4	        Nettowert
  ;; PRICE_CURRENCY	CHAR	3	0	        ISO Code Währung
  ;; REQ_QTY	        QUAN	15	3	        Kumulierte Auftragsmenge in VME

  (if (not (cffi:null-pointer-p et-line-items-table-handle))
      (progn
	(let ((nr-items (rfc-get-row-count et-line-items-table-handle)))
	  (log:info "SET-SERVB-MAT-PRICING-ITEMS-OUT-FROM-SAP-ET-LINE-ITEMS: Nr items = ~S." nr-items)
	  (loop for index from 0 to (1- nr-items)
	     do
	       (rfc-move-to et-line-items-table-handle index)
	       (let ((row-handle (rfc-get-current-row et-line-items-table-handle)))
		 (if (not (cffi:null-pointer-p row-handle))
		     (let* ((pos-nr   (cl-strings:clean (rfc-get-chars row-handle "ITM_NUMBER"      6 sap-uc-encoding)))
			    (item-nr  (cl-strings:clean (rfc-get-chars row-handle "MATERIAL"       18 sap-uc-encoding)))
			    (price    (cl-strings:clean (rfc-get-chars row-handle "MATERIAL_PRICE" 23 sap-uc-encoding)))
			    (currency (cl-strings:clean (rfc-get-chars row-handle "PRICE_CURRENCY"  3 sap-uc-encoding)))
			    (quantity (cl-strings:clean (rfc-get-chars row-handle "REQ_QTY"        15 sap-uc-encoding)))
			    (out      (make-instance 'servb-mat-pricing-item-out
						     :pos-nr pos-nr
						     :item-nr item-nr
						     :price (read-from-string price)
						     :currency currency
						     :quantity (read-from-string quantity))))
		       (log:info "Adding item ~S." out)
		       (add-mat-pricing-item-out servb-mat-pricing-items-out out))))
	       ))))
  servb-mat-pricing-items-out)

(defun set-servb-mat-pricing-item-conditions-out-from-sap-et-line-item-conds (servb-mat-pricing-item-conds-out
									      et-line-items-cond-table-handle
									      &key (sap-uc-encoding (rfc-sap-uc-encoding)))

  ;; Tabelle ZMAT_T_PRICING_ITEMS_OUT_COND / Struktur ZMAT_S_PRICING_ITEMS_OUT_COND
  ;;
  ;; ITM_NUMBER	NUMC	6	0	Verkaufsbelegposition
  ;; COND_TYPE	CHAR	4	0	Konditionsart
  ;; COND_VALUE	DEC	28	9	Konditionsbetrag
  ;; CURR_ISO	CHAR	3	0	ISO Code Währung
  ;; COND_UNIT	UNIT	3	0	Konditionsmengeneinheit
  ;; CONDVALUE	DEC	28	9	Konditionswert
  ;; CURR_ISO_2	CHAR	3	0	ISO Code Währung
  ;; CONPRICDAT	DATS	8	0	Konditions-Preisstellungsdatum
  ;; COND_DESC_LANG     3       0       Sprache der Konditionsbeschreibung
  ;; COND_DESC_TEXT    32       0       Konditonsbeschreibung

  (if (not (cffi:null-pointer-p et-line-items-cond-table-handle))
      (let ((nr-conds (rfc-get-row-count et-line-items-cond-table-handle)))
	(loop for index from 0 to (1- nr-conds)
	   do
	     (rfc-move-to et-line-items-cond-table-handle index)
	     (let ((row-handle (rfc-get-current-row et-line-items-cond-table-handle)))
	       (if (not (cffi:null-pointer-p row-handle))
		   (let* ((pos-nr                         (cl-strings:clean (rfc-get-chars row-handle "ITM_NUMBER"      6 sap-uc-encoding)))
			  (condition-type                 (cl-strings:clean (rfc-get-chars row-handle "COND_TYPE"       4 sap-uc-encoding)))
			  (condition-value                (cl-strings:clean (rfc-get-chars row-handle "COND_VALUE"     28 sap-uc-encoding)))
			  (condition-currency             (cl-strings:clean (rfc-get-chars row-handle "CURR_ISO"        3 sap-uc-encoding)))
			  (uom                            (cl-strings:clean (rfc-get-chars row-handle "COND_UNIT"       3 sap-uc-encoding)))
			  (condition-price                (cl-strings:clean (rfc-get-chars row-handle "CONDVALUE"      28 sap-uc-encoding)))
			  (condition-price-currency       (cl-strings:clean (rfc-get-chars row-handle "CURR_ISO_2"      3 sap-uc-encoding)))
			  (condition-price-date           (cl-strings:clean (rfc-get-chars row-handle "CONPRICDAT"      8 sap-uc-encoding)))
			  (condition-description-language (cl-strings:clean (rfc-get-chars row-handle "COND_DESC_LANG"  3 sap-uc-encoding)))
			  (condition-description-text     (cl-strings:clean (rfc-get-chars row-handle "COND_DESC_TEXT" 32 sap-uc-encoding)))
			  (condition-index-nr             (rfc-get-int row-handle "COND_IDX_NR"))
			  (cond (make-instance 'servb-mat-pricing-item-cond
					       :pos-nr pos-nr
					       :condition-type condition-type
					       :condition-value (read-from-string condition-value)
					       :condition-currency condition-currency
					       :uom (tb.core:map-value :SAP-SERVB :THINGBONE nil nil uom)
					       :condition-price (read-from-string condition-price)
					       :condition-price-currency condition-price-currency
					       :condition-price-date condition-price-date
					       :condition-description-language condition-description-language
					       :condition-description-text condition-description-text
					       :condition-index-nr condition-index-nr)))
		     (add-mat-pricing-item-cond servb-mat-pricing-item-conds-out cond))))
	     )))
  servb-mat-pricing-item-conds-out)

(defun set-pricing-conds-for-items (servb-mat-pricing-items-out servb-mat-pricing-item-conds-out)
  (let ((nr-item-pricing-infos (length (mat-pricing-items-out servb-mat-pricing-items-out))))
    (loop for item-index from 0 to (1- nr-item-pricing-infos)
       do
	 (let ((mat-pricing-item-out (nth-mat-pricing-item-out servb-mat-pricing-items-out item-index)))
	   (loop for cond-index from 0 to (1- (length  (mat-pricing-item-conds servb-mat-pricing-item-conds-out)))
	      do
		(let ((item-cond (nth-mat-pricing-item-cond servb-mat-pricing-item-conds-out cond-index)))
		  (if (string= (pos-nr mat-pricing-item-out) (pos-nr item-cond))
		      (push item-cond (pricing-conditions mat-pricing-item-out)))))))))

(defun z-mat-get-price (connection-handle servb-mat-pricing-items-in &key (sap-uc-encoding (rfc-sap-uc-encoding)))

  ;; FUNCTION Z_MAT_GET_PRICE .
  ;; *"----------------------------------------------------------------------
  ;; *"*"Lokale Schnittstelle:
  ;; *"  IMPORTING
  ;; *"     VALUE(IV_DOC_TYPE)     TYPE  AUART OPTIONAL
  ;; *"     VALUE(IV_SALES_ORG)    TYPE  VKORG OPTIONAL
  ;; *"     VALUE(IV_DISTR_CHAN)   TYPE  VTWEG OPTIONAL
  ;; *"     VALUE(IV_DIVISION)     TYPE  SPART OPTIONAL
  ;; *"     VALUE(IV_PARTNER_ROLE) TYPE  PARVW OPTIONAL
  ;; *"     VALUE(IV_CUSTOMER)     TYPE  KUNNR
  ;; *"     VALUE(IV_PRICE_DATE)   TYPE  PRSDT OPTIONAL
  ;; *"     VALUE(IS_LINE_ITEM)    TYPE  ZMAT_S_PRICING_ITEMS OPTIONAL
  ;; *"     VALUE(IT_LINE_ITEMS)   TYPE  ZMAT_T_PRICING_ITEMS OPTIONAL
  ;; *"  EXPORTING
  ;; *"     VALUE(ES_RETURN)          TYPE  BAPIRET2
  ;; *"     VALUE(ES_LINE_ITEM)       TYPE  ZMAT_S_PRICING_ITEMS_OUT
  ;; *"     VALUE(ET_LINE_ITEMS)      TYPE  ZMAT_T_PRICING_ITEMS_OUT
  ;; *"     VALUE(ET_LINE_ITEMS_COND) TYPE  ZMAT_T_PRICING_ITEMS_OUT_COND
  ;; *"----------------------------------------------------------------------

  (log:debug "Z-MAT-GET-PRICE: Executing request...")

  (let* ((func-handle                      (rfc-function connection-handle "Z_MAT_GET_PRICE"))
	 (it-line-items-table-handle       (get-it-line-items-table-handle-from-function-handle func-handle))
	 (et-line-items-table-handle       (get-et-line-items-table-handle-from-function-handle func-handle))
	 (et-line-items-cond-table-handle  (get-et-line-items-cond-table-handle-from-function-handle func-handle))
	 (servb-mat-pricing-item-conds-out (make-instance 'servb-mat-pricing-item-conds-out))
	 (servb-mat-pricing-items-out      (make-instance 'servb-mat-pricing-items-out)))

    (with-slots (business-partner-nr order-type sales-org distribution-channel division partner-role price-date) servb-mat-pricing-items-in

      (log:debug "In Z-MAT-GET-PRICE: order-type = ~S, sales-org = ~S, distribution-channel = ~S, division = ~S, partner-role = ~S, customer = ~S, price-date = ~S"
		 order-type sales-org distribution-channel division partner-role business-partner-nr  price-date)

      (if order-type
	  (rfc-set-chars func-handle "IV_DOC_TYPE"     order-type           (length order-type)           sap-uc-encoding))
      (if sales-org
	  (rfc-set-chars func-handle "IV_SALES_ORG"    sales-org            (length sales-org)            sap-uc-encoding))
      (if distribution-channel
	  (rfc-set-chars func-handle "IV_DISTR_CHAN"   distribution-channel (length distribution-channel) sap-uc-encoding))
      (if division
	  (rfc-set-chars func-handle "IV_DIVISION"     division             (length division)             sap-uc-encoding))
      (if partner-role
	  (rfc-set-chars func-handle "IV_PARTNER_ROLE" partner-role         (length partner-role)         sap-uc-encoding))
      (if business-partner-nr
	  (rfc-set-chars func-handle "IV_CUSTOMER"     business-partner-nr  (length business-partner-nr)  sap-uc-encoding))
      (if price-date
	  (rfc-set-chars func-handle "IV_PRICE_DATE"   price-date           (length price-date)           sap-uc-encoding))
      )

    (set-sap-it-line-items-from-servb-mat-pricing-items-in servb-mat-pricing-items-in it-line-items-table-handle :sap-uc-encoding sap-uc-encoding)

    (let* ((rc                (rfc-invoke connection-handle func-handle))
	   (servb-return-info (get-servb-return-info-from-function-handle func-handle)))
      (setf (rfc-return-code servb-return-info) rc)
      (setf (fm-name servb-return-info) "Z-MAT-GET-PRICE")

      (if (not (errorp servb-return-info))
	  (progn
	    (set-servb-mat-pricing-items-out-from-sap-et-line-items servb-mat-pricing-items-out et-line-items-table-handle :sap-uc-encoding sap-uc-encoding)
	    (set-servb-mat-pricing-item-conditions-out-from-sap-et-line-item-conds servb-mat-pricing-item-conds-out et-line-items-cond-table-handle :sap-uc-encoding sap-uc-encoding)
	    (set-pricing-conds-for-items servb-mat-pricing-items-out servb-mat-pricing-item-conds-out))
	  )

      (tb.core:send (sap-servb-return-info-log-channel) servb-return-info)

      (values servb-mat-pricing-items-out servb-return-info))))

(defun sap-direct-mat-get-price (servb-sap-rfc-connection servb-mat-pricing-items-in)
  (check-type servb-sap-rfc-connection servb-sap-rfc-connection)
  (check-type servb-mat-pricing-items-in servb-mat-pricing-items-in)
  (let ((data-context (data-context servb-mat-pricing-items-in)))
    (with-rfc-connection-ensured (servb-sap-rfc-connection "SAP-DIRECT-MAT-GET-PRICE")
      (multiple-value-bind (servb-mat-pricing-items-out servb-return-info)
	  (z-mat-get-price (connection-handle servb-sap-rfc-connection) servb-mat-pricing-items-in)
	(if servb-mat-pricing-items-out
	    (setf (data-context servb-mat-pricing-items-out) data-context))
	(values servb-mat-pricing-items-out servb-return-info)))))
