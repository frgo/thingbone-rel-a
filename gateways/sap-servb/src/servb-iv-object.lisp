;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

;;; SAP-SERVB - SPECIFIC DATA STRUCTURES

;; IMPLEMENTATION NOTE: IV_OBJECT DEFINITION
;;
;; IV_OBJECT	STRING

(eval-when (:compile-toplevel :execute :load-toplevel)
  (defconstant +default-servb-iv-object-data-string-length+ (* 1 1024))
  (defparameter *servb-iv-object-data-string-length* +default-servb-iv-object-data-string-length+))

(defclass servb-iv-object (servb-data)
  ((data :accessor data :initarg :data :initform nil)))

(defmethod print-object ((servb-iv-object servb-iv-object) stream)
  (print-unreadable-object (servb-iv-object stream :identity t :type t)
    (format stream ":DATA ~S" (data servb-iv-object))))

(defun set-sap-iv-object-from-servb-iv-object (servb-iv-object container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let ((data (data servb-iv-object)))
    (rfc-set-string container-handle "IV_OBJECT" data (length data) sap-uc-encoding)
    (values)))

(defun set-servb-iv-object-from-sap-iv-object (servb-iv-object container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (setf (data servb-iv-object) (rfc-get-string container-handle "IV_OBJECT" *servb-iv-object-data-string-length* sap-uc-encoding))
  (log:debug "SET-SERVB-IV-OBJECT-FROM-SAP-IV-OBJECT - ~S" servb-iv-object)
  servb-iv-object)

(defun make-servb-iv-object-from-sap-container-handle (container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (if (not (cffi:null-pointer-p container-handle))
      (let ((servb-iv-object (make-instance 'servb-iv-object)))
	(set-servb-iv-object-from-sap-iv-object servb-iv-object container-handle :sap-uc-encoding sap-uc-encoding)
	servb-iv-object)))
