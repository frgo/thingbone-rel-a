;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

(defparameter *servb-gateway-registry* (make-hash-table :test 'equal))

(defun reset-servb-gateway-registry ()
  (clrhash *servb-gateway-registry*))

(defclass servb-gateway (tb.core:tb-gateway servb-server)
  ((external-system-kind :accessor external-system-kind :initarg :external-system-kind :initform nil)
   (function-modules :accessor function-modules :initarg :function-modules :initform (make-hash-table :test 'equal)))
  (:default-initargs
   :name "SAP SERVB GATEWAY"))

(defclass sapr3-gateway (servb-gateway)
  ()
  (:default-initargs
   :external-system-kind :sapr3
    :name "SAP R/3 SERVB GATEWAY"))

(defun register-servb-gateway (servb-gateway)
  (setf (gethash (tb.core:site servb-gateway) *servb-gateway-registry*) servb-gateway))

(defun deregister-servb-gateway (servb-gateway)
  (remhash (tb.core:site servb-gateway) *servb-gateway-registry*))

(defun make-servb-gateway (&key site servb-domain external-system-kind (client-connection-params nil) (server-gw-connection-params nil)
			     )
  (make-instance 'servb-gateway :site site :domain servb-domain :external-system-kind external-system-kind
		 :client-connection-params (or client-connection-params
					       (sap-servb-client-rfc-connection-params))
		 :server-gw-connection-params (or server-gw-connection-params
						  (sap-servb-server-rfc-connection-params))))

(defun make-sapr3-gateway (&key site servb-domain (client-connection-params nil) (server-gw-connection-params nil))
  (make-instance 'sapr3-gateway :site site :domain servb-domain
		 :client-connection-params (or client-connection-params
					       (sap-servb-client-rfc-connection-params))
		 :server-gw-connection-params (or server-gw-connection-params
						  (sap-servb-server-rfc-connection-params))))

(defun make-and-register-servb-gateway(&key site servb-domain (client-connection-params nil) (server-gw-connection-params nil))
  (register-servb-gateway (make-servb-gateway :site site :servb-domain servb-domain
					      :client-connection-params client-connection-params :server-gw-connection-params server-gw-connection-params)))

(defun make-and-register-sapr3-gateway(&key site servb-domain (client-connection-params nil) (server-gw-connection-params nil))
  (register-servb-gateway (make-sapr3-gateway :site site :servb-domain servb-domain
					      :client-connection-params client-connection-params :server-gw-connection-params server-gw-connection-params)))

(defun find-servb-gateway-for-site (site)
  (gethash site *servb-gateway-registry*))

(defmethod ensure-client-connection ((servb-gateway servb-gateway))
  (let ((result (or (client-rfc-connection servb-gateway)
		    (setf (client-rfc-connection servb-gateway)
			  (make-servb-sap-rfc-connection "SERVB CLIENT RFC CONNECTION" (client-connection-params servb-gateway))))))
    result))

(defmethod ensure-server-connection ((servb-gateway servb-gateway))
  (or (server-gw-rfc-connection servb-gateway)
      (setf (server-gw-rfc-connection servb-gateway)
	    (make-servb-sap-rfc-connection "SERVB SERVER RFC CONNECTION" (server-gw-connection-params servb-gateway)))))


(defmethod client-connection-handle ((gateway servb-gateway))
  (let* ((connection (ensure-client-connection gateway))
	 (connected-p (ensure-connection connection)))
    (if connected-p
	(connection-handle connection)
	(cffi:null-pointer))))

;; DATA ELEMENT

(defclass servb-data-element ()
  ((name                 :accessor name                 :initarg :name                 :initform nil)
   (kind                 :accessor kind                 :initarg :kind                 :initform nil)
   (data-type            :accessor data-type            :initarg :data-type            :initform nil)
   (data-length          :accessor data-length          :initarg :data-length          :initform nil)
   (required-or-optional :accessor required-or-optional :initarg :required-or-optional :initform nil)
   (handle               :accessor handle               :initarg :handle               :initform nil)))

;; FUNCTION MODULE HANDLING

(defclass servb-function-module (servb-data-element)
  ((containers :accessor containers :initarg :containers :initform (make-hash-table :test 'equal)))
  (:default-initargs
   :kind :function-module))

(defun make-function-module (name)
  (make-instance 'servb-function-module :name name))

(defmethod add-function-module ((self servb-gateway) (function-module servb-function-module))
  (setf (gethash (name function-module) (function-modules self)) function-module))

(defmethod init-function-module ((self servb-gateway) (function-module servb-function-module))
  (setf (handle function-module) (tb.sapnwrfc:rfc-function (ensure-client-connection self) function-module)))

(defmethod add-and-init-function-module ((self servb-gateway) (function-module servb-function-module))
  (add-function-module self function-module)
  (init-function-module self function-module))

(defmethod function-handle ((self servb-gateway) (function-module servb-function-module))
  (rfc-function (client-connection-handle self) (name function-module)))

(defmethod function-handle ((self servb-gateway) (function-module-name string))
  (let ((function-module (gethash function-module-name (function-modules self))))
    (function-handle self function-module)))

(defmethod function-module ((self servb-gateway) function-module-name)
  (gethash function-module-name (function-modules self)))

(defmethod invoke-function-module ((gateway servb-gateway) (servb-function-module servb-function-module))
  (let* ((connection-handle (client-connection-handle gateway))
	 (func-handle (function-handle gateway servb-function-module))
	 (rc (rfc-invoke connection-handle func-handle))
	 (servb-return-info (get-servb-return-info-from-function-handle func-handle)))
    (setf (rfc-return-code servb-return-info) rc)
    (setf (fm-name servb-return-info) (name servb-function-module))
    servb-return-info))

(defmethod invoke-function-module ((gateway servb-gateway) (function-module-name string))
  (let ((function-module (gethash function-module-name (function-modules gateway))))
    (invoke-function-module gateway function-module)))

(defmacro define-function-module (function-module-name &key site-list)
  `(let ((gateways (loop for site in ',site-list
		      collect (find-servb-gateway-for-site site))))
     (if (or (null gateways)
	     (and (listp gateways)
		  (null (first gateways))))
	 (error "No gateways found for sites ~A!" ',site-list)
	 (let ((function-module (make-function-module ,function-module-name)))
	   (loop for gateway in gateways
	      do
		(if gateway
		    (add-function-module gateway function-module)))))))

(defmethod container ((self servb-function-module) container-name)
  (gethash container-name (containers self)))

;; (define-function-module "Z_SALES_ORDER_REQUEST" :site-list ("STU"))

;; DATA CONTAINER HANDLING

(defclass servb-container-slot (servb-data-element)
  ())

(defclass servb-container (servb-data-element)
  ((slots :accessor slots :initarg :slots :initform (make-hash-table :test 'equal))
   (importing-or-exporting :accessor importing-or-exporting :initarg :importing-or-exporting :initform (error ":IMPORTING or :EXPORTING required for slot importing-or-exporting!"))))

(defmethod add-slot ((container servb-container) (slot servb-container-slot))
  (setf (gethash (name slot) (slots container)) slot))

(defmethod container-slot ((container servb-container) slot-name)
  (gethash slot-name (slots container)))

(defun make-servb-container (name kind &key direction data-type slots)
  (make-instance 'servb-container :name name :kind kind :data-type data-type :importing-or-exporting direction :slots (or slots
															  (make-hash-table :test 'equal))))

(defmethod add-container ((function-module servb-function-module) (container servb-container))
  (setf (gethash (name container) (containers function-module)) container))

(defmacro define-servb-data-container-for-function-module (function-module-name container-name container-kind importing-or-exporting &key site-list slot-definition-list)
  `(let* ((gateways (loop for site in ',site-list
		       collect (find-servb-gateway-for-site site)))
	  (function-modules (if (and gateways
				     (listp gateways)
				     (not (null (first gateways))))
				(loop for gateway in gateways
				   collect (if gateway
					       (tb.gw.sap-servb:function-module gateway ,function-module-name))))))
     ;; (log:debug "gateways = ~S, function-modules = ~S" gateways function-modules)
     (if (or (null function-modules)
	     (and (listp function-modules)
		  (null (first function-modules))))
	 (error "No function modules named ~A found for site-list ~A!" ,function-module-name ',site-list)
	 (let ((container (tb.gw.sap-servb:make-servb-container ,container-name ,container-kind
								:direction ,importing-or-exporting)))
	   (loop for slot-definition in ',slot-definition-list
	      do
		(let* ((slot-def-length (length slot-definition))
		       (slot-name (first slot-definition))
		       (data-type (second slot-definition))
		       (data-length (if (>= slot-def-length 3)
					(third slot-definition)
					nil))
		       (required-or-optional (if (>= slot-def-length 4)
						 (fourth slot-definition)
						 :optional)))
		  (add-slot container (make-instance 'tb.gw.sap-servb:servb-container-slot
						     :name slot-name
						     :kind :container-slot
						     :data-type data-type
						     :data-length data-length
						     :required-or-optional required-or-optional))))
	   (loop for function-module in function-modules
	      do
	      ;; (log:debug "Adding container ~A to function module ~A..." (name container) (name function-module))
		(if function-module
		    (tb.gw.sap-servb:add-container function-module container)))))))



;; TESTING

;; NOT IMPLEMENTED YET ...
;; (define-servb-data-writer-for-tb-extended-msg-class tb-request-sales-order-create "Z_MAT_SALES_ORDER_REQUEST" :site-list ("STU" "HAM")
;; 						    (("IS_HEADER" nil (("ORDER_DATE" order-date)
;; 								       ("SALES_ORG"  sales-organisation)))
;; 						     ("IT_LINE_ITEMS" "line-items" (("ITM_NUMBER" pos-nr)
;; 										    ("MATERIAL" item-nr)
;; 										    ("MATERIAL_VERSION" item-version)))))


(defun sapr3-write-chars (container-handle param-name value max-length sap-uc-encoding)
  (if value
      (let ((chars (tb.msgs:stringify-msg-value value)))
      	(let ((rc (rfc-set-chars container-handle param-name chars (servb-length max-length chars) sap-uc-encoding)))
	  (if (not (eql rc :rfc-ok))
	      (tb.sapnwrfc::dispatch-rfc-condition (make-condition 'tb.sapnwrfc::rfc-invalid-parameter :message  "INVALID PARAMETER OR VALUE!" :parameter-name param-name :parameter-value value)))
	  rc))))

(defun sapr3-write-float (container-handle param-name value sap-uc-encoding)
  (if value
      (let ((rc (rfc-set-float container-handle param-name (coerce value 'float) sap-uc-encoding)))
	(if (not (eql rc :rfc-ok))
	    (tb.sapnwrfc::dispatch-rfc-condition (make-condition 'tb.sapnwrfc::rfc-invalid-parameter :message  "INVALID PARAMETER OR VALUE!" :parameter-name param-name :parameter-value value)))
	rc)))

(defun sapr3-write-number (container-handle param-name value max-length decimal-places as-rfc-chars-p sap-uc-encoding)
  (if value
      (let ((rc
	     (if as-rfc-chars-p
		 (let ((chars (etypecase value
				(integer (format nil "~d" value))
				(float   (format nil "~v$" (or decimal-places 0) value))
				(string  value))))
		   (rfc-set-chars container-handle param-name chars (servb-length max-length chars) sap-uc-encoding))
		 (etypecase value
		   (string  (rfc-set-chars container-handle param-name value max-length sap-uc-encoding))
		   (integer (rfc-set-int   container-handle param-name value sap-uc-encoding))
		   (float   (rfc-set-float container-handle param-name value sap-uc-encoding))))))
	(if (not (eql rc :rfc-ok))
	    (tb.sapnwrfc::dispatch-rfc-condition (make-condition 'tb.sapnwrfc::rfc-invalid-parameter :message  "INVALID PARAMETER OR VALUE!" :parameter-name param-name :parameter-value value)))
	rc)))

(defmethod servb-sap-write-value ((type (eql :char)) container-handle param-name length required-or-optional value value-bound-p &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (if (and (eql required-or-optional :required)
	   (not value-bound-p))
      (error "Required servb slot value but NIL!"))
  (etypecase value
    (string (rfc-set-chars container-handle param-name value (servb-length length value) sap-uc-encoding))
    ))

(defmethod structure-handle-from-function-module-and-structure ((gateway servb-gateway) function-module-name structure-name)
  (let ((function-module (function-module gateway function-module-name)))
    (if function-module
	(rfc-get-structure (function-handle gateway function-module-name) structure-name))))

(defmethod table-handle-from-function-module-and-table ((gateway servb-gateway) function-module-name table-name)
  (let ((function-module (function-module gateway function-module-name)))
    (if function-module
	(rfc-get-table (function-handle gateway function-module-name) table-name))))

(defmethod slot-from-function-module-and-container ((gateway servb-gateway) function-module-name container-name slot-name)
  (let* ((function-module (function-module gateway function-module-name))
	 (container (if function-module
			(container function-module container-name))))
    (if container
	(container-slot container slot-name))))


(defmethod servb-sap-write-field ((gateway sapr3-gateway) function-module-name container-name param-name value &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let ((handle (function-handle gateway function-module-name)))

    (if (null handle)
	(error "Could not derive handle for function module ~A!" function-module-name))

    (let* ((slot (slot-from-function-module-and-container gateway function-module-name container-name param-name))
	   (data-type (if slot
			  (data-type slot)))
	   (data-length (if slot
			    (data-length slot)))
	   (required-or-optional (if slot
				     (required-or-optional slot))))
      (servb-sap-write-value data-type handle param-name data-length required-or-optional value t :sap-uc-encoding sap-uc-encoding))))

(defmethod servb-sap-write-structure ((gateway sapr3-gateway) function-module-name structure-name parameter-value-pair-list &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  "parameter-value-pair-list => (( \"ITM_NUMBER\" \"20\" )
                                 ( \"MATERIAL\"   \"1234-456\" )) "
  (let ((handle (structure-handle-from-function-module-and-structure gateway function-module-name structure-name)))

    (if (null handle)
	(error "Could not derive handle for structure ~A in function module ~A!" structure-name function-module-name))

    (loop for p-v-pair in parameter-value-pair-list
       do
	 (let* ((param-name (first p-v-pair))
		(value (second p-v-pair))
		(slot (slot-from-function-module-and-container gateway function-module-name structure-name param-name))
		(data-type (if slot
			       (data-type slot)))
		(data-length (if slot
				 (data-length slot)))
		(required-or-optional (if slot
					  (required-or-optional slot))))
	   (servb-sap-write-value data-type handle param-name data-length required-or-optional value t :sap-uc-encoding sap-uc-encoding)))))



(defmethod servb-sap-init-table-for-writing ((gateway sapr3-gateway) function-module-name table-name &optional nr-rows)
  (let ((handle (table-handle-from-function-module-and-table gateway function-module-name table-name)))
    (if (null handle)
	(error "Could not derive handle for table ~A in function module ~A!" table-name function-module-name))

    (rfc-delete-all-rows handle)
    (if nr-rows
	(rfc-append-new-rows handle nr-rows))
    (rfc-move-to-first-row handle)))


(defmethod servb-sap-write-table-row ((gateway sapr3-gateway) function-module-name table-name table-row-parameter-value-pair-list &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let ((handle (table-handle-from-function-module-and-table gateway function-module-name table-name)))
    (if (null handle)
	(error "Could not derive handle for table ~A in function module ~A!" table-name function-module-name))
    (loop for p-v-pair in table-row-parameter-value-pair-list
       do
	 (let* ((param-name (first p-v-pair))
		(value (second p-v-pair))
		(slot (slot-from-function-module-and-container gateway function-module-name table-name param-name))
		(data-type (if slot
			       (data-type slot)))
		(data-length (if slot
				 (data-length slot)))
		(required-or-optional (if slot
					  (required-or-optional slot))))
	   (servb-sap-write-value data-type handle param-name data-length required-or-optional value t :sap-uc-encoding sap-uc-encoding)))
    (rfc-move-to-next-row handle)))

(defmethod servb-sap-write-table ((gateway sapr3-gateway) function-module-name table-name table-rows-parameter-value-pair-list &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let ((handle (table-handle-from-function-module-and-table gateway function-module-name table-name)))

    (if (null handle)
	(error "Could not derive handle for table ~A in function module ~A!" table-name function-module-name))

    (loop for table-row in table-rows-parameter-value-pair-list
       do
	 (loop for p-v-pair in table-row
	    do
	      (let* ((param-name (first p-v-pair))
		     (value (second p-v-pair))
		     (slot (slot-from-function-module-and-container gateway function-module-name table-name param-name))
		     (data-type (if slot
				    (data-type slot)))
		     (data-length (if slot
				      (data-length slot)))
		     (required-or-optional (if slot
					       (required-or-optional slot))))
		(servb-sap-write-value data-type handle param-name data-length required-or-optional value t :sap-uc-encoding sap-uc-encoding))))))

(defgeneric servb-gateway-write (gateway object &key &allow-other-keys))
