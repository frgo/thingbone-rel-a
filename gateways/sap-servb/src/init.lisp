;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

(let ((sap-servb-initialized nil))

  (defparameter lock-sap-servb-initialized (bt:make-recursive-lock))

  (defun sap-servb-init ()
    (bt:with-recursive-lock-held (lock-sap-servb-initialized)

      ;; Load SAP RFC Bindings
      (tb.sapnwrfc:libsapnwrfc-init)

      ;; Initalize Message Channels in this Gateway

      (init-sap-servb-appl-log-channel)
      (init-sap-servb-return-info-log-channel)
      (init-sap-servb-outbound-channel)
      (init-sap-servb-inbound-channel)

      (start-sap-servb-appl-log-channel-dispatcher)
      (start-sap-servb-return-info-log-channel-dispatcher)
      (start-sap-servb-outbound-channel-dispatcher)
      (start-sap-servb-inbound-channel-dispatcher)

      ;; Initialize Main SAP SERVICE BUS RFC Server
      (start-sap-servb-outbound-server)

      ;; Init completed
      (setq sap-servb-initialized t)
      (log:info "THINGBONE.GATEWAY.SAP-SERVB initialized.")
      (values)))

  (defun sap-servb-reset ()
    (bt:with-recursive-lock-held (lock-sap-servb-initialized)
      (stop-sap-servb-outbound-server)
      (stop-sap-servb-outbound-channel-dispatcher)
      (stop-sap-servb-inbound-channel-dispatcher)
      (stop-sap-servb-appl-log-channel-dispatcher)
      (stop-sap-servb-return-info-log-channel-dispatcher)
      (tb.sapnwrfc:libsapnwrfc-reset)
      (setq sap-servb-initialized nil)
      (log:info "THINGBONE.GATEWAY.SAP-SERVB reset.")))

  (defun sap-servb-initialized-p ()
    (bt:with-recursive-lock-held (lock-sap-servb-initialized)
      sap-servb-initialized))
  )

(defun ensure-sap-servb-initialized ()
  (if (not (sap-servb-initialized-p))
      (sap-servb-init))
  (sap-servb-initialized-p))

(defun start-sap-servb ()
  (log:info "Starting SAP SERVB...")
  (ensure-sap-servb-initialized)
  (log:info "SAP SERVB started.")
  (values))

(defun stop-sap-servb ()
  (log:info "Stopping SAP SERVB...")
  (sap-servb-reset)
  (log:info "SAP SERVB stopped.")
  (values))
