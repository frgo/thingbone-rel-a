;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

(defclass servb-address (servb-data)
  ((addressee-name :accessor addressee-name :initarg :addressee-name :initform nil)
   (address-line-1 :accessor address-line-1 :initarg :address-line-1 :initform nil)
   (address-line-2 :accessor address-line-2 :initarg :address-line-2 :initform nil)
   (address-line-3 :accessor address-line-3 :initarg :address-line-3 :initform nil)
   (address-line-4 :accessor address-line-4 :initarg :address-line-4 :initform nil)
   (address-line-5 :accessor address-line-5 :initarg :address-line-5 :initform nil)
   (city           :accessor city           :initarg :city           :initform nil)
   (postal-code    :accessor postal-code    :initarg :postal-code    :initform nil)
   (state-code     :accessor state-code     :initarg :state-code     :initform nil)
   (country-code   :accessor country-code   :initarg :country-code   :initform nil)
   (region         :accessor region         :initarg :region         :initform nil)
   ))

(defmethod print-object ((self servb-address) stream)
  (print-unreadable-object (self stream :identity t :type t)
    (format stream ":ADDRESSEE-NAME ~S :ADDRESS-LINE-4 ~S :CITY ~S :POSTAL-CODE ~S :STATE-CODE ~S :COUNTRY-CODE ~S :REGION ~S"
	    (slot-value self 'addressee-name)
	    (slot-value self 'address-line-4)
	    (slot-value self 'city)
	    (slot-value self 'postal-code)
	    (slot-value self 'state-code)
	    (slot-value self 'country-code)
	    (slot-value self 'region)
	    )))

(defmethod jonathan:%to-json ((servb-address servb-address))
  (jonathan:write-value
   (jonathan:with-object
     (jonathan:write-key-value "addressee-name" (addressee-name servb-address))
     (jonathan:write-key-value "address-line-1" (address-line-1 servb-address))
     (jonathan:write-key-value "address-line-2" (address-line-2 servb-address))
     (jonathan:write-key-value "address-line-3" (address-line-3 servb-address))
     (jonathan:write-key-value "address-line-4" (address-line-4 servb-address))
     (jonathan:write-key-value "address-line-5" (address-line-5 servb-address))
     (jonathan:write-key-value "city"           (city           servb-address))
     (jonathan:write-key-value "postal-code"    (postal-code    servb-address))
     (jonathan:write-key-value "state-code"     (state-code     servb-address))
     (jonathan:write-key-value "country-code"   (country-code   servb-address))
     (jonathan:write-key-value "region"         (region         servb-address))
     )))


(defclass servb-order-address (serv-address)
  ())

(defmethod sap-container-handle-from-sap-func-handle ((servb-order-address servb-order-address) func-handle)
  (rfc-get-structure func-handle "IS_ADDRESS"))


;; Tabelle  ZMAT_T_SALES_ORDER_ADDRESS / Struktur ZMAT_S_SALES_ORDER_ADDRESS
;; Attribut	Format	Länge	Dez.-Stellen	Beschreibung	                                        JSON Dokument Objekt
;; NAME	        CHAR	40	0	        Name 1	                                                addressee-name
;; STREET	CHAR	40	0	        Straße	                                                address-line-4
;; CITY	        CHAR	40	0	        Ort	                                                city
;; POSTAL_CODE	CHAR	10	0	        Postleitzahl des Orts	                                postal-code
;; STATE_CODE	CHAR	3	0	        Region (Bundesstaat, Bundesland, Provinz, Grafschaft)	state-code
;; COUNTRY	CHAR	3	0	        Länderschlüssel	                                        country-code
;; REGION	CHAR	32	0	        Region	                                                region

(defmethod servb-to-sap ((servb-order-address servb-order-address) func-handle container-handle  &key (sap-uc-encoding (rfc-sap-uc-encoding)))

  (let ((%container-handle (sap-container-handle servb-order-address func-handle container-handle)))

    (with-slots (addressee-name address-line-4 city postal-code state-code country-code region) servb-order-address

      (when addressee-name
	(rfc-set-chars %container-handle "NAME"          addressee-name (servb-length 40 (length addressee-name)) sap-uc-encoding))

      (when address-line-4
	(rfc-set-chars %container-handle "STREET"        address-line-4 (servb-length 40 (length address-line-4)) sap-uc-encoding))

      (when city
	(rfc-set-chars %container-handle "CITY"          city           (servb-length 40 (length city))           sap-uc-encoding))

      (when postal-code
	(rfc-set-chars %container-handle "POSTAL_CODE"   postal-code    (servb-length 10 (length postal-code))    sap-uc-encoding))

      (when state-code
	(rfc-set-chars %container-handle "STATE_CODE"    state-code     (servb-length  3 (length state-code))     sap-uc-encoding))

      (when country-code
	(rfc-set-chars %container-handle "COUNTRY_CODE"  country-code   (servb-length  3 (length country-code))   sap-uc-encoding))

      (when region
	(rfc-set-chars %container-handle "REGION"        region         (servb-length 32 (length region))         sap-uc-encoding))

      ))

  servb-order-address)

(defmethod sap-to-servb ((servb-order-address servb-order-address) func-handle container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let ((%container-handle (sap-container-handle servb-order-address func-handle container-handle)))
    )
  servb-order-address)

;;; --- ORDER SHIP-TO ADDRESS

(defclass servb-order-ship-to-address (servb-order-address)
  ())

(defmethod jonathan:%to-json ((servb-order-ship-to-address servb-order-ship-to-address))
  (jonathan:write-key "ship-to-address")
  (call-next-method))

(defun make-servb-order-ship-to-address-from-sap (func-handle container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let* ((instance (make-instance 'servb-order-ship-to-address)))
    (sap-to-servb instance func-handle container-handle sap-uc-encoding)))
