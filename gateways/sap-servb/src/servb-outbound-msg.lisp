;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

;;; SAP-SERVB - SPECIFIC DATA STRUCTURES

(defclass servb-outbound-msg ()
  ((is-header     :accessor is-header     :initarg :is-header     :initform nil)
   (iv-data       :accessor iv-data       :initarg :iv-data       :initform nil)
   (iv-object     :accessor iv-object     :initarg :iv-object     :initform nil)
   (iv-object-key :accessor iv-object-key :initarg :iv-object-key :initform nil)
   (iv-event      :accessor iv-event      :initarg :iv-event      :initform nil)
   ))

(defmethod print-object ((servb-outbound-msg servb-outbound-msg) stream)
  (print-unreadable-object (servb-outbound-msg stream :identity t :type t)
    (with-slots (is-header iv-object iv-object-key iv-event) servb-outbound-msg
      (format stream ":IS-HEADER ~S :IV-OBJECT ~S :IV-OBJECT-KEY ~S :IV-EVENT ~S"
	      is-header iv-object iv-object-key iv-event))))

(defmacro define-servb-outbound-msg-is-header-slot-value-getter (slot-name)
  (let ((g-value (gensym))
	(g-is-header (gensym)))
    `(defmethod ,slot-name ((servb-outbound-msg servb-outbound-msg))
       (let* ((,g-is-header (is-header servb-outbound-msg))
	      (,g-value (if ,g-is-header
			    (,slot-name ,g-is-header)
			    nil)))
	 ,g-value))))

(define-servb-outbound-msg-is-header-slot-value-getter conversation-id)
(define-servb-outbound-msg-is-header-slot-value-getter encoding)
(define-servb-outbound-msg-is-header-slot-value-getter language)
(define-servb-outbound-msg-is-header-slot-value-getter protocol)
(define-servb-outbound-msg-is-header-slot-value-getter protocol-version)
(define-servb-outbound-msg-is-header-slot-value-getter timestamp)
(define-servb-outbound-msg-is-header-slot-value-getter app-id)
(define-servb-outbound-msg-is-header-slot-value-getter app-instance-id)
(define-servb-outbound-msg-is-header-slot-value-getter comment)
(define-servb-outbound-msg-is-header-slot-value-getter o)
(define-servb-outbound-msg-is-header-slot-value-getter ou)
(define-servb-outbound-msg-is-header-slot-value-getter support)
(define-servb-outbound-msg-is-header-slot-value-getter reply-with)
(define-servb-outbound-msg-is-header-slot-value-getter in-reply-to)
(define-servb-outbound-msg-is-header-slot-value-getter reply-by)
(define-servb-outbound-msg-is-header-slot-value-getter performative)
(define-servb-outbound-msg-is-header-slot-value-getter credentials-type)
(define-servb-outbound-msg-is-header-slot-value-getter requestor-user-id)
(define-servb-outbound-msg-is-header-slot-value-getter requestor-passcode)
(define-servb-outbound-msg-is-header-slot-value-getter operation)
(define-servb-outbound-msg-is-header-slot-value-getter object-class)
(define-servb-outbound-msg-is-header-slot-value-getter qos)

(defmacro define-servb-outbound-msg-iv-data-slot-value-getter (slot-name)
  (let ((g-value (gensym))
	(g-iv-data (gensym)))
    `(defmethod ,slot-name ((servb-outbound-msg servb-outbound-msg))
       (let* ((,g-iv-data (iv-data servb-outbound-msg))
	      (,g-value (if ,g-iv-data
			    (,slot-name ,g-iv-data)
			    nil)))
	 ,g-value))))

(define-servb-outbound-msg-iv-data-slot-value-getter data)

(defun make-requestor-context-from-servb-outbound-msg (servb-outbound-msg)
  (let ((requestor-user-id (requestor-user-id servb-outbound-msg))
	(requestor-passcode (requestor-passcode servb-outbound-msg)))
    (cond
      ;; TODO: Extend for other kinds of msg-request classes
      ;; frgo, 2019-05-16
      ;;
      ;; Case: USER kind
      ((string= (string-downcase (credentials-type servb-outbound-msg)) "user")
       (tb.msgs:make-msg-user-id-passwd-requestor-context requestor-user-id requestor-passcode)))))

(defun make-sender-from-servb-outbound-msg (servb-outbound-msg)
  (tb.msgs:make-msg-sender (app-id servb-outbound-msg) (app-instance-id servb-outbound-msg)
			   (comment servb-outbound-msg) (o servb-outbound-msg)
			   (ou servb-outbound-msg) (support servb-outbound-msg)))

(defun make-msg-data-from-servb-outbound-msg (servb-outbound-msg)
  (let* ((iv-data (iv-data servb-outbound-msg))
	 (data (if iv-data
		   (data iv-data))))
    (tb.msgs:make-msg-string-data (tb.msgs:stringify-msg-value data))))

(defun make-msg-content-from-servb-outbound-msg (servb-outbound-msg)
  (tb.msgs:make-msg-extended-content (object-class servb-outbound-msg)
				     (operation servb-outbound-msg)
				     nil
				     (make-msg-data-from-servb-outbound-msg servb-outbound-msg)
				     nil))

(defun make-qos-from-servb-outbound-msg (servb-outbound-msg)
  (let ((servb-qos (qos servb-outbound-msg)))
    (make-instance 'tb.msgs:msg-qos :request-status-response-required-p
		   (request-status-response-required-p servb-qos))))


(defmethod to-tb-extended-msg ((servb-outbound-msg servb-outbound-msg))
  (let* ((qos (make-qos-from-servb-outbound-msg servb-outbound-msg))
	 (content (make-msg-content-from-servb-outbound-msg servb-outbound-msg))
	 (data (data (iv-data servb-outbound-msg)))
	 (tb-extended-msg
	  (make-instance 'tb.msgs:extended-msg
			 :conversation-id (conversation-id servb-outbound-msg)
			 :encoding (encoding servb-outbound-msg)
			 :language (language servb-outbound-msg)
			 :protocol (protocol servb-outbound-msg)
			 :protocol-version (protocol-version servb-outbound-msg)
			 :timestamp (timestamp servb-outbound-msg)
			 :requestor-context (make-requestor-context-from-servb-outbound-msg servb-outbound-msg)
			 :sender (make-sender-from-servb-outbound-msg servb-outbound-msg)
			 :reply-with (reply-with servb-outbound-msg)
			 :in-reply-to (in-reply-to servb-outbound-msg)
			 :reply-by (reply-by servb-outbound-msg)
			 :performative (performative servb-outbound-msg)
			 :qos qos
			 :content content
			 :msg-as-string data)))
    (log:debug "TO-TB-EXTENDED-MSG: Created msg ~S" tb-extended-msg)
    tb-extended-msg))
