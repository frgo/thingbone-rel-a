;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

(defun make-sales-order-update-response-from-extended-msg (extended-msg)
  (let* ((data-context (make-instance 'tb.msgs:tb-data-context :environment (tb.core:tb-site)))
	 (sales-order-update-result (make-instance 'tb.msgs:tb-sales-order-update-result))
	 (content (make-instance 'tb.msgs:msg-extended-content
				 :object-class :sales-order
				 :operation :update
				 :data nil))
	 (in-reply-to (tb.msgs:conversation-id extended-msg))
	 (request (tb.msgs:data (tb.msgs:content extended-msg)))
	 (response (make-instance 'tb.msgs:tb-response-sales-order-update
				  :conversation-id (format nil "~A" (tb.core:make-uuid))
				  :encoding (tb.msgs:extended-msg-encoding)
				  :protocol (tb.msgs:extended-msg-protocol)
				  :protocol-version (tb.msgs:extended-msg-protocol-version)
				  :in-reply-to in-reply-to
				  :performative "response"
				  :sender (tb.msgs::make-msg-sender-for-this-app-instance)
				  :content content
				  :data-context data-context
				  :request request
				  :sales-order-update-result sales-order-update-result)))
    response))

;; FUNCTION Z_MAT_S_O_UPD_REQ .
;; *"----------------------------------------------------------------------
;; *"*"Lokale Schnittstelle:
;; *"  IMPORTING
;; *"     VALUE(IS_HEADER) TYPE  ZMAT_S_SALES_ORDER_HEADER
;; *"     VALUE(IT_LINE_ITEMS) TYPE  ZMAT_T_SALES_ORDER_ITEMS OPTIONAL
;; *"     VALUE(IT_COMMENTS) TYPE  ZMAT_T_SALES_ORDER_COMMENT_LI OPTIONAL
;; *"     VALUE(IT_PARTNERS) TYPE  ZMAT_T_SALES_ORDER_PARTNERS OPTIONAL
;; *"     VALUE(IT_ADDRESS) TYPE  ZMAT_T_SALES_ORDER_ADDRESS OPTIONAL
;; *"     VALUE(IV_WO_COMMIT) TYPE  FLAG OPTIONAL
;; *"  EXPORTING
;; *"     VALUE(ES_SERVB_RETURN) TYPE  ZSERVB_S_RETURN
;; *"     VALUE(EV_SALES_ORDER_NR) TYPE  VBELN_VA
;; *"     VALUE(ET_MESSAGES) TYPE  ZMAT_T_SALES_ORDER_RETURN
;; *"     VALUE(ET_DETAIL_STATUS) TYPE  ZMAT_T_SALES_ORDER_STATUS
;; *"----------------------------------------------------------------------

;; +++ IMPORTING +++

;; Struktur ZMAT_S_SALES_ORDER_HEADER
;;
;; Attribut	        Format	Länge	Dez.-Stellen	Beschreibung	JSON Dokument Objekt
;; ORDER_DATE	        NUMC	8	0	        Datum im CHAR-Format (YYYYMMTT)	order-date
;; VISIBL_DISC_ABS	CHAR	15	2	        Sichtbarer Discount absolut	order-global-discount-absolute-amount, wenn show-order-global-discount = true
;; UNVISIBL_DISC_ABS	CHAR	15	2	        Unsichtbarer Discount absolut	order-global-discount-absolute-amount, wenn show-order-global-discount = false
;; VISIBL_DISC_PRC	CHAR	5	2	        Sichtbarer Discount in Prozent	order-global-discount-percentage, wenn show-order-global-discount = true
;; UNVISIBL_DISC_PRC	QUAN	5	2 	        Unsichtbarer Discount in Prozent	order-global-discount-percentage, wenn show-order-global-discount = false
;; VISIBL_SURC_ABS	UNIT	15	2	        Sichtbarer Zuschlag absolut	order-global-surcharge-absolute-amount, wenn show-order-global-surcharge = true
;; UNVISIBL_SURC_ABS	CURR	15	2	        Unsichtbarer Zuschlag absolut	order-global-surcharge-absolute-amount, wenn show-order-global-surcharge = false
;; VISIBL_SURC_PRC	CHAR	5	2	        Sichtbarert Zuschlag in Prozent	order-global-surcharge-percentage, wenn show-order-global-surcharge = true
;; UNVISIBL_SURC_PRC	CURR	5	2	        Unsichtbarer Zuschlag in Prozent	order-global-surcharge-percentage, wenn show-order-global-surcharge = false
;; RESULTING_PRICE	CURR	15	2	        Resultierender Gesamt-Order-Price Netto
;; CURRENCY	        DEC	5	0	        Währungsschlüssel	order-currency
;; DOC_TYPE	        DEC	4	0	        Verkaufsbelegart	order-kind
;; SALES_ORG	        CURR	4	0	        Verkaufsorganisation	sales-organisation
;; DISTR_CHAN	        CURR	2	0	        Vertriebsweg	sales-channel
;; SALES_OFFICE	        DEC	4	0	        Verkaufsbüro	sales-office
;; SALES_GROUP	        DEC	3	0	        Verkäufergruppe	sales-group
;; DIVISION	        CURR	2	0	        Sparte
;; PARTNER_ROLE	        CUKY	2	0	        Partnerrolle
;; CUSTOMER	        CHAR	10	0	        Debitorennummer
;; DESIRED_DATE	        CHAR	8	0	        Wunschlieferdatum des Beleges (YYYYMMTT)	order-global-desired-delivery-date
;; REASON	        CHAR	3	0	        Auftragsgrund (Grund des Vorgangs)	order-reason
;; SHIP_WHEN_COMPL	CHAR	1	0	        allgemeines flag	ship-when-complete
;; PAYMENT_TERM	        CHAR	4	0	        Zahlungsbedingungsschlüssel	payment-term-code
;; INCOTERMS	        CHAR	3	0	        Incoterms Teil 1	incoterms-terms
;; INCOTERMS_LOC	CHAR	28	0	        Incoterms Teil 2	incoterms-location
;; PURCHASE_NR	        CHAR	35	0	        Bestellnummer des Kunden	customer-purchase-nr
;; PURCHASE_DATE	DATS	8	0	        Bestelldatum des Kunden (YYYYMMTT)	customer-purchase-date

(defun sapr3-write-is-header-from-request-sales-order-update (container-handle request-sales-order-update &key (sap-uc-encoding (rfc-sap-uc-encoding)))

  (let* ((sales-order       (tb.msgs:sales-order request-sales-order-update))
	 (business-partners (tb.msgs:order-global-customer sales-order)))
    (with-slots ((order-date                             tb.msgs:order-date)
    		 (order-kind                             tb.msgs:order-kind)
    		 (order-reason                           tb.msgs:order-reason)
		 (order-nr                               tb.msgs:order-nr)
    		 (order-currency                         tb.msgs:order-currency)
    		 (order-global-discount-percentage       tb.msgs:order-global-discount-percentage)
    		 (order-global-discount-absolute-amount  tb.msgs:order-global-discount-absolute-amount)
    		 (order-global-surcharge-percentage      tb.msgs:order-global-surcharge-percentage)
    		 (order-global-surcharge-absolute-amount tb.msgs:order-global-surcharge-absolute-amount)
    		 (order-global-total-price               tb.msgs:order-global-total-price)
    		 (order-confirmation-configuration       tb.msgs:order-confirmation-configuration)
    		 (order-logistics-configuration          tb.msgs:order-logistics-configuration)
    		 (order-payment-terms-code               tb.msgs:order-payment-terms-code)
    		 (order-level-incoterms                  tb.msgs:order-level-incoterms)
    		 (order-global-desired-delivery-date     tb.msgs:order-global-desired-delivery-date)
    		 (customer-purchase-nr                   tb.msgs:customer-purchase-nr)
    		 (customer-purchase-date                 tb.msgs:customer-purchase-date)
    		 (sales-channel                          tb.msgs:sales-channel)
    		 (sales-organisation                     tb.msgs:sales-organisation)
    		 (sales-office                           tb.msgs:sales-office)
    		 (sales-group                            tb.msgs:sales-group)) sales-order

      (sapr3-write-chars container-handle "ORDER_DATE" order-date      8 sap-uc-encoding)
      (sapr3-write-chars container-handle "DOC_TYPE"   order-kind      4 sap-uc-encoding)
      (sapr3-write-chars container-handle "REASON"     order-reason    3 sap-uc-encoding)
      (sapr3-write-chars container-handle "CURRENCY"   order-currency  3 sap-uc-encoding)

      (sapr3-write-chars container-handle "CURRENCY"   order-currency  3 sap-uc-encoding)

      (with-slots ((sold-to tb.msgs:sold-to)) business-partners
	(sapr3-write-chars container-handle "CUSTOMER" sold-to 10 sap-uc-encoding))

      (with-slots ((show-order-global-discount  tb.msgs:show-order-global-discount)
      		   (show-order-global-surcharge tb.msgs:show-order-global-surcharge)) order-confirmation-configuration

        (if show-order-global-discount
      	    ;; -> CLOBAL DISCOUNT VISIBLE ON ORDER CONFIRMATION
      	    (progn
	      (sapr3-write-chars container-handle "VISIBL_DISC_ABS" order-global-discount-absolute-amount  15 sap-uc-encoding)
	      (sapr3-write-chars container-handle "VISIBL_DISC_PRC" order-global-discount-percentage        5 sap-uc-encoding))
      	    ;; -> CLOBAL DISCOUNT NOT VISIBLE ON ORDER CONFIRMATION
      	    (progn
	      (sapr3-write-chars container-handle "UNVISIBL_DISC_ABS" order-global-discount-absolute-amount  15 sap-uc-encoding)
	      (sapr3-write-chars container-handle "UNVISIBL_DISC_PRC" order-global-discount-percentage        5 sap-uc-encoding)))

	(if show-order-global-surcharge
      	    ;; -> CLOBAL SURCHARGE VISIBLE ON ORDER CONFIRMATION
      	    (progn
	      (sapr3-write-chars container-handle "VISIBL_SURC_ABS" order-global-surcharge-absolute-amount  15 sap-uc-encoding)
	      (sapr3-write-chars container-handle "VISIBL_SURC_PRC" order-global-surcharge-percentage        5 sap-uc-encoding))
      	    ;; -> CLOBAL SURCHARGE NOT VISIBLE ON ORDER CONFIRMATION
      	    (progn
	      (sapr3-write-chars container-handle "UNVISIBL_SURC_ABS" order-global-surcharge-absolute-amount  15 sap-uc-encoding)
	      (sapr3-write-chars container-handle "UNVISIBL_SURC_PRC" order-global-surcharge-percentage        5 sap-uc-encoding))))

      (sapr3-write-number container-handle "RESULTING_PRICE" order-global-total-price 15 2 t sap-uc-encoding)

      (with-slots ((ship-when-complete tb.msgs:ship-when-complete)) order-logistics-configuration
	(if ship-when-complete
      	    (rfc-set-chars container-handle "SHIP_WHEN_COMPL" "X" 1 sap-uc-encoding)
      	    (rfc-set-chars container-handle "SHIP_WHEN_COMPL" "0" 1 sap-uc-encoding)))

      (with-slots ((terms tb.msgs:terms)
      		   (location tb.msgs:location)) order-level-incoterms
	(sapr3-write-chars container-handle "INCOTERMS"     terms      3 sap-uc-encoding)
	(sapr3-write-chars container-handle "INCOTERMS_LOC" location  28 sap-uc-encoding))

      (sapr3-write-chars container-handle "PAYMENT_TERM"    order-payment-terms-code             4 sap-uc-encoding)
      (sapr3-write-chars container-handle "DESIRED_DATE"    order-global-desired-delivery-date   8 sap-uc-encoding)
      (sapr3-write-chars container-handle "PURCHASE_NR"     customer-purchase-nr                35 sap-uc-encoding)
      (sapr3-write-chars container-handle "PURCHASE_DATE"   customer-purchase-date               8 sap-uc-encoding)
      (sapr3-write-chars container-handle "DISTR_CHAN"      sales-channel                        2 sap-uc-encoding)
      (sapr3-write-chars container-handle "SALES_ORG"       sales-organisation                   4 sap-uc-encoding)
      (sapr3-write-chars container-handle "SALES_OFFICE"    sales-office                         4 sap-uc-encoding)
      (sapr3-write-chars container-handle "SALES_GROUP"     sales-group                          3 sap-uc-encoding)))

  (values))


;; Tabelle ZMAT_T_SALES_ORDER_ITEMS / Struktur ZMAT_S_SALES_ORDER_ITEMS
;;
;; Attribut	        Format	Länge	Dez.-Stellen	Beschreibung	JSON Dokument Objekt
;; ITM_NUMBER	        NUMC	6	0	Verkaufsbelegposition	pos-nr
;; MATERIAL	        CHAR	18	0	Materialnummer	item-nr
;; MATERIAL_VERSION	CHAR	4	0	Material version	item-version
;; MATERIAL_REVISION	CHAR	4	0	Material revision	item-revision
;; MATERIAL_QTY	        QUAN	13	3	Zielmenge in Verkaufsmengeneinheit	quantity
;; MATERIAL_QU	        UNIT	3	0	Zielmengeneinheit	uom
;; MATERIAL_PRICE	CURR	15	2	Materialpreis des Line Items	resulting-price
;; SHOW_LINE_ITEM	CHAR	1	0	Anzeige des Line Items in der Order	show-line-item-on-order
;; VISIBL_DISC_ABS	CURR	15	2	Sichbarer Discount absolut	customer-visible-discount-amount
;; UNVISIBL_DISC_ABS	CURR	15	2	Unsichtbarer Discount absolut	customer-invisible-discount-amount
;; VISIBL_DISC_PRC	DEC	5	2	Sichtbarer Discount in Prozent	customer-visible-discount-percentage
;; UNVISIBL_DISC_PRC	DEC	5	2	Unsichtbarer Discount in Prozent	customer-invisible-discount-percentage
;; VISIBL_SURC_ABS	CURR	15	2	Sichtbarer Zuschlag absolut	customer-visible-surcharge-amount
;; UNVISIBL_SURC_ABS	CURR	15	2	Unsichtbarer Zuschlag absolut	customer-invisible-surcharge-amount
;; VISIBL_SURC_PRC	DEC	5	2	Sichtbarer Zuschlag in Prozent	customer-visible-surcharge-percentage
;; UNVISIBL_SURC_PRC	DEC	5	2	Unsichtbarer Zuschlag in Prozent	customer-invisible-surcharge-percentage
;; RESULTING_PRICE	CURR	15	2	Resultierender Line Item Preis	resulting-price
;; CALC_DELIVERY_DATE	CHAR	10	0	Einteilungsdatum
;; COMM_DELIVERY_DATE	CHAR	10	0	Commited Delivery Date
;; PAYMENT_TERM	        CHAR	4	0	Zahlungsbedingungsschlüssel	payment-term-code
;; INCOTERMS	        CHAR	3	0	Incoterms Teil 1	incorterms-terms
;; INCOTERMS_LOC	CHAR	28	0	Incoterms Teil 2	incoterms-location
;; PLANT	        CHAR	4	0	Werk	plant
;; LI_PRICE_VISIBLE	CHAR	1	0	allgemeines flag	show-line-item-unit-price

(defun sapr3-write-it-line-items-from-request-sales-order-update (container-handle request-sales-order-update &key (sap-uc-encoding (rfc-sap-uc-encoding)))

  (let* ((sales-order (tb.msgs:sales-order request-sales-order-update))
	 (line-items  (tb.msgs:order-line-items sales-order))
	 (nr-items    (length line-items)))

    (if (> nr-items 0)
	(progn
	  (rfc-delete-all-rows container-handle)
	  (rfc-append-new-rows container-handle nr-items)
	  (rfc-move-to-first-row container-handle)

	  ;; INSERT LINES INTO TABLE

	  (loop for index from 0 to (1- nr-items)
	     do
	       (rfc-move-to container-handle index)

	       (let ((row-handle (rfc-get-current-row container-handle))
		     (line-item (aref line-items index)))

		 (if (not (cffi:null-pointer-p row-handle))

		     (with-slots ((pos-nr tb.msgs:pos-nr)
		     		  (item-nr tb.msgs:item-nr)
		     		  (item-version tb.msgs:item-version)
		     		  (item-revision tb.msgs:item-revision)
		     		  (quantity tb.msgs:quantity)
		     		  (uom tb.msgs:uom)
		     		  (ship-from tb.msgs:ship-from)
		     		  (show-line-item-on-order tb.msgs:show-line-item-on-order)
		     		  (line-item-calculated-delivery-date tb.msgs:line-item-calculated-delivery-date)
		     		  (line-item-committed-delivery-date tb.msgs:line-item-committed-delivery-date)
		     		  (line-item-payment-terms-code tb.msgs:line-item-payment-terms-code)
		     		  (line-item-level-incoterms tb.msgs:line-item-level-incoterms)
		     		  (line-item-customer tb.msgs:line-item-customer)
		     		  (line-item-price-information tb.msgs:line-item-price-information)
		     		  (line-item-comments tb.msgs:line-item-comments)) line-item

		       (with-slots ((environment tb.msgs:environment)
		     		    (company-nr tb.msgs:company-nr)
		     		    (warehouse-nr tb.msgs:warehouse-nr)
		     		    (plant-nr tb.msgs:plant-nr)) ship-from

			 (with-slots ((unit-price tb.msgs:unit-price)
		     		      (customer-visible-surcharge-percentage tb.msgs:customer-visible-surcharge-percentage)
		     		      (customer-visible-discount-percentage tb.msgs:customer-visible-discount-percentage)
		     		      (customer-invisible-surcharge-percentage tb.msgs:customer-invisible-surcharge-percentage)
		     		      (customer-invisible-discount-percentage tb.msgs:customer-invisible-discount-percentage)
		     		      (customer-visible-surcharge-amount tb.msgs:customer-visible-surcharge-amount)
		     		      (customer-visible-discount-amount tb.msgs:customer-visible-discount-amount)
		     		      (customer-invisible-surcharge-amount tb.msgs:customer-invisible-surcharge-amount)
		     		      (customer-invisible-discount-amount tb.msgs:customer-invisible-discount-amount)
		     		      (line-item-resulting-price tb.msgs:line-item-resulting-price)
		     		      (show-line-item-price tb.msgs:show-line-item-price)) line-item-price-information

			   (sapr3-write-chars row-handle "ITM_NUMBER"           pos-nr          6 sap-uc-encoding)
			   (sapr3-write-chars row-handle "MATERIAL"             item-nr        18 sap-uc-encoding)
			   (sapr3-write-chars row-handle "MATERIAL_VERSION"     item-version    4 sap-uc-encoding)
			   (sapr3-write-chars row-handle "MATERIAL_REVISION"    item-revision   4 sap-uc-encoding)

			   (if quantity
			       (let ((value (etypecase quantity
					      (integer (format nil "~d" quantity))
					      (float   (format nil "~3$" quantity))
					      (string  quantity))))
				 (sapr3-write-chars row-handle "MATERIAL_QTY" value 13 sap-uc-encoding)))
			   (if uom
			       (let ((value (tb.core:map-value :THINGBONE :SAP-SERVB nil nil uom)))
				 (sapr3-write-chars row-handle "MATERIAL_QU" value  3 sap-uc-encoding)))

			   ;;(sapr3-write-chars row-handle "MATERIAL_PRICE"       unit-price     15 sap-uc-encoding)
			   (sapr3-write-number row-handle "MATERIAL_PRICE" unit-price 15 2 t sap-uc-encoding)

			   (if show-line-item-on-order
			       (rfc-set-chars row-handle "SHOW_LINE_ITEM" "X" 1 sap-uc-encoding)
			       (rfc-set-chars row-handle "SHOW_LINE_ITEM" "0" 1 sap-uc-encoding))

			   (sapr3-write-chars row-handle "VISIBL_DISC_ABS"     customer-visible-discount-amount         15 sap-uc-encoding)
			   (sapr3-write-chars row-handle "UNVISIBL_DISC_ABS"   customer-invisible-discount-amount       15 sap-uc-encoding)
			   (sapr3-write-chars row-handle "VISIBL_DISC_PRC"     customer-visible-discount-percentage      5 sap-uc-encoding)
			   (sapr3-write-chars row-handle "UNVISIBL_DISC_PRC"   customer-invisible-discount-percentage    5 sap-uc-encoding)

			   (sapr3-write-chars row-handle "VISIBL_SURC_ABS"     customer-visible-surcharge-amount        15 sap-uc-encoding)
			   (sapr3-write-chars row-handle "UNVISIBL_SURC_ABS"   customer-invisible-surcharge-amount      15 sap-uc-encoding)
			   (sapr3-write-chars row-handle "VISIBL_SURC_PRC"     customer-visible-surcharge-percentage     5 sap-uc-encoding)
			   (sapr3-write-chars row-handle "UNVISIBL_SURC_PRC"   customer-invisible-surcharge-percentage   5 sap-uc-encoding)

			   ;;(sapr3-write-chars row-handle "RESULTING_PRICE"     line-item-resulting-price            15 sap-uc-encoding)
			   (sapr3-write-number row-handle "RESULTING_PRICE"     line-item-resulting-price  15 2 t sap-uc-encoding)

			   (sapr3-write-chars row-handle "CALC_DELIVERY_DATE"  line-item-calculated-delivery-date   10 sap-uc-encoding)
			   (sapr3-write-chars row-handle "COMM_DELIVERY_DATE"  line-item-committed-delivery-date    10 sap-uc-encoding)
			   (sapr3-write-chars row-handle "PAYMENT_TERM"        line-item-payment-terms-code          4 sap-uc-encoding)

			   (with-slots ((terms tb.msgs:terms)
					(location tb.msgs:location)) line-item-level-incoterms
			     (sapr3-write-chars row-handle "INCOTERMS"     terms      3 sap-uc-encoding)
			     (sapr3-write-chars row-handle "INCOTERMS_LOC" location  28 sap-uc-encoding))

			   (sapr3-write-chars row-handle "PLANT" plant-nr   4 sap-uc-encoding)

			   (if show-line-item-price
			       (rfc-set-chars row-handle "LI_PRICE_VISIBLE" "X" 1 sap-uc-encoding)
			       (rfc-set-chars row-handle "LI_PRICE_VISIBLE" "0" 1 sap-uc-encoding))

			   )))))))))
  (values))

;; Tabelle Struktur ZMAT_T_SALES_ORDER_COMMENT_LI  / Struktur ZMAT_S_SALES_ORDER_COMMENT_LI
;;
;; Attribut	Format	Länge	Dez.-Stellen	Beschreibung	JSON Dokument Objekt
;; ITM_NUMBER	NUMC	6	0	Verkaufsbelegposition
;; TEXT_TYPE	CHAR	64	0	Text Typ
;; LINE_INDEX	INT4	10	0	Zeilennummer
;; COMMENT_TEXT	CHAR	132	0	Textzeile
;; LAISO3	CHAR	3	0	Sprache (z.B. ger, eng, usw)

;; (Defun

(defun sapr3-write-it-comments-from-request-sales-order-update (container-handle request-sales-order-update &key (sap-uc-encoding (rfc-sap-uc-encoding)))

  (let* ((sales-order              (tb.msgs:sales-order request-sales-order-update))
	 (order-global-comments    (tb.msgs:order-global-comments sales-order))
	 (nr-order-global-comments (length order-global-comments))
	 (line-items               (tb.msgs:order-line-items sales-order))
	 (nr-items                 (length line-items))
	 (row-index                0))

    (rfc-delete-all-rows container-handle)

    (if (> nr-order-global-comments 0)
	(progn
	  (rfc-append-new-rows container-handle nr-order-global-comments)
	  (rfc-move-to-first-row container-handle)

	  ;; INSERT LINES INTO TABLE

	  (let ((itm-number "0"))
	    (loop for index from 0 to (1- nr-order-global-comments)
	       do
		 (let ((comment (aref order-global-comments index)))
		   (if comment
		       (progn
			 (rfc-move-to container-handle row-index)
			 (let ((row-handle (rfc-get-current-row container-handle)))
			   (if (not (cffi:null-pointer-p row-handle))
			       (with-slots ((lang                  tb.msgs:lang)
					    (comment-line-index-nr tb.msgs:comment-line-index-nr)
					    (comment-category      tb.msgs:comment-category)
					    (text                  tb.msgs:text)) comment
				 (sapr3-write-chars row-handle "ITM_NUMBER" itm-number         6 sap-uc-encoding)
				 (sapr3-write-chars row-handle "TEXT_TYPE"  comment-category  64 sap-uc-encoding)
				 (if comment-line-index-nr
				     (let ((value (etypecase comment-line-index-nr
						    (integer (format nil "~d" comment-line-index-nr))
						    (string  comment-line-index-nr))))
				       (sapr3-write-chars row-handle "LINE_INDEX"  value  64 sap-uc-encoding)))
				 (sapr3-write-chars row-handle "COMMENT_TEXT" text 132 sap-uc-encoding)
				 (sapr3-write-chars row-handle "LAISO3"       lang   3 sap-uc-encoding)
				 (incf row-index)))))))))))

    (if (> nr-items 0)
	(loop for index from 0 to (1- nr-items)
	   do
	     (let ((line-item (aref line-items index)))
	       (if line-item
		   (let* ((comments (tb.msgs:line-item-comments line-item))
			  (nr-comments (length comments)))
		     (if (> nr-comments 0)
			 (progn
			   (rfc-append-new-rows container-handle nr-comments)
			   (loop for comment-index from 0 to (1- nr-comments)
			      do
				(let ((comment    (aref comments comment-index))
				      (itm-number (tb.msgs:pos-nr line-item)))
				  (if comment
				      (progn
					(rfc-move-to container-handle row-index)
					(let ((row-handle (rfc-get-current-row container-handle)))
					  (with-slots ((lang                  tb.msgs:lang)
						       (comment-line-index-nr tb.msgs:comment-line-index-nr)
						       (comment-category      tb.msgs:comment-category)
						       (text                  tb.msgs:text)) comment
					    (sapr3-write-chars row-handle "ITM_NUMBER" itm-number         6 sap-uc-encoding)
					    (sapr3-write-chars row-handle "TEXT_TYPE"  comment-category  64 sap-uc-encoding)
					    (if comment-line-index-nr
						(let ((value (etypecase comment-line-index-nr
							       (integer (format nil "~d" comment-line-index-nr))
							       (string  comment-line-index-nr))))
						  (sapr3-write-chars row-handle "LINE_INDEX"  value  64 sap-uc-encoding)))
					    (sapr3-write-chars row-handle "COMMENT_TEXT" text 132 sap-uc-encoding)
					    (sapr3-write-chars row-handle "LAISO3"       lang   3 sap-uc-encoding)
					    (incf row-index)))))))))))))))

  (values))

(defun sapr3-write-it-partners-from-request-sales-order-update (container-handle request-sales-order-update &key (sap-uc-encoding (rfc-sap-uc-encoding)))

  (let* ((sales-order                    (tb.msgs:sales-order request-sales-order-update))
	 (order-global-business-partners (tb.msgs:order-global-customer sales-order))
	 (order-global-itm-number        "0")
	 (line-items                     (tb.msgs:order-line-items sales-order))
	 (nr-items                       (length line-items))
	 (row-index                      0))

    (rfc-delete-all-rows container-handle)

    (setq row-index (sapr3-write-it-partners-from-business-partners container-handle order-global-business-partners order-global-itm-number row-index :sap-uc-encoding sap-uc-encoding))

    (if (> nr-items 0)
	(loop for index from 0 to (1- nr-items)
	   do
	     (let ((line-item (aref line-items index)))
	       (if line-item
		   (let ((line-item-customer (tb.msgs:line-item-customer line-item))
			 (itm-number (tb.msgs:pos-nr line-item)))
		     (setq row-index (sapr3-write-it-partners-from-business-partners container-handle line-item-customer itm-number row-index :sap-uc-encoding sap-uc-encoding))))))))

  (values))

(defun sapr3-write-iv-sales-order-nr-from-request-sales-order-update (container-handle request-sales-order-update &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let* ((sales-order (tb.msgs:sales-order request-sales-order-update))
	 (order-nr (tb.msgs:order-nr sales-order)))
    (sapr3-write-chars container-handle "IV_SALES_ORDER_NR" order-nr 10 sap-uc-encoding))
  (values))


;; Tabelle  ZMAT_T_SALES_ORDER_ADDRESS / Struktur ZMAT_S_SALES_ORDER_ADDRESS
;;
;; Attribut	Format	Länge	Dez.-Stellen	Beschreibung	JSON Dokument Objekt
;; NAME	        CHAR	40	0	Name 1	addressee-name
;; STREET	CHAR	40	0	Straße	address-line-4
;; CITY	        CHAR	40	0	Ort	city
;; POSTAL_CODE	CHAR	10	0	Postleitzahl des Orts	postal-code
;; STATE_CODE	CHAR	3	0	Region (Bundesstaat, Bundesland, Provinz, Grafschaft)	state-code
;; COUNTRY	CHAR	3	0	Länderschlüssel	country-code
;; REGION	CHAR	32	0	Region	region

(defun sapr3-write-it-address-from-request-sales-order-update (container-handle request-sales-order-update &key (sap-uc-encoding (rfc-sap-uc-encoding)))

  (let* ((sales-order  (tb.msgs:sales-order request-sales-order-update))
	 (line-items   (tb.msgs:order-line-items sales-order))
	 (nr-items     (length line-items))
	 (row-index    0))

    ;; ALLOCATE TABLE ROWS

    (rfc-delete-all-rows container-handle)

    ;; INSERT LINES INTO TABLE

    (let* ((itm-number "0")
	   (ship-to-address (tb.msgs:ship-to-address sales-order)))

      (if ship-to-address
	  (progn
	    (rfc-append-new-row container-handle)
	    (rfc-move-to-first-row container-handle)

	    (rfc-move-to container-handle row-index)
	    (let ((row-handle (rfc-get-current-row container-handle)))
	      (sapr3-write-single-ship-to-address row-handle ship-to-address itm-number :sap-uc-encoding sap-uc-encoding))
	    (incf row-index))))

    (if (> nr-items 0)
	(loop for index from 0 to (1- nr-items)
	   do
	     (let ((line-item (aref line-items index)))
	       (if line-item
		   (let ((ship-to-address (tb.msgs:ship-to-address line-item)))
		     (if ship-to-address
			 (progn
			   (rfc-append-new-row container-handle)
			   (rfc-move-to container-handle row-index)
			   (let ((row-handle (rfc-get-current-row container-handle))
				 (itm-number (tb.msgs:pos-nr line-item)))
			     (sapr3-write-single-ship-to-address row-handle ship-to-address itm-number :sap-uc-encoding sap-uc-encoding)
			     (incf row-index))))))))))

  (values))

;; +++ EXPORTING +++

;; Exporting-Parameter
;;
;; Parameter	        Data element	Type	Length	Hinweis
;; EV_SALES_ORDER_NR	VBELN_VA	CHAR	10	Sales Order Nr
;; EV_TAX	        NETWR	        CURR	15 (Dez.-Stellen: 2)	MWST Betrag

(defun sapr3-read-ev-tax-for-response-sales-order-update (container-handle response-sales-order-update &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let ((sales-order-update-result (tb.msgs:sales-order-update-result response-sales-order-update))
	(order-global-tax-amount (rfc-get-float container-handle "EV_TAX" sap-uc-encoding)))
    (setf (tb.msgs:order-global-tax-amount sales-order-update-result) order-global-tax-amount)
    (log:debug "order-global-tax-amount = ~S" order-global-tax-amount))
  (values))

;; Tabelle ZMAT_T_SALES_ORDER_RETURN/ Struktur ZMAT_S_SALES_ORDER_RETURN
;;
;; Attribut	        Format	Länge	Dez.-Stellen	Beschreibung
;; MSG_LINE	        INT4	10	0	Zeilennummer
;; MSG_ITM_NUMBER	NUMC	6	0	Verkaufsbelegposition
;; MSG_LEVEL	        CHAR	1	0	Type
;; MSG_MESSAGE	        CHAR	256	0	Meldung
;; MSG_LANGUAGE	        CHAR	3	0	Sprache der Meldung
;; MSG_SOURCE	        CHAR	32	0	Quelle der Meldung
;; MSG_TIMESTAMP	DEC	21	7	UTC-Zeitstempel in Langform (JJJJMMTThhmmssmmmuuun)

(defmethod add-request-result-info ((sales-order-update-result tb.msgs:tb-sales-order-update-result) (result-info tb.msgs:tb-request-result-info))
  (vector-push-extend result-info (tb.msgs:request-result-info sales-order-update-result)))

(defun sapr3-read-et-messages-for-response-sales-order-update (container-handle response-sales-order-update &key (sap-uc-encoding (rfc-sap-uc-encoding)))

  (let* ((sales-order-update-result (tb.msgs:sales-order-update-result response-sales-order-update))
	 (order-line-items-results (tb.msgs:order-line-items-results sales-order-update-result))
	 (nr-order-line-item-results (if order-line-items-results
					 (length order-line-items-results)
					 0))
	 (messages (sapr3-get-t-sales-order-return container-handle 'tb.msgs:tb-request-result-info :sap-uc-encoding sap-uc-encoding))
	 (nr-messages (length messages)))

    (if (> nr-messages 0)

	;; SET ORDER GLOBAL RESULTS
	(loop for index from 0 to (1- nr-messages)
	   do
	     (let ((r-r-info (aref messages index)))
	       (if (= (tb.msgs:pos-nr r-r-info) 0)
		   (add-request-result-info sales-order-update-result r-r-info))))


	;; SET ORDER LINE ITEM RESULTS
	(if (> nr-order-line-item-results 0)
	    (loop for index from 0 to (1- nr-messages)
	       do
		 (let ((r-r-info (aref messages index)))
		   (if (not (= (tb.msgs:pos-nr r-r-info) 0))
		       (loop for o-l-i-r-index from 0 to (1- nr-order-line-item-results)
			  do
			    (let ((order-line-item-result (aref order-line-items-results o-l-i-r-index))
				  (r-r-info (aref messages index)))
			      (if (= (pos-nr order-line-item-result) (pos-nr r-r-info))
				  (progn
				    (add-request-result-info order-line-item-result r-r-info)))))))))))

  (values))


(defun sapr3-read-et-detail-status-for-response-sales-order-update (container-handle response-sales-order-update &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let* ((sales-order-update-result (tb.msgs:sales-order-update-result response-sales-order-update))
	 (nr-entries (rfc-get-row-count container-handle)))
    (loop for index from 0 to (1- nr-entries)
       do
	 (rfc-move-to container-handle index)
	 (let ((detail-status-info (sapr3-get-s-sales-order-status (rfc-get-current-row container-handle) :sap-uc-encoding sap-uc-encoding)))
	   (if (= (tb.msgs:pos-nr detail-status-info) 0)
	       (setf (tb.msgs:order-global-status-info sales-order-update-result) detail-status-info)
	       (vector-push-extend detail-status-info (tb.msgs:order-line-items-results sales-order-update-result))))))
  (values))

;; +++ FUNCTION MODULE +++

(defun z-mat-s-o-upd-req (connection-handle extended-msg &key (sap-uc-encoding (rfc-sap-uc-encoding)))

  (let* ((request-sales-order-update (tb.msgs:data (tb.msgs:content extended-msg)))
  	 (response-sales-order-update (make-sales-order-update-response-from-extended-msg extended-msg))
  	 (fm-name "Z_MAT_S_O_UPD_REQ")
	 (func-handle (let ((handle (rfc-function connection-handle fm-name)))
			(if (null handle)
			    (tb.sapnwrfc:signal-rfc-invalid-parameter "FM NAME" "Z_MAT_S_O_UPD_REQ"))
			handle))
	 ;; IMPORTING
	 (is-header-handle     (let ((handle (rfc-get-structure func-handle "IS_HEADER")))
				 (if (null handle)
				     (tb.sapnwrfc:signal-rfc-invalid-parameter "STRUCTURE NAME" "IS_HEADER"))
				 handle))
	 (it-line-items-handle (let ((handle (rfc-get-table func-handle "IT_LINE_ITEMS")))
				 (if (null handle)
				     (tb.sapnwrfc:signal-rfc-invalid-parameter "TABLE NAME" "IT_LINE_ITEMS"))
				 handle))
	 (it-comments-handle   (let ((handle (rfc-get-table func-handle "IT_COMMENTS")))
				 (if (null handle)
				     (tb.sapnwrfc:signal-rfc-invalid-parameter "TABLE NAME" "IT_COMMENTS"))
				 handle))
	 (it-partners-handle   (let ((handle (rfc-get-table func-handle "IT_PARTNERS")))
				 (if (null handle)
				     (tb.sapnwrfc:signal-rfc-invalid-parameter "TABLE NAME" "IT_PARTNERS"))
				 handle))
	 (it-address-handle    (let ((handle (rfc-get-table func-handle "IT_ADDRESS")))
				 (if (null handle)
				     (tb.sapnwrfc:signal-rfc-invalid-parameter "TABLE NAME" "IT_ADDRESS"))
				 handle)))

    ;; WRITE DATA INTO IMPORTING STRUCTURES AND TABLES

    (sapr3-write-is-header-from-request-sales-order-update         is-header-handle     request-sales-order-update :sap-uc-encoding sap-uc-encoding)
    (sapr3-write-it-line-items-from-request-sales-order-update     it-line-items-handle request-sales-order-update :sap-uc-encoding sap-uc-encoding)
    (sapr3-write-it-comments-from-request-sales-order-update       it-comments-handle   request-sales-order-update :sap-uc-encoding sap-uc-encoding)
    (sapr3-write-it-partners-from-request-sales-order-update       it-partners-handle   request-sales-order-update :sap-uc-encoding sap-uc-encoding)
    (sapr3-write-it-address-from-request-sales-order-update        it-address-handle    request-sales-order-update :sap-uc-encoding sap-uc-encoding)

    (sapr3-write-iv-sales-order-nr-from-request-sales-order-update func-handle          request-sales-order-update :sap-uc-encoding sap-uc-encoding)

    ;; INVOKE FUNCTION

    (let* ((rc                (rfc-invoke connection-handle func-handle))
	   (servb-return-info (get-servb-return-info-from-function-handle func-handle)))
      (setf (rfc-return-code servb-return-info) rc)
      (setf (fm-name servb-return-info) fm-name)

      ;; READ DATA FROM EXPORTING VALUES, STRUCTURES AND TABLES

      (if (not (errorp servb-return-info))
	  (progn
	    (let ((et-messages-handle       (rfc-get-table func-handle "ET_MESSAGES"))
		  (et-detail-status-handle  (rfc-get-table func-handle "ET_DETAIL_STATUS")))
	      (sapr3-read-ev-tax-for-response-sales-order-update            func-handle              response-sales-order-update :sap-uc-encoding sap-uc-encoding)
	      ;; Order matters here with the next two calls!
	      (sapr3-read-et-detail-status-for-response-sales-order-update  et-detail-status-handle  response-sales-order-update :sap-uc-encoding sap-uc-encoding)
	      (sapr3-read-et-messages-for-response-sales-order-update       et-messages-handle       response-sales-order-update :sap-uc-encoding sap-uc-encoding)
	      )))
      (tb.core:send (sap-servb-return-info-log-channel) servb-return-info)
      (values response-sales-order-update servb-return-info))))


(defun sap-direct-request-sales-order-update (servb-sap-rfc-connection extended-msg-with-request-sales-order-update)
  (check-type servb-sap-rfc-connection servb-sap-rfc-connection)
  (check-type extended-msg-with-request-sales-order-update tb.msgs:extended-msg)
  (with-rfc-connection-ensured (servb-sap-rfc-connection "SAP-DIRECT-REQUEST-SALES-ORDER-UPDATE")
    (multiple-value-bind (response-sales-order-update servb-return-info)
	(z-mat-s-o-upd-req (connection-handle servb-sap-rfc-connection) extended-msg-with-request-sales-order-update)
      (setf (tb.msgs:data (tb.msgs:content response-sales-order-update))
	    (make-array 2 :initial-contents (list (tb.msgs:data-context response-sales-order-update)
						  (tb.msgs:sales-order-update-result response-sales-order-update))))
      (values response-sales-order-update servb-return-info))))
