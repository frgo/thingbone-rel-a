;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

(defmethod sap-container-handle-from-sap-func-handle ((self tb-order-confirmation-configuration) func-handle)
  (rfc-get-structure func-handle "IS_HEADER"))

(defmethod servb-to-sap ((self tb-order-confirmation-configuration) func-handle container-handle  &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (declare (ignore func-handle container-handle sap-uc-encoding))
  ;; NOTE:
  ;; Nothing to be done here. The flags control price, discount and surcharge storage in visible-... or unvisible-... fields of the values.
  self)

(defmethod sap-to-servb ((self tb-order-confirmation-configuration) func-handle container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (declare (ignore func-handle container-handle sap-uc-encoding))
  self)
