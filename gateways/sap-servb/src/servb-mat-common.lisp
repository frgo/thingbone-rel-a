;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

(defun sapr3-get-t-sales-order-return (container-handle instance-class &key (sap-uc-encoding (rfc-sap-uc-encoding))
									 )

  (let* ((nr-messages (rfc-get-row-count container-handle))
	 (messages (make-array nr-messages :fill-pointer 0 :adjustable t)))

    (if (> nr-messages 0)
	(progn

	  ;; READ ET-MESSAGES FROM SAP
	  (loop for index from 0 to (1- nr-messages)
	     do
	       (rfc-move-to container-handle index)

	       (let* ((row-handle     (rfc-get-current-row container-handle))
		      (msg-itm-number (rfc-get-int   row-handle "MSG_ITM_NUMBER" sap-uc-encoding))
		      (msg-line       (rfc-get-int   row-handle "MSG_LINE"       sap-uc-encoding))
		      (msg-level      (rfc-get-int   row-handle "MSG_LEVEL"      sap-uc-encoding))
		      (msg-message    (rfc-get-chars row-handle "MSG_MESSAGE"    256 sap-uc-encoding))
		      (msg-language   (rfc-get-chars row-handle "MSG_LANGUAGE"     3 sap-uc-encoding))
		      (msg-source     (rfc-get-chars row-handle "MSG_SOURCE"      32 sap-uc-encoding))
		      (msg-timestamp  (rfc-get-chars row-handle "MSG_TIMESTAMP"   22 sap-uc-encoding))
		      (instance (make-instance instance-class
					       :pos-nr     msg-itm-number
					       :line-nr    msg-line
					       :level      msg-level
					       :lang       (cl-strings:clean msg-language)
					       :text       (cl-strings:clean msg-message)
					       :source     (cl-strings:clean msg-source)
					       :timestamp  (cl-strings:clean msg-timestamp))))

		 (vector-push-extend instance messages)))
	  messages)
	nil)))

;; Tabelle ZMAT_T_SALES_ORDER_STATUS/ Struktur ZMAT_S_SALES_ORDER_STATUS
;;
;; Attribut	        Format	Länge	Dez.-Stellen	Beschreibung	JSON Dokument Objekt
;; ITM_NUMBER	        NUMC	6	0	Zeilennummer
;; MATERIAL	        CHAR	18	0	Materialnummer	item-nr
;; MATERIAL_VERSION	CHAR	4	0	Materialversion	item-version
;; MATERIAL_REVISION	CHAR	4	0	Materialrevision	item-revision
;; CANCEL_STATUS	CHAR	20	0	Statusbezeichnung	order-cancellation-status
;; CONFIRM_STATUS	CHAR	20	0	Statusbezeichnung	order-confirmation-status
;; BOOKING_STATUS	CHAR	20	0	Statusbezeichnung	order-invoice-booking-status
;; CREDIT_CH_STATUS	CHAR	20	0	Statusbezeichnung	order-credit-check-status
;; DELAY_STATUS	        CHAR	20	0	Statusbezeichnung	order-delay-status
;; BLOCKING_STATUS	CHAR	20	0	Statusbezeichnung	order-invoice-blocking-status
;; RECEIVED_STATUS	CHAR	20	0	Statusbezeichnung	order-goods-received-status"
;; COMMISION_STATUS	CHAR	20	0	Statusbezeichnung	order-commisioning-status
;; PACKING_STATUS	CHAR	20	0	Statusbezeichnung	order-packing-status
;; DELIVERY_STATUS	CHAR	20	0	Statusbezeichnung	order-delivery-status
;; COMPLETED_STATUS	CHAR	20	0	Statusbezeichnung	order-delivery-completed-status
;; REVENUE_STATUS	CHAR	20	0	Statusbezeichnung	order-revenue-booking-status
;; STAGING_STATUS	NUMC	2	0	Staging Status Sales Order	order-staging-status
;; LAISO3	        CHAR	3	0	Sprache

(defun sapr3-get-s-sales-order-status (container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let* ((pos-nr                  (rfc-get-int container-handle "ITM_NUMBER" sap-uc-encoding))
	 (item-nr                 (cl-strings:clean (rfc-get-chars container-handle "MATERIAL"              18 sap-uc-encoding)))
	 (item-version            (cl-strings:clean (rfc-get-chars container-handle "MATERIAL_VERSION"       4 sap-uc-encoding)))
	 (item-revision           (cl-strings:clean (rfc-get-chars container-handle "MATERIAL_REVISION"      4 sap-uc-encoding)))
	 (cancellation-status     (cl-strings:clean (rfc-get-chars container-handle "CANCEL_STATUS"         20 sap-uc-encoding)))
	 (confirmation-status     (cl-strings:clean (rfc-get-chars container-handle "CONFIRM_STATUS"        20 sap-uc-encoding)))
	 (invoice-booking-status  (cl-strings:clean (rfc-get-chars container-handle "BOOKING_STATUS"        20 sap-uc-encoding)))
	 (credit-check-status     (cl-strings:clean (rfc-get-chars container-handle "CREDIT_CH_STATUS"      20 sap-uc-encoding)))
	 (delay-status            (cl-strings:clean (rfc-get-chars container-handle "DELAY_STATUS"          20 sap-uc-encoding)))
	 (invoice-blocking-status (cl-strings:clean (rfc-get-chars container-handle "BLOCKING_STATUS"       20 sap-uc-encoding)))
	 (goods-received-status   (cl-strings:clean (rfc-get-chars container-handle "RECEIVED_STATUS"       20 sap-uc-encoding)))
	 (commissioning-status    (cl-strings:clean (rfc-get-chars container-handle "COMMISION_STATUS"      20 sap-uc-encoding)))
	 (packing-status          (cl-strings:clean (rfc-get-chars container-handle "PACKING_STATUS"        20 sap-uc-encoding)))
	 (delivery-status         (cl-strings:clean (rfc-get-chars container-handle "DELIVERY_STATUS"       20 sap-uc-encoding)))
	 (revenue-booking-status  (cl-strings:clean (rfc-get-chars container-handle "REVENUE_STATUS"        20 sap-uc-encoding)))
	 (staging-status          (cl-strings:clean (rfc-get-chars container-handle "STAGING_STATUS"         2 sap-uc-encoding)))
	 (lang                    (cl-strings:clean (rfc-get-chars container-handle "LAISO3"                 3 sap-uc-encoding))))
    (if (= pos-nr 0)
	(make-instance 'tb.msgs:tb-order-global-status-info
		       :pos-nr pos-nr
		       :item-nr item-nr
		       :item-version item-version
		       :item-revision item-revision
		       :cancellation-status cancellation-status
		       :confirmation-status confirmation-status
		       :invoice-booking-status invoice-booking-status
		       :credit-check-status credit-check-status
		       :delay-status delay-status
		       :invoice-blocking-status invoice-blocking-status
		       :goods-received-status goods-received-status
		       :commissioning-status commissioning-status
		       :packing-status packing-status
		       :delivery-status delivery-status
		       :revenue-booking-status revenue-booking-status
		       :staging-status staging-status
		       :lang lang)
	(make-instance 'tb.msgs:tb-order-line-item-status-info
		       :pos-nr pos-nr
		       :item-nr item-nr
		       :item-version item-version
		       :item-revision item-revision
		       :cancellation-status cancellation-status
		       :confirmation-status confirmation-status
		       :invoice-booking-status invoice-booking-status
		       :credit-check-status credit-check-status
		       :delay-status delay-status
		       :invoice-blocking-status invoice-blocking-status
		       :goods-received-status goods-received-status
		       :commissioning-status commissioning-status
		       :packing-status packing-status
		       :delivery-status delivery-status
		       :revenue-booking-status revenue-booking-status
		       :staging-status staging-status
		       :lang lang))))


;; Tabelle  ZMAT_T_SALES_ORDER_ADDRESS / Struktur ZMAT_S_SALES_ORDER_ADDRESS
;;
;; Attribut	Format	Länge	Dez.-Stellen	Beschreibung	JSON Dokument Objekt
;; ITM_NUMBER   NUMC    6       0       Pos Nr
;; NAME	        CHAR	40	0	Name 1	addressee-name
;; STREET	CHAR	40	0	Straße	address-line-4
;; CITY	        CHAR	40	0	Ort	city
;; POSTAL_CODE	CHAR	10	0	Postleitzahl des Orts	postal-code
;; STATE_CODE	CHAR	3	0	Region (Bundesstaat, Bundesland, Provinz, Grafschaft)	state-code
;; COUNTRY	CHAR	3	0	Länderschlüssel	country-code
;; REGION	CHAR	32	0	Region	region

(defun sapr3-write-single-ship-to-address (container-handle ship-to-address pos-nr &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (if ship-to-address
      (with-slots ((addressee-name  tb.msgs:addressee-name)
    		   (address-line-4  tb.msgs:address-line-4)
		   (city            tb.msgs:city)
		   (postal-code     tb.msgs:postal-code)
		   (state-code      tb.msgs:state-code)
		   (country-code    tb.msgs:country-code)
		   (region          tb.msgs:region)) ship-to-address
	(sapr3-write-chars container-handle "ITM_NUMBER"  pos-nr            6 sap-uc-encoding)
	(sapr3-write-chars container-handle "NAME"        addressee-name   40 sap-uc-encoding)
	(sapr3-write-chars container-handle "STREET"      address-line-4   40 sap-uc-encoding)
	(sapr3-write-chars container-handle "CITY"        city             40 sap-uc-encoding)
	(sapr3-write-chars container-handle "POSTAL_CODE" postal-code      10 sap-uc-encoding)
	(sapr3-write-chars container-handle "STATE_CODE"  state-code        3 sap-uc-encoding)
	(sapr3-write-chars container-handle "COUNTRY"     country-code      3 sap-uc-encoding)
	(sapr3-write-chars container-handle "REGION"      region           32 sap-uc-encoding)))
  (values))
