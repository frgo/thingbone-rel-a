;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

(defclass servb-order-customer (servb-data)
  ((line-item-nr                       :accessor line-item-nr                       :initarg :line-item-nr                       :initform nil)
   (business-partner-nr-sold-to        :accessor business-partner-nr-sold-to        :initarg :business-partner-nr-sold-to        :initform nil)
   (business-partner-nr-invoice-to     :accessor business-partner-nr-invoice-to     :initarg :business-partner-nr-invoice-to     :initform nil)
   (business-partner-nr-ship-to        :accessor business-partner-nr-ship-to        :initarg :business-partner-nr-ship-to        :initform nil)
   (business-partner-nr-final-receiver :accessor business-partner-nr-final-receiver :initarg :business-partner-nr-final-receiver :initform nil)
   (business-partner-nr-clearance-from :accessor business-partner-nr-clearance-from :initarg :business-partner-nr-clearance-from :initform nil)
   (business-partner-contact-name      :accessor business-partner-contact-name      :initarg :business-partner-contact-name      :initform nil)
   ))

(defmethod print-object ((self servb-order-customer) stream)
  (print-unreadable-object (self stream :identity t :type t)
    (format stream ":LINE-ITEM-NR ~S :SOLD-TO ~S :INVOICE-TO ~S :SHIP-TO ~S :FINAL-RECEIVER ~S :CLEARANCE-FROM ~S :CONTACT-NAME ~S"
	    (slot-value self 'line-item-nr)
	    (slot-value self 'business-partner-nr-sold-to)
	    (slot-value self 'business-partner-nr-invoice-to)
	    (slot-value self 'business-partner-nr-ship-to)
	    (slot-value self 'business-partner-nr-final-receiver)
	    (slot-value self 'business-partner-nr-clearance-from)
	    (slot-value self 'business-partner-contact-name)
	    )))

(defmethod jonathan:%to-json ((servb-order-customer servb-order-customer))
  (jonathan:write-value
   (jonathan:with-object
     (jonathan:write-key-value "business-partner-nr-sold-to"        (business-partner-nr-sold-to servb-order-customer))
     (jonathan:write-key-value "business-partner-nr-invoice-to"     (business-partner-nr-invoice-to servb-order-customer))
     (jonathan:write-key-value "business-partner-nr-ship-to"        (business-partner-nr-ship-to servb-order-customer))
     (jonathan:write-key-value "business-partner-nr-final-receiver" (business-partner-nr-final-receiver servb-order-customer))
     (jonathan:write-key-value "business-partner-nr-clearance-from" (business-partner-nr-clearance-from servb-order-customer))
     (jonathan:write-key-value "business-partner-contact-name"      (business-partner-contact-name servb-order-customer))
     )))

(defmethod sap-container-handle-from-sap-func-handle ((servb-order-customer servb-order-customer) func-handle)
  (rfc-get-table func-handle "IT_PARTNERS"))


;; Tabelle Struktur ZMAT_T_SALES_ORDER_PARTNERS / Struktur ZMAT_S_SALES_ORDER_PARTNERS

;; Attribut	Format	Länge	Dez.-Stellen	Beschreibung	                                JSON Dokument Objekt
;; ITM_NUMBER	NUMC	6	0	        Verkaufsbelegposition	                        pos-nr
;; PARTNER_NR	CHAR	10	0	        Partner Nummer (Debitorennummer)	        value of business-partner-nr-*
;; PARTNER_ROLE	CHAR	64	0	        JSON Partner Rolle für Sales Order Request	business-partner-nr-*

(defmethod servb-to-sap ((servb-order-customer servb-order-customer) func-handle container-handle  &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let ((%container-handle (sap-container-handle servb-order-customer func-handle container-handle)))

    (rfc-move-to-last-row  %container-handle)
    (rfc-append-new-rows   %container-handle 5)

    (rfc-move-to-next-row %container-handle)

    (let ((itm-number (line-item-nr servb-order-customer)))

      (let ((partner-role "BUSINESS-PARTNER-NR-SOLD-TO")
	    (partner-nr   (business-partner-nr-sold-to servb-order-customer)))

	(rfc-set-chars %container-handle "ITM_NUMBER"   (servb-length  6 (length (tb.msgs:stringify-msg-value itm-number))) sap-uc-encoding)
	(rfc-set-chars %container-handle "PARTNER_ROLE" (servb-length 10 (length partner-role)) sap-uc-encoding)
	(rfc-set-chars %container-handle "PARTNER_NR"   (servb-length 10 (length partner-nr))   sap-uc-encoding))

      (rfc-move-to-next-row %container-handle)

      (let ((partner-role "BUSINESS-PARTNER-NR-INVOICE-TO")
	    (partner-nr   (business-partner-nr-invoice-to servb-order-customer)))

	(rfc-set-chars %container-handle "ITM_NUMBER"   (servb-length  6 (length (tb.msgs:stringify-msg-value itm-number))) sap-uc-encoding)
	(rfc-set-chars %container-handle "PARTNER_ROLE" (servb-length 10 (length partner-role)) sap-uc-encoding)
	(rfc-set-chars %container-handle "PARTNER_NR"   (servb-length 10 (length partner-nr))   sap-uc-encoding))

      (rfc-move-to-next-row %container-handle)

      (let ((partner-role "BUSINESS-PARTNER-NR-SHIP-TO")
	    (partner-nr   (business-partner-nr-ship-to servb-order-customer)))

	(rfc-set-chars %container-handle "ITM_NUMBER"   (servb-length  6 (length (tb.msgs:stringify-msg-value itm-number))) sap-uc-encoding)
	(rfc-set-chars %container-handle "PARTNER_ROLE" (servb-length 10 (length partner-role)) sap-uc-encoding)
	(rfc-set-chars %container-handle "PARTNER_NR"   (servb-length 10 (length partner-nr))   sap-uc-encoding))

      (rfc-move-to-next-row %container-handle)

      (let ((partner-role "BUSINESS-PARTNER-NR-FINAL-RECEIVER")
	    (partner-nr   (business-partner-nr-final-receiver servb-order-customer)))

	(rfc-set-chars %container-handle "ITM_NUMBER"   (servb-length  6 (length (tb.msgs:stringify-msg-value itm-number))) sap-uc-encoding)
	(rfc-set-chars %container-handle "PARTNER_ROLE" (servb-length 10 (length partner-role)) sap-uc-encoding)
	(rfc-set-chars %container-handle "PARTNER_NR"   (servb-length 10 (length partner-nr))   sap-uc-encoding))

      (rfc-move-to-next-row %container-handle)

      (let ((partner-role "BUSINESS-PARTNER-NR-CLEARANCE-FROM")
	    (partner-nr   (business-partner-nr-clearance-from servb-order-customer)))

	(rfc-set-chars %container-handle "ITM_NUMBER"   (servb-length  6 (length (tb.msgs:stringify-msg-value itm-number))) sap-uc-encoding)
	(rfc-set-chars %container-handle "PARTNER_ROLE" (servb-length 10 (length partner-role)) sap-uc-encoding)
	(rfc-set-chars %container-handle "PARTNER_NR"   (servb-length 10 (length partner-nr))   sap-uc-encoding))

      ))

  servb-order-customer)

(defmethod sap-to-servb ((servb-order-customer servb-order-global-customer) func-handle container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let ((%container-handle (sap-container-handle servb-order-customer func-handle container-handle)))
    )
  servb-order-customer)

;;; --- ORDER GLOBAL CUSTOMER

(defclass servb-order-global-customer (servb-order-customer)
  ()
  (:default-initargs
   :line-item-nr 0))

(defmethod jonathan:%to-json ((servb-order-global-customer servb-order-global-customer))
  (jonathan:write-key "order-global-customer")
  (call-next-method))


(defun make-servb-order-global-customer-from-sap (func-handle container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let* ((instance (make-instance 'servb-order-global-customer)))
    (sap-to-servb instance func-handle container-handle sap-uc-encoding)))

;;; --- ORDER LINE ITEM CUSTOMER

(defclass servb-order-line-item-customer (servb-order-customer)
  ())

(defmethod jonathan:%to-json ((servb-order-line-item-customer servb-order-line-item-customer))
  (jonathan:write-key "line-item-customer")
  (call-next-method))

(defun make-servb-order-line-item-customer-from-sap (func-handle container-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let* ((instance (make-instance 'servb-order-line-item-customer)))
    (sap-to-servb instance func-handle container-handle sap-uc-encoding)))
