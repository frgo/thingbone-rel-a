;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

(defun make-inform-sales-order-status-update ()
  (let* ((data-context (make-instance 'tb.msgs:tb-data-context :environment (tb.core:tb-site)))
	 (sales-order-status (make-instance 'tb.msgs:tb-sales-order-status))
	 (content (make-instance 'tb.msgs:msg-extended-content
				 :object-class :sales-order-status
				 :operation :update
				 :data nil))
	 (extended-msg (make-instance 'tb.msgs:tb-inform-sales-order-status-update
				      :conversation-id (format nil "~A" (tb.core:make-uuid))
				      :encoding (tb.msgs:extended-msg-encoding)
				      :protocol (tb.msgs:extended-msg-protocol)
				      :protocol-version (tb.msgs:extended-msg-protocol-version)
				      :performative "inform"
				      :sender (tb.msgs::make-msg-sender-for-this-app-instance)
				      :content content
				      :data-context data-context
				      :sales-order-status sales-order-status)))
    extended-msg))


;; INCOMPLETE !!! - frgo, 2019-11-02
;;
;; (defmethod add-status-message ((sales-order-status tb.msgs:tb-sales-order-status) (message tb.msgs:tb-request-result-info))
;;   (vector-push-extend message (tb.msgs:request-result-info sales-order-create-result)))

;; (defun sapr3-read-et-messages-for-inform-sales-order-status-update (container-handle inform-sales-order-status-update &key (sap-uc-encoding (rfc-sap-uc-encoding)))

;;   (let* ((sales-order-status (tb.msgs:sales-order-status inform-sales-order-status-update))
;; 	 (order-line-items-results (tb.msgs:order-line-items-results sales-order-create-result))
;; 	 (nr-order-line-item-results (if order-line-items-results
;; 					 (length order-line-items-results)
;; 					 0))
;; 	 (messages (sapr3-get-t-sales-order-return container-handle 'tb.msgs:tb-request-result-info :sap-uc-encoding sap-uc-encoding))
;; 	 (nr-messages (length messages)))

;;     (if (> nr-messages 0)

;; 	;; SET ORDER GLOBAL RESULTS
;; 	(loop for index from 0 to (1- nr-messages)
;; 	   do
;; 	     (let ((r-r-info (aref messages index)))
;; 	       (if (= (tb.msgs:pos-nr r-r-info) 0)
;; 		   (add-status-message sales-order-create-result r-r-info))))


;; 	;; SET ORDER LINE ITEM RESULTS
;; 	(if (> nr-order-line-item-results 0)
;; 	    (loop for index from 0 to (1- nr-messages)
;; 	       do
;; 		 (let ((r-r-info (aref messages index)))
;; 		   (if (not (= (tb.msgs:pos-nr r-r-info) 0))
;; 		       (loop for o-l-i-r-index from 0 to (1- nr-order-line-item-results)
;; 			  do
;; 			    (let ((order-line-item-result (aref order-line-items-results o-l-i-r-index))
;; 				  (r-r-info (aref messages index)))
;; 			      (if (= (pos-nr order-line-item-result) (pos-nr r-r-info))
;; 				  (progn
;; 				    (add-request-result-info order-line-item-result r-r-info)))))))))))

;;   (values))


(defun sapr3-read-iv-sales-order-for-inform-sales-order-status-update (container-handle inform-sales-order-status-update &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let* ((sales-order-status (tb.msgs:sales-order-status inform-sales-order-status-update))
	 (order-nr (cl-strings:clean (rfc-get-chars container-handle "IV_SALES_ORDER" 10 sap-uc-encoding))))
    (setf (tb.msgs:order-nr sales-order-status) order-nr))
  (values))

(defun sapr3-read-ct-detail-status-for-inform-sales-order-status-update (container-handle inform-sales-order-status-update &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (let* ((sales-order-status (tb.msgs:sales-order-status inform-sales-order-status-update))
	 (nr-entries (rfc-get-row-count container-handle)))
    (if (or (null (tb.msgs:order-line-item-status-info sales-order-status))
	    (not (arrayp (tb.msgs:order-line-item-status-info sales-order-status))))
	(setf (tb.msgs:order-line-item-status-info sales-order-status) (make-array nr-entries :adjustable t :fill-pointer 0)))
    (loop for index from 0 to (1- nr-entries)
       do
	 (rfc-move-to container-handle index)
	 (let ((detail-status-info (sapr3-get-s-sales-order-status (rfc-get-current-row container-handle) :sap-uc-encoding sap-uc-encoding)))
	   (if (= (tb.msgs:pos-nr detail-status-info) 0)
	       (setf (tb.msgs:order-global-status-info sales-order-status) detail-status-info)
	       (vector-push-extend detail-status-info (tb.msgs:order-line-item-status-info sales-order-status))))))
  (values))


(defun %do-mat-s-o-s-upd-req-fm (func-handle)

  (let ((inform-sales-order-status-update (make-inform-sales-order-status-update))
	(sap-uc-encoding (rfc-sap-uc-encoding))
	;; CHANGING
	(ct-detail-status-handle (let ((handle (rfc-get-table func-handle "CT_DETAIL_STATUS")))
				   (if (null handle)
				       (tb.sapnwrfc:signal-rfc-invalid-parameter "TABLE NAME" "CT_DETAIL_STATUS"))
				   handle)))
    (sapr3-read-iv-sales-order-for-inform-sales-order-status-update func-handle inform-sales-order-status-update :sap-uc-encoding sap-uc-encoding)
    (sapr3-read-ct-detail-status-for-inform-sales-order-status-update ct-detail-status-handle inform-sales-order-status-update :sap-uc-encoding sap-uc-encoding)

    (setf (tb.msgs:data (tb.msgs:content inform-sales-order-status-update))
	  (make-array 2 :initial-contents (list (tb.msgs:data-context inform-sales-order-status-update)
						(tb.msgs:sales-order-status inform-sales-order-status-update))))

    (log:info "Z-MAT-S-O-S-UPD-REQ: inform-sales-order-status-update: ~S." inform-sales-order-status-update)
    (tb.core:send (sap-servb-outbound-channel) inform-sales-order-status-update))

  (values))

;;; das ist die USER FUNCTION !!
(defun z-mat-s-o-s-upd-req-fm (connection-handle func-handle error-info-ptr)

  (declare (ignore connection-handle error-info-ptr))

  (log:info "z-mat-s-o-s-upd-req-fm: ENTRY !")

  (let* ((rc :rfc-ok)
	 (condition nil))

    (handler-case (%do-mat-s-o-s-upd-req-fm func-handle)
      (error (caught-condition)
	(log:error "Z-MAT-S-O-S-UPD-REQ-FM *** Caught error ~S: ~A !"
		   caught-condition
		   (format nil (slot-value condition 'format-control) (slot-value condition 'format-arguments)))
	(setq condition caught-condition)
	(setq rc :rfc-external-failure))
      (:no-error ()
	(values)))

    (ecase rc
      (:rfc-ok
       (set-sap-servb-es-servb-return-from-params-in-func func-handle
							  "Z-MAT-S-O-S-UPD-REQ-FM"
							  :rfc-ok
							  log4cl:+log-level-info+
							  "00000000"
							  "TBGS000S"
							  "SUCCESS."))
      (:rfc-abap-runtime-failure
       (set-sap-servb-es-servb-return-from-params-in-func func-handle
							  "Z-MAT-S-O-S-UPD-REQ-FM"
							  :rfc-abap-runtime-failure
							  log4cl:+log-level-error+
							  "00000001"
							  "TBGS001E"
							  "ERROR: RUNTIME FAILURE!"))
      (:rfc-abap-exception
       (set-sap-servb-es-servb-return-from-params-in-func func-handle
							  "Z-MAT-S-O-S-UPD-REQ-FM"
							  :rfc-abap-exception
							  log4cl:+log-level-error+
							  "00000002"
							  "TBGS001E"
							  "ERROR: ABAP EXCEPTION !"))
      (:rfc-external-failure
       (set-sap-servb-es-servb-return-from-params-in-func func-handle
							  "Z-MAT-S-O-S-UPD-REQ-FM"
							  :rfc-external-failure
							  log4cl:+log-level-error+
							  "00000003"
							  "TBGS001E"
							  "ERROR: EXTERNAL FAILURE!"))
      (otherwise
       (log:error "Z-MAT-S-O-S-UPD-REQ-FM: RFC Error ~S." rc)
       (set-sap-servb-es-servb-return-from-params-in-func func-handle
							  "Z-MAT-S-O-S-UPD-REQ-FM"
							  :rfc-external-failure
							  log4cl:+log-level-error+
							  "00000001"
							  "TBGS002E"
							  "ERROR: UNKNOWN ERROR!"))
      )

    (log:info "Z-MAT-S-O-S-UPD-REQ-FM: Exit (rc = ~S)." rc)
    (cffi:foreign-enum-value 'rfc-rc :rfc-ok)))

(define-servb-server-function Z_MAT_S_O_S_UPD_REQ :servb-outbound 'z-mat-s-o-s-upd-req-fm)
