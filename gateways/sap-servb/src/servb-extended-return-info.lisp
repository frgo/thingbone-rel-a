;;; -*- Mode:Lisp; Syntax:ANSI-Common-Lisp; Coding:utf-8 -*-
;;; =====================================================================
;;;
;;;  T H I N G B O N E  -   The BackBONE for all THINGs
;;;
;;;  A data-centric messaging middleware for real-time application
;;;  integration and IoT integration based on OMG DDS and Common Lisp.
;;;
;;;  Copyright © 2019 by Gönninger B&T UG (haftungsbeschränkt), Germany
;;;  For licensing information see file license.md.
;;;
;;;  All Rights Reserved. German law applies exclusively in all cases.
;;;
;;;  Author: Frank Gönninger <frgo@me.com>
;;;  Maintainer: Gönninger B&T UG (haftungsbeschränkt)
;;;              <support@goenninger.net>
;;;
;;; =====================================================================
;;;
;;;  SAP SERVICE BUS "SAP-SERVB"
;;;
;;; =====================================================================

#+thingbone-production
(declaim (optimize (speed 3) (compilation-speed 0) (safety 1) (debug 1)))

(cl:in-package "THINGBONE.GATEWAY.SAP-SERVB")

;;; ---------------------------------------------------------------------------

;;; SAP-SERVB - SPECIFIC DATA STRUCTURES

;; Levels:
;;
;; 0    Emergency
;; 1    Alert
;; 2    Critical
;; 3    Error
;; 4    Warning
;; 5    Notice
;; 6    Informational
;; 7    Debug

(defclass servb-return-info ()
  ((fm-name         :accessor fm-name         :initarg :fm-name         :initform nil)
   (rfc-return-code :accessor rfc-return-code :initarg :rfc-return-code :initform nil)
   (level           :accessor level           :initarg :level           :initform tb.core:+TB-MSG-LEVEL-UNDEFINED+)
   (msg-nr          :accessor msg-nr          :initarg :msg-nr          :initform nil)
   (reason-code     :accessor reason-code     :initarg :reason-code     :initform nil)
   (reason-text     :accessor reason-text     :initarg :reason-text     :initform nil)))

(defmethod print-object ((servb-return-info servb-return-info) stream)
  (print-unreadable-object (servb-return-info stream :identity t :type t)
    (with-slots (fm-name rfc-return-code level msg-nr reason-code reason-text) servb-return-info
      (format stream ":FM-NAME ~S :RFC-RETURN-CODE ~S :LEVEL ~S :MSG-NR ~S :REASON-CODE ~S :REASON-TEXT ~S"
	      fm-name rfc-return-code level msg-nr reason-code reason-text))))

(defun make-servb-return-info (fm-name rfc-return-code level msg-nr reason-code reason-text)
  (make-instance 'servb-return-info
		 :fm-name fm-name :rfc-return-code rfc-return-code :level level
		 :msg-nr msg-nr :reason-code reason-code :reason-text reason-text))

(defun sap-servb-es-servb-return-to-servb-return-info (es-servb-return-ptr)
  (let ((level       (rfc-get-int    es-servb-return-ptr  "RETURN_CODE"))
	(msg-nr      (rfc-get-string es-servb-return-ptr  "RETURN_MSGNR" 8))
	(reason-code (rfc-get-string es-servb-return-ptr  "REASON_CODE" 8))
	(reason-text (rfc-get-string es-servb-return-ptr  "REASON_TEXT" 1024)))
    (make-servb-return-info nil nil level msg-nr reason-code reason-text)))

(defun set-sap-es-servb-return-from-servb-return-info (servb-return-info es-servb-return-struct-handle &key (sap-uc-encoding (rfc-sap-uc-encoding)))
  (with-slots (level msg-nr reason-code reason-text) servb-return-info
    (rfc-set-int es-servb-return-struct-handle "RETURN_CODE" level)
    (rfc-set-chars es-servb-return-struct-handle "RETURN_MSGNR" msg-nr (length msg-nr) sap-uc-encoding)
    (rfc-set-chars es-servb-return-struct-handle "REASON_CODE"  reason-code (length reason-code) sap-uc-encoding)
    (rfc-set-chars es-servb-return-struct-handle "REASON_TEXT"  reason-text (length reason-text) sap-uc-encoding))
  (values))

(defmethod errorp ((servb-return-info servb-return-info))
  (< (level servb-return-info) tb.core:+TB-MSG-LEVEL-WARNING+))

(defun servb-return-info-level-to-tb-msg-error-info-level (level)
  (if level
      (tb.core:msg-level-to-keyword level)
      :__UNSPECFIED__))

(defun get-es-servb-return-structure-handle-from-function-handle (func-handle)
  (rfc-get-structure func-handle "ES_SERVB_RETURN"))

(defun get-servb-return-info-from-function-handle (func-handle)
  (let ((es-servb-return-ptr (get-es-servb-return-structure-handle-from-function-handle func-handle)))
    (sap-servb-es-servb-return-to-servb-return-info es-servb-return-ptr)))

(defun servb-return-info-to-tb-msg-error-info (servb-return-info)
  (check-type servb-return-info servb-return-info)
  (let* ((string (tb.msgs:stringify-msg-value (reason-text servb-return-info)))
	 (msg-text (tb.msgs:make-msg-text "eng" string nil))
	 (msg-nr (msg-nr servb-return-info))
	 (reason-code (reason-code servb-return-info))
	 (category :THINGBONE-SAP-SERVB-ERROR-INFO)
	 (level (servb-return-info-level-to-tb-msg-error-info-level (level servb-return-info)))
	 (tb-msg-error-info (tb.msgs:make-msg-error-info (if (errorp servb-return-info) -1 0) level category reason-code msg-nr nil)))
    (tb.msgs:add-msg-text tb-msg-error-info msg-text)
    tb-msg-error-info))

(defun set-sap-servb-es-servb-return-from-params-in-func (func-handle fm-name rfc-return-code level msg-nr reason-code reason-text)
  (let ((servb-return-info (make-servb-return-info fm-name rfc-return-code level msg-nr reason-code reason-text)))
    (set-sap-es-servb-return-from-servb-return-info servb-return-info
						    (get-es-servb-return-structure-handle-from-function-handle func-handle)))
  (values))

(defmethod format-msg-for-slack ((servb-return-info servb-return-info))
  (with-slots (fm-name rfc-return-code level msg-nr reason-code reason-text) servb-return-info
    (format nil "*SAP SERVB: RETURN INFO for ~A*~%~%~30A ~32A ~29A~%~23A ~32A ~30A~%~%*Reason Text*:~%~A"
	    fm-name
	    "*Msg Nr*:"
	    "*Reason Code*:"
	    "*RFC Return Code*:"
	    msg-nr
	    reason-code
	    (princ-to-string rfc-return-code)
	    reason-text)))

(defmethod tb.msgs:dispatch ((servb-return-info servb-return-info) &key &allow-other-keys)
  (let ((level (level servb-return-info))
	(msg (format-msg-for-slack servb-return-info)))
    (cond
      ((= level tb.core:+TB-MSG-LEVEL-UNDEFINED+)
       nil) ;; NOP
      ((= level tb.core:+TB-MSG-LEVEL-DEBUG+)
       (log:debug "~A" msg))
      ((= level tb.core:+TB-MSG-LEVEL-INFORMATIONAL+)
       (log:info "~A" msg))
      ((or (= level tb.core:+TB-MSG-LEVEL-NOTICE+)
	   (= level tb.core:+TB-MSG-LEVEL-WARNING+))
       (log:warn "~A" msg))
      ((or (= level tb.core:+TB-MSG-LEVEL-ERROR+)
	   (= level tb.core:+TB-MSG-LEVEL-CRITICAL+))
       (log:error "~A" msg))
      ((or (= level tb.core:+TB-MSG-LEVEL-ALERT+)
	   (= level tb.core:+TB-MSG-LEVEL-EMERGENCY+))
       (log:fatal "~A" msg))
      (t (log:fatal "Cannot dispatch SERVB-RETURN-INFO: Cannot determine log function from level ~S. Message was:~%~%~A" level msg))))
  (values))
